import qbs

Project {
    references: [
        "libs/CExtendedArray/extendedarray.qbs",
        "libs/NetBase/netbase.qbs",

        "src/logicbase/logicbase.qbs",
        "src/gamebase/gamebase.qbs",
        "src/network/network.qbs",
        "src/ui/ui.qbs",

        "src/editor/editor.qbs",
        "src/client/client.qbs",
        "src/server/server.qbs"
    ]
}
