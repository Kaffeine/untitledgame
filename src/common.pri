
include(../options.pri)

DESTDIR = $$PWD/../bin
LIBS += -L$$PWD/../bin
INCLUDEPATH += $$PWD/

INCLUDEPATH += $$PWD/../libs/CExtendedArray
INCLUDEPATH += $$PWD/../libs/NetBase

QMAKE_CXXFLAGS += $$(CXXFLAGS)
QMAKE_LFLAGS += $$(LDFLAGS)

#QMAKE_LFLAGS= -enable-auto-import
