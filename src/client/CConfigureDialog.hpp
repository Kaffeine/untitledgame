#ifndef CCONFIGUREDIALOG_HPP
#define CCONFIGUREDIALOG_HPP

#include <QDialog>

namespace Ui {
class CConfigureDialog;
}

class CConfigureDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CConfigureDialog(QWidget *parent = 0);
    ~CConfigureDialog();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void updateColorPreview();
    void setAvatarColor(QString newColor);

private:
    QString avatarColor() const;

    void showEvent(QShowEvent *);
    void keyPressEvent(QKeyEvent *pKeyEvent);

    Ui::CConfigureDialog *ui;
};

#endif // CCONFIGUREDIALOG_HPP
