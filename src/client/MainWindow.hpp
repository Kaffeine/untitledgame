#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QTime;
class CAvatar;
class CWorld;
class IGameUI;
class CConsole;

class CClient;

class CConsoleLineHelper;

class CConfigureDialog;

class CGameUIProcessor;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();
    void on_actionQuit_triggered();
    void on_actionSpawn_triggered();

    void on_actionSaveReplay_triggered();
    void on_actionLoadReplay_triggered();
    void on_actionPlayReplay_triggered();
    void on_actionRecordReplay_triggered();
    void on_actionStopReplay_triggered();

    void on_actionToggle_fullscreen_triggered();
    void on_actionShowConsole_triggered(bool checked);

    void on_actionConfigure_triggered();

    void whenCameraModeActionTriggered(QAction *triggerAction);

//    void whenTitleChanged(QString newTitle);
//    void whenStarted();
//    void whenFinished();
    void updateView();

    void whenAvatarWantLeaveWorld_SinglePlayer(CAvatar *pAvatar, QString destWorld);

    void on_actionBoundedCamera_triggered(bool checked);

    void on_actionConnectToLastServer_triggered();

    void on_actionDisconnect_triggered();

private:
    Ui::MainWindow *ui;

    CWorld *m_world;
    CConsole *m_console;
    CConsoleLineHelper *m_consoleIOHelper;

    CGameUIProcessor *m_processor;
    IGameUI *m_gameUI;

//    QTime *m_gameTimeTimer;
//    bool m_finished;

    QTimer *m_viewUpdateTimer;
    CConfigureDialog *m_configureDialog;
    QList<QObject *> m_consoleAccessibleObjects;

    CClient *m_client;
};

#endif // MAINWINDOW_HPP
