#include "CConfigureDialog.hpp"
#include "ui_CConfigureDialog.h"

#include "logicbase/CAvatarAppearance.hpp"
#include "gamebase/CClientSettings.hpp"

#include <QKeyEvent>

CConfigureDialog::CConfigureDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CConfigureDialog)
{
    ui->setupUi(this);

    connect(ui->customColorValue , SIGNAL(textChanged(QString)), SLOT(updateColorPreview()));
    connect(ui->colorsButtonGroup, SIGNAL(buttonClicked(int))  , SLOT(updateColorPreview()));
}

CConfigureDialog::~CConfigureDialog()
{
    delete ui;
}

QString CConfigureDialog::avatarColor() const
{
    if (ui->colorRed->isChecked())
        return QLatin1String("#ff0000");
    else if (ui->colorGreen->isChecked())
        return QLatin1String("#00ff00");
    else if (ui->colorBlue->isChecked())
        return QLatin1String("#0000ff");
    else if (ui->colorCustom->isChecked()) {
        if (ui->customColorValue->text().length() == 7)
            return ui->customColorValue->text();
    }
    return QLatin1String("#808080");
}

void CConfigureDialog::on_buttonBox_accepted()
{
    CClientSettings::instance()->setPlayerName(ui->name->text());

    CClientSettings::instance()->avatarAppearance()->setColor(avatarColor());
    CClientSettings::instance()->avatarAppearance()->setEyesCount(ui->eyesCount->value());
    CClientSettings::instance()->avatarAppearance()->setFootsCount(ui->footsCount->value());
    close();
}

void CConfigureDialog::on_buttonBox_rejected()
{
    close();
}

void CConfigureDialog::updateColorPreview()
{
    ui->colorPreview->setStyleSheet(QString("background: %1").arg(avatarColor()));
}

void CConfigureDialog::setAvatarColor(QString newColor)
{
    if (newColor == QLatin1String("#ff0000"))
        ui->colorRed->setChecked(true);
    else if (newColor == QLatin1String("#00ff00"))
        ui->colorGreen->setChecked(true);
    else if (newColor == QLatin1String("#0000ff"))
        ui->colorBlue->setChecked(true);
    else {
        ui->colorCustom->setChecked(true);
        ui->customColorValue->setText(newColor);
    }

    updateColorPreview();
}

void CConfigureDialog::showEvent(QShowEvent *)
{
    ui->name->setText(CClientSettings::instance()->playerName());

    setAvatarColor(CClientSettings::instance()->avatarAppearance()->color());
    ui->eyesCount->setValue(CClientSettings::instance()->avatarAppearance()->eyesCount());
    ui->footsCount->setValue(CClientSettings::instance()->avatarAppearance()->footsCount());
}

void CConfigureDialog::keyPressEvent(QKeyEvent *pKeyEvent)
{
    if (pKeyEvent->key() == Qt::Key_Escape)
        on_buttonBox_rejected();
    if ((pKeyEvent->key() == Qt::Key_Return) || (pKeyEvent->key() == Qt::Key_Enter))
        on_buttonBox_accepted();
}
