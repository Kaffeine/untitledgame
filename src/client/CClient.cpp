#include "CClient.hpp"

#include "logicbase/CAvatar.hpp"
#include "logicbase/CAvatarAppearance.hpp"
#include "logicbase/CPlayer.hpp"
#include "logicbase/CWorld.hpp"
#include "logicbase/CWorldUtils.hpp"
#include "network/CGameNetwork.hpp"

#include "gamebase/CClientSettings.hpp"

#include "CNetBase.hpp"

#include <QDebug>

CClient::CClient(QObject *parent) :
    QObject(parent),
    m_net(0),
    m_world(0),
    m_active(false),
    m_player(0)
{
}

void CClient::setOutput(QObject *pObj, QString slotName)
{
    connect(this, SIGNAL(out(QString)), pObj, slotName.toLatin1().constData());
}

void CClient::setWorld(CWorld *pWorld)
{
    m_world = pWorld;
}

void CClient::setLocalPlayer(CPlayer *pPlayer)
{
    m_player = pPlayer;
    connect(m_player, SIGNAL(actionsChanged(quint16)), SLOT(whenPlayerActionsChanged()));
    connect(m_player, SIGNAL(lookDestinationChanged()), SLOT(whenPlayerLookDestinationChanged()));
}

void CClient::cmdChats(const QString &line)
{
    int index = line.indexOf(" ");

    if (index <= 0)
        return;

    QString key = line.left(index);

    if (key == QLatin1String("say"))
        sendMessage(line.mid(index + 1), CGameNetwork::MessageAll);
    else if (key == QLatin1String("tsay"))
        sendMessage(line.mid(index + 1), CGameNetwork::MessageTeam);
}

void CClient::cmdConnect(const QString &line)
{
    int index = line.indexOf(" ");

    QString key;
    QString url;

    if (index <= 0)
        key = line;
    else {
        key = line.left(index);
        url = line.mid(index + 1);
    }

    if (key != QLatin1String("connect"))
        return;

    if (!m_world) {
        emit out(tr("Can't begin to connect due to world's missing."));
        return;
    }

    m_localAvatarId = -1;

    m_world->setPaused(true);

    if (url.isEmpty()) {
        url = CClientSettings::instance()->lastServer();
    }
    else {
        if (url.indexOf(":") < 0)
            url.append(":25577");

        CClientSettings::instance()->setLastServer(url);
    }

    index = url.indexOf(":");

    if (index > 0) {
        emit out(tr("Connecting to %1, port %2.").arg(url.left(index)).arg(url.mid(index + 1)));
        if (!m_net) {
            m_net = new CNetBase(CGameNetwork::protocolVersion, this);
            connect(m_net, SIGNAL(connected()), SLOT(whenConnected()));
            connect(m_net, SIGNAL(disconnected()), SLOT(whenDisconnected()));
            //        connect(m_net, SIGNAL(sendLog(QString)), this, SLOT(debugSink(QString)), Qt::DirectConnection);

            connect(m_world , SIGNAL(avatarSpawned(CAvatar*)), SLOT(whenAvatarSpawned(CAvatar*)));
        }

        m_net->doConnect(url.left(index), url.mid(index + 1).toUShort());
    }
}

void CClient::cmdDisconnect(const QString &line)
{
    Q_UNUSED(line)

    if (!m_net->isConnected())
        return;

    m_net->doDisconnect("");
}

void CClient::sendMessage(const QString &message, int type)
{
    qDebug() << QString("sendMessage: %1(%2, %3)").arg(Q_FUNC_INFO).arg(message).arg(type);
    if (m_net) {
        m_net->writeQUInt8(CGameNetwork::NetCommandChat);
        m_net->writeQUInt8(CGameNetwork::NetCommandReceive);
        m_net->writeQUInt8(type);
        m_net->writeQString(message);
        m_net->requestSendPackage();
    }
}

void CClient::doWantSpawn()
{
    if (!m_net)
        return;

    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarSpawn);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->requestSendPackage();
}

void CClient::processPackageContent()
{
    quint8 com = m_net->readNetCommand();
    qDebug() << "processPackageContent:" << CGameNetwork::commandToText(com) << "at time" << m_world->time();
    CAvatar *pAvatar;

    if (com == CGameNetwork::NetCommandChat) {
        m_net->readNetCommand(); // NetCommandReceive
        int type = m_net->readQUInt8();
        QString message = m_net->readQString();
        emit messageSignal(message, type);

        if (type == CGameNetwork::MessageAll)
            emit out("A:" + message);
        else if (type == CGameNetwork::MessageTeam)
            emit out("T:" + message);
        else
            emit out("message:" + message + "(type)" + type);
    }
    else if (com == CGameNetwork::NetCommandWorld) {
        if (m_net->readNetCommand() != CGameNetwork::NetCommandReceive)
            qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));

        m_worldsSignature = m_net->readQString();

        if (!CWorldUtils::isWorldsSignatureAvailable(m_worldsSignature)) {
            emit out("No such world:" + m_worldsSignature);
            m_net->writeNetCommand(CGameNetwork::NetCommandWorld);
            m_net->writeNetCommand(CGameNetwork::NetCommandAccept); // World received/accepted, but can't be loaded.

            m_net->writeNetCommand(CGameNetwork::NetCommandFile);
            m_net->writeNetCommand(CGameNetwork::NetCommandRequest);
            m_net->writeNetCommand(CGameNetwork::NetCommandWorld);
            m_net->writeQString(m_worldsSignature);
        }
        else {
            CWorldUtils::loadWorldBySignature(m_world, m_worldsSignature);
            emit out("Set world:" + m_worldsSignature);

            m_net->writeNetCommand(CGameNetwork::NetCommandWorld);
            m_net->writeNetCommand(CGameNetwork::NetCommandAck);  // World received/accepted, and can be loaded.
        }
    }
    else if (com == CGameNetwork::NetCommandWorldTime) {
        if (m_net->readNetCommand() != CGameNetwork::NetCommandReceive)
            qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));
        quint32 worldTime = m_net->readQUInt32();
        m_world->start(worldTime);

        m_net->writeNetCommand(CGameNetwork::NetCommandWorldTime);
        m_net->writeNetCommand(CGameNetwork::NetCommandAck);
        m_net->writeQUInt32(m_world->time());
    }
    else if (com == CGameNetwork::NetCommandAvatarSpawn) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandReceive) {
            m_net->readQUInt8(); // Avatar id
            int appearanceId = m_net->readQUInt8();
            pAvatar = m_world->spawnAvatar();
            if (!pAvatar) {
                qDebug() << "    " << "Can't spawn avatar.";
                /* All bad and something wrong. */
                m_net->writeNetCommand(CGameNetwork::NetCommandAvatarSpawn);
                m_net->writeNetCommand(CGameNetwork::NetCommandReject);
            }
            else {
                pAvatar->appearance()->copyAppearance(m_appearances.at(appearanceId));
                /* Else, if avatar spawned, world emit signal. We (CClient) handle it in another place to be able delayed. */
                qDebug() << "    " << "Avatar spawned.";
            }
        }
        else
            qDebug() << "    " << "!!!NetCommandAvatarSpawn, but not receive.";
    }
    else if (com == CGameNetwork::NetCommandAvatarRemove) {
        if (m_net->readNetCommand() != CGameNetwork::NetCommandReceive)
            qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));

        quint8 avId = m_net->readQUInt8(); // Avatar id
        m_world->deleteAvatar(m_world->avatarAt(avId));

        m_net->writeNetCommand(CGameNetwork::NetCommandAvatarRemove);
        m_net->writeNetCommand(CGameNetwork::NetCommandAck);
    }
    else if (com == CGameNetwork::NetCommandPlayerAvatar) {
        if (m_net->readNetCommand() != CGameNetwork::NetCommandReceive)
            qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));

        m_localAvatarId = m_net->readQUInt8();
        qDebug() << "    " << "Avatar assigned." << m_localAvatarId;
        m_player->setAvatar(m_world->avatarAt(m_localAvatarId));
        m_net->setDumpFilename(QString("client-%1.dump").arg(m_localAvatarId));
        m_net->enableDebug(true);
    }
    else if (com == CGameNetwork::NetCommandAvatarsWills) {
        if (m_net->readNetCommand() != CGameNetwork::NetCommandReceive)
            qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));

        quint8 avatarId = m_net->readQUInt8();
        CExtendedArray avatarWillsData = m_net->readBlock();
        CGameNetwork::unpackWillsToAvatar(m_world->avatarAt(avatarId), &avatarWillsData);
    }
    else if (com == CGameNetwork::NetCommandPing) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandRequest) {
            qDebug() << "    " << "Ping rq";

            m_net->writeNetCommand(CGameNetwork::NetCommandPing);
            m_net->writeNetCommand(CGameNetwork::NetCommandAck);
        }
    }
    else if (com == CGameNetwork::NetCommandFullSnapshot) {
        if (m_net->readNetCommand() != CGameNetwork::NetCommandReceive)
            qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));

        CExtendedArray snapshotData = m_net->readBlock();
        qDebug() << "Snapshot timestamp:" << snapshotData.readQUInt32() << "accepted at" << m_world->time();
        int avatarsInPackage = snapshotData.readQUInt8();
        qDebug() << "    " << "FullSnapshot: Avatars in package" << avatarsInPackage;

        for (int i = 0; i < avatarsInPackage; ++i) {
            quint8 avatarId = snapshotData.readQUInt8();

            qDebug() << "    " << "FullSnapshot: Accept avatar" << avatarId;
            if (avatarId == m_localAvatarId)
                CGameNetwork::setStateToAvatar(m_world->avatarAt(avatarId), &snapshotData, false);
            else
                CGameNetwork::setStateToAvatar(m_world->avatarAt(avatarId), &snapshotData);
        }

        m_net->writeNetCommand(CGameNetwork::NetCommandFullSnapshot);
        m_net->writeNetCommand(CGameNetwork::NetCommandAck);
    }
    else if (com == CGameNetwork::NetCommandAvatarAppearance) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandReceive) {
            int appearanceId = m_net->readQUInt8();

            while (m_appearances.count() <= appearanceId)
                m_appearances.append(new CAvatarAppearance(this));

            CExtendedArray appearanceData = m_net->readBlock();
            CGameNetwork::unpackAvatarAppearance(m_appearances.at(appearanceId), &appearanceData);
        }
    }
    else if (com == CGameNetwork::NetCommandAvatarLookDest) {
        com = m_net->readNetCommand();
        if (com == CGameNetwork::NetCommandReceive) {
            quint8 avId = m_net->readQUInt8(); // Avatar id
            PointF2D newLookDest;
            newLookDest.setX(m_net->readQReal());
            newLookDest.setY(m_net->readQReal());

            m_world->avatarAt(avId)->appearance()->setEyeDestination(newLookDest);
        }
    }
    else if (com ==CGameNetwork::NetCommandFile) {
        com = m_net->readNetCommand();
        if (com == CGameNetwork::NetCommandReceive) {
            quint8 fileType = m_net->readNetCommand();
            QString fileName;
            CExtendedArray fileContent;
            m_net->readNamedBlock(fileName, fileContent);

            if (fileType == CGameNetwork::NetCommandWorld) {
                CWorldUtils::saveBufferAsFile(fileContent, CWorldUtils::fixedFilename(fileName));

                if (fileName == m_worldsSignature) {
                    CWorldUtils::loadWorldBySignature(m_world, m_worldsSignature);
                    emit out("Set world:" + m_worldsSignature);

                    m_net->writeNetCommand(CGameNetwork::NetCommandWorld);
                    m_net->writeNetCommand(CGameNetwork::NetCommandAck);  // World received/accepted, and can be loaded.
                }
            }
            else if (fileType == CGameNetwork::NetCommandAvatarAppearance) {
                CWorldUtils::saveBufferAsFile(fileContent, fileName);
            }

            m_net->writeNetCommand(CGameNetwork::NetCommandFile);
            m_net->writeNetCommand(CGameNetwork::NetCommandAck);
        }
    }

    m_net->requestSendPackage();

    /* Singlethread code! */
    if (!m_net->atEnd())
        return processPackageContent();
}

void CClient::whenConnected()
{
    m_world->reset();
    connect(m_net, SIGNAL(processPackage()), SLOT(processPackageContent()), Qt::DirectConnection);
    m_active = true;
}

void CClient::whenDisconnected()
{
    m_active = false;
    disconnect(m_net, SIGNAL(processPackage()), this, SLOT(processPackageContent()));
    m_world->reset();
    m_localAvatarId = -1;
}

void CClient::whenAvatarSpawned(CAvatar *who)
{
    qDebug() << "    " << "Avatar spawned ack. Id:" << m_world->avatarId(who);
    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarSpawn);
    m_net->writeNetCommand(CGameNetwork::NetCommandAck);
    m_net->writeQUInt8(m_world->avatarId(who));
    m_net->requestSendPackage();
}

void CClient::whenPlayerActionsChanged()
{
    if (!isConnected()) {
        return;
    }

    qDebug() << "    " << "Player control changed:" << m_player->actions() << "at " << m_world->time();
    m_net->writeNetCommand(CGameNetwork::NetCommandPlayerActions);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt32(m_world->time());

    CExtendedArray playerActionsData;
    CGameNetwork::packActionsOfPlayer(m_player, &playerActionsData);
    m_net->writeBlock(playerActionsData);

    CExtendedArray avatarState;
    CGameNetwork::getStateOfAvatar(m_player->avatar(), &avatarState);
    m_net->writeBlock(avatarState);

    m_net->requestSendPackage();
}

void CClient::whenPlayerLookDestinationChanged()
{
    if (!isConnected())
        return;

    m_net->writeNetCommand(CGameNetwork::NetCommandPlayerLookDest);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQReal(m_player->lookDestination().x());
    m_net->writeQReal(m_player->lookDestination().y());
    m_net->requestSendPackage();
}

void CClient::debugSink(QString text)
{
    if (m_world)
        qDebug() << QString("%1|NetDebug: %2").arg(m_world->time()).arg(text);
    else
        qDebug() << QString("NetDebug: %1").arg(text);
}
