
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app

include(../common.pri)

LIBS += -lextendedarray -lNetBase
LIBS += -llogicbase -lgamebase -lnetwork -lui

SOURCES += main.cpp \
    MainWindow.cpp \
    CClient.cpp \
    CConfigureDialog.cpp

HEADERS  +=  \
    MainWindow.hpp \
    CClient.hpp \
    CConfigureDialog.hpp

FORMS    += MainWindow.ui \
    CConfigureDialog.ui
