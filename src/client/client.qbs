import qbs 1.0

Product {
    Depends { name: "extendedarray" }
    Depends { name: "netbase" }
    Depends { name: "logicbase" }
    Depends { name: "gamebase" }
    Depends { name: "ui" }
    Depends { name: "network" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui", "network"] }
    name: "client"
    type: "application"

    cpp.includePaths: [
        "..",
        "../../libs/CExtendedArray",
        "../../libs/NetBase"
    ]

    Group {
        name: "files"
        files: [
            "CClient.cpp",
            "CClient.hpp",
            "CConfigurePlayer.cpp",
            "CConfigurePlayer.hpp",
            "CConfigurePlayer.ui",
            "CGameUIProcessor.cpp",
            "CGameUIProcessor.hpp",
            "MainWindow.cpp",
            "MainWindow.hpp",
            "MainWindow.ui",
            "main.cpp"
        ]
    }
}
