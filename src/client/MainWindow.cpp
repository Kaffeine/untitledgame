#include "MainWindow.hpp"
#include "ui_MainWindow.h"
#include "CConfigureDialog.hpp"

#include "logicbase/CWorld.hpp"
#include "logicbase/CWorldUtils.hpp"
#include "logicbase/CConsole.hpp"
#include "gamebase/CLocalPlayer.hpp"
#include "gamebase/CClientSettings.hpp"
#include "gamebase/CGameUIProcessor.hpp"
#include "ui/CConsoleLineHelper.hpp"
#include "ui/IGameUI.hpp"
#include "ui/CCamera.hpp"

#include "CClient.hpp"

#include <QTimer>
#include <QTime>
#include <QFileDialog>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_configureDialog(0)
{
    ui->setupUi(this);

    CClientSettings::instance(this);

    QActionGroup *pCameraModesGroup = new QActionGroup(this);
    ui->actionFreeCamera       ->setActionGroup(pCameraModesGroup);
    ui->actionAvatarInCenter   ->setActionGroup(pCameraModesGroup);
    ui->actionAvatarAndVector  ->setActionGroup(pCameraModesGroup);
    ui->actionAvatarWithMargins->setActionGroup(pCameraModesGroup);
    connect(pCameraModesGroup, SIGNAL(triggered(QAction*)), SLOT(whenCameraModeActionTriggered(QAction*)));

    m_world = new CWorld(this);
//    connect(m_world, SIGNAL(aboutReload()), this, SLOT(on_actionSpawn_triggered()));
    connect(m_world, SIGNAL(avatarLeaveWorld(CAvatar*,QString)), SLOT(whenAvatarWantLeaveWorld_SinglePlayer(CAvatar*,QString)));

//    m_finished = true;

    m_processor = new CGameUIProcessor(this);

    m_gameUI = IGameUI::createUI();
    m_gameUI->setProcessor(m_processor);
    ui->centralWidget->layout()->addWidget(m_gameUI->widget());
    m_gameUI->widget()->setFocusPolicy(Qt::StrongFocus);
    m_gameUI->widget()->setFocus();

//    connect(m_processor, SIGNAL(titleChanged(QString)), SLOT(whenTitleChanged(QString)));
    m_processor->setWorld(m_world);

//    m_gameTimeTimer = new QTime(this);

    /* View */
    m_viewUpdateTimer = new QTimer(this);
    m_viewUpdateTimer->setInterval(20);
    m_viewUpdateTimer->setSingleShot(false);
    connect(m_viewUpdateTimer, SIGNAL(timeout()), SLOT(updateView()));
    m_viewUpdateTimer->start();

    m_console = new CConsole(this);
    m_console->addConsoleObject(m_world         , "world");
    m_console->addConsoleObject(m_processor->camera(), "camera");
    m_console->addConsoleObject(CClientSettings::instance(), "settings");

    on_actionShowConsole_triggered(false);

    m_consoleIOHelper = new CConsoleLineHelper(this);
    m_consoleIOHelper->setObjects(ui->consoleInput, ui->consoleOutput, m_console);

    m_client = new CClient(this);
    m_client->setWorld(m_world);
    connect(m_client, SIGNAL(out(QString)), m_consoleIOHelper, SLOT(whenConsoleOutput(QString)));

    m_processor->setLocalPlayer(new CLocalPlayer(this));
    m_console->addConsoleCommand(m_client, "cmdChats", "say");
    m_console->addConsoleCommand(m_client, "cmdChats", "tsay");
    m_console->addConsoleCommand(m_client, "cmdConnect", "connect");

    m_client->setLocalPlayer(m_processor->localPlayer());

    CWorldUtils::loadWorldByFilename(m_world, "01");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open map..."), "maps", tr("Map files (*%1)").arg(CWorldUtils::fileExtension));

    if (fileName.isEmpty())
        return;

    CWorldUtils::loadWorldByFilename(m_world, fileName);
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

//void MainWindow::whenTitleChanged(QString newTitle)
//{
//    setWindowTitle(newTitle);
//}

void MainWindow::on_actionSpawn_triggered()
{
    if (m_client->isConnected())
        m_client->doWantSpawn();
    else {
        CAvatar *pAvatar = m_world->spawnAvatar();

        if (!pAvatar)
            return;

        m_processor->localPlayer()->setAvatar(pAvatar);
        m_world->start();
        m_processor->localPlayer()->setCheckpointsCount(m_world->checkpointsCount());

//        connect(m_processor->localPlayer(), SIGNAL(started()), this, SLOT(whenStarted()));
//        connect(m_processor->localPlayer(), SIGNAL(finished()), this, SLOT(whenFinished()));
    }
}

//void MainWindow::whenStarted()
//{
//    m_gameTimeTimer->start();
//    m_finished = false;
//}

//void MainWindow::whenFinished()
//{
//    if (!m_finished) {
//        m_gameTimeTimer->elapsed();
//        m_finished = true;
//    }
//}

void MainWindow::on_actionToggle_fullscreen_triggered()
{
//    m_processor->setWindowFlags(Qt::Window|Qt::FramelessWindowHint);
//    m_processor->showFullScreen();
}

void MainWindow::updateView()
{
    m_gameUI->updateUI();
//    qDebug() << "Graphics take: " << time.elapsed();

    /* Update clocks */
//    if (!m_finished)
//        ui->outTime->setText(QString("%1 secs").arg(m_gameTimeTimer->elapsed() / 1000.0, 6, 'f', 2));
}

void MainWindow::on_actionConfigure_triggered()
{
    if (!m_configureDialog)
        m_configureDialog = new CConfigureDialog(this);

    m_configureDialog->show();
}

void MainWindow::whenCameraModeActionTriggered(QAction *triggerAction)
{
    if (triggerAction == ui->actionFreeCamera)
        m_processor->camera()->setCameraMode(CCamera::FreeCamera);
    else if (triggerAction == ui->actionAvatarInCenter)
        m_processor->camera()->setCameraMode(CCamera::AvatarInCenter);
    else if (triggerAction == ui->actionAvatarWithMargins)
        m_processor->camera()->setCameraMode(CCamera::AvatarWithMargins);
    else if (triggerAction == ui->actionAvatarAndVector)
        m_processor->camera()->setCameraMode(CCamera::AvatarAndVector);
}

void MainWindow::whenAvatarWantLeaveWorld_SinglePlayer(CAvatar *pAvatar, QString destWorldSignature)
{
    CAvatar *singlePlayerAvatar = pAvatar;
    CWorldUtils::loadWorldBySignature(m_world, destWorldSignature);
    m_world->addAvatar(singlePlayerAvatar);
    m_world->start();
}

void MainWindow::on_actionSaveReplay_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save replay..."), "replays", tr("Replay files (*.rec)"));

    if (fileName.isEmpty())
        return;

    m_world->dumpRecord(fileName);
}

void MainWindow::on_actionLoadReplay_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open replay..."), "replays", tr("Replay files (*.rec)"));

    if (fileName.isEmpty())
        return;

//    m_world->playRecord(fileName);

//    if (m_world->avatarsCount()) {
//        if (m_uiProcessor->localPlayer())
//            m_uiProcessor->localPlayer()->setAvatar(m_world->avatarAt(0));
//    }
}

void MainWindow::on_actionRecordReplay_triggered()
{
//    m_world->startRecording();
}

void MainWindow::on_actionPlayReplay_triggered()
{
//    m_world->playRecord();
}

void MainWindow::on_actionStopReplay_triggered()
{
//    m_world->stopRecording();
}

void MainWindow::on_actionShowConsole_triggered(bool checked)
{
    ui->consoleInput->setVisible(checked);
    ui->consoleOutput->setVisible(checked);

    if (checked)
        ui->consoleInput->setFocus();
    else
        m_gameUI->widget()->setFocus();
}

void MainWindow::on_actionBoundedCamera_triggered(bool checked)
{
    m_processor->camera()->setStopAtBounds(checked);
}

void MainWindow::on_actionConnectToLastServer_triggered()
{
    if (m_client && (!m_client->isConnected()))
        m_client->cmdConnect("connect");
}

void MainWindow::on_actionDisconnect_triggered()
{
    if (m_client)
        m_client->cmdDisconnect("");
}
