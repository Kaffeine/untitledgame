#ifndef CCLIENT_HPP
#define CCLIENT_HPP

#include <QObject>
#include <QList>

class CNetBase;

class CWorld;
class CAvatar;
class CPlayer;
class CAvatarAppearance;

class CClient : public QObject
{
    Q_OBJECT
public:
    explicit CClient(QObject *parent = 0);
    inline bool isConnected() const { return m_active; }
    void setOutput(QObject *pObj, QString slotName);
    void setWorld(CWorld *pWorld);
    void setLocalPlayer(CPlayer *pPlayer);

public slots:
    void cmdChats(const QString &line);
    void cmdConnect(const QString &line);
    void cmdDisconnect(const QString &line);
    void sendMessage(const QString &message, int type);
    void doWantSpawn();

signals:
    void messageSignal(const QString &message, int type);
    void acceptedSetWorld(const QString &worldName);
    void out(QString);

private slots:
    void processPackageContent();
    void whenConnected();
    void whenDisconnected();
    void whenAvatarSpawned(CAvatar *who);
    void whenPlayerActionsChanged();
    void whenPlayerLookDestinationChanged();

    void debugSink(QString text);

private:
    CNetBase *m_net;
    QString m_worldsSignature;
    CWorld *m_world;
    bool m_active;
    int m_localPlayerId;
    int m_localAvatarId;
    CPlayer *m_player;
    QList<CAvatarAppearance *> m_appearances;

};

#endif // CCLIENT_HPP
