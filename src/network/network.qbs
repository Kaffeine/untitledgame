import qbs 1.0

Product {
    Depends { name: "extendedarray" }
    Depends { name: "netbase" }
    Depends { name: "logicbase" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "network"] }
    name: "network"
    type: "dynamiclibrary"

    cpp.includePaths: [
        "..",
        "../../libs/CExtendedArray"
    ]

    Group {
        name: "files"
        files: [
            "CGameNetwork.cpp",
            "CGameNetwork.hpp"
        ]
    }
}
