#include "CGameNetwork.hpp"

#include "CExtendedArray.hpp"

#include "logicbase/CPlayer.hpp"
#include "logicbase/CAvatar.hpp"
#include "logicbase/CAvatarAppearance.hpp"

#include <QDebug>
#include <QMetaEnum>

static inline PointF2D readPointF2D(CExtendedArray *source)
{
    qreal x = source->readQReal();
    qreal y = source->readQReal();
    return PointF2D(x, y);
}

static inline void writePointF2D(CExtendedArray *dest, const PointF2D& point)
{
    dest->writeQReal(point.x());
    dest->writeQReal(point.y());
}

CGameNetwork::CGameNetwork(QObject *parent) :
    QObject(parent)
{
}

void CGameNetwork::getStateOfAvatar(CAvatar *pAvatar, CExtendedArray *data)
{
    writePointF2D(data, pAvatar->position());
    writePointF2D(data, pAvatar->speed());

    data->writeQUInt8(pAvatar->m_willDirections); // Read raw value without freeze effect
    data->writeQInt8(pAvatar->gravityDirection());
}

void CGameNetwork::setStateToAvatar(CAvatar *pAvatar, CExtendedArray *data, bool overwriteWills)
{
    pAvatar->setPosition(readPointF2D(data));
    pAvatar->m_speed = readPointF2D(data);

    quint8 newWills = data->readQUInt8();

    if (overwriteWills)
        pAvatar->m_willDirections = newWills;

    pAvatar->setOwnGravityDirection((Directions) data->readQInt8());
}

void CGameNetwork::packAvatarAppearance(CAvatarAppearance *pAppearance, CExtendedArray *data)
{
    data->writeQString(pAppearance->color());

    data->writeQInt8(pAppearance->eyesCount());
    data->writeQReal(pAppearance->eyesSize());

    data->writeQInt8(pAppearance->footsCount());
    data->writeQReal(pAppearance->footsSize());
}

void CGameNetwork::unpackAvatarAppearance(CAvatarAppearance *pAppearance, CExtendedArray *data)
{
    pAppearance->setColor(data->readQString());

    pAppearance->setEyesCount(data->readQInt8());
    pAppearance->setEyesSize (data->readQReal());

    pAppearance->setFootsCount(data->readQInt8());
    pAppearance->setFootsSize (data->readQReal());
}

void CGameNetwork::packWillsOfAvatar(CAvatar *pAvatar, CExtendedArray *data)
{
    data->writeQInt16(pAvatar->m_willDirections);
}

void CGameNetwork::unpackWillsToAvatar(CAvatar *pAvatar, CExtendedArray *data)
{
    pAvatar->m_willDirections = data->readQUInt16();
}

void CGameNetwork::packActionsOfPlayer(CPlayer *pPlayer, CExtendedArray *data)
{
    data->writeQInt16(pPlayer->actions());
    writePointF2D(data, pPlayer->actionDestination());
    writePointF2D(data, pPlayer->altActionDestination());
}

void CGameNetwork::unpackActionsOfPlayer(CPlayer *pPlayer, CExtendedArray *data)
{
    quint16 actions = data->readQUInt16();

    if (!(pPlayer->actions() & CPlayer::ActionAction) && (actions & CPlayer::ActionAction))
        pPlayer->doAction(readPointF2D(data));

    if (!(pPlayer->actions() & CPlayer::ActionAltAction) && (actions & CPlayer::ActionAltAction))
        pPlayer->doAltAction(readPointF2D(data));

    pPlayer->m_actions = actions;
}

QString CGameNetwork::commandToText(quint8 value)
{
    static QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("NetCommand"));
    return QString::number(value) + QString(metaEnum.valueToKey(value));
}
