#ifndef CGAMENETWORK_HPP
#define CGAMENETWORK_HPP

#include <QObject>

class CPlayer;
class CAvatar;
class CExtendedArray;
class CAvatarAppearance;

class CGameNetwork : public QObject
{
    Q_OBJECT
    Q_ENUMS(NetCommand)
    Q_ENUMS(MessagesTypes)
public:
    enum NetCommand
    {
        NetCommandAccept          , // Client accept versions, server accept password
        NetCommandAck             , // Acknowledge
        NetCommandReject          , // Client or server reject connection
        NetCommandReceive         ,
        NetCommandRequest         ,
        NetCommandWorld           , // Server to client signal about world filename (TODO: and control sum)
        NetCommandWorldTime       ,
        NetCommandFile            ,
        NetCommandChat            ,
        NetCommandAvatarSpawn     , // Parameter: AvatarAppearance number
        NetCommandAvatarRemove    ,
        NetCommandAvatarAppearance, // Consist of id-number and appearance details.
        NetCommandAvatarLookDest  ,
        NetCommandPlayerAvatar    ,
        NetCommandPlayerActions   ,
        NetCommandPlayerLookDest  ,
        NetCommandAvatarsWills    ,
        NetCommandPlayerInfo      ,
        NetCommandFullSnapshot    ,
        NetCommandPing            ,
        NetCommandPingInfo

        // TODO: Think about merge AvatarWills and PlayerActions. Reason for "no" is that player actions
        //       is much wider, but clients don't know avatars<->players relationship.

        /*
        NetCommandWhoId          , // Name, uint8 playerId
        NetCommandSetName        , // Name, uint8 playerId, iunt8 length
        NetCommandReceiveParticleRaw,
        NetCommandGamePause      , // From server to clients
        NetCommandGameStart      , // From server to clients
        NetCommandJoinGame       , // From client to server and backward.
        NetCommandAvatarSpawnAt */
    };

    enum MessagesTypes {
        MessageAll,
        MessageTeam,
        MessagePrivate
    };

    static const int protocolVersion = 1;

    explicit CGameNetwork(QObject *parent = 0);
    static void getStateOfAvatar(CAvatar *pAvatar, CExtendedArray *data);
    static void setStateToAvatar(CAvatar *pAvatar, CExtendedArray *data, bool overwriteWills = true);

    static void packAvatarAppearance(CAvatarAppearance *pAppearance, CExtendedArray *data);
    static void unpackAvatarAppearance(CAvatarAppearance *pAppearance, CExtendedArray *data);

    static void packWillsOfAvatar(CAvatar *pAvatar, CExtendedArray *data);
    static void unpackWillsToAvatar(CAvatar *pAvatar, CExtendedArray *data);

    static void packActionsOfPlayer(CPlayer *pPlayer, CExtendedArray *data);
    static void unpackActionsOfPlayer(CPlayer *pPlayer, CExtendedArray *data);

    static QString commandToText(quint8 value);

};

#endif // CGAMENETWORK_HPP
