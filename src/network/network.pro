
QT = core

TEMPLATE = lib
TARGET = network
CONFIG += dll

include(../common.pri)

LIBS += -lextendedarray -lNetBase
LIBS += -llogicbase

SOURCES = CGameNetwork.cpp
HEADERS = CGameNetwork.hpp
