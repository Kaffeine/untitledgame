#include <QCoreApplication>
#include <QFile>
#include <QStringList>
#include <QPointF>

#include "network/CGameNetwork.hpp"
#include "CExtendedArray.hpp"

#include "logicbase/CAvatar.hpp"

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QStringList arguments = a.arguments();

    if (arguments.count() != 2)
        return 0;

    CExtendedArray fileArray;

    QFile inputFile(arguments.at(1));
    inputFile.open(QIODevice::ReadOnly);

    fileArray = inputFile.readAll();

    CExtendedArray *m_net = &fileArray;

    while (!m_net->atEnd()) {
        quint8 com = m_net->readQUInt8();
        quint8 com2;
        qDebug() << "processPackageContent:" << CGameNetwork::commandToText(com);

        if (com == CGameNetwork::NetCommandChat) {
            qDebug() << CGameNetwork::commandToText(m_net->readQUInt8());
            int type = m_net->readQUInt8();
            QString message = m_net->readQString();
            qDebug() << type << message;
        }
        else if (com == CGameNetwork::NetCommandWorld) {
            qDebug() << CGameNetwork::commandToText(m_net->readQUInt8());
            qDebug() << "World signature:" << m_net->readQString();
        }
        else if (com == CGameNetwork::NetCommandWorldTime) {
            com2 = m_net->readQUInt8();
            qDebug() << CGameNetwork::commandToText(com);
            if (com2 == CGameNetwork::NetCommandReceive) {
                qDebug() << "WorldTime:" << m_net->readQUInt32();
            }
            else {
                qDebug() << "BUG";
            }
        }
        else if (com == CGameNetwork::NetCommandAvatarSpawn) {
            com2 = m_net->readQUInt8();
            qDebug() << CGameNetwork::commandToText(com);
            if (com2 == CGameNetwork::NetCommandReceive) {
                qDebug() << "Avatar id" << m_net->readQUInt8();
                qDebug() << "appearanceId" << m_net->readQUInt8();
            }
            else {
                qDebug() << "BUG";
            }
        }
        else if (com == CGameNetwork::NetCommandAvatarRemove) {
            com2 = m_net->readQUInt8();
            qDebug() << CGameNetwork::commandToText(com2);
            if (com2 == CGameNetwork::NetCommandReceive) {
                qDebug() << "Avatar id:" << m_net->readQUInt8(); // Avatar id;
            }
            else {
                qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));
            }
        }
        else if (com == CGameNetwork::NetCommandPlayerAvatar) {
            if (m_net->readQUInt8() != CGameNetwork::NetCommandReceive)
                qDebug() << QString("    !!!%1, but not receive.").arg(CGameNetwork::commandToText(com));

            qDebug() << "LocalAvatarId:" << m_net->readQUInt8();
        }
        else if (com == CGameNetwork::NetCommandAvatarsWills) {
            com2 = m_net->readQUInt8();
            qDebug() << CGameNetwork::commandToText(com2);
            if (com2 == CGameNetwork::NetCommandReceive) {
                qDebug() << "Avatar id:" << m_net->readQUInt8();

                CExtendedArray avatarWillsData = m_net->readBlock();
//            CGameNetwork::unpackWillsToAvatar(m_world->avatarAt(avatarId), &avatarWillsData);
            }
            else {
                qDebug() << "BUG";
            }
        }
        else if (com == CGameNetwork::NetCommandFullSnapshot) {

            com2 = m_net->readQUInt8();
            qDebug() << CGameNetwork::commandToText(com2) << com2;
            if (com2 == CGameNetwork::NetCommandReceive) {

                CExtendedArray snapshotData = m_net->readBlock();
                qDebug() << "Snapshot timestamp:" << snapshotData.readQUInt32(); // << "accepted at" << m_world->time();
                int avatarsInPackage = snapshotData.readQUInt8();
                qDebug() << "    " << "FullSnapshot: Avatars in package" << avatarsInPackage;

                for (int i = 0; i < avatarsInPackage; ++i) {
                    quint8 avatarId = snapshotData.readQUInt8();

                    qDebug() << "    " << "FullSnapshot: Accept avatar" << avatarId;

                    QPointF value;
                    value.setX(snapshotData.readQReal());
                    value.setY(snapshotData.readQReal());

                    qDebug() << "        " << "Pos:" << value;
                    value.setX(snapshotData.readQReal());
                    value.setY(snapshotData.readQReal());
                    qDebug() << "        " << "Speed:" << value;

                    quint8 wills = snapshotData.readQUInt8();
                    qDebug() << "        " << "Wills:" << wills;

                    if (wills & CAvatar::WillAction2) {

                        value.setX(snapshotData.readQReal());
                        value.setY(snapshotData.readQReal());
                        qDebug() << "        " << "hook dest:" << value;
                    }

                    quint8 gravity = snapshotData.readQUInt8();
                    qDebug() << "        " << "Gravity direction:" << gravity;

//                    if (avatarId == m_localAvatarId)
//                        CGameNetwork::setStateToAvatar(m_world->avatarAt(avatarId), &snapshotData, false);
//                    else
//                        CGameNetwork::setStateToAvatar(m_world->avatarAt(avatarId), &snapshotData);
                }
            }
        }
        else if (com == CGameNetwork::NetCommandAvatarAppearance) {
            if (m_net->readQUInt8() == CGameNetwork::NetCommandReceive) {
                int appearanceId = m_net->readQUInt8();
                qDebug() << "        " << "appearanceId:" << appearanceId;

//                while (m_appearances.count() <= appearanceId)
//                    m_appearances.append(new CAvatarAppearance(this));

                CExtendedArray appearanceData = m_net->readBlock();
//                CGameNetwork::unpackAvatarAppearance(m_appearances.at(appearanceId), &appearanceData);
            }
        }
        else if (com == CGameNetwork::NetCommandAvatarLookDest) {
            com = m_net->readQUInt8();
            if (com == CGameNetwork::NetCommandReceive) {
                quint8 avId = m_net->readQUInt8(); // Avatar id
                qDebug() << "        " << "Avatar id:" << avId;
                QPointF newLookDest;
                newLookDest.setX(m_net->readQReal());
                newLookDest.setY(m_net->readQReal());

                qDebug() << "new dest:" << newLookDest;
            }
            else {
                qDebug() << "BUG";
            }
        }
        else if (com ==CGameNetwork::NetCommandFile) {
            com2 = m_net->readQUInt8();
            if (com2 == CGameNetwork::NetCommandReceive) {
                quint8 fileType = m_net->readQUInt8();
                QString fileName;
                CExtendedArray fileContent;
                m_net->readNamedBlock(fileName, fileContent);

                qDebug() << "File type:" << fileType;


//                if (fileType == CGameNetwork::NetCommandWorld) {
//                    CWorldUtils::saveBufferAsFile(fileContent, CWorldUtils::fixedFilename(fileName));

//                    if (fileName == m_worldsSignature) {
//                        CWorldUtils::loadWorldBySignature(m_world, m_worldsSignature);
//                        emit out("Set world:" + m_worldsSignature);

//                        m_net->writeNetCommand(CGameNetwork::NetCommandWorld);
//                        m_net->writeNetCommand(CGameNetwork::NetCommandAck);  // World received/accepted, and can be loaded.
//                    }
//                }
//                else if (fileType == CGameNetwork::NetCommandAvatarAppearance) {
//                    CWorldUtils::saveBufferAsFile(fileContent, fileName);
//                }

//                m_net->writeNetCommand(CGameNetwork::NetCommandFile);
//                m_net->writeNetCommand(CGameNetwork::NetCommandAck);
            }
        }
    }

    return 0;
}
