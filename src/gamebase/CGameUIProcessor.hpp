#ifndef CGAMEUIPROCESSOR_HPP
#define CGAMEUIPROCESSOR_HPP

#include "ui/IUIProcessor.hpp"

#include <QObject>

class CLocalPlayer;
class CCamera;
class IGameUI;
class CWorld;
class CAvatar;

class CGameUIProcessor : public QObject, public IUIProcessor
{
    Q_OBJECT
public:
    explicit CGameUIProcessor(QObject *parent = 0);
    virtual ~CGameUIProcessor();

    void setUI(IGameUI *pUI);
    void gameKeyPressEvent(QKeyEvent *event);
    void gameKeyReleaseEvent(QKeyEvent *event);
    void gameMousePressEvent(const PointI2D &uiCoord, Qt::MouseButton button);
    void gameMouseReleaseEvent(const PointI2D &uiCoord, Qt::MouseButton button);
    void gameMouseMoveEvent(const PointI2D &uiCoord);
    void gameWheelEvent(int delta) { Q_UNUSED(delta); }

    inline CCamera *camera() const { return m_camera; }

    inline CWorld *world() const { return m_world; }
    virtual void setWorld(CWorld *newWorld);

    inline CLocalPlayer *localPlayer() const { return m_localPlayer; }
    void setLocalPlayer(CLocalPlayer *newLocalPlayer);

protected:
    void defaultSetupCamera();

protected slots:
    void reloadWorld();

    void addAvatar(CAvatar *pAvatar);
    void removeAvatar(CAvatar *pAvatar);

    void whenPlayerAvatarChanged(CAvatar *pNewAvatar);

protected:
    IGameUI *m_ui;
    CWorld *m_world;
    CCamera *m_camera;
    CLocalPlayer *m_localPlayer;


//signals:
//    void titleChanged(QString newTitle);
};

#endif // CGAMEUIPROCESSOR_HPP
