#ifndef CCLIENTSETTINGS_HPP
#define CCLIENTSETTINGS_HPP

#include <QObject>

class CAvatarAppearance;

class CClientSettings : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(QString playerName READ playerName WRITE setPlayerName NOTIFY playerNameChanged)
    Q_PROPERTY(QString playerClan READ playerClan WRITE setPlayerClan NOTIFY playerClanChanged)

    Q_PROPERTY(QString lastServer READ lastServer WRITE setLastServer NOTIFY lastServerChanged)

    static CClientSettings *instance(QObject *parent = 0);

    CAvatarAppearance *avatarAppearance() const { return m_appearance; }

    inline QString playerName() { return instance()->m_playerName; }
    inline QString playerClan() { return instance()->m_playerClan; }

    inline QString lastServer() { return instance()->m_lastServer; }

signals:
    void playerNameChanged(QString arg);
    void playerClanChanged(QString arg);

    void lastServerChanged(QString arg);

public slots:
    void setPlayerName(QString newPlayerName);
    void setPlayerClan(QString newPlayerClan);

    void setLastServer(QString newLastServer);

    void load(const QString &fileName);
    void save(const QString &fileName);
    void reset();

private:
    explicit CClientSettings(QObject *parent = 0);
    static CClientSettings *m_instance;

    QString m_playerName;
    QString m_playerClan;

    QString m_lastServer;

    CAvatarAppearance *m_appearance;

};

#endif // CCLIENTSETTINGS_HPP
