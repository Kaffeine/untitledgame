import qbs 1.0

Product {
    Depends { name: "extendedarray" }
    Depends { name: "logicbase" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui"] }
    name: "gamebase"
    type: "dynamiclibrary"

    cpp.includePaths: [
        ".."
    ]

    Group {
        name: "files"
        files: [
            "CClientSettings.cpp",
            "CClientSettings.hpp",
            "CLocalPlayer.cpp",
            "CLocalPlayer.hpp"
        ]
    }
}
