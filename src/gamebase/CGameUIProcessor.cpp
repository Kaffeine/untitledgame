#include "CGameUIProcessor.hpp"

#include "logicbase/CWorld.hpp"
#include "logicbase/CAbstractGameBlock.hpp"
#include "logicbase/CLayer.hpp"
#include "gamebase/CLocalPlayer.hpp"
#include "ui/IGameUI.hpp"
#include "ui/CCamera.hpp"

#include <QKeyEvent>

CGameUIProcessor::CGameUIProcessor(QObject *parent) :
    QObject(parent),
    m_world(0),
    m_camera(new CCamera(this)),
    m_localPlayer(0)
{
    defaultSetupCamera();
}

CGameUIProcessor::~CGameUIProcessor()
{

}

void CGameUIProcessor::setUI(IGameUI *pUI)
{
    m_ui = pUI;
}

void CGameUIProcessor::gameKeyPressEvent(QKeyEvent *event)
{
    if (m_localPlayer)
        m_localPlayer->keyPressEvent(event);
}

void CGameUIProcessor::gameKeyReleaseEvent(QKeyEvent *event)
{
    if (m_localPlayer)
        m_localPlayer->keyReleaseEvent(event);
}

void CGameUIProcessor::gameMousePressEvent(const PointI2D &uiCoord, Qt::MouseButton button)
{
    if (m_localPlayer)
        m_localPlayer->mousePressEvent(m_ui->uiCoordToWorldCoord(uiCoord), button);
}

void CGameUIProcessor::gameMouseReleaseEvent(const PointI2D &uiCoord, Qt::MouseButton button)
{
    if (m_localPlayer)
        m_localPlayer->mouseReleaseEvent(m_ui->uiCoordToWorldCoord(uiCoord), button);
}

void CGameUIProcessor::gameMouseMoveEvent(const PointI2D &uiCoord)
{
    if (m_localPlayer) {
        m_localPlayer->mouseMoveEvent(m_ui->uiCoordToWorldCoord(uiCoord));
        m_camera->setVector(uiCoord - m_camera->viewportSize() / 2);
    }
}

void CGameUIProcessor::defaultSetupCamera()
{
    m_camera->setCameraMode(CCamera::AvatarAndVector);
    m_camera->setMarginsInUnite(23, 12);
    m_camera->setStopAtBounds(false);
    m_camera->setScale(0.4);
}

void CGameUIProcessor::setWorld(CWorld *newWorld)
{
    if (m_world) {
//        if (m_autoupdate)
//            m_viewUpdateTimer->stop();
        disconnect(m_world, SIGNAL(aboutReload()), this, SLOT(reloadWorld()));
        disconnect(m_world, SIGNAL(avatarSpawned(CAvatar*)), this, SLOT(addAvatar(CAvatar*)));
        disconnect(m_world, SIGNAL(avatarRemovedBegin(CAvatar*)), this, SLOT(removeAvatar(CAvatar*)));
    }

    m_world = newWorld;

    if (m_world) {
        connect(m_world, SIGNAL(aboutReload()), SLOT(reloadWorld()));
        connect(m_world, SIGNAL(avatarSpawned(CAvatar*)), SLOT(addAvatar(CAvatar*)));
        connect(m_world, SIGNAL(avatarRemovedBegin(CAvatar*)), SLOT(removeAvatar(CAvatar*)), Qt::DirectConnection);

        reloadWorld();
    }

    m_camera->setWorld(m_world);
}

void CGameUIProcessor::setLocalPlayer(CLocalPlayer *newLocalPlayer)
{
    m_localPlayer = newLocalPlayer;
    m_camera->setAvatar(m_localPlayer->avatar());
    connect(m_localPlayer, SIGNAL(avatarChanged(CAvatar*)), SLOT(whenPlayerAvatarChanged(CAvatar*)));
}

void CGameUIProcessor::reloadWorld()
{
    m_ui->clean();

    CLayer *currentLayer;
    int i, j;
    for (i = 0; i < m_world->layersCount(); ++i) {
        currentLayer = m_world->layer(i);
        for (j = 0; j < currentLayer->blocksCount(); ++j)
            m_ui->addGameBlock(currentLayer->blockAt(j));
    }

    CAbstractGameBlock *pSpawn = m_world->spawnForAvatar();
    if (pSpawn)
        m_camera->setCameraCenterPoint(pSpawn->center());

//    emit titleChanged(m_world->title());
}

void CGameUIProcessor::addAvatar(CAvatar *pAvatar)
{
    m_ui->addAvatar(pAvatar);
}

void CGameUIProcessor::removeAvatar(CAvatar *pAvatar)
{
    m_ui->removeAvatar(pAvatar);
}

void CGameUIProcessor::whenPlayerAvatarChanged(CAvatar *pNewAvatar)
{
    m_camera->setAvatar(pNewAvatar);
}
