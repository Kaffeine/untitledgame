#include "CClientSettings.hpp"

#include "logicbase/CAvatarAppearance.hpp"

#include <QSettings>

CClientSettings *CClientSettings::m_instance = 0;

CClientSettings::CClientSettings(QObject *parent) :
    QObject(parent),
    m_appearance(new CAvatarAppearance(this))
{
    reset();
}

CClientSettings *CClientSettings::instance(QObject *parent)
{
    if (!m_instance)
        m_instance = new CClientSettings(parent);

    return m_instance;
}

void CClientSettings::reset()
{
    setPlayerName(QLatin1String("unnamed gamer"));

    m_appearance->reset();

    setLastServer(QLatin1String("127.0.0.1:25577"));
}

void CClientSettings::load(const QString &fileName)
{
    QSettings *pSettings = new QSettings(fileName, QSettings::IniFormat, this);

    pSettings->beginGroup("Player");
    setPlayerName(pSettings->value("Name", playerName()).toString());
    pSettings->endGroup();

    pSettings->beginGroup("Avatar");
    m_appearance->setColor(pSettings->value("Color", m_appearance->color()).toString());
    m_appearance->setEyesCount(pSettings->value("EyesCount", m_appearance->eyesCount()).toInt());
    m_appearance->setFootsCount(pSettings->value("FootsCount", m_appearance->footsCount()).toInt());
    pSettings->endGroup();

    pSettings->beginGroup("Network");
    setLastServer(pSettings->value("LastServer", lastServer()).toString());
    pSettings->endGroup();
    delete pSettings;
}

void CClientSettings::save(const QString &fileName)
{
    QSettings *pSettings = new QSettings(fileName, QSettings::IniFormat, this);

    pSettings->beginGroup("Player");
    pSettings->setValue("Name", playerName());
    pSettings->endGroup();

    pSettings->beginGroup("Avatar");
    pSettings->setValue("Color", m_appearance->color());
    pSettings->setValue("EyesCount", m_appearance->eyesCount());
    pSettings->setValue("FootsCount", m_appearance->footsCount());
    pSettings->endGroup();

    pSettings->beginGroup("Network");
    pSettings->setValue("LastServer", lastServer());
    pSettings->endGroup();
    pSettings->sync();

    delete pSettings;
}

void CClientSettings::setPlayerName(QString newPlayerName)
{
    if (m_playerName == newPlayerName)
        return;

    m_playerName = newPlayerName;
    emit playerNameChanged(newPlayerName);
}

void CClientSettings::setPlayerClan(QString newPlayerClan)
{
    if (m_playerClan == newPlayerClan)
        return;

    m_playerClan = newPlayerClan;
    emit playerClanChanged(newPlayerClan);
}

void CClientSettings::setLastServer(QString newLastServer)
{
    if (m_lastServer == newLastServer)
        return;

    m_lastServer = newLastServer;
    emit lastServerChanged(newLastServer);
}
