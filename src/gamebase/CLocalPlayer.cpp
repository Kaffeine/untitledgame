#include "CLocalPlayer.hpp"

#include "logicbase/CAvatar.hpp"
#include "logicbase/CAvatarAppearance.hpp"

#include "CClientSettings.hpp"

#include <QKeyEvent>

CLocalPlayer::CLocalPlayer(QObject *parent) :
    CPlayer(parent)
{
}

void CLocalPlayer::keyPressEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
        return;

    if (event->key() == Qt::Key_A)
        m_actions |= ActionLeft;

    if (event->key() == Qt::Key_D)
        m_actions |= ActionRight;

    if (event->key() == Qt::Key_Space)
        m_actions |= ActionJump;

    if (event->key() == Qt::Key_K)
        doSuicide();

    applyActions();
}

void CLocalPlayer::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
        return;

    if (event->key() == Qt::Key_A)
        m_actions &= ~ActionLeft;

    if (event->key() == Qt::Key_D)
        m_actions &= ~ActionRight;

    if (event->key() == Qt::Key_Space)
        m_actions &= ~ActionJump;

    applyActions();
}

void CLocalPlayer::mousePressEvent(const PointF2D &worldPos, Qt::MouseButton button)
{
    if (button == Qt::LeftButton)
        doAction(worldPos);
    else if (button == Qt::RightButton)
        doAltAction(worldPos);
}

void CLocalPlayer::mouseMoveEvent(const PointF2D &worldPos)
{
    if (!m_avatar || !m_avatar->appearance())
        return;

    setLookDestination(worldPos);
}

void CLocalPlayer::mouseReleaseEvent(const PointF2D &worldPos, Qt::MouseButton button)
{
    Q_UNUSED(worldPos);

    if (button == Qt::LeftButton)
        cancelAction();
    else if (button == Qt::RightButton)
        cancelAltAction();
}

void CLocalPlayer::beforeSetAvatarEvent(CAvatar *newAvatar)
{
    if (m_avatar) {
        m_avatar->appearance()->disconnectFromAppearance(CClientSettings::instance()->avatarAppearance());
    }

    if (newAvatar) {
        newAvatar->appearance()->connectToAppearance(CClientSettings::instance()->avatarAppearance());
    }
}
