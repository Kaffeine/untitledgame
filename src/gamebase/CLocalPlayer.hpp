#ifndef CLOCALPLAYER_HPP
#define CLOCALPLAYER_HPP

#include "logicbase/CPlayer.hpp"

class QKeyEvent;

class CLocalPlayer : public CPlayer
{
    Q_OBJECT
public:
    CLocalPlayer(QObject *parent = 0);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mousePressEvent(const PointF2D &worldPos, Qt::MouseButton button);
    void mouseMoveEvent(const PointF2D &worldPos);
    void mouseReleaseEvent(const PointF2D &worldPos, Qt::MouseButton button);
    void beforeSetAvatarEvent(CAvatar *newAvatar);

};

#endif // CLOCALPLAYER_HPP
