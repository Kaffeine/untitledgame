# Files common for client and editor

QT += core gui

TEMPLATE = lib
TARGET = gamebase
CONFIG += dll

include(../common.pri)
LIBS += -lextendedarray
LIBS += -llogicbase -lui

SOURCES += \
    CGameUIProcessor.cpp \
    CLocalPlayer.cpp \
    CClientSettings.cpp

HEADERS += \
    CGameUIProcessor.hpp \
    CLocalPlayer.hpp \
    CClientSettings.hpp
