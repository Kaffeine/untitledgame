#ifndef CBLOCKSMODEL_HPP
#define CBLOCKSMODEL_HPP

#include <QAbstractTableModel>

class CLayer;
class CLayerModel;
class CWorld;
class CEditorMainUIProcessor;

class CBlocksModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum LayerProperties {
        Title,
        IsVisible,
        BlocksCount,
        LayerPropertiesCount
    };

    explicit CBlocksModel(QObject *parent = 0);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    void setWorld(CWorld *pWorld);
    void setProcessor(CEditorMainUIProcessor *pNewProcessor);

private slots:
    void whenDataChanged();

private:
    CWorld *m_world;
    CEditorMainUIProcessor *m_processor;
    QList<CLayerModel *> m_data;
    QList<CLayer *> m_layers;
    QList<QString> m_layersNames;

};

#endif // CBLOCKSMODEL_HPP
