#include "CWorldPropertiesDialog.hpp"
#include "ui_CWorldPropertiesDialog.h"

#include "logicbase/CWorld.hpp"

CWorldPropertiesDialog::CWorldPropertiesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CWorldPropertiesDialog),
    m_world(0)
{
    ui->setupUi(this);
}

CWorldPropertiesDialog::~CWorldPropertiesDialog()
{
    delete ui;
}

void CWorldPropertiesDialog::readWorldInfo(CWorld *pWorld)
{
    m_world = pWorld;
    ui->title->setText(m_world->title());
    ui->nextWorldSignature->setText(m_world->nextWorldSignature());
    ui->signature->setText(m_world->signature());
}

void CWorldPropertiesDialog::accept()
{
    if (m_world) {
        m_world->setTitle(ui->title->text());
        m_world->setSignature(ui->signature->text());
        m_world->setNextWorldSignature(ui->nextWorldSignature->text());
    }
    close();
}
