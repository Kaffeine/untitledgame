#ifndef CWORLDPROPERTIESDIALOG_HPP
#define CWORLDPROPERTIESDIALOG_HPP

#include <QDialog>

namespace Ui {
class CWorldPropertiesDialog;
}

class CWorld;

class CWorldPropertiesDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit CWorldPropertiesDialog(QWidget *parent = 0);
    ~CWorldPropertiesDialog();
    void readWorldInfo(CWorld *pWorld);

public slots:
    void accept();

private:
    Ui::CWorldPropertiesDialog *ui;
    CWorld *m_world;
};

#endif // CWORLDPROPERTIESDIALOG_HPP
