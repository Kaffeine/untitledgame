
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = editor
TEMPLATE = app

include(../common.pri)

LIBS += -lextendedarray
LIBS += -llogicbase -lgamebase -lui

SOURCES += main.cpp \
    MainWindow.cpp \
    CWorldPropertiesDialog.cpp \
    CEditorMainUIProcessor.cpp \
    CEditorBlocksUIProcessor.cpp \
    CBlocksModel.cpp \
    CAddBlockCommand.cpp

HEADERS  +=  \
    MainWindow.hpp \
    CWorldPropertiesDialog.hpp \
    CEditorMainUIProcessor.hpp \
    CEditorBlocksUIProcessor.hpp \
    CBlocksModel.hpp \
    CAddBlockCommand.hpp

FORMS    += MainWindow.ui \
    CWorldPropertiesDialog.ui
