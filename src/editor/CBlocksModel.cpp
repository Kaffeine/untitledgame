#include "CBlocksModel.hpp"

#include "logicbase/CWorld.hpp"
#include "logicbase/CLayer.hpp"
#include "CEditorMainUIProcessor.hpp"

CBlocksModel::CBlocksModel(QObject *parent) :
    QAbstractTableModel(parent)
{

}

void CBlocksModel::whenDataChanged()
{
    beginResetModel();
    endResetModel();
}

int CBlocksModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_world->layersCount();
}

int CBlocksModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return LayerPropertiesCount;
}

void CBlocksModel::setWorld(CWorld *pWorld)
{
    m_world = pWorld;

    m_layersNames << tr("Space/Solid");
    m_layersNames << tr("Spawn/Exit");
    m_layersNames << tr("Start/Finish");
    m_layersNames << tr("Freeze/Unfreeze");
    m_layersNames << tr("TeleIn/TeleOut");
    m_layersNames << tr("GravityMod");
    m_layersNames << tr("Acceleration");

    for (int i = 0; i < m_world->layersCount(); ++i)
        connect(m_world->layer(i), SIGNAL(itemsCountChanged()), SLOT(whenDataChanged()));
}

void CBlocksModel::setProcessor(CEditorMainUIProcessor *pNewProcessor)
{
    m_processor = pNewProcessor;
}

QVariant CBlocksModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if ((index.column() == IsVisible) && (role == Qt::CheckStateRole)) {
        if (m_processor->isLayerVisible(index.row()))
            return Qt::Checked;
        else
            return Qt::Unchecked;
    }

    if ((role != Qt::DisplayRole) || !index.isValid())
        return QVariant();

    switch ((LayerProperties) index.column()) {
    case Title:
        return m_layersNames.at(index.row());
    case BlocksCount:
        return m_world->layer(index.row())->blocksCount();
    default:
        return QVariant();
    }
}

bool CBlocksModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ((!index.isValid()) || (index.column() != IsVisible) || (role != Qt::CheckStateRole))
        return false;

    Qt::CheckState checkState = static_cast<Qt::CheckState>(value.toInt());
    if (checkState == Qt::Checked)
        m_processor->setLayerVisible(index.row(), true);
    else
        m_processor->setLayerVisible(index.row(), false);

    return true;
}

QVariant CBlocksModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ((orientation != Qt::Horizontal) || (role != Qt::DisplayRole))
        return QVariant();

    if (section == Title)
        return tr("Title");

    if (section == IsVisible)
        return tr("Visible");

    if (section == BlocksCount)
        return tr("Blocks count");

    return QVariant();
}

Qt::ItemFlags CBlocksModel::flags(const QModelIndex &index) const
{
    if (index.column() == IsVisible)
        return (Qt::ItemIsSelectable|Qt::ItemIsEditable|Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);

    return (Qt::ItemIsSelectable|Qt::ItemIsEnabled);
}
