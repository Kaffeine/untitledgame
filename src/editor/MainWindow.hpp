#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QModelIndex>

namespace Ui {
class MainWindow;
}

class QTime;

class CWorld;
class CEditorMainUIProcessor;
class CEditorBlocksScene;
class CWorldPropertiesDialog;

class IGameUI;
class CEditorMainUIProcessor;
class CEditorBlocksUIProcessor;

class CConsole;
class CConsoleLineHelper;

class QLabel;

class QUndoStack;
class QUndoCommand;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void whenBlockSelectedFromList(int layer, int index);

    void on_actionOpen_triggered();
    void on_actionSaveAs_triggered();
    void on_actionSave_triggered();
    void on_actionQuit_triggered();
    void on_actionSpawn_triggered();
    void on_actionProperties_triggered();
    void on_layersListView_clicked(const QModelIndex &index);
    void on_actionShowConsole_triggered(bool checked);

    void on_actionNew_triggered();

    void whenActionsSetMode();
    void updateActionsSetModeState();

    void on_actionZoom11_triggered();
    void on_actionZoomIn_triggered();
    void on_actionZoomOut_triggered();
    void resetCamera();
    void updateCaption();

    void whenActionPerformed(QUndoCommand *command);

    void whenMainUIProcessorStatusChanged();

private:

    Ui::MainWindow *ui;
    QLabel *m_mainUIProcessorStatus;

    CWorld *m_world;
    CConsole *m_console;
    CConsoleLineHelper *m_consoleIOHelper;

    CEditorMainUIProcessor   *m_mainProcessor;
    CEditorBlocksUIProcessor *m_blocksProcessor;
    IGameUI *m_gameUI;
    IGameUI *m_blocksUI;

//    QTime *m_gameTimeTimer;
//    bool m_finished;

    QTimer *m_viewUpdateTimer;
    CWorldPropertiesDialog *m_worldPropertiesDialog;
    QString m_mapFilename;

    QUndoStack *m_undoStack;
};

#endif // MAINWINDOW_HPP
