#include "MainWindow.hpp"
#include "ui_MainWindow.h"
#include "CWorldPropertiesDialog.hpp"

#include "logicbase/CWorld.hpp"
#include "logicbase/CWorldUtils.hpp"
#include "logicbase/CConsole.hpp"
#include "gamebase/CLocalPlayer.hpp"
#include "ui/CCamera.hpp"
#include "ui/CConsoleLineHelper.hpp"
#include "ui/IGameUI.hpp"

#include "CBlocksModel.hpp"
#include "CEditorMainUIProcessor.hpp"
#include "CEditorBlocksUIProcessor.hpp"

#include <QTimer>
#include <QTime>
#include <QFileDialog>

#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QUndoStack>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_worldPropertiesDialog(0)
{
    ui->setupUi(this);

    m_mainUIProcessorStatus = new QLabel;
    ui->statusBar->addWidget(m_mainUIProcessorStatus);

    m_world = new CWorld(this);

//    m_finished = true;

    m_mainProcessor   = new CEditorMainUIProcessor(this);
    m_blocksProcessor = new CEditorBlocksUIProcessor(this);

    m_gameUI = IGameUI::createUI();
    m_gameUI->setProcessor(m_mainProcessor);
    ((QGridLayout *) ui->centralWidget->layout())->addWidget(m_gameUI->widget(), 0, 1, 2, 1);
    m_gameUI->widget()->setFocusPolicy(Qt::StrongFocus);
    m_gameUI->widget()->setFocus();

    m_blocksUI = IGameUI::createUI();
    m_blocksUI->widget()->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    m_blocksUI->setProcessor(m_blocksProcessor);
    ((QGridLayout *) ui->centralWidget->layout())->addWidget(m_blocksUI->widget(), 1, 0);
//    m_blocksUI->hide();

    connect(m_blocksProcessor, SIGNAL(blockSelected(int,int)), SLOT(whenBlockSelectedFromList(int,int)));

    for (int i = 0; i < m_world->layersCount(); ++i)
        m_blocksProcessor->addLayerNum(i); // In future may be change it to use CLayer::m_layerType

    CBlocksModel *layersModel = new CBlocksModel(this);
    m_mainProcessor->setWorld(m_world);
    layersModel->setWorld(m_world);
    layersModel->setProcessor(m_mainProcessor);
    ui->layersListView->setModel(layersModel);
    ui->layersListView->resizeColumnsToContents();
    ui->layersListView->setMinimumWidth(ui->layersListView->horizontalHeader()->length() +
                                        ui->layersListView->verticalHeader()->width() +
                                        ui->layersListView->frameWidth() * 2);
    ui->layersListView->selectRow(0);

    whenBlockSelectedFromList(0, 0);

//    m_gameTimeTimer = new QTime();

    /* View */
    m_viewUpdateTimer = new QTimer(this);
    m_viewUpdateTimer->setInterval(20);
    m_viewUpdateTimer->setSingleShot(false);
    connect(m_viewUpdateTimer, SIGNAL(timeout()), m_gameUI  ->object(), SLOT(updateUI()));
//    connect(m_viewUpdateTimer, SIGNAL(timeout()), m_blocksUI->object(), SLOT(updateUI()));
    m_viewUpdateTimer->start();

    connect(m_world, SIGNAL(signatureChanged(QString)), SLOT(updateCaption()));
    connect(m_world, SIGNAL(aboutReload()), SLOT(resetCamera()));
    updateActionsSetModeState();

    m_console = new CConsole(this);
    m_console->addConsoleObject(m_world                  , "world");
    m_console->addConsoleObject(m_mainProcessor->camera(), "camera");
    m_console->addConsoleObject(m_mainProcessor          , "editor");

    on_actionShowConsole_triggered(false);

    m_consoleIOHelper = new CConsoleLineHelper(this);
    m_consoleIOHelper->setObjects(ui->consoleInput, ui->consoleOutput, m_console);

    m_mainProcessor->setLocalPlayer(new CLocalPlayer(this));

    connect(ui->actionDrawMode     , SIGNAL(triggered()), SLOT(whenActionsSetMode()));
    connect(ui->actionDivideMode   , SIGNAL(triggered()), SLOT(whenActionsSetMode()));
    connect(ui->actionInsertionMode, SIGNAL(triggered()), SLOT(whenActionsSetMode()));
    connect(ui->actionPlayingMode  , SIGNAL(triggered()), SLOT(whenActionsSetMode()));

    connect(ui->actionFlipHorizontal, SIGNAL(triggered()), m_mainProcessor, SLOT(selectionFlipHorizontal()));
    connect(ui->actionFlipVertical  , SIGNAL(triggered()), m_mainProcessor, SLOT(selectionFlipVertical()));

    m_undoStack = new QUndoStack(this);
    QAction *pAction;
    pAction = m_undoStack->createUndoAction(this, tr("Undo: "));
    pAction->setShortcut(QKeySequence::Undo);
    ui->menuEdition->addAction(pAction);
    pAction = m_undoStack->createRedoAction(this, tr("Redo: "));
    pAction->setShortcut(QKeySequence::Redo);
    ui->menuEdition->addAction(pAction);

    connect(m_mainProcessor, SIGNAL(actionPerformed(QUndoCommand*)), SLOT(whenActionPerformed(QUndoCommand*)));
    connect(m_undoStack, SIGNAL(cleanChanged(bool)), SLOT(updateCaption()));
    connect(m_mainProcessor, SIGNAL(statusChanged()), SLOT(whenMainUIProcessorStatusChanged()));

    connect(m_world, SIGNAL(aboutReload()), m_undoStack, SLOT(setClean()));
}

MainWindow::~MainWindow()
{
    CWorldUtils::saveAs(m_world, "lastFile");

    delete ui;
}

void MainWindow::on_actionNew_triggered()
{
    m_world->reset();
}

void MainWindow::whenActionsSetMode()
{
    if (!sender())
        return;

    if (sender() == ui->actionDrawMode)
        m_mainProcessor->setEditionMode(CEditorMainUIProcessor::DrawMode);
    else if (sender() == ui->actionDivideMode)
        m_mainProcessor->setEditionMode(CEditorMainUIProcessor::DivideMode);
    else if (sender() == ui->actionInsertionMode)
        m_mainProcessor->setEditionMode(CEditorMainUIProcessor::InsertionMode);
    else if (sender() == ui->actionPlayingMode)
        m_mainProcessor->setEditionMode(CEditorMainUIProcessor::PlayingMode);

    updateActionsSetModeState();
}

void MainWindow::updateActionsSetModeState()
{
    ui->actionDrawMode     ->setChecked(m_mainProcessor->editionMode() == CEditorMainUIProcessor::DrawMode     );
    ui->actionDivideMode   ->setChecked(m_mainProcessor->editionMode() == CEditorMainUIProcessor::DivideMode   );
    ui->actionInsertionMode->setChecked(m_mainProcessor->editionMode() == CEditorMainUIProcessor::InsertionMode);
    ui->actionPlayingMode  ->setChecked(m_mainProcessor->editionMode() == CEditorMainUIProcessor::PlayingMode  );
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open map..."), "maps", tr("Map files (*%1)").arg(CWorldUtils::fileExtension));

    if (fileName.isEmpty())
        return;

    m_mapFilename = fileName;

    CWorldUtils::loadWorldByFilename(m_world, fileName);
}

void MainWindow::on_actionSaveAs_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save map..."), m_mapFilename, tr("Map files (*%1)").arg(CWorldUtils::fileExtension));

    if (fileName.isEmpty())
        return;

    m_mapFilename = fileName;

    if (CWorldUtils::saveAs(m_world, fileName))
        m_undoStack->setClean();
}

void MainWindow::on_actionSave_triggered()
{
    if (m_mapFilename.isEmpty())
        return on_actionSaveAs_triggered();

    if (CWorldUtils::saveAs(m_world, m_mapFilename))
        m_undoStack->setClean();
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_layersListView_clicked(const QModelIndex &index)
{
    m_mainProcessor->setCurrentLayer(index.row());
}

void MainWindow::on_actionSpawn_triggered()
{
    CAvatar *pAvatar = m_world->spawnAvatar();

    if (!pAvatar)
        return;

    m_mainProcessor->localPlayer()->setAvatar(pAvatar);
    m_world->start();
    m_mainProcessor->localPlayer()->setCheckpointsCount(m_world->checkpointsCount());
}

void MainWindow::whenBlockSelectedFromList(int layer, int index)
{
    m_mainProcessor->setCurrentLayer(layer);
    m_mainProcessor->setDrawingIndex(index);
}

void MainWindow::on_actionProperties_triggered()
{
    if (!m_worldPropertiesDialog) {
        m_worldPropertiesDialog = new CWorldPropertiesDialog(this);
    }
    m_worldPropertiesDialog->readWorldInfo(m_world);
    m_worldPropertiesDialog->exec();
}

void MainWindow::on_actionZoom11_triggered()
{
    m_mainProcessor->camera()->setScale(1.0);
}

void MainWindow::on_actionZoomIn_triggered()
{
    float scale = m_mainProcessor->camera()->scale();
    m_mainProcessor->camera()->setScale(scale - 0.1f);
}

void MainWindow::on_actionZoomOut_triggered()
{
    float scale = m_mainProcessor->camera()->scale();
    m_mainProcessor->camera()->setScale(scale + 0.1f);
}

void MainWindow::on_actionShowConsole_triggered(bool checked)
{
    ui->consoleInput->setVisible(checked);
    ui->consoleOutput->setVisible(checked);

    if (checked)
        ui->consoleInput->setFocus();
    else
        m_gameUI->widget()->setFocus();
}

void MainWindow::resetCamera()
{
    PointF2D centralPoint;
    centralPoint.setX(m_mainProcessor->camera()->viewportSize().x() / 2);
    centralPoint.setY(m_mainProcessor->camera()->viewportSize().y() / 2);
    m_mainProcessor->camera()->setCameraCenterPoint(centralPoint);
    ui->actionZoom11->trigger();
}

void MainWindow::updateCaption()
{
    QString caption = tr("Untitled editor");
    if (!m_world->signature().isEmpty()) {
        caption.append(QString(" %1 %2").arg(QChar(0x2013)).arg(m_world->signature()));
        if (!m_undoStack->isClean())
            caption.append("*");
    }
    setWindowTitle(caption);
}

void MainWindow::whenActionPerformed(QUndoCommand *command)
{
    m_undoStack->push(command);
}

void MainWindow::whenMainUIProcessorStatusChanged()
{
    m_mainUIProcessorStatus->setText(m_mainProcessor->status());
}
