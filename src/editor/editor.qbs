import qbs 1.0

Product {
    Depends { name: "extendedarray" }
    Depends { name: "logicbase" }
    Depends { name: "gamebase" }
    Depends { name: "ui" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui"] }
    name: "editor"
    type: "application"

    cpp.includePaths: [
        ".."
    ]

    Group {
        name: "files"
        files: [
            "CAddBlockCommand.cpp",
            "CAddBlockCommand.hpp",
            "CBlocksModel.cpp",
            "CBlocksModel.hpp",
            "CEditorBlocksUIProcessor.cpp",
            "CEditorBlocksUIProcessor.hpp",
            "CEditorMainUIProcessor.cpp",
            "CEditorMainUIProcessor.hpp",
            "CWorldPropertiesDialog.cpp",
            "CWorldPropertiesDialog.hpp",
            "CWorldPropertiesDialog.ui",
            "MainWindow.cpp",
            "MainWindow.hpp",
            "MainWindow.ui",
	    "main.cpp"
        ]
    }
}
