#include "CEditorBlocksUIProcessor.hpp"

#include "logicbase/CLayer.hpp"
#include "logicbase/CBlocksFactory.hpp"
#include "logicbase/CAbstractGameBlock.hpp"
#include "ui/IGameUI.hpp"
#include "ui/CCamera.hpp"

CEditorBlocksUIProcessor::CEditorBlocksUIProcessor(QObject *parent) :
    QObject(parent),
    m_camera(new CCamera(this))
{
    defaultSetupCamera();
}

void CEditorBlocksUIProcessor::addLayerNum(int layer)
{
    CLayer *pLayer = new CLayer(this);
    for (int i = 0; i < CBlocksFactory::subblocksCount(layer); ++i) {
        CAbstractGameBlock *pBlock = CBlocksFactory::newGameBlock(layer, i);
        pLayer->addBlock(pBlock);
        pBlock->setCell(i, m_layers.count());
        m_ui->addGameBlock(pBlock);
    }
    m_layers.append(pLayer);
}

void CEditorBlocksUIProcessor::defaultSetupCamera()
{
    m_camera->setStopAtBounds(true);
    m_camera->setCameraMode(CCamera::FreeCamera);
    m_camera->setViewportSize(800, 600);
}

void CEditorBlocksUIProcessor::gameMousePressEvent(const PointI2D &uiCoord, Qt::MouseButton button)
{
    Q_UNUSED(button)

    PointF2D worldPos = m_ui->uiCoordToWorldCoord(uiCoord);

    int cellX = worldPos.x() / CWorldObject::CellLength;
    int cellY = worldPos.y() / CWorldObject::CellLength;

    if (cellY < m_layers.count()) {
        if (cellX < CBlocksFactory::subblocksCount(cellY))
            emit blockSelected(cellY, cellX);
    }
}
