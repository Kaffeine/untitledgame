#ifndef CEDITORBLOCKSUIPROCESSOR_HPP
#define CEDITORBLOCKSUIPROCESSOR_HPP

#include "ui/IUIProcessor.hpp"

#include <QObject>

class CLayer;

class CEditorBlocksUIProcessor : public QObject, public IUIProcessor
{
    Q_OBJECT
public:
    explicit CEditorBlocksUIProcessor(QObject *parent = 0);
    void addLayerNum(int layer);

    void setUI(IGameUI *pUI) { m_ui = pUI; }
    void gameKeyPressEvent(QKeyEvent *event) { Q_UNUSED(event); }
    void gameKeyReleaseEvent(QKeyEvent *event) { Q_UNUSED(event); }
    void gameMousePressEvent(const PointI2D &uiCoord, Qt::MouseButton button);
    void gameMouseMoveEvent(const PointI2D &uiCoord) { Q_UNUSED(uiCoord); }
    void gameMouseReleaseEvent(const PointI2D &uiCoord, Qt::MouseButton button) { Q_UNUSED(uiCoord); Q_UNUSED(button); }
    void gameWheelEvent(int delta) { Q_UNUSED(delta) }
    virtual CCamera *camera() const { return m_camera; }

signals:
    void blockSelected(int layer, int index);

public slots:

private:
    void defaultSetupCamera();
    IGameUI *m_ui;
    CCamera *m_camera;
    QList<CLayer *> m_layers;
};

#endif // CEDITORBLOCKSUIPROCESSOR_HPP
