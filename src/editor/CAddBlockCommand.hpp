#ifndef CADDBLOCKCOMMAND_HPP
#define CADDBLOCKCOMMAND_HPP

#include <QUndoCommand>

#include "logicbase/Directions.hpp"
#include "logicbase/Points2D.hpp"

class CEditorMainUIProcessor;

class CAddBlockCommand : public QUndoCommand
{
public:
    explicit CAddBlockCommand(CEditorMainUIProcessor *processor, QUndoCommand *parent = 0);

    void setPosition(const PointF2D &pos) { m_position = pos; }
    void setBlockType(int type) { m_blockType = type; }
    void setSubBlockType(int type) { m_subBlockType = type; }
    void setDirection(Directions newDirection) { m_direction = newDirection; }
    void setErase(bool newErase) { m_erase = newErase; }
    void setActionNumber(quint32 newNumber) { m_actionNumber = newNumber; }

    void undo();
    void redo();

private:
    CEditorMainUIProcessor *m_mainProcessor;

    int m_blockType;
    PointF2D m_position;
    Directions m_direction;
    int m_subBlockType;
    bool m_erase;
    quint32 m_actionNumber; // actions in same mouse pression have same number

};

#endif // CADDBLOCKCOMMAND_HPP
