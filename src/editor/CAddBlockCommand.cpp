#include "CAddBlockCommand.hpp"

#include "CEditorMainUIProcessor.hpp"

CAddBlockCommand::CAddBlockCommand(CEditorMainUIProcessor *processor, QUndoCommand *parent) :
    QUndoCommand(parent),
    m_mainProcessor(processor),
    m_erase(false),
    m_actionNumber(0)
{
}

void CAddBlockCommand::undo()
{
    if (m_erase)
        m_mainProcessor->addBlock(m_blockType, m_position, m_direction, m_subBlockType);
    else
        m_mainProcessor->removeBlock(m_blockType, m_position, m_direction, m_subBlockType);
}

void CAddBlockCommand::redo()
{
    if (m_erase)
        m_mainProcessor->removeBlock(m_blockType, m_position, m_direction, m_subBlockType);
    else
        m_mainProcessor->addBlock(m_blockType, m_position, m_direction, m_subBlockType);
}
