#include "CEditorMainUIProcessor.hpp"

#include "logicbase/CWorld.hpp"
#include "logicbase/CLayer.hpp"
#include "logicbase/CAbstractGameBlock.hpp"
#include "logicbase/Functions.hpp"
#include "logicbase/CBlocksFactory.hpp"
#include "ui/CCamera.hpp"
#include "ui/IGameUI.hpp"
#include "ui/IUIRectangle.hpp"
#include "ui/IUIBlocksPresent.hpp"

#include "CAddBlockCommand.hpp"

#include <QKeyEvent>
#include <QDebug>

/* This is GUI class for editor */
CEditorMainUIProcessor::CEditorMainUIProcessor(QObject *parent) :
    CGameUIProcessor(parent),
    m_currentDrawingIndex(0),
    m_moveCamera(false)
{
    m_erase                 = false;
    m_selectionInProgress   = false;
    m_selectionIsMultilayer = false;
    m_lastPressedBlock     = 0;
    m_pressedBlock         = 0;
    m_currentActionNumber  = 0;

    setEditionMode(DrawMode);

    defaultSetupCamera();
}

void CEditorMainUIProcessor::setEditionMode(EditionMode newMode)
{
    m_editionMode = newMode;

    if (newMode == PlayingMode) {
        m_erase                 = false;
        m_selectionInProgress   = false;
        m_selectionIsMultilayer = false;
        m_lastPressedBlock      = 0;
        m_pressedBlock          = 0;
        hideSelection();
    }

    updateStatus();
}

void CEditorMainUIProcessor::defaultSetupCamera()
{
    m_camera->setStopAtBounds(false);
    m_camera->setCameraMode(CCamera::FreeCamera);
    m_camera->setViewportSize(800, 600);
}

void CEditorMainUIProcessor::setDrawingIndex(int newIndex)
{
    m_currentDrawingIndex = newIndex;

    /* TODO: Rewrite it. */
    selectionClear();
}

void CEditorMainUIProcessor::selectionFlipHorizontal()
{
    if (m_selectedBlocks.isEmpty())
        return;

    CAbstractGameBlock *pBlock;
    PointF2D correction;
    qreal measure = m_selectionSize.x();
    for (int i = 0; i < m_selectedBlocks.count(); ++i) {
        pBlock = m_selectedBlocks.at(i);
        correction.setX(measure - pBlock->width() - 2 * pBlock->position().x());
        pBlock->setCellByPos(pBlock->position() + correction);
        if (pBlock->direction() != DirectionNone) {
            if (pBlock->direction() == DirectionLeft)
                pBlock->setDirection(DirectionRight);
            else if (pBlock->direction() == DirectionRight)
                pBlock->setDirection(DirectionLeft);
        }
    }
}

void CEditorMainUIProcessor::selectionFlipVertical()
{
    if (m_selectedBlocks.isEmpty())
        return;

    CAbstractGameBlock *pBlock;
    PointF2D correction;
    qreal measure = m_selectionSize.y();
    for (int i = 0; i < m_selectedBlocks.count(); ++i) {
        pBlock = m_selectedBlocks.at(i);
        correction.setY(measure - pBlock->height() - 2 * pBlock->position().y());
        pBlock->setCellByPos(pBlock->position() + correction);
        if (pBlock->direction() != DirectionNone) {
            if (pBlock->direction() == DirectionUp)
                pBlock->setDirection(DirectionDown);
            else if (pBlock->direction() == DirectionDown)
                pBlock->setDirection(DirectionUp);
        }
    }
}

void CEditorMainUIProcessor::setUI(IGameUI *pUI)
{
    CGameUIProcessor::setUI(pUI);

    m_selectionRect = m_ui->addRectangle();

    if (m_world && m_ui)
        reloadWorld();
}

void CEditorMainUIProcessor::gameKeyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_P) {
        m_world->togglePause();
    }

    CGameUIProcessor::gameKeyPressEvent(event);
}

void CEditorMainUIProcessor::gameMousePressEvent(const PointI2D &uiCoord, Qt::MouseButton button)
{
    m_mousePressedPosition = m_ui->uiCoordToWorldCoord(uiCoord);

    if (button == Qt::MiddleButton) {
        m_moveCamera = true;
        return;
    }

    if (m_editionMode == PlayingMode) {
        return CGameUIProcessor::gameMousePressEvent(uiCoord, button);
    }

    if (m_editionMode == InsertionMode) {
        if (m_ui->gameKeyboardModifiers() & Qt::ControlModifier) {
            if (button == Qt::LeftButton) {
                int row = posToCell(m_mousePressedPosition).y();
                for (int i = 0; i < m_world->layersCount(); ++i)
                    m_world->layer(i)->removeRow(row);
            }
            if (button == Qt::RightButton) {
                int column = posToCell(m_mousePressedPosition).x();
                for (int i = 0; i < m_world->layersCount(); ++i)
                    m_world->layer(i)->removeColumn(column);
            }
        }
        else {
            if (button == Qt::LeftButton) {
                int row = posToCell(m_mousePressedPosition).y();
                for (int i = 0; i < m_world->layersCount(); ++i)
                    m_world->layer(i)->insertRow(row);
            }
            if (button == Qt::RightButton) {
                int column = posToCell(m_mousePressedPosition).x();
                for (int i = 0; i < m_world->layersCount(); ++i)
                    m_world->layer(i)->insertColumn(column);
            }
        }
        return;
    }

    m_erase = false;
    m_selectionInProgress = false;
    m_lastPressedBlock = 0;
    m_pressedBlock = m_world->blockAt(m_mousePressedPosition, m_currentLayer);
    m_roundedMousePressedPosition = roundPosToCell(m_mousePressedPosition);

    switch (m_editionMode) {
    case DrawMode:
        if (button == Qt::LeftButton) {
            if (m_ui->gameKeyboardModifiers() & Qt::ControlModifier) {
                m_selectionInProgress = true;
                m_selectionIsMultilayer = m_ui->gameKeyboardModifiers() & Qt::ShiftModifier;
            }
            else {
                if (m_pressedBlock && (m_pressedBlock->subBlockType() == m_currentDrawingIndex))
                    m_erase = true;

                doDrawAt(m_mousePressedPosition);
            }
        }
        else if (button == Qt::RightButton)
            selectionAbort();
        break;
    case DivideMode:
        if (m_pressedBlock && (m_pressedBlock->direction() != DirectionNone))
            m_erase = true;
        /* Else - divide and keep m_erase == false. */
        doDivideAt(m_mousePressedPosition);
        break;
    default:
        break;
    }
}

void CEditorMainUIProcessor::gameMouseMoveEvent(const PointI2D &uiCoord)
{
    PointF2D worldPos = m_ui->uiCoordToWorldCoord(uiCoord);

    PointF2D motion = m_mousePos - PointF2D(worldPos.x(), worldPos.y());
    if (motion.isNull())
        return;

    m_mousePos = worldPos;

    if (m_moveCamera)
        return m_camera->setCameraCenterPoint(m_camera->centerPoint() + m_mousePressedPosition - m_mousePos);

    if (m_editionMode == PlayingMode)
        return CGameUIProcessor::gameMouseMoveEvent(uiCoord);

    updateStatus();

    PointF2D currentRoundedPosition = roundPosToCell(m_mousePos);

    if (!m_selectionSize.isNull())
        selectionMove(currentRoundedPosition);

//    if (!event->buttons())
//        return;

    CAbstractGameBlock *pBlockUnderCursor = m_world->blockAt(m_mousePos, m_currentLayer);

    if (pBlockUnderCursor && (pBlockUnderCursor == m_lastPressedBlock))
        return;

    if (pBlockUnderCursor)
        m_lastPressedBlock = pBlockUnderCursor;

    PointF2D currentRoundedUpPosition = roundUpPosToCell(m_mousePos);
    PointF2D topLeft     = minPoint(m_roundedMousePressedPosition, currentRoundedUpPosition);
    PointF2D bottomRight = maxPoint(m_roundedMousePressedPosition, currentRoundedUpPosition);

    switch (m_editionMode) {
    case DrawMode:
        if (m_ui->gameMouseButtons() == Qt::LeftButton) {
            /* User doing selection */
            if (m_selectionInProgress) {
                m_selectionRect->setVisible(true);
                m_selectionRect->setOffset(topLeft);
                m_selectionRect->setRect(0, 0, bottomRight.x() - topLeft.x(), bottomRight.y() - topLeft.y());
            }
            else
                doDrawAt(m_mousePos);
        }
        /* End of DrawMode */
        break;
    case DivideMode:
        if (m_ui->gameMouseButtons() == Qt::LeftButton)
            doDivideAt(m_mousePos);
        break;
    case PlayingMode: // Processed early.
    case InsertionMode: // Nothing to do.
        break;
    }
}

void CEditorMainUIProcessor::gameMouseReleaseEvent(const PointI2D &uiCoord, Qt::MouseButton button)
{
    if (button == Qt::MiddleButton) {
        m_moveCamera = false;
        return;
    }

    if (m_editionMode == PlayingMode) {
        return CGameUIProcessor::gameMouseReleaseEvent(uiCoord, button);
    }

    if (m_editionMode == InsertionMode)
        return;

    PointF2D worldPos = m_ui->uiCoordToWorldCoord(uiCoord);

    PointF2D releasedPosition = worldPos;

    if (m_selectionInProgress) {
        PointF2D topLeft     = minPoint(m_roundedMousePressedPosition, releasedPosition);
        PointF2D bottomRight = maxPoint(m_roundedMousePressedPosition, releasedPosition);

        selectionClear();
        m_selectionSize = roundUpPosToCell(bottomRight - topLeft);

        if (m_selectionIsMultilayer) {
            for (int i = 0; i < m_world->layersCount(); ++i) {
                fillSelectionLayer(m_world->layer(i)->blocksInRect(topLeft, bottomRight), -topLeft);
            }
        }
        else
            fillSelectionLayer(m_world->layer(m_currentLayer)->blocksInRect(topLeft, bottomRight), -topLeft);

        selectionMove(roundPosToCell(releasedPosition));
        showSelection();
        m_selectionInProgress = false;
    }
    else
        ++m_currentActionNumber;
}

void CEditorMainUIProcessor::gameWheelEvent(int delta)
{
    /* Enable it when it's needed. */
//    if (m_editionMode == PlayingMode) {
//        return CGameUIProcessor::gameWheelEvent(delta);
//    }

    if (delta > 0)
        m_camera->setScale(m_camera->scale() - 0.1);
    else
        m_camera->setScale(m_camera->scale() + 0.1);
}

void CEditorMainUIProcessor::doDrawAt(const PointF2D &where)
{
    /* Dev: Call only editor[Add,Remove,Replace]Block[s] in this method! */
    CAbstractGameBlock *pBlockUnderAction = m_world->blockAt(where, m_currentLayer);
    if (pBlockUnderAction && (pBlockUnderAction->subBlockType() == m_currentDrawingIndex) && !m_erase)
        return;

    m_currentUndoCommand = new QUndoCommand(tr("Draw command"));

    PointF2D currentRoundedPosition = roundPosToCell(where);

    /* If selection is null, then user want to draw single block */
    if (m_selectionSize.isNull()) {
        if (m_erase) {
            if (pBlockUnderAction)
                editorRemoveBlock(pBlockUnderAction);
        }
        else {
            if (pBlockUnderAction && (pBlockUnderAction->subBlockType() != m_currentDrawingIndex))
                editorReplaceBlock(pBlockUnderAction, pBlockUnderAction->direction(), m_currentDrawingIndex);
            else {
                QList<CAbstractGameBlock *> blocksInCell = m_world->layer(m_currentLayer)->getBlocksInCell(posToCell(where));
                if (!blocksInCell.isEmpty())
                    editorAddBlock(m_currentLayer, currentRoundedPosition,
                                   partByCoordinates(where - currentRoundedPosition), m_currentDrawingIndex);
                else
                    editorAddBlock(m_currentLayer, currentRoundedPosition, DirectionNone, m_currentDrawingIndex);
            }
        }
    }
    /* User want to draw selection */
    else {
        /* Cleanup zone */
        if (m_selectionIsMultilayer) {
            for (int i = 0; i < m_world->layersCount(); ++i) {
                editorRemoveBlocks(m_world->layer(i)->blocksInRect(currentRoundedPosition, currentRoundedPosition + m_selectionSize));
            }
        }
        else
            editorRemoveBlocks(m_world->layer(m_currentLayer)->blocksInRect(currentRoundedPosition, currentRoundedPosition + m_selectionSize));

        /* Add blocks from selections layer */
        for (int i = 0; i < m_selectedBlocks.count(); ++i) {
            editorAddBlock(m_selectedBlocks.at(i)->blockType(), m_selectedBlocks.at(i)->position() + currentRoundedPosition,
                           m_selectedBlocks.at(i)->direction(), m_selectedBlocks.at(i)->subBlockType());
        }
    }

    emit actionPerformed(m_currentUndoCommand);
    m_currentUndoCommand = 0;
}

void CEditorMainUIProcessor::doDivideAt(const PointF2D &where)
{
    /* Dev: Call only editor[Add,Remove,Replace]Block[s] in this method! */
    CAbstractGameBlock *pBlockUnderAction = m_world->blockAt(where, m_currentLayer);
    if (!pBlockUnderAction)
        return;

    m_currentUndoCommand = new QUndoCommand(tr("Divide command"));

    PointF2D position;
    int subBlockType = pBlockUnderAction->subBlockType();

    if (pBlockUnderAction->direction() != DirectionNone) {
        position = roundPosToCell(where);
        if (m_erase) {
            editorRemoveBlocks(m_world->layer(m_currentLayer)->getBlocksInCell(pBlockUnderAction));
            editorAddBlock(m_currentLayer, position, DirectionNone, subBlockType);
        }
    }
    else {
        position     = pBlockUnderAction->position();
        subBlockType = pBlockUnderAction->subBlockType();

        editorRemoveBlock(pBlockUnderAction);
        editorAddBlock(m_currentLayer, position, DirectionUp   , subBlockType);
        editorAddBlock(m_currentLayer, position, DirectionDown , subBlockType);
        editorAddBlock(m_currentLayer, position, DirectionLeft , subBlockType);
        editorAddBlock(m_currentLayer, position, DirectionRight, subBlockType);
    }

    emit actionPerformed(m_currentUndoCommand);
    m_currentUndoCommand = 0;
}

CAbstractGameBlock *CEditorMainUIProcessor::addBlock(int layer, const PointF2D &where, Directions direction, int subBlockType)
{
    CAbstractGameBlock *pBlock = m_world->layer(layer)->addBlock(where, direction, subBlockType);
    m_ui->addGameBlock(pBlock);
    m_ui->setGameBlockVisible(pBlock, isLayerVisible(layer));
    return pBlock;
}

void CEditorMainUIProcessor::removeBlock(int layer, const PointF2D &where, Directions direction, int subBlockType)
{
    CAbstractGameBlock *pBlock = m_world->layer(layer)->blockAtCell(posToCell(where), direction);
    m_ui->removeGameBlock(pBlock);
    m_world->layer(layer)->removeBlock(pBlock);
}

void CEditorMainUIProcessor::fillSelectionLayer(const QList<CAbstractGameBlock *> &blocks, const PointF2D &offset)
{
    for (int i = 0; i < blocks.count(); ++i) {
        m_selectedBlocks.append(CBlocksFactory::cloneBlock(blocks.at(i)));
        m_selectedBlocks.last()->setPosition(blocks.at(i)->position() + offset);

        m_selectedBlocksPresents.append(m_ui->addGameBlock(m_selectedBlocks.last()));
    }
}

bool CEditorMainUIProcessor::isLayerVisible(int layer) const
{
    if (layer < m_layersVisible.count())
        return m_layersVisible.at(layer);

    qDebug() << QString("CEditorMainUIProcessor::isLayerVisible(%1) - out of range.").arg(layer);
    return true;
}

void CEditorMainUIProcessor::setLayerVisible(int layer, bool visible)
{
    if (layer >= m_layersVisible.count())
        return;

    m_layersVisible[layer] = visible;
    for (int i = 0; i < m_world->layer(layer)->blocksCount(); ++i) {
        m_ui->setGameBlockVisible(m_world->layer(layer)->blockAt(i), visible);
    }
}

void CEditorMainUIProcessor::selectionMove(const PointF2D &newPosition)
{
    m_selectionRect->setOffset(newPosition);
    for (int i = 0; i < m_selectedBlocksPresents.count(); ++i)
        m_selectedBlocksPresents.at(i)->setOffset(newPosition);
}

void CEditorMainUIProcessor::updateStatus()
{
    PointI2D mouseCell = posToCell(m_mousePos);

    if (m_editionMode == DrawMode) {
        if (m_selectionInProgress) {

            PointF2D topLeft     = minPoint(m_roundedMousePressedPosition, m_mousePos);
            PointF2D bottomRight = maxPoint(m_roundedMousePressedPosition, m_mousePos);

            PointF2D cellTopLeft = posToCell(topLeft);
            PointF2D cellBottomRight = posToCell(bottomRight);

            setStatus(QString("%1x%2 (Selection %3x%4-%5x%6)").arg(m_mousePos.x()).arg(m_mousePos.y())
                      .arg(cellTopLeft.x()).arg(cellTopLeft.y()).arg(cellBottomRight.x()).arg(cellBottomRight.y()));
        }
        else
            setStatus(QString("%1x%2 (Cell %3x%4)").arg(m_mousePos.x()).arg(m_mousePos.y()).arg(mouseCell.x()).arg(mouseCell.y()));
    }
    else if (m_editionMode == PlayingMode)
        setStatus(tr("Playing mode"));
    else
        setStatus("");
}

void CEditorMainUIProcessor::setStatus(const QString &newStatus)
{
    m_status = newStatus;
    emit statusChanged();
}

void CEditorMainUIProcessor::selectionClear()
{
    for (int i = 0; i < m_selectedBlocks.count(); ++i) {
        m_ui->removeGameBlock(m_selectedBlocks.at(i));
        delete m_selectedBlocks.at(i);
    }

    m_selectedBlocks.clear();
    m_selectedBlocksPresents.clear();

    m_selectionRect->setVisible(false);
    m_selectionRect->setOffset(PointF2D(0, 0));
}

void CEditorMainUIProcessor::selectionAbort()
{
    if (m_selectionSize.isNull())
        return;

    selectionClear();
    m_selectionSize         = PointF2D(0, 0);
    m_selectionInProgress   = false;
    m_selectionIsMultilayer = false;
}

void CEditorMainUIProcessor::setSelectionVisible(bool newVisible)
{
    m_selectionRect->setVisible(newVisible);

    for (int i = 0; i < m_selectedBlocksPresents.count(); ++i)
        m_selectedBlocksPresents.at(i)->setVisible(newVisible);
}

//void CEditorMainUIProcessor::setGridVisible(bool visible)
//{
//    /* */
//}

void CEditorMainUIProcessor::setCurrentLayer(int newLayer)
{
    m_currentLayer = newLayer;
}

void CEditorMainUIProcessor::setWorld(CWorld *newWorld)
{
    CGameUIProcessor::setWorld(newWorld);
    m_layersVisible.clear();

    if (m_world) {
        m_layersVisible.reserve(m_world->layersCount());
        for (int i = 0; i < m_world->layersCount(); ++i)
            m_layersVisible.append(true);
    }

    m_currentActionNumber = 0;
}

void CEditorMainUIProcessor::editorAddBlock(int layer, const PointF2D &where, Directions direction, int subBlockType)
{
    CAddBlockCommand *pCommand = new CAddBlockCommand(this, m_currentUndoCommand);
    pCommand->setBlockType(layer);
    pCommand->setPosition(where);
    pCommand->setDirection(direction);
    pCommand->setSubBlockType(subBlockType);
}

void CEditorMainUIProcessor::editorRemoveBlock(CAbstractGameBlock *pBlock)
{
    editorRemoveBlock(pBlock->blockType(), pBlock->position(), pBlock->direction(), pBlock->subBlockType());
}

void CEditorMainUIProcessor::editorRemoveBlock(int layer, const PointF2D &where, Directions direction, int subBlockType)
{
    CAddBlockCommand *pCommand = new CAddBlockCommand(this, m_currentUndoCommand);
    pCommand->setBlockType(layer);
    pCommand->setPosition(where);
    pCommand->setDirection(direction);
    pCommand->setSubBlockType(subBlockType);
    pCommand->setErase(true);
}

void CEditorMainUIProcessor::editorRemoveBlocks(const QList<CAbstractGameBlock *> &blocks)
{
    for (int i = 0; i < blocks.count(); ++i)
        editorRemoveBlock(blocks.at(i));
}

void CEditorMainUIProcessor::editorReplaceBlock(CAbstractGameBlock *pBlock, Directions direction, int subBlockType)
{
    /* This will place deletion in order, that will be executed later, so we can use block instance in next line. */
    editorRemoveBlock(pBlock);
    editorAddBlock(pBlock->blockType(), pBlock->position(), direction, subBlockType);
}
