#ifndef CEDITORMAINUIPROCESSOR_HPP
#define CEDITORMAINUIPROCESSOR_HPP

#include "gamebase/CGameUIProcessor.hpp"
#include "ui/IUIProcessor.hpp"
#include "logicbase/Directions.hpp"
#include "logicbase/Points2D.hpp"

#include <QObject>

class CLocalPlayer;
class CCamera;
class IGameUI;
class IUIBlocksPresent;
class IUIRectangle;
class CWorld;
class CAvatar;
class CAbstractGameBlock;
class CLayer;
class QUndoCommand;

class CEditorMainUIProcessor : public CGameUIProcessor
{
    Q_OBJECT
    Q_ENUMS(EditionMode)
    Q_PROPERTY(EditionMode editionMode READ editionMode WRITE setEditionMode)
public:
    enum EditionMode {
        DrawMode,
        DivideMode,
        PlayingMode,
        InsertionMode
    };

    explicit CEditorMainUIProcessor(QObject *parent = 0);

    inline EditionMode editionMode() const { return m_editionMode; }
    void setEditionMode(EditionMode newMode);

    bool isLayerVisible(int layer) const;

    inline QString status() const { return m_status; }

    /* Interface for undo commands */
    CAbstractGameBlock *addBlock(int layer, const PointF2D &where, Directions direction, int subBlockType);
    void removeBlock(int layer, const PointF2D &where, Directions direction, int subBlockType);

public slots:
    void setCurrentLayer(int newLayer);
    void setDrawingIndex(int newIndex);
    void selectionFlipHorizontal();
    void selectionFlipVertical();
    void selectionClear();
    void selectionAbort();
    void setLayerVisible(int layer, bool visible);

signals:
    // WARNING! This signal emit unparented objects. If noone accept it, it will cause memory leak!
    void actionPerformed(QUndoCommand *command);
    void statusChanged();

public:
    void setUI(IGameUI *pUI);
    void gameKeyPressEvent(QKeyEvent *event);
    void gameMousePressEvent(const PointI2D &uiCoord, Qt::MouseButton button);
    void gameMouseMoveEvent(const PointI2D &uiCoord);
    void gameMouseReleaseEvent(const PointI2D &uiCoord, Qt::MouseButton button);
    void gameWheelEvent(int delta);

    void setWorld(CWorld *newWorld);

protected:
    void defaultSetupCamera();

    void doDrawAt(const PointF2D &where);
    void doDivideAt(const PointF2D &where);

private:
    EditionMode m_editionMode;

    void editorAddBlock(int layer, const PointF2D &where, Directions direction, int subBlockType);
    void editorRemoveBlock(int layer, const PointF2D &where, Directions direction, int subBlockType);
    inline void editorRemoveBlock(CAbstractGameBlock *pBlock);
    inline void editorRemoveBlocks(const QList<CAbstractGameBlock *> &blocks);

    void editorReplaceBlock(CAbstractGameBlock *pBlock, Directions direction, int subBlockType);

    void fillSelectionLayer(const QList<CAbstractGameBlock *> &blocks, const PointF2D &offset);
    inline void showSelection() { setSelectionVisible(true); }
    inline void hideSelection() { setSelectionVisible(false); }
    void setSelectionVisible(bool newVisible);
    void selectionMove(const PointF2D &newPosition);

    void updateStatus();
    void setStatus(const QString &newStatus);

    int m_currentLayer;
    int m_currentDrawingIndex;
    PointF2D m_roundedMousePressedPosition;

    QList<CAbstractGameBlock *> m_selectedBlocks;
    QList<IUIBlocksPresent *> m_selectedBlocksPresents;
    IUIRectangle *m_selectionRect;
    PointF2D m_selectionSize;
    bool m_selectionInProgress;
    bool m_selectionIsMultilayer;

    bool m_erase;
    PointF2D m_mousePressedPosition;
    PointF2D m_mousePos;
    bool m_moveCamera;
    CAbstractGameBlock *m_pressedBlock;
    CAbstractGameBlock *m_lastPressedBlock;
    QList<bool> m_layersVisible;
    QUndoCommand *m_currentUndoCommand;
    quint32 m_currentActionNumber;
    QString m_status;

};

#endif // CEDITORMAINUIPROCESSOR_HPP
