#ifndef CGLWINDOW_HPP
#define CGLWINDOW_HPP

#include <QGLWidget>

class CGLWindow : public QGLWidget
{
    Q_OBJECT
public:
    explicit CGLWindow(QWidget *parent = 0);
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void resizeScene(int w, int h);

    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);

    void mouseMoveEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

    void drawText(QPainter *painter);
    void drawMainMenu();

    void genTextures();

signals:

public slots:

private:
    int m_width;
    int m_height;

    int m_sceneWidth;
    int m_sceneHeight;

};

#endif // CGLWINDOW_HPP
