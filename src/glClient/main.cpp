#include <QApplication>
#include <QTimer>

#include "logicbase/CWorld.hpp"
#include "logicbase/CWorldUtils.hpp"
#include "gamebase/CGameUIProcessor.hpp"
#include "gamebase/CLocalPlayer.hpp"
#include "ui/renderGL/CGameUI_GL.hpp"

//#include "CGLWindow.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    IGameUI *ui = IGameUI::createUI();
    CGameUIProcessor *processor = new CGameUIProcessor(&a);

    CWorld *world = new CWorld(&a);

    ui->setProcessor(processor);
    ui->widget()->showFullScreen();
    processor->setWorld(world);

    QTimer screenUpdater;

    screenUpdater.setInterval(20);
    screenUpdater.setSingleShot(false);

    QObject::connect(&screenUpdater, SIGNAL(timeout()), ui->object(), SLOT(updateUI()));

    CWorldUtils::loadWorldByFilename(world, "01");

    processor->setLocalPlayer(new CLocalPlayer(&a));
    processor->localPlayer()->setAvatar(world->spawnAvatar());

    world->start();
    screenUpdater.start();

    return a.exec();
}
