QT       += core gui opengl

include(../common.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = glClient
TEMPLATE = app

LIBS += -lextendedarray -lNetBase
LIBS += -llogicbase -lgamebase -lnetwork -lui

SOURCES += main.cpp
#    CGLWindow.cpp

HEADERS += \
#    CGLWindow.hpp
