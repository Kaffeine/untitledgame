#include "CGLWindow.hpp"

GLuint textureID[2];

void CGLWindow::genTextures()
{
   textureID[0]= bindTexture(QPixmap(QString("../textures/picture1.jpg")), GL_TEXTURE_2D);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

   textureID[1]= bindTexture(QPixmap(QString("../textures/picture2.jpg")), GL_TEXTURE_2D);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

CGLWindow::CGLWindow(QWidget *parent) :
    QGLWidget(parent)
{
    setCursor(Qt::BlankCursor);
}

void CGLWindow::initializeGL()
{
    glClearColor(1, 1, 1, 1);
    glEnable(GL_DEPTH_TEST);
}

void CGLWindow::resizeScene(int w, int h)
{
    m_sceneWidth  = w;
    m_sceneHeight = h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    qreal aspect = (qreal) m_sceneWidth / (qreal) m_sceneHeight;
//    glFrustum(-aspect, +aspect, -1.0, +1.0, 0.05, 100.0);
    glOrtho(-m_sceneWidth / 2, +m_sceneWidth / 2, -m_sceneHeight / 2, +m_sceneHeight / 2, 0.05, 100.0);
    glMatrixMode(GL_MODELVIEW);
}

void CGLWindow::resizeGL(int w, int h)
{
    m_width  = w;
    m_height = h;
    glViewport(0, 0, (GLint)w, (GLint)h);

    resizeScene(w, h);
    glMatrixMode(GL_MODELVIEW);

//    if (m_processor->camera())
//        m_processor->camera()->setViewportSize(w, h);
}

void CGLWindow::paintGL()
{
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glTranslatef(-m_sceneWidth / 2, m_sceneHeight / 2,  -5.0f);
//    glTranslatef(-viewGeom.x(),viewGeom.y(), 0);

    glBegin(GL_QUADS);
    glColor3f(0.16, 0.48, 0.85);
    /* Right/top vertex */
    glVertex3f(40960, 40960, -1);
    /* Left/top vertex */
    glVertex3f(-40960, +40960, -1);
    glColor3f(0.65, 0.77, 0.85);
    glVertex3f(-40960, -40960, -1);
    glVertex3f(+40960, -40960, -1);
    glEnd();

//    glBegin(GL_QUADS);
//    glColor3f(0.16, 0.48, 0.85);
//    /* Right/top vertex */
//    glVertex3f( 60,  30, 1);
//    glVertex3f(-60,  30, 1);
//    glVertex3f(-60, -30, 1);
//    glVertex3f( 60, -30, 1);

////    glVertex3f(+m_world->maxX() * 1.5, +m_world->maxY() * 0.5, -1);
////    /* Left/top vertex */
////    glVertex3f(-m_world->maxX() * 0.5, +m_world->maxY() * 0.5, -1);
////    glColor3f(0.65, 0.77, 0.85);
////    glVertex3f(-m_world->maxX() * 0.5, -m_world->maxY() * 1.5, -1);
////    glVertex3f(+m_world->maxX() * 1.5, -m_world->maxY() * 1.5, -1);
////    glEnd();

    glBegin (GL_QUADS);
    glColor3f (0.0, 0.3, 0.2);
    glVertex3f( 60,  30, 1);
    glVertex3f(-60,  30, 1);
    glVertex3f(-60, -30, 1);
    glVertex3f( 60, -30, 1);
//    glVertex3f (30 , 10, 1);
//    glVertex3f (-30, 10, 1);
//    glVertex3f (-30,  0, 1);
//    glVertex3f (30 ,  0, 1);
    glEnd ();
}

void CGLWindow::keyPressEvent(QKeyEvent *)
{
}

void CGLWindow::keyReleaseEvent(QKeyEvent *)
{
}

void CGLWindow::mouseMoveEvent(QMouseEvent *)
{
}

void CGLWindow::mousePressEvent(QMouseEvent *)
{
}

void CGLWindow::mouseReleaseEvent(QMouseEvent *)
{
}

void CGLWindow::drawText(QPainter *painter)
{
    QString text = tr("Click and drag with the left mouse button "
                      "to rotate the Qt logo.");
    QFontMetrics metrics = QFontMetrics(font());
    int border = qMax(4, metrics.leading());

    QRect rect = metrics.boundingRect(0, 0, width() - 2*border, int(height()*0.125),
                                      Qt::AlignCenter | Qt::TextWordWrap, text);
    painter->setRenderHint(QPainter::TextAntialiasing);
    painter->fillRect(QRect(0, 0, width(), rect.height() + 2*border),
                     QColor(0, 0, 0, 127));
    painter->setPen(Qt::white);
    painter->fillRect(QRect(0, 0, width(), rect.height() + 2*border),
                      QColor(0, 0, 0, 127));
    painter->drawText((width() - rect.width())/2, border,
                      rect.width(), rect.height(),
                      Qt::AlignCenter | Qt::TextWordWrap, text);
}

void CGLWindow::drawMainMenu()
{

}
