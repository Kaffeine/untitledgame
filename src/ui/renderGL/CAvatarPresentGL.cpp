#include "CAvatarPresentGL.hpp"
#include "logicbase/CAvatar.hpp"
#include "logicbase/CAvatarAppearance.hpp"
#include "logicbase/CHookAction.hpp"

#include "logicbase/Functions.hpp"

GLuint CAvatarPresentGL::texturesPull[3];

const qreal CAvatarPresentGL::m_near = 0.5;
const qreal CAvatarPresentGL::m_far =  0.2;

CAvatarPresentGL::CAvatarPresentGL(CAvatar *pAvatar) :
    m_avatar(pAvatar)
{
    m_freezeColor.setRgbF(0, 0.5, 0.9, 0.1);

    m_bodyTexture = 0;
    m_eyeTexture  = 1;
    m_footTexture = 2;
}

void CAvatarPresentGL::render()
{
    qreal overload = 0.1;
    qreal footsDistance = m_avatar->width() * 0.1;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texturesPull[m_bodyTexture]);

    glPushMatrix();
    uiTranslate(m_avatar->center());

    glBegin (GL_QUADS);
    setNearColor();
    glTexCoord2f(0, 0);
    uiVertex(-m_avatar->width() * (overload + 0.5), +m_avatar->height() * (overload + 0.5), m_near);
    glTexCoord2f(1, 0);
    uiVertex(+m_avatar->width() * (overload + 0.5), +m_avatar->height() * (overload + 0.5), m_near);
    glTexCoord2f(1, 1);
    uiVertex(+m_avatar->width() * (overload + 0.5), -m_avatar->height() * (overload + 0.5), m_near);
    glTexCoord2f(0, 1);
    uiVertex(-m_avatar->width() * (overload + 0.5), -m_avatar->height() * (overload + 0.5), m_near);
    glEnd();

    if (m_avatar->appearance()->footsCount() > 0) {
        glBindTexture(GL_TEXTURE_2D, texturesPull[m_footTexture]);
        glPushMatrix();

        if (m_avatar->touchesDirections() & DirectionDown) {
            /* Grounded */
            uiTranslate(0, m_avatar->height() / 2);
        }
        else {
            /* Fly */
            if (m_avatar->jumpIsAvailable() > 0) {
                uiTranslate(0, m_avatar->height() * 0.3);
            }
            else {
                uiTranslate(0, m_avatar->height() * 0.6);
            }
        }

        glBegin (GL_QUADS);

        qreal horizontalPos = 0;
        horizontalPos += m_avatar->appearance()->footsCount() * m_avatar->appearance()->footsSize();
        horizontalPos += (m_avatar->appearance()->footsCount() - 1) * footsDistance;
        horizontalPos = - horizontalPos / 2;

        for (int i = 0; i < m_avatar->appearance()->footsCount(); ++i) {
            glTexCoord2f(0, 0);
            uiVertex(horizontalPos, +m_avatar->appearance()->footsSize() / 2, m_near + 0.01);
            glTexCoord2f(1, 0);
            uiVertex(horizontalPos + m_avatar->appearance()->footsSize(), +m_avatar->appearance()->footsSize() / 2, m_near + 0.01);
            glTexCoord2f(1, 1);
            uiVertex(horizontalPos + m_avatar->appearance()->footsSize(), -m_avatar->appearance()->footsSize() / 2, m_near + 0.01);
            glTexCoord2f(0, 1);
            uiVertex(horizontalPos, -m_avatar->appearance()->footsSize() / 2, m_near + 0.01);

            horizontalPos += m_avatar->appearance()->footsSize() + footsDistance;
        }

        glEnd();
        glPopMatrix();
    }

    for (int i = 0; i < m_avatar->appearance()->eyesCount(); ++i) {
        glBindTexture(GL_TEXTURE_2D, texturesPull[m_eyeTexture]);
        glPushMatrix();

        uiTranslate(m_avatar->appearance()->positionOfEye(i));

        glBegin (GL_QUADS);

        glColor3f(1, 1, 1);
        glTexCoord2f(0, 0);
        uiVertex (- m_avatar->appearance()->eyesSize() / 2, + m_avatar->appearance()->eyesSize() / 2, m_near + 0.01);
        glTexCoord2f(1, 0);
        uiVertex (+ m_avatar->appearance()->eyesSize() / 2, + m_avatar->appearance()->eyesSize() / 2, m_near + 0.01);
        glTexCoord2f(1, 1);
        uiVertex (+ m_avatar->appearance()->eyesSize() / 2, - m_avatar->appearance()->eyesSize() / 2, m_near + 0.01);
        glTexCoord2f(0, 1);
        uiVertex (- m_avatar->appearance()->eyesSize() / 2, - m_avatar->appearance()->eyesSize() / 2, m_near + 0.01);

        glEnd();
        glPopMatrix();
    }

    glDisable(GL_TEXTURE_2D);

    CHookAction *hookAction = qobject_cast<CHookAction*>(m_avatar->currentAltAction());
    if (hookAction && hookAction->hookLength() != 0) {
        glPushMatrix();
        glRotatef(angleByVector(hookAction->hookVectorNormalized()), 0, 0, 1);
        glBegin(GL_QUADS);
        uiColor(QColor("gray"));
        uiVertex(-10, 0, m_near);
        uiVertex(-10, hookAction->hookLength(), m_near);
        uiVertex( 10, hookAction->hookLength(), m_near);
        uiVertex( 10, 0, m_near);
        glEnd();

        glPopMatrix();
    }

    glPopMatrix();
}

void CAvatarPresentGL::setNearColor()
{
    m_color = m_avatar->appearance()->color();

    qreal freezeTime = m_avatar->freezedTime();

    if (freezeTime > 0) {
        static const qreal maxDrawedFreezeTime = 3.0;

        if (freezeTime > maxDrawedFreezeTime)
            freezeTime = maxDrawedFreezeTime;

        qreal prop1 = 0.6 * (freezeTime / maxDrawedFreezeTime);
        qreal prop2 = 1 - prop1;

        QColor mixedFreezed;
        mixedFreezed.setRgbF(m_freezeColor.redF() * prop1 + m_color.redF()   * prop2,
                          m_freezeColor.greenF() * prop1 + m_color.greenF() * prop2,
                          m_freezeColor.blueF()  * prop1 + m_color.blueF()  * prop2,
                          m_freezeColor.alphaF() * prop1 + m_color.alphaF() * prop2);
        uiColor(mixedFreezed);
    }
    else
        uiColor(m_color);
}

void CAvatarPresentGL::setFarColor()
{
    uiColor(m_color.darker(150));
}
