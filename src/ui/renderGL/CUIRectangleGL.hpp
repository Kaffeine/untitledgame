#ifndef CUIRECTANGLEGL_HPP
#define CUIRECTANGLEGL_HPP

#include "CGLBase.hpp"
#include "../IUIRectangle.hpp"

class CUIRectangleGL : public IUIRectangle, public CGLBase
{
public:
    explicit CUIRectangleGL() { m_isVisible = false; m_z = 0.8; }
    void render();

    void setRect(float x, float y, float width, float height);

    bool isVisible() const { return m_isVisible; }
    void setVisible(bool newVisible) { m_isVisible = newVisible; }

    void setZValue(float z) { m_z = z; }
    float zValue() const { return m_z; }

    void setOffset(const PointF2D &newOffset) { m_offset = newOffset; }
    PointF2D offset() const { return m_offset; }

private:
    bool m_isVisible;
    PointF2D m_pos;
    float m_width;
    float m_height;
    float m_z;
    PointF2D m_offset;

};

#endif // CUIRECTANGLEGL_HPP
