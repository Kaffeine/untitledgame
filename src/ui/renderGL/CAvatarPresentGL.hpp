#ifndef CAVATARPRESENTGL_HPP
#define CAVATARPRESENTGL_HPP

#include <QList>
#include "CGLBase.hpp"

class CAvatar;
class CHookPresentGL;

class CAvatarPresentGL : public CGLBase
{
public:
    explicit CAvatarPresentGL(CAvatar *pAvatar);
    void render();

    inline CAvatar *avatar() const { return m_avatar; }
    static GLuint texturesPull[3];
private:
    CAvatar *m_avatar;
    CHookPresentGL *m_hookPresent;

    QColor m_freezeColor;
    QColor m_color;
    QColor m_borderColor;

    inline void setNearColor();
    inline void setFarColor();

    static const qreal m_near;
    static const qreal m_far;

    GLuint m_bodyTexture;
    GLuint m_eyeTexture;
    GLuint m_footTexture;

};

#endif // CAVATARPRESENTGL_HPP
