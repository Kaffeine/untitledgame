#include "CUIRectangleGL.hpp"

void CUIRectangleGL::render()
{
    if (!m_isVisible)
        return;

    glBegin(GL_QUADS);
    uiColor(QColor(0, 0, 128, 128));
    uiVertex(m_pos + m_offset, m_z);
    uiVertex(m_pos + m_offset + PointF2D(m_width, 0), m_z);
    uiVertex(m_pos + m_offset + PointF2D(m_width, m_height), m_z);
    uiVertex(m_pos + m_offset + PointF2D(0, m_height), m_z);
    glEnd();
}

void CUIRectangleGL::setRect(float x, float y, float width, float height)
{
    m_pos.setX(x);
    m_pos.setY(y);

    m_width  = width;
    m_height = height;
}
