#ifndef CGAMEUI_GL_HPP
#define CGAMEUI_GL_HPP

#include "../IGameUI.hpp"

#include <QGLWidget>

class CAvatarPresentGL;
class CBlocksPresentGL;
class CUIRectangleGL;

class IUIProcessor;
class CAvatar;
class CAbstractGameBlock;

class CCamera;

class CGameUI_GL : public QGLWidget, public IGameUI
{
    Q_OBJECT
public:
    explicit CGameUI_GL(QWidget *parent = 0);

    void setProcessor(IUIProcessor *pProcessor);

    Qt::MouseButtons gameMouseButtons() const { return m_mouseButtons; }
    Qt::KeyboardModifiers gameKeyboardModifiers() const { return m_keyboardModifiers; }

    PointF2D uiCoordToWorldCoord(const PointI2D &uiCoord) const;

public slots:
    IUIBlocksPresent *addGameBlock(CAbstractGameBlock *pGameBlock);
    void updateGameBlock(CAbstractGameBlock *pGameBlock) { Q_UNUSED(pGameBlock); }
    void removeGameBlock(CAbstractGameBlock *pGameBlock);
    void setGameBlockVisible(CAbstractGameBlock *pGameBlock, bool visible);

    void addAvatar(CAvatar *pAvatar);
    void removeAvatar(CAvatar *pAvatar);

    IUIRectangle *addRectangle();

    void updateUI();
    void clean();
    QWidget *widget() { return this; }
    QObject *object() { return this; }

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    Qt::MouseButtons m_mouseButtons;
    Qt::KeyboardModifiers m_keyboardModifiers;
    IUIProcessor *m_processor;

protected:
    PointF2D m_sceneSize;

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void resizeScene(PointF2D newSize);
    void genTextures();

    void updateBlocksInArea(const PointF2D &point1, const PointF2D &point2, int type = -1);

    QList<CAvatarPresentGL *> m_avatarsPresents;
    QList<CBlocksPresentGL *> m_blocksPresents;
    QList<CBlocksPresentGL *> m_blocksPresentsTranslucenct;
    QList<CUIRectangleGL *> m_rectangles;

public:
    /* Methods to be called from CBlocksPresentGL: */
    QList<CBlocksPresentGL *> blocksInRect(const PointF2D &point1, const PointF2D &point2, int type) const;
    QList<CBlocksPresentGL *> blocksInRect(const PointF2D &point1, const PointF2D &point2, int type, const QList<CBlocksPresentGL *> &list) const;
    QList<CBlocksPresentGL *> getNeighbors(CBlocksPresentGL *rootBlock, bool sameType = true) const;

    QList<CBlocksPresentGL *> getBlocksInCell(CBlocksPresentGL *rootBlock) const;

    CBlocksPresentGL *blockPresentByGameBlock(CAbstractGameBlock *pGameBlock) const;
    CBlocksPresentGL *blockPresentByGameBlock(CAbstractGameBlock *pGameBlock, const QList<CBlocksPresentGL *> &list) const;

};

#endif // CGAMEUI_GL_HPP
