#ifndef CGLBASE_HPP
#define CGLBASE_HPP

#include <QColor>
#include <GL/gl.h>

#include "logicbase/Points2D.hpp"

#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812F
#endif

class CGLBase {
protected:
    inline void uiColor(const QColor &baseColor) { glColor4f(baseColor.redF(), baseColor.greenF(), baseColor.blueF(), baseColor.alphaF()); }
    inline void uiVertex(const PointF2D &point, float z = 0) { glVertex3f(point.x(), -point.y(), z); }
    inline void uiVertex(float x, float y, float z) { glVertex3f(x, -y, z); }
    inline void uiTranslate(const PointF2D &point, float z = 0) { glTranslated(point.x(), -point.y(), z); }
    inline void uiTranslate(float x, float y, float z = 0) { glTranslated(x, -y, z); }
};


#endif // CGLBASE_HPP
