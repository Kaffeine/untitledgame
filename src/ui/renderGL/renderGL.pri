
DEFINES  += USE_GL

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/CGameUI_GL.cpp \
    $$PWD/CAvatarPresentGL.cpp \
    $$PWD/CBlocksPresentGL.cpp \
    $$PWD/CUIRectangleGL.cpp \

HEADERS  +=  \
    $$PWD/CGameUI_GL.hpp \
    $$PWD/CAvatarPresentGL.hpp \
    $$PWD/CBlocksPresentGL.hpp \
    $$PWD/CUIRectangleGL.hpp \
    $$PWD/CGLBase.hpp
