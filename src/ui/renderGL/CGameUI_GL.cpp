#include "CGameUI_GL.hpp"
#include "../CCamera.hpp"
#include "../IUIProcessor.hpp"

#include "CBlocksPresentGL.hpp"
#include "CAvatarPresentGL.hpp"
#include "CUIRectangleGL.hpp"

#include "logicbase/CAbstractGameBlock.hpp"
#include "logicbase/CLayer.hpp"
#include "logicbase/BlocksTypes.hpp"
#include "logicbase/Functions.hpp"

#include <QDebug>

#include <QMouseEvent>

IGameUI *IGameUI::createUI()
{
    return new CGameUI_GL();
}

CGameUI_GL::CGameUI_GL(QWidget *parent) :
    QGLWidget(parent),
    m_processor(0)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setMouseTracking(true);
}

void CGameUI_GL::keyPressEvent(QKeyEvent *event)
{
    m_processor->gameKeyPressEvent(event);
}

void CGameUI_GL::keyReleaseEvent(QKeyEvent *event)
{
    m_processor->gameKeyReleaseEvent(event);
}

void CGameUI_GL::mousePressEvent(QMouseEvent *event)
{
    m_mouseButtons = event->buttons();
    m_keyboardModifiers = event->modifiers();
    m_processor->gameMousePressEvent(PointI2D(event->pos().x(), event->pos().y()), event->button());
}

void CGameUI_GL::mouseReleaseEvent(QMouseEvent *event)
{
    m_mouseButtons = event->buttons();
    m_keyboardModifiers = event->modifiers();
    m_processor->gameMouseReleaseEvent(PointI2D(event->pos().x(), event->pos().y()), event->button());
}

void CGameUI_GL::mouseMoveEvent(QMouseEvent *event)
{
    m_processor->gameMouseMoveEvent(PointI2D(event->pos().x(), event->pos().y()));
}

void CGameUI_GL::wheelEvent(QWheelEvent *event)
{
    m_processor->gameWheelEvent(event->delta());
}

PointF2D CGameUI_GL::uiCoordToWorldCoord(const PointI2D &uiCoord) const
{
    RectF2D viewGeometry = m_processor->camera()->viewGeometry();

    PointF2D totalRelevantPos;

    totalRelevantPos.setX((uiCoord.x() * viewGeometry.width () / width ()) + viewGeometry.x());
    totalRelevantPos.setY((uiCoord.y() * viewGeometry.height() / height()) + viewGeometry.y());

    return totalRelevantPos;
}

void CGameUI_GL::setProcessor(IUIProcessor *pProcessor)
{
    m_processor = pProcessor;
    m_processor->setUI(this);
}

void CGameUI_GL::clean()
{
    for (int i = 0; i < m_avatarsPresents.count(); ++i)
        delete m_avatarsPresents.at(i);
    m_avatarsPresents.clear();

    for (int i = 0; i < m_blocksPresents.count(); ++i)
        delete m_blocksPresents.at(i);
    m_blocksPresents.clear();

    for (int i = 0; i < m_blocksPresentsTranslucenct.count(); ++i)
        delete m_blocksPresentsTranslucenct.at(i);
    m_blocksPresentsTranslucenct.clear();
}

void CGameUI_GL::addAvatar(CAvatar *pAvatar)
{
    m_avatarsPresents.append(new CAvatarPresentGL(pAvatar));
}

void CGameUI_GL::removeAvatar(CAvatar *pAvatar)
{
    for (int i = m_avatarsPresents.count() - 1; i >= 0; --i) {
        if (m_avatarsPresents.at(i)->avatar() == pAvatar) {
            delete m_avatarsPresents.at(i);
            m_avatarsPresents.removeAt(i);
        }
    }
}

IUIRectangle *CGameUI_GL::addRectangle()
{
    m_rectangles.append(new CUIRectangleGL());
    return m_rectangles.last();
}

void CGameUI_GL::genTextures()
{
    CAvatarPresentGL::texturesPull[0] = bindTexture(QPixmap(QString("data/player.png")), GL_TEXTURE_2D);
    CAvatarPresentGL::texturesPull[1] = bindTexture(QPixmap(QString("data/eye.png")), GL_TEXTURE_2D);
    CAvatarPresentGL::texturesPull[2] = bindTexture(QPixmap(QString("data/foot.png")), GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

CBlocksPresentGL *CGameUI_GL::blockPresentByGameBlock(CAbstractGameBlock *pGameBlock) const
{
    if (pGameBlock->blockType() == BlockFreeze)
        return blockPresentByGameBlock(pGameBlock, m_blocksPresentsTranslucenct);
    else
        return blockPresentByGameBlock(pGameBlock, m_blocksPresents);
}

CBlocksPresentGL *CGameUI_GL::blockPresentByGameBlock(CAbstractGameBlock *pGameBlock, const QList<CBlocksPresentGL *> &list) const
{
    for (int i = list.count() - 1; i >= 0; --i) {
        if (list.at(i)->gameBlock() == pGameBlock)
            return list.at(i);
    }

    return 0;
}

void CGameUI_GL::initializeGL()
{
    glClearColor(1, 1, 1, 1);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//    glEnable(GL_TEXTURE_2D);
//    glEnable(GL_CULL_FACE);
    genTextures();
}

void CGameUI_GL::resizeScene(PointF2D newSize)
{
    m_sceneSize = newSize;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    qreal aspect = (qreal) m_sceneSize.x() / (qreal) m_sceneSize.y();
//    glFrustum(-aspect, +aspect, -1.0, +1.0, 0.05, 100.0);
    glOrtho(-m_sceneSize.x() / 2, +m_sceneSize.x() / 2, -m_sceneSize.y() / 2, +m_sceneSize.y() / 2, 0.05, 100.0);
    glMatrixMode(GL_MODELVIEW);
}

void CGameUI_GL::resizeGL(int w, int h)
{
    glViewport(0, 0, (GLint)w, (GLint)h);

    if (m_processor->camera())
        m_processor->camera()->setViewportSize(w, h);
}

void CGameUI_GL::paintGL()
{
    if (m_processor->camera()) {
        RectF2D viewGeom = m_processor->camera()->viewGeometry();

        if (viewGeom.size() != m_sceneSize)
            resizeScene(viewGeom.size());

        glLoadIdentity();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glTranslatef(-viewGeom.x() - viewGeom.width() / 2, viewGeom.y() + viewGeom.height() / 2,  -5.0f);

        glBegin(GL_QUADS);
        glColor3f(0.16, 0.48, 0.85);
        /* Right/top vertex */
        glVertex3f(40960, 40960, -1);
        /* Left/top vertex */
        glVertex3f(-40960, +40960, -1);
        glColor3f(0.65, 0.77, 0.85);
        glVertex3f(-40960, -40960, -1);
        glVertex3f(+40960, -40960, -1);
        glEnd();

        for (int i = 0; i < m_blocksPresents.count(); ++i)
            m_blocksPresents.at(i)->render();

        for (int i = 0; i < m_blocksPresentsTranslucenct.count(); ++i)
            m_blocksPresentsTranslucenct.at(i)->render();

        for (int i = 0; i < m_avatarsPresents.count(); ++i)
            m_avatarsPresents.at(i)->render();

        for (int i = 0; i < m_rectangles.count(); ++i)
            m_rectangles.at(i)->render();
    }
    else {
        glLoadIdentity();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}

IUIBlocksPresent *CGameUI_GL::addGameBlock(CAbstractGameBlock *pGameBlock)
{
    CBlocksPresentGL *pNewBlock = new CBlocksPresentGL(pGameBlock, this);

    if (pGameBlock->blockType() == BlockFreeze)
        m_blocksPresentsTranslucenct.append(pNewBlock);
    else
        m_blocksPresents.append(pNewBlock);

    updateBlocksInArea(pGameBlock->position() - pGameBlock->size() * 1.1, pGameBlock->position() + pGameBlock->size() * 1.1, pGameBlock->blockType());

    return pNewBlock;
}

void CGameUI_GL::removeGameBlock(CAbstractGameBlock *pGameBlock)
{
    QList<CBlocksPresentGL *> *blocksList;

    if (pGameBlock->blockType() == BlockFreeze)
        blocksList = &m_blocksPresentsTranslucenct;
    else
        blocksList = &m_blocksPresents;

    for (int i = blocksList->count() - 1; i >= 0; --i) {
        if (blocksList->at(i)->gameBlock() == pGameBlock) {
            delete blocksList->at(i);
            blocksList->removeAt(i);
            break; // Assume that there is no more than one present for each block.
        }
    }

    updateBlocksInArea(pGameBlock->position() - pGameBlock->size() * 1.1, pGameBlock->position() + pGameBlock->size() * 1.1, pGameBlock->blockType());
}

void CGameUI_GL::setGameBlockVisible(CAbstractGameBlock *pGameBlock, bool visible)
{
    if (!pGameBlock)
        return;

    CBlocksPresentGL *pPresent = blockPresentByGameBlock(pGameBlock);

    if (pPresent)
        pPresent->setVisible(visible);
}

void CGameUI_GL::updateBlocksInArea(const PointF2D &point1, const PointF2D &point2, int type)
{
    QList <CBlocksPresentGL *> blocksToUpdate = blocksInRect(point1, point2, type);

    for (int i = 0; i < blocksToUpdate.count(); ++i)
        blocksToUpdate.at(i)->updateNeighbors();
}

QList<CBlocksPresentGL *> CGameUI_GL::getBlocksInCell(CBlocksPresentGL *rootBlock) const
{
    return blocksInRect(rootBlock->gameBlock()->cellPosition(), rootBlock->gameBlock()->cellPosition() + rootBlock->gameBlock()->CellSize, rootBlock->gameBlock()->blockType());
}

QList<CBlocksPresentGL *> CGameUI_GL::blocksInRect(const PointF2D &point1, const PointF2D &point2, int type) const
{
    return blocksInRect(point1, point2, type, m_blocksPresents) + blocksInRect(point1, point2, type, m_blocksPresentsTranslucenct);
}

QList<CBlocksPresentGL *> CGameUI_GL::blocksInRect(const PointF2D &point1, const PointF2D &point2, int type, const QList<CBlocksPresentGL *> &list) const
{
    PointF2D topLeft     = minPoint(point1, point2);
    PointF2D bottomRight = maxPoint(point1, point2);

    QList<CBlocksPresentGL *> blocksInRectList;

    for (int i = 0; i < list.count(); ++i) {
        CAbstractGameBlock *currentBlock = list.at(i)->gameBlock();
        if (type >= 0 && currentBlock->blockType() != type)
            continue;

        if (bottomRight.x() <= currentBlock->minX())
            continue;

        if (bottomRight.y() <= currentBlock->minY())
            continue;

        if (currentBlock->maxX() <= topLeft.x())
            continue;

        if (currentBlock->maxY() <= topLeft.y())
            continue;

        blocksInRectList.append(list.at(i));
    }

    return blocksInRectList;
}

QList<CBlocksPresentGL *> CGameUI_GL::getNeighbors(CBlocksPresentGL *rootBlock, bool sameType) const
{
    QList<CBlocksPresentGL *> neighborsList;

    int type;
    if (sameType)
        type = rootBlock->gameBlock()->blockType();
    else
        type = -1;

    neighborsList = blocksInRect(rootBlock->position() - CWorldObject::CellSize * 1.1, rootBlock->position() + CWorldObject::CellSize * 1.1, type, m_blocksPresents);
    neighborsList += blocksInRect(rootBlock->position() - CWorldObject::CellSize * 1.1, rootBlock->position() + CWorldObject::CellSize * 1.1, type, m_blocksPresentsTranslucenct);
    neighborsList.removeAll(rootBlock);
    return neighborsList;
}

void CGameUI_GL::updateUI()
{
    updateGL();
}
