#include "CBlocksPresentGL.hpp"

#include "logicbase/mapBlocks/allBlocks.hpp"
#include "logicbase/CLayer.hpp"

#include "CGameUI_GL.hpp"

#include "../CBlocksAppearance.hpp"
#include <qmath.h>
#include <QDebug>

CBlocksPresentGL::CBlocksPresentGL(CAbstractGameBlock *pGameBlock, CGameUI_GL *gameUI) :
    m_gameUI(gameUI),
    m_block(pGameBlock),
    m_isVisible(true)
{
    m_nearOffsetLeft  = 0;
    m_nearOffsetRight = 0;

    m_sameCellOffsetValue = 0;

    if (m_block->blockType() == BlockSolid)
        m_nearValue = 0.499;
    else if (m_block->blockType() == BlockFreeze) {
        m_nearValue = 0.40;
        m_nearOffsetLeft  = CWorldObject::CellLength * 0.25;
        m_nearOffsetRight = CWorldObject::CellLength * 0.25;
        m_nearOffsetUp    = CWorldObject::CellLength * 0.25;
        m_nearOffsetDown  = CWorldObject::CellLength * 0.25;
        m_nearOffsetBase  = CWorldObject::CellLength * 0.25;
        m_sameCellOffsetValue = qSqrt(m_nearOffsetBase * m_nearOffsetBase * 2) / 2;
//        qDebug() << "sOV:" << m_sameCellOffsetValue;
    }
    else if (m_block->blockType() == BlockTele)
        m_nearValue = 0.15;
    else
        m_nearValue = 0.21;

    if (m_block->blockType() == BlockFreeze)
        m_farValue = 0.10;
    else
        m_farValue = 0;

    for (int i = 0; i < NeighborsMaxCount; ++i)
        m_neighbors[i] = 0;

    m_sameCellNeighbors[NeighborLeft ] = 0;
    m_sameCellNeighbors[NeighborRight] = 0;
    m_sameCellNeighbors[NeighborBase ] = 0;
}

void CBlocksPresentGL::render()
{
    if (!m_isVisible)
        return;

    glPushMatrix();
    uiTranslate(offset());
    switch(m_block->blockType()) {
    case BlockAcceleration:
        renderAccelerationBlock();
        break;
    case BlockFreeze:
        renderFreezeBlock();
        break;
    case BlockGravity:
        renderGravityBlock();
        break;
    case BlockSolid:
        renderDefaultBlock();
        break;
    case BlockTele:
        renderTeleBlock();
        break;
    default:
        break;
    }
    glPopMatrix();
}

void CBlocksPresentGL::updateNeighbors()
{
    if (!m_block->layer())
        return;

    for (int i = 0; i < NeighborsMaxCount; ++i)
        m_neighbors[i] = 0;

    if (m_block->blockType() == BlockFreeze) {
        QList<CBlocksPresentGL *> neighbors = m_gameUI->getNeighbors(this, true);

        for (int i = 0; i < neighbors.count(); ++i) {
            if (neighbors.at(i)->gameBlock()->minY() == m_block->minY()) {
                if (neighbors.at(i)->gameBlock()->maxX() == m_block->minX())
                    m_neighbors[NeighborLeft] = neighbors.at(i);
                else if (neighbors.at(i)->gameBlock()->minX() == m_block->maxX())
                    m_neighbors[NeighborRight] = neighbors.at(i);
            }
            else if (neighbors.at(i)->gameBlock()->minX() == m_block->minX()) {
                if (neighbors.at(i)->gameBlock()->maxY() == m_block->minY())
                    m_neighbors[NeighborUp] = neighbors.at(i);
                else if (neighbors.at(i)->gameBlock()->minY() == m_block->maxY())
                    m_neighbors[NeighborDown] = neighbors.at(i);
            }
        }

        if (m_neighbors[NeighborLeft])
            m_nearOffsetLeft  = 0;
        else
            m_nearOffsetLeft  = CWorldObject::CellLength * 0.25;

        if (m_neighbors[NeighborRight])
            m_nearOffsetRight = 0;
        else
            m_nearOffsetRight = CWorldObject::CellLength * 0.25;

        if (m_neighbors[NeighborUp])
            m_nearOffsetUp    = 0;
        else
            m_nearOffsetUp    = CWorldObject::CellLength * 0.25;

        if (m_neighbors[NeighborDown])
            m_nearOffsetDown  = 0;
        else
            m_nearOffsetDown  = CWorldObject::CellLength * 0.25;
    }

    if (m_block->direction() != DirectionNone) {
        QList <CBlocksPresentGL *> neighbors = m_gameUI->getBlocksInCell(this);
        for (int i = 0; i < neighbors.count(); ++i) {
            switch (neighbors.at(i)->gameBlock()->direction()) {
            case DirectionLeft:
                if (m_block->direction() == DirectionDown)
                    m_sameCellNeighbors[NeighborLeft] = neighbors.at(i);
                else if (m_block->direction() == DirectionUp)
                    m_sameCellNeighbors[NeighborRight] = neighbors.at(i);
                break;
            case DirectionRight:
                if (m_block->direction() == DirectionDown)
                    m_sameCellNeighbors[NeighborRight] = neighbors.at(i);
                else if (m_block->direction() == DirectionUp)
                    m_sameCellNeighbors[NeighborLeft] = neighbors.at(i);
                break;
            case DirectionUp:
                if (m_block->direction() == DirectionRight)
                    m_sameCellNeighbors[NeighborRight] = neighbors.at(i);
                else if (m_block->direction() == DirectionLeft)
                    m_sameCellNeighbors[NeighborLeft] = neighbors.at(i);
                break;
            case DirectionDown:
                if (m_block->direction() == DirectionRight)
                    m_sameCellNeighbors[NeighborLeft] = neighbors.at(i);
                else if (m_block->direction() == DirectionLeft)
                    m_sameCellNeighbors[NeighborRight] = neighbors.at(i);
                break;
            default:
                break;
            }
        }
        switch (m_block->direction()) {
        case DirectionLeft:
            if ((m_neighbors[NeighborLeft]) &&
                    ((m_neighbors[NeighborLeft]->gameBlock()->direction() == DirectionNone)
                     || (m_neighbors[NeighborLeft]->gameBlock()->direction() == DirectionRight)))
                m_sameCellNeighbors[NeighborBase] = m_neighbors[NeighborLeft];
            break;
        case DirectionRight:
            if ((m_neighbors[NeighborRight]) &&
                    ((m_neighbors[NeighborRight]->gameBlock()->direction() == DirectionNone)
                     || (m_neighbors[NeighborRight]->gameBlock()->direction() == DirectionLeft)))
                m_sameCellNeighbors[NeighborBase] = m_neighbors[NeighborRight];
            break;
        case DirectionUp:
            if ((m_neighbors[NeighborUp]) &&
                    ((m_neighbors[NeighborUp]->gameBlock()->direction() == DirectionNone)
                     || (m_neighbors[NeighborUp]->gameBlock()->direction() == DirectionDown)))
                m_sameCellNeighbors[NeighborBase] = m_neighbors[NeighborUp];
            break;
        case DirectionDown:
            if ((m_neighbors[NeighborDown]) &&
                    ((m_neighbors[NeighborDown]->gameBlock()->direction() == DirectionNone)
                     || (m_neighbors[NeighborDown]->gameBlock()->direction() == DirectionUp)))
                m_sameCellNeighbors[NeighborBase] = m_neighbors[NeighborDown];
            break;
        default:
            break;
        }
    }
}

void CBlocksPresentGL::renderAccelerationBlock()
{
    switch(m_block->subBlockType()) {
    case CAccelerationBlock::Left:
        uiTranslate(m_block->cellPosition());
        break;
    case CAccelerationBlock::Right:
        uiTranslate(m_block->cellPosition() + CWorldObject::CellSize);
        glRotatef(180, 0, 0, 1);
        break;
    case CAccelerationBlock::Up:
        uiTranslate(m_block->cellPosition() + PointF2D(CWorldObject::CellLength, 0));
        glRotatef(-90, 0, 0, 1);
        break;
    case CAccelerationBlock::Down:
        uiTranslate(m_block->cellPosition() + PointF2D(0, CWorldObject::CellLength));
        glRotatef(90, 0, 0, 1);
        break;
    }

    qreal motion = CWorldObject::CellLength / 8;
    glBegin(GL_TRIANGLES);
    setFarColor(m_block);
    uiVertex(CWorldObject::CellSize - PointF2D(motion, motion), m_nearValue);
    uiVertex(CWorldObject::CellLength - motion, motion, m_nearValue);
    setNearColor(m_block);
    uiVertex(CBlocksAppearance::Points[CBlocksAppearance::HCenterVCenter], m_nearValue);
    glEnd();
}

void CBlocksPresentGL::renderBigFreezeBlock()
{
    // TODO: Check corners neighbors and reduce central area.
    if (m_block->direction() == DirectionNone) {
        uiTranslate(m_block->cellPosition());
        glBegin (GL_QUADS);
        setNearColor(m_block);

        uiVertex(m_nearOffsetLeft, m_nearOffsetUp, m_nearValue);
        uiVertex(CWorldObject::CellLength - m_nearOffsetRight, m_nearOffsetUp, m_nearValue);
        uiVertex(CWorldObject::CellLength - m_nearOffsetRight, CWorldObject::CellLength - m_nearOffsetDown, m_nearValue);
        uiVertex(m_nearOffsetLeft, CWorldObject::CellLength - m_nearOffsetDown, m_nearValue);

        if (m_nearOffsetLeft) {
            setNearColor(m_block);
            uiVertex(m_nearOffsetLeft, m_nearOffsetUp, m_nearValue);
            uiVertex(m_nearOffsetLeft, CWorldObject::CellLength - m_nearOffsetDown, m_nearValue);
            setFarColor(m_block);
            uiVertex(0, CWorldObject::CellLength, m_farValue);
            uiVertex(0, 0, m_farValue);
        }

        if (m_nearOffsetRight) {
            setNearColor(m_block);
            uiVertex(CWorldObject::CellLength - m_nearOffsetRight, CWorldObject::CellLength - m_nearOffsetDown, m_nearValue);
            uiVertex(CWorldObject::CellLength - m_nearOffsetRight, m_nearOffsetUp, m_nearValue);
            setFarColor(m_block);
            uiVertex(CWorldObject::CellLength, 0, m_farValue);
            uiVertex(CWorldObject::CellSize, m_farValue);
        }

        if (m_nearOffsetUp) {
            setNearColor(m_block);
            uiVertex(CWorldObject::CellLength - m_nearOffsetRight, m_nearOffsetUp, m_nearValue);
            uiVertex(m_nearOffsetLeft, m_nearOffsetUp, m_nearValue);
            setFarColor(m_block);
            uiVertex(0, 0, m_farValue);
            uiVertex(CWorldObject::CellLength, 0, m_farValue);
        }

        if (m_nearOffsetDown) {
            setNearColor(m_block);
            uiVertex(m_nearOffsetLeft, CWorldObject::CellLength - m_nearOffsetDown, m_nearValue);
            uiVertex(CWorldObject::CellLength - m_nearOffsetRight, CWorldObject::CellLength - m_nearOffsetDown, m_nearValue);
            setFarColor(m_block);
            uiVertex(CWorldObject::CellSize, m_farValue);
            uiVertex(0, CWorldObject::CellLength, m_farValue);
        }
        glEnd ();
    }
    else {

        PointF2D pointsFar [TriVertexCount] = { PointF2D(0,0), CWorldObject::CellSize / 2, PointF2D(0,CWorldObject::CellLength) };
        PointF2D pointsNear[TriVertexCount] = { PointF2D(0,m_nearOffsetBase),
                                               CWorldObject::CellSize / 2 + PointF2D(-m_nearOffsetBase, 0),
                                               PointF2D(0,CWorldObject::CellLength - m_nearOffsetBase) };
        PointF2D sameCellMagicPointLeft  = CWorldObject::CellSize / 2 - PointF2D (m_sameCellOffsetValue, m_sameCellOffsetValue);
        PointF2D sameCellMagicPointRight = CWorldObject::CellSize / 2 - PointF2D (m_sameCellOffsetValue, -m_sameCellOffsetValue);

        if (!m_sameCellNeighbors[NeighborBase]) {
            pointsNear[TriVertexFirst] = PointF2D(m_nearOffsetBase, m_nearOffsetBase * 2);
            pointsNear[TriVertexLast ] = PointF2D(m_nearOffsetBase, CWorldObject::CellLength - m_nearOffsetBase * 2);
        }

        switch(m_block->direction()) {
        case DirectionLeft:
            uiTranslate(m_block->cellPosition());
            break;
        case DirectionRight:
            uiTranslate(m_block->cellPosition() + CWorldObject::CellSize);
            glRotatef(180, 0, 0, 1);
            break;
        case DirectionUp:
            uiTranslate(m_block->cellPosition() + PointF2D(CWorldObject::CellLength, 0));
            glRotatef(-90, 0, 0, 1);
            break;
        case DirectionDown:
            uiTranslate(m_block->cellPosition() + PointF2D(0, CWorldObject::CellLength));
            glRotatef(90, 0, 0, 1);
            break;
        }

        glBegin(GL_TRIANGLES);

        if (!m_sameCellNeighbors[NeighborLeft] && !m_sameCellNeighbors[NeighborRight]) {
            /* FIXME: Broken order (CW/CCW) */
            setNearColor(m_block);
            uiVertex(pointsNear[TriVertexFirst  ], m_nearValue);
            uiVertex(pointsNear[TriVertexCentral], m_nearValue);
            uiVertex(pointsNear[TriVertexLast   ], m_nearValue);

            uiVertex(pointsNear[TriVertexFirst  ], m_nearValue);
            uiVertex(pointsNear[TriVertexCentral], m_nearValue);
            setFarColor(m_block);
            uiVertex(pointsFar[TriVertexCentral ], m_farValue);

            uiVertex(pointsFar[TriVertexCentral ], m_farValue);
            setNearColor(m_block);
            uiVertex(pointsNear[TriVertexLast   ], m_nearValue);
            uiVertex(pointsNear[TriVertexCentral], m_nearValue);

            uiVertex(pointsNear[TriVertexFirst  ], m_nearValue);
            setFarColor(m_block);
            uiVertex(pointsFar[TriVertexFirst   ], m_farValue);
            uiVertex(pointsFar[TriVertexCentral ], m_farValue);

            uiVertex(pointsFar[TriVertexCentral ], m_farValue);
            uiVertex(pointsFar[TriVertexLast    ], m_farValue);
            setNearColor(m_block);
            uiVertex(pointsNear[TriVertexLast   ], m_nearValue);
        }
        else if (!m_sameCellNeighbors[NeighborLeft] && m_sameCellNeighbors[NeighborRight]) {
            setNearColor(m_block);
            uiVertex(pointsNear[TriVertexFirst ], m_nearValue);
            uiVertex(sameCellMagicPointRight    , m_nearValue);
            uiVertex(pointsFar[TriVertexLast   ], m_nearValue);

            uiVertex(sameCellMagicPointRight    , m_nearValue);
            uiVertex(pointsNear[TriVertexFirst ], m_nearValue);
            setFarColor(m_block);
            uiVertex(pointsFar[TriVertexCentral], m_farValue);

            uiVertex(pointsFar[TriVertexFirst  ], m_farValue);
            uiVertex(pointsFar[TriVertexCentral], m_farValue);
            setNearColor(m_block);
            uiVertex(pointsNear[TriVertexFirst ], m_nearValue);
        }
        else if (m_sameCellNeighbors[NeighborLeft] && !m_sameCellNeighbors[NeighborRight]) {
            setNearColor(m_block);
            uiVertex(pointsFar[TriVertexFirst  ], m_nearValue);
            uiVertex(sameCellMagicPointLeft     , m_nearValue);
            uiVertex(pointsNear[TriVertexLast  ], m_nearValue);

            uiVertex(pointsNear[TriVertexLast  ], m_nearValue);
            setFarColor(m_block);
            uiVertex(pointsFar[TriVertexCentral], m_farValue);
            uiVertex(pointsFar[TriVertexLast   ], m_farValue);

            uiVertex(pointsFar[TriVertexCentral], m_farValue);
            setNearColor(m_block);
            uiVertex(pointsNear[TriVertexLast  ], m_nearValue);
            uiVertex(sameCellMagicPointLeft     , m_nearValue);
        }
        else {

        }

        if (!m_sameCellNeighbors[NeighborBase]) {
            setFarColor(m_block);
            uiVertex(pointsFar[TriVertexLast   ], m_farValue);
            uiVertex(pointsFar[TriVertexFirst  ], m_farValue);
            setNearColor(m_block);
            uiVertex(pointsNear[TriVertexFirst ], m_nearValue);

            uiVertex(pointsNear[TriVertexFirst ], m_nearValue);
            uiVertex(pointsNear[TriVertexLast  ], m_nearValue);
            setFarColor(m_block);
            uiVertex(pointsFar[TriVertexLast   ], m_farValue);
        }

        glEnd ();
    }
}

void CBlocksPresentGL::renderSmallFreezeBlock()
{
    m_farValue = 0.2;
    uiTranslate(m_block->center());
    glBegin(GL_TRIANGLES);

    static const qreal centralLinesLength = 0.4;

    setNearColor(m_block);
    uiVertex(0, m_block->height() * centralLinesLength, m_farValue);
    setFarColor(m_block);
    uiVertex(-m_block->width() / 4, -m_block->height() / 4, m_nearValue);
    setNearColor(m_block);
    uiVertex(0, 0, m_nearValue);

    uiVertex(0, 0, m_nearValue);
    setFarColor(m_block);
    uiVertex(m_block->width() / 4, -m_block->height() /4, m_nearValue);
    setNearColor(m_block);
    uiVertex(0, m_block->height() * centralLinesLength, m_farValue);


    uiVertex(0, -m_block->height() * centralLinesLength, m_farValue);
    setFarColor(m_block);
    uiVertex(+m_block->width() / 4, +m_block->height() / 4, m_nearValue);
    setNearColor(m_block);
    uiVertex(0, 0, m_nearValue);

    uiVertex(0, 0, m_nearValue);
    setFarColor(m_block);
    uiVertex(-m_block->width() / 4, +m_block->height() / 4, m_nearValue);
    setNearColor(m_block);
    uiVertex(0, -m_block->height() * centralLinesLength, m_farValue);


    uiVertex (-m_block->width() * centralLinesLength, 0, m_farValue);
    setFarColor(m_block);
    uiVertex (+m_block->width() / 4, -m_block->height() / 4, m_nearValue);
    setNearColor(m_block);
    uiVertex (0, 0, m_nearValue);

    uiVertex (0, 0, m_nearValue);
    setFarColor(m_block);
    uiVertex (+m_block->width() / 4, +m_block->height() / 4, m_nearValue);
    setNearColor(m_block);
    uiVertex (-m_block->width() * centralLinesLength, 0, m_farValue);


    uiVertex (+m_block->width() * centralLinesLength, 0, m_farValue);
    setFarColor(m_block);
    uiVertex (-m_block->width() / 4, -m_block->height() / 4, m_nearValue);
    setNearColor(m_block);
    uiVertex (0, 0, m_nearValue);

    uiVertex (0, 0, m_nearValue);
    setFarColor(m_block);
    uiVertex (-m_block->width() / 4, +m_block->height() / 4, m_nearValue);
    setNearColor(m_block);
    uiVertex (+m_block->width() * centralLinesLength, 0, m_farValue);
    glEnd ();
}

PointF2D CBlocksPresentGL::position() const
{
    return m_block->position();
}

PointF2D CBlocksPresentGL::size() const
{
    return m_block->size();
}

void CBlocksPresentGL::renderFreezeBlock()
{
    if (m_block->subBlockType() > 1)
        renderSmallFreezeBlock();
    else
        renderBigFreezeBlock();
}

void CBlocksPresentGL::renderGravityBlock()
{
    switch(m_block->subBlockType()) {
    default:
    case CGravityModBlock::Extra:
    case CGravityModBlock::None:
    case CGravityModBlock::Left:
        uiTranslate(m_block->cellPosition());
        break;
    case CGravityModBlock::Right:
        uiTranslate(m_block->cellPosition() + CWorldObject::CellSize);
        glRotatef(180, 0, 0, 1);
        break;
    case CGravityModBlock::Up:
        uiTranslate(m_block->cellPosition() + PointF2D(CWorldObject::CellLength, 0));
        glRotatef(-90, 0, 0, 1);
        break;
    case CGravityModBlock::Down:
        uiTranslate(m_block->cellPosition() + PointF2D(0, CWorldObject::CellLength));
        glRotatef(90, 0, 0, 1);
        break;
    }

    qreal motion = CWorldObject::CellLength / 8;
    if (m_block->subBlockType() == CGravityModBlock::Extra) {
        motion *= 2;

        glBegin(GL_TRIANGLES);
        setFarColor(m_block);
        // 1
        uiVertex(motion, 0, m_farValue);
        uiVertex(CWorldObject::CellSize / 2, m_nearValue);
        uiVertex(motion, CWorldObject::CellLength / 2, m_farValue);

        // 2
        uiVertex(CWorldObject::CellLength, motion, m_farValue);
        uiVertex(CWorldObject::CellSize / 2, m_nearValue);
        uiVertex(CWorldObject::CellLength / 2, motion, m_farValue);

        // 3
        uiVertex(CWorldObject::CellLength - motion, CWorldObject::CellLength, m_farValue);
        uiVertex(CWorldObject::CellSize / 2, m_nearValue);
        uiVertex(CWorldObject::CellLength - motion, CWorldObject::CellLength / 2, m_farValue);

        // 4
        uiVertex(0, CWorldObject::CellLength - motion, m_farValue);
        uiVertex(CWorldObject::CellSize / 2, m_nearValue);
        uiVertex(CWorldObject::CellLength / 2, CWorldObject::CellLength - motion, m_farValue);
        glEnd();
    }
    else if (m_block->subBlockType() == CGravityModBlock::None) {
        glBegin(GL_QUADS);
        setNearColor(m_block);
        uiVertex(motion, motion, m_nearValue);
        uiVertex(m_block->width() - motion, motion, m_nearValue);
        uiVertex(m_block->width() - motion, m_block->height() - motion, m_nearValue);
        uiVertex(motion, m_block->height() - motion, m_nearValue);
        glEnd();

    }
    else {
        glBegin(GL_TRIANGLES);
        setFarColor(m_block);
        uiVertex(CWorldObject::CellLength - motion, CWorldObject::CellLength - motion, m_nearValue);
        uiVertex(CWorldObject::CellLength - motion, motion, m_nearValue);
        setNearColor(m_block);
        uiVertex(CBlocksAppearance::Points[CBlocksAppearance::HCenterVCenter], m_nearValue);
        glEnd();
    }
}

void CBlocksPresentGL::renderDefaultBlock()
{
    switch(m_block->direction()) {
    case DirectionNone:
    case DirectionLeft:
        uiTranslate(m_block->cellPosition());
        break;
    case DirectionRight:
        uiTranslate(m_block->cellPosition() + CWorldObject::CellSize);
        glRotatef(180, 0, 0, 1);
        break;
    case DirectionUp:
        uiTranslate(m_block->cellPosition() + PointF2D(CWorldObject::CellLength, 0));
        glRotatef(-90, 0, 0, 1);
        break;
    case DirectionDown:
        uiTranslate(m_block->cellPosition() + PointF2D(0, CWorldObject::CellLength));
        glRotatef(90, 0, 0, 1);
        break;
    }

    if (m_block->direction() == DirectionNone) {
        glBegin (GL_QUADS);
        setNearColor(m_block);
        uiVertex (0, 0, m_nearValue);
        uiVertex (m_block->width(), 0, m_nearValue);
        uiVertex (m_block->width(), m_block->height(), m_nearValue);
        uiVertex (0, m_block->height(), m_nearValue);

        uiVertex (0, 0, m_nearValue);
        uiVertex (m_block->width(), 0, m_nearValue);
        setFarColor(m_block);
        uiVertex (m_block->width(), 0, m_farValue);
        uiVertex (0, 0, m_farValue);

        uiVertex (m_block->width(), m_block->height(), m_farValue);
        uiVertex (0, m_block->height(), m_farValue);
        setNearColor(m_block);
        uiVertex (0, m_block->height(), m_nearValue);
        uiVertex (m_block->width(), m_block->height(), m_nearValue);

        uiVertex (m_block->width(), 0, m_nearValue);
        uiVertex (m_block->width(), m_block->height(), m_nearValue);
        setFarColor(m_block);
        uiVertex (m_block->width(), m_block->height(), m_farValue);
        uiVertex (m_block->width(), 0, m_farValue);

        uiVertex (0, 0, m_farValue);
        uiVertex (0, m_block->height(), m_farValue);
        setNearColor(m_block);
        uiVertex (0, m_block->height(), m_nearValue);
        uiVertex (0, 0, m_nearValue);
        glEnd ();
    }
    else {
        glBegin(GL_TRIANGLES);
        setNearColor(m_block);
        uiVertex(0, 0, m_nearValue);
        uiVertex(CBlocksAppearance::Points[CBlocksAppearance::HCenterVCenter], m_nearValue);
        uiVertex(0, CWorldObject::CellLength, m_nearValue);

        glEnd();
    }
}

void CBlocksPresentGL::renderTeleBlock()
{
    uiTranslate(m_block->cellPosition());

    qreal outer0, outer1;

    outer0 = 0;
    outer1 = 0.12;

    setNearColor(m_block);

    for (int i = 0; i < 4; ++i) {
        glBegin(GL_TRIANGLES);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer0), m_nearValue);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer1), m_nearValue);
        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer1), m_nearValue);

        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer1), m_nearValue);
        uiVertex(m_block->width() * outer0, m_block->height() * outer1, m_nearValue);
        uiVertex(m_block->width() * outer1, m_block->height() * outer1, m_nearValue);

        uiVertex(m_block->width() * outer1, m_block->height() * outer1, m_nearValue);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer1), m_nearValue);
        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer1), m_nearValue);
        glEnd();
        uiTranslate(m_block->width(), 0);
        glRotatef(-90, 0, 0, 1);
    }

    outer0 = outer1;
    outer1 += outer1;

    for (int i = 0; i < 4; ++i) {
        glBegin(GL_TRIANGLES);
        setNearColor(m_block);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer0), m_nearValue);
        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer0), m_nearValue);
        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer1), m_nearValue);

        setNearColor(m_block);
        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer1), m_nearValue);
        uiVertex(m_block->width() * outer0, m_block->height() * outer1, m_nearValue);
        setFarColor(m_block);
        uiVertex(m_block->width() * outer1, m_block->height() * outer1, m_farValue);

        uiVertex(m_block->width() * outer1, m_block->height() * outer1, m_farValue);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer1), m_farValue);
        setNearColor(m_block);
        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer1), m_nearValue);

        /* Center */
        setFarColor(m_block);
        uiVertex(m_block->width() / 2, m_block->height() / 2, m_farValue);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer1), m_farValue);
        uiVertex(m_block->width() * outer1, m_block->height() * (outer1), m_farValue);

        /* Corner */
        setNearColor(m_block);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer0), m_farValue);
        setFarColor(m_block);
        uiVertex(m_block->width() * outer1, m_block->height() * (1 - outer1), m_farValue);
        setNearColor(m_block);
        uiVertex(m_block->width() * outer0, m_block->height() * (1 - outer1), m_nearValue);

        glEnd();
        uiTranslate(m_block->width(), 0);
        glRotatef(-90, 0, 0, 1);
    }
}

inline void CBlocksPresentGL::setNearColor(CAbstractGameBlock *pBlock)
{
    uiColor(CBlocksAppearance::blocksNearColor(pBlock));
}

inline void CBlocksPresentGL::setFarColor(CAbstractGameBlock *pBlock)
{
    uiColor(CBlocksAppearance::blocksFarColor(pBlock));
}
