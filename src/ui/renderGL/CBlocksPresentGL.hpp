#ifndef CBLOCKSPRESENTGL_HPP
#define CBLOCKSPRESENTGL_HPP

#include <QList>
#include "CGLBase.hpp"
#include "../IUIBlocksPresent.hpp"

class CGameUI_GL;

class CBlocksPresentGL : public IUIBlocksPresent, public CGLBase
{
public:
    enum NeighborsPosition {
        NeighborLeft,
        NeighborRight,
        NeighborUp,
        NeighborDown,
        NeighborsMaxCount,
        NeighborBase = NeighborUp
    };

    enum TriVertex {
        TriVertexFirst,
        TriVertexCentral,
        TriVertexLast,
        TriVertexCount
    };

    explicit CBlocksPresentGL(CAbstractGameBlock *pGameBlock, CGameUI_GL *gameUI);
    void render();
    void updateNeighbors();

    inline CAbstractGameBlock *gameBlock() const { return m_block; }

    void renderBigFreezeBlock();
    void renderSmallFreezeBlock();

    inline bool isVisible() const { return m_isVisible; }
    void setVisible(bool visible) { m_isVisible = visible; }

    void setZValue(float z) { m_z = z; }
    float zValue() const { return m_z; }

    void setOffset(const PointF2D &newOffset) { m_offset = newOffset; }
    inline PointF2D offset() const { return m_offset; }

    PointF2D position() const;
    PointF2D size() const;

private:
    void setNearColor(CAbstractGameBlock *pBlock);
    void setFarColor(CAbstractGameBlock *pBlock);

    void renderAccelerationBlock();
    void renderFreezeBlock();
    void renderGravityBlock();

//    void renderSpaceBlock(); // Solid
//    void renderSpawnBlock();
//    void renderStartBlock();
    void renderDefaultBlock();
    void renderTeleBlock();

    CGameUI_GL *m_gameUI;

    CAbstractGameBlock *m_block;
    CBlocksPresentGL *m_neighbors[NeighborsMaxCount];
    CBlocksPresentGL *m_sameCellNeighbors[3];

    qreal m_nearOffsetLeft;
    qreal m_nearOffsetRight;
    qreal m_nearOffsetUp;
    qreal m_nearOffsetDown;
    qreal m_nearOffsetBase;
    qreal m_nearValue;
    qreal m_farValue;
    qreal m_sameCellOffsetValue;

    bool m_isVisible;
    qreal m_z;

    PointF2D m_offset;
};

#endif // CBLOCKSPRESENTGL_HPP
