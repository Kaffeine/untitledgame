#ifndef IUIPROCESSOR_HPP
#define IUIPROCESSOR_HPP

#include <QtCore/qnamespace.h>

#include "logicbase/Points2D.hpp"

class IGameUI;
class CCamera;

class QKeyEvent;

class IUIProcessor {
public:
    virtual void setUI(IGameUI *pUI) = 0;
    virtual void gameKeyPressEvent(QKeyEvent *event) = 0;
    virtual void gameKeyReleaseEvent(QKeyEvent *event) = 0;
    virtual void gameMousePressEvent(const PointI2D &uiCoord, Qt::MouseButton button) = 0;
    virtual void gameMouseMoveEvent(const PointI2D &uiCoord) = 0;
    virtual void gameMouseReleaseEvent(const PointI2D &uiCoord, Qt::MouseButton button) = 0;
    virtual void gameWheelEvent(int delta) = 0;
    virtual CCamera *camera() const = 0;
};

#endif // IUIPROCESSOR_HPP
