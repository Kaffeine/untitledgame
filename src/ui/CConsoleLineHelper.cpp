#include "CConsoleLineHelper.hpp"
#include "logicbase/CConsole.hpp"
#include <QKeyEvent>

#include <QLineEdit>
#include <QPlainTextEdit>

#include <QDebug>

CConsoleLineHelper::CConsoleLineHelper(QObject *parent) :
    QObject(parent)
{
}

void CConsoleLineHelper::setObjects(QLineEdit *pInput, QPlainTextEdit *pOutput, CConsole *pConsole)
{
    m_input = pInput;
    m_input->installEventFilter(this);
    m_output = pOutput;
    m_console = pConsole;

    connect(m_console, SIGNAL(output(QString)), SLOT(whenConsoleOutput(QString)));
}

bool CConsoleLineHelper::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        switch(keyEvent->key()) {
        case Qt::Key_Tab:
            getAutocompletion();
            break;
        case Qt::Key_Up:
            ++m_currentHistoryCommandNum;
            m_input->setText(m_console->historyAtNumFromEnd(m_currentHistoryCommandNum));
            break;
        case Qt::Key_Down:
            --m_currentHistoryCommandNum;
            m_input->setText(m_console->historyAtNumFromEnd(m_currentHistoryCommandNum));
            break;
        case Qt::Key_Return:
        case Qt::Key_Enter:
            m_currentHistoryCommandNum = -1;
            if (!m_input->text().isEmpty()) {
                m_console->executeCommand(m_input->text());
                m_input->clear();
            }
        default:
            return QObject::eventFilter(object, event);

        }
        return true;
    }

    return QObject::eventFilter(object, event);
}

void CConsoleLineHelper::whenConsoleOutput(const QString &text)
{
    m_output->appendPlainText(text);
}

void CConsoleLineHelper::getAutocompletion()
{
    QStringList variants = m_console->autocomplete(m_input->text());

    if (variants.isEmpty())
        m_output->appendPlainText("No variants for autocompletion.");
    else if (variants.count() == 1)
        m_input->setText(m_input->text().left(m_input->text().lastIndexOf(" ") + 1) + variants.at(0));
    else {
        m_output->appendPlainText("Possible completes:");
        for (int i = 0; i < variants.count(); ++i)
            m_output->appendPlainText("    " + variants.at(i));
    }
}
