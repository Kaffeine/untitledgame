#ifndef CBLOCKSAPPEARANCE_HPP
#define CBLOCKSAPPEARANCE_HPP

#include "logicbase/Directions.hpp"
#include "logicbase/Points2D.hpp"

#include <QColor>

class CAbstractGameBlock;

class CBlocksAppearance
{
public:
    enum Vertexes {
        LeftTop,
        HCenterTop,
        RightTop,
        LeftVCenter,
        HCenterVCenter,
        RightVCenter,
        LeftBottom,
        HCenterBottom,
        RightBottom,
        VertexesCount
    };

    const static PointF2D Points[VertexesCount];

    CBlocksAppearance();

    static QColor blocksNearColor(CAbstractGameBlock *pBlock);
    static QColor blocksFarColor(CAbstractGameBlock *pBlock);

};

#endif // CBLOCKSAPPEARANCE_HPP
