#ifndef CCAMERA_HPP
#define CCAMERA_HPP

#include <QObject>

#include "logicbase/Points2D.hpp"
#include "logicbase/Rects2D.hpp"

class CAvatar;
class CWorld;

class CCamera : public QObject
{
    Q_OBJECT
    Q_ENUMS(CameraModes)
public:
    Q_PROPERTY(bool stopAtBounds READ stopAtBounds WRITE setStopAtBounds NOTIFY stopAtBoundsChanged)
    Q_PROPERTY(CameraModes cameraMode READ cameraMode WRITE setCameraMode NOTIFY cameraModeChanged)
    Q_PROPERTY(float scale READ scale WRITE setScale NOTIFY scaleChanged)

    enum CameraModes {
        AvatarInCenter,
        AvatarAndVector,
        AvatarWithMargins,
        FreeCamera,
        CameraModesCount
    };

    CCamera(QObject *parent = 0);

    inline CAvatar *avatar() const { return m_avatar; }

    inline PointF2D centerPoint() const { return m_centerPoint; }
    void setCameraCenterPoint(PointF2D center);

    CameraModes cameraMode() const;
    void setCameraMode(CameraModes newMode);

    bool stopAtBounds() const;
    void setStopAtBounds(bool newStop);

    inline float scale() const { return m_scale; }
    void setScale(float newScale);

    PointF2D viewportSize() const { return m_viewportSize; }
    void setViewportSize(float width, float height);
    void setMargins(float marginX, float marginY);
    void setVector(const PointF2D &vector);
    void setMarginsInUnite(int marginX, int marginY);
    void setMotionSpeed(float maxSpeed);
    RectF2D viewGeometry();

    CWorld *world() const;
    void setWorld(CWorld *newWorld);

public slots:
    void setAvatar(CAvatar *newAvatar);

signals:
    void scaleChanged(float newScale);
    void cameraModeChanged(CameraModes newCameraMode);
    void stopAtBoundsChanged(bool newStopAtBound);

private:
    CameraModes m_cameraMode;
    CAvatar *m_avatar;
    CWorld *m_world;
    bool m_stopAtBounds;
    float m_scale;
    PointF2D m_viewportSize;
    PointF2D m_margins;

    PointF2D m_showOffset;

    PointF2D m_centerPoint;
    PointF2D m_vector;

};

#endif // CCAMERA_HPP
