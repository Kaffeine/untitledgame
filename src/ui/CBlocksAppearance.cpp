#include "CBlocksAppearance.hpp"

#include "logicbase/mapBlocks/allBlocks.hpp"

const PointF2D CBlocksAppearance::Points[] = {
    PointF2D(                               0.0, 0.0),
    PointF2D(CAbstractGameBlock::CellLength / 2, 0.0),
    PointF2D(CAbstractGameBlock::CellLength    , 0.0),

    PointF2D(                               0.0, CAbstractGameBlock::CellLength / 2),
    PointF2D(CAbstractGameBlock::CellLength / 2, CAbstractGameBlock::CellLength / 2),
    PointF2D(CAbstractGameBlock::CellLength    , CAbstractGameBlock::CellLength / 2),

    PointF2D(                               0.0, CAbstractGameBlock::CellLength),
    PointF2D(CAbstractGameBlock::CellLength / 2, CAbstractGameBlock::CellLength),
    PointF2D(CAbstractGameBlock::CellLength    , CAbstractGameBlock::CellLength)
};

CBlocksAppearance::CBlocksAppearance()
{
}

QColor CBlocksAppearance::blocksNearColor(CAbstractGameBlock *pBlock)
{
    switch (pBlock->blockType()) {
    case BlockSolid:
        switch (pBlock->subBlockType()) {
        default:
        case CSolidBlock::Hookable:
            return QColor(0xcc, 0x99, 0x66);
            break;
        case CSolidBlock::Unhookable:
            return QColor(0xcc, 0xcc, 0xcc);
            break;
        case CSolidBlock::Hookthrow:
            return QColor(0xee, 0xee, 0xcc);
            break;
        case CSolidBlock::Ice:
            return QColor(0xff, 0xff, 0xff);
            break;
        }
        break;
    case BlockSpawn:
        return QColor(0xcc, 0x99, 0x66);
        break;
//    case LayerStart:
//        break;
    case BlockFreeze:
        if (pBlock->subBlockType() % 2)
            return QColor(0xff, 0xd0, 0x0, 0xa0);
        else
            return QColor(0x55, 0x55, 0x99, 0xa0);
        break;

    case BlockTele:
        if (pBlock->subBlockType() % 2)
            return QColor(0x50, 0x50, 0xff);
        else
            return QColor(0x22, 0x22, 0x99);
        break;
    case BlockGravity:
        return QColor(0x88, 0x88, 0xaa);
        break;
    case BlockAcceleration:
        return QColor(0x22, 0xee, 0x22);
        break;
    default:
        break;
    }

    return QColor(0.6, 0.6, 0.8);
}

QColor CBlocksAppearance::blocksFarColor(CAbstractGameBlock *pBlock)
{
    switch (pBlock->blockType()) {
    case BlockSolid:
        switch (pBlock->subBlockType()) {
        default:
        case CSolidBlock::Hookable:
            return QColor(0x99, 0x77, 0x44);
            break;
        case CSolidBlock::Unhookable:
            return QColor(0x99, 0x99, 0x99);
            break;
        case CSolidBlock::Hookthrow:
            return QColor(0xcc, 0xdd, 0xaa);
            break;
        case CSolidBlock::Ice:
            return QColor(0xff, 0xff, 0xcc);
            break;
        }
        break;
    case BlockSpawn:
        return QColor(0x99, 0x77, 0x44);
        break;
//    case LayerStart:
//        break;
    case BlockFreeze:
        if (pBlock->subBlockType() % 2)
            return QColor(0xaa, 0x66, 0x0, 0xe0);
        else
            return QColor(0x10, 0x10, 0x80, 0xe0);
        break;
    case BlockTele:
        if (pBlock->subBlockType() % 2)
            return QColor(0x77, 0x88, 0xff, 0xa0);
        else
            return QColor(0x33, 0x33, 0xaa, 0x80);
        break;
    case BlockGravity:
        return QColor(0x88, 0x88, 0xaa);
        break;
    case BlockAcceleration:
        return QColor(0x11, 0xaa, 0x11);
        break;
    default:
        break;
    }

    return QColor(0.6, 0.6, 0.8);
}
