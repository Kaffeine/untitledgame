#ifndef IUIBLOCKSPRESENT_HPP
#define IUIBLOCKSPRESENT_HPP

#include "IUIPrimitive.hpp"

class CAbstractGameBlock;

class IUIBlocksPresent : public IUIPrimitive {
public:
    virtual CAbstractGameBlock *gameBlock() const = 0;
};

#endif // IUIBLOCKSPRESENT_HPP
