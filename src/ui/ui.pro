
QT      += core gui

TEMPLATE = lib
TARGET = ui
CONFIG += dll

include(../common.pri)

LIBS += -llogicbase

INCLUDEPATH += $$PWD

SOURCES += \
    CBlocksAppearance.cpp \
    CCamera.cpp \
    CConsoleLineHelper.cpp

HEADERS += \
    IUIProcessor.hpp \
    IGameUI.hpp \
    CBlocksAppearance.hpp \
    CCamera.hpp \
    CConsoleLineHelper.hpp \
    IUIPrimitive.hpp \
    IUIBlocksPresent.hpp \
    IUIRectangle.hpp

contains(options, opengl) {
    message("UI with OpenGL render.")
    include(renderGL/renderGL.pri)
} else {
    message("UI with GraphicsView render.")
    include(renderGV/renderGV.pri)
}
