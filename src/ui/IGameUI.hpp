#ifndef IGAMEUI_HPP
#define IGAMEUI_HPP

#include <QtCore/qnamespace.h>

#include "logicbase/Points2D.hpp"

class CAvatar;
class CAbstractGameBlock;

class QObject;
class QWidget;

class IUIProcessor;
class IUIRectangle;
class IUIBlocksPresent;

class IGameUI {
public:
    virtual void setProcessor(IUIProcessor *pProcessor) = 0;
    virtual IUIBlocksPresent *addGameBlock(CAbstractGameBlock *pGameBlock) = 0;
    virtual void updateGameBlock(CAbstractGameBlock *pGameBlock) = 0;
    virtual void removeGameBlock(CAbstractGameBlock *pGameBlock) = 0;
    virtual void setGameBlockVisible(CAbstractGameBlock *pGameBlock, bool visible) = 0;
    virtual void addAvatar(CAvatar *pAvatar) = 0;
    virtual void removeAvatar(CAvatar *pAvatar) = 0;
    virtual IUIRectangle *addRectangle() = 0;

    virtual void updateUI() = 0;
    virtual void clean() = 0;
    virtual QWidget *widget() = 0;
    virtual QObject *object() = 0;

    virtual Qt::MouseButtons gameMouseButtons() const = 0;
    virtual Qt::KeyboardModifiers gameKeyboardModifiers() const = 0;

    virtual PointF2D uiCoordToWorldCoord(const PointI2D &uiCoordinates) const = 0;

    static IGameUI *createUI();
};

#endif // IGAMEUI_HPP
