#ifndef IUIPRIMITIVE_HPP
#define IUIPRIMITIVE_HPP

#include "logicbase/Points2D.hpp"

class IUIPrimitive {
public:
    virtual void setOffset(const PointF2D &newOffset) = 0;
    virtual PointF2D offset() const = 0;
    virtual bool isVisible() const = 0;
    virtual void setVisible(bool newVisible) = 0;

    virtual float zValue() const = 0;
    virtual void setZValue(float z) = 0;
};

#endif // IUIPRIMITIVE_HPP
