#include "CCamera.hpp"
#include "logicbase/CAvatar.hpp"
#include "logicbase/CWorld.hpp"
#include "logicbase/Functions.hpp"

/* Pure logical class, determinant showing geometry */
CCamera::CCamera(QObject *parent) :
    QObject(parent),
    m_avatar(0),
    m_world(0),
    m_scale(1)
{
    setCameraMode(AvatarWithMargins);
    setStopAtBounds(true);
    setMarginsInUnite(12, 10);
}

void CCamera::setAvatar(CAvatar *newAvatar)
{
    m_avatar = newAvatar;
}

CCamera::CameraModes CCamera::cameraMode() const
{
    return m_cameraMode;
}

void CCamera::setCameraMode(CCamera::CameraModes newMode)
{
    if (m_cameraMode == newMode)
        return;

    m_cameraMode = newMode;
    emit cameraModeChanged(m_cameraMode);
}

void CCamera::setViewportSize(float width, float height)
{
    m_viewportSize = PointF2D(width, height);
}

void CCamera::setMargins(float marginX, float marginY)
{
    m_margins = PointF2D(marginX, marginY);
}

void CCamera::setVector(const PointF2D &vector)
{
    m_vector = vector;
}

void CCamera::setMarginsInUnite(int marginX, int marginY)
{
    setMargins(CWorldObject::CellLength * marginX, CWorldObject::CellLength * marginY);
}

void CCamera::setMotionSpeed(float maxSpeed)
{
    Q_UNUSED(maxSpeed);
    /* TODO: setMotionSpeed. */
}

RectF2D CCamera::viewGeometry()
{
    /* showX, showY and m_centerPoint is in world scale */
    /* m_vector, maxVectorLength and cameraVectorReduce is in screen scale */

    const float maxVectorLength = m_viewportSize.y() * 0.25;
    const float cameraVectorReduce = m_viewportSize.y() * 0.12;
    const float cameraVectorScale = 0.25;

    if (!m_world)
        return RectF2D(PointF2D(), m_viewportSize);

    PointF2D fixedVector;
    float vectorLength;

    if ((m_cameraMode == FreeCamera) || !m_avatar)
        m_showOffset = m_centerPoint - m_viewportSize * m_scale / 2;
    else if (m_avatar) {
        switch (m_cameraMode) {
        default:
        case AvatarInCenter:
            m_centerPoint = m_avatar->center();
            m_showOffset = m_centerPoint - m_viewportSize * m_scale / 2;
            break;

        case AvatarAndVector:
            m_centerPoint = m_avatar->center();

            vectorLength = m_vector.length() - cameraVectorReduce;
            if (vectorLength < 0)
                fixedVector = m_centerPoint;
            else {
                if (vectorLength > maxVectorLength)
                    vectorLength = maxVectorLength;

                vectorLength *= cameraVectorScale;
                fixedVector = m_vector.normalized() * vectorLength + m_centerPoint;
            }

            m_showOffset = fixedVector - m_viewportSize * m_scale / 2;
            break;

        case AvatarWithMargins:
            m_centerPoint = m_avatar->center();
            RectF2D rect = RectF2D(m_centerPoint - (m_viewportSize - m_margins) * m_scale, (m_viewportSize - m_margins * 2) * m_scale);
            rect.clamp(&m_showOffset);
            break;
        }
    }

    if (m_stopAtBounds) {
//        m_showOffset = m_world->rect().clamp(m_showOffset + m_viewportSize * m_scale);
//        m_world->rect().clamp(&m_showOffset);
    }

    return RectF2D(m_showOffset, m_viewportSize * m_scale);
}

void CCamera::setScale(float newScale)
{
    if (newScale < 0.1)
        newScale = 0.1;

    if (newScale > 10)
        newScale = 10;

    if (newScale == m_scale)
        return;

    m_scale = newScale;
    emit scaleChanged(m_scale);
}

CWorld *CCamera::world() const
{
    return m_world;
}

void CCamera::setWorld(CWorld *newWorld)
{
    m_world = newWorld;
}

bool CCamera::stopAtBounds() const
{
    return m_stopAtBounds;
}

void CCamera::setCameraCenterPoint(PointF2D center)
{
    if (center.x() < 0)
        center.setX(0);

    if (center.y() < 0)
        center.setY(0);

    m_centerPoint = center;
}

void CCamera::setStopAtBounds(bool newStop)
{
    if (m_stopAtBounds == newStop)
        return;

    m_stopAtBounds = newStop;
    emit stopAtBoundsChanged(m_stopAtBounds);
}
