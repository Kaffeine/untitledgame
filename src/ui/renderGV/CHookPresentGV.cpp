#include "CHookPresentGV.hpp"
#include <QPainter>

CHookPresentGV::CHookPresentGV()
{
    m_color = QColor(0x20, 0x10, 0x10);

    m_pen.setStyle(Qt::SolidLine);
    m_pen.setWidth(10);
}

void CHookPresentGV::redrawAt(const QPointF &begin, const QPointF &end)
{
    if (begin == end)
        return setVisible(false);

    m_begin = begin;
    m_end   = end;

    m_posMin.setX(qMin(begin.x(), end.x()));
    m_posMin.setY(qMin(begin.y(), end.y()));

    m_posMax.setX(qMax(begin.x(), end.x()));
    m_posMax.setY(qMax(begin.y(), end.y()));

    setPos(m_posMin);
    setVisible(true);
    update();
}

QRectF CHookPresentGV::boundingRect() const
{
    return QRectF(QPointF(0,0), m_posMax - m_posMin);
}

void CHookPresentGV::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    painter->setPen(m_pen);
    painter->setBrush(m_color);
    painter->drawLine(m_begin - m_posMin, m_end - m_posMin);
}
