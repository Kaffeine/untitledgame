#ifndef CGAMEUI_GV_HPP
#define CGAMEUI_GV_HPP

#include "IGameUI.hpp"

#include <QRectF>
#include "logicbase/Rects2D.hpp"

#include <QGraphicsView>

class QGraphicsScene;
class CAvatarPresentGV;
class CBlocksPresentGV;
class CUIRectangleGV;

class CCamera;

class CGameUI_GV : public QGraphicsView, public IGameUI
{
    Q_OBJECT
public:
    explicit CGameUI_GV(QWidget *parent = 0);

    void setProcessor(IUIProcessor *pProcessor);

    Qt::MouseButtons gameMouseButtons() const { return m_mouseButtons; }
    Qt::KeyboardModifiers gameKeyboardModifiers() const { return m_keyboardModifiers; }

public slots:
    IUIBlocksPresent *addGameBlock(CAbstractGameBlock *pGameBlock);
    void updateGameBlock(CAbstractGameBlock *pGameBlock);
    void removeGameBlock(CAbstractGameBlock *pGameBlock);
    void setGameBlockVisible(CAbstractGameBlock *pGameBlock, bool visible);

    void addAvatar(CAvatar *pAvatar);
    void removeAvatar(CAvatar *pAvatar);

    IUIRectangle *addRectangle();

    void updateUI();
    void clean();
    QWidget *widget() { return this; }
    QObject *object() { return this; }

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

    void showEvent(QShowEvent *event);

    PointF2D uiCoordToWorldCoord(const PointI2D &uiCoordinates) const;

private:
    Qt::MouseButtons m_mouseButtons;
    Qt::KeyboardModifiers m_keyboardModifiers;
    IUIProcessor *m_processor;

public:
    void restoreWidgetMode();

    static QPointF pointToQPointF(const PointF2D &point);
    static QRectF rectToQRectF(const RectF2D &rect);

protected:
    void resizeEvent(QResizeEvent *event);

    inline CAvatarPresentGV *avatarPresentByAvatar(CAvatar *pAvatar) const;
    inline CBlocksPresentGV *blockPresentByGameBlock(CAbstractGameBlock *pGameBlock) const;
    CBlocksPresentGV *blockPresentByGameBlock(CAbstractGameBlock *pGameBlock, const QList<CBlocksPresentGV *> &list) const;
    void removeGameBlock(CBlocksPresentGV *pBlockPresent);

    QGraphicsScene *m_scene;

    QList<CAvatarPresentGV *> m_avatarsPresents;
    QList<CBlocksPresentGV *> m_blocksPresents;
    QList<CBlocksPresentGV *> m_blocksToUpdate;
    QList<CUIRectangleGV *> m_rectangles;

};

#endif // CGAMEUI_GV_HPP
