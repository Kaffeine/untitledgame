#ifndef CUIRECTANGLEGV_HPP
#define CUIRECTANGLEGV_HPP

#include "IUIRectangle.hpp"

class QGraphicsRectItem;

class CUIRectangleGV : public IUIRectangle
{
public:
    explicit CUIRectangleGV();
    ~CUIRectangleGV();

    QGraphicsRectItem *itemToRender() const { return m_itemToRender; }

    void setRect(float x, float y, float width, float height);

    bool isVisible() const;
    void setVisible(bool newVisible);

    void setZValue(float z);
    float zValue() const;

    void setOffset(const PointF2D &newOffset);
    PointF2D offset() const;

private:
    QGraphicsRectItem *m_itemToRender;

};

#endif // CUIRECTANGLEGV_HPP
