#ifndef CHOOKPRESENTGV_HPP
#define CHOOKPRESENTGV_HPP

#include <QGraphicsItem>
#include <QPen>

class CHookPresentGV : public QGraphicsItem
{
public:
    CHookPresentGV();

    void redrawAt(const QPointF &begin, const QPointF &end);

    QRectF boundingRect() const;
//    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    QPointF m_begin;
    QPointF m_end;
    QPointF m_posMin;
    QPointF m_posMax;
    QColor m_color;
    QPen m_pen;
};

#endif // CHOOKPRESENTGV_HPP
