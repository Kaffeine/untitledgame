#include "CPeace.hpp"
#include <QPainter>

CPeace::CPeace()
{
    fillColor = QColor(0,0,0);
}

void CPeace::setBoundingRect(QRectF newBoundingRect)
{
    m_boundingRect = newBoundingRect;
}

void CPeace::setCustomColor(QColor newColor)
{
    fillColor = newColor;
}

QPainterPath CPeace::shape() const
{
    QPainterPath path;
    path.addPolygon(m_peacePoly);
    path.addPath(m_overdraw);
    return path;
}

void CPeace::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);
    QPen pen;

    pen.setStyle(Qt::SolidLine);
    pen.setWidth(3);

    painter->setPen(pen);
    painter->setBrush(fillColor);
    painter->drawPolygon(m_peacePoly);
    painter->drawPath(m_overdraw);
}
