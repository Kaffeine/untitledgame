#ifndef CBLOCKSPRESENTGV_HPP
#define CBLOCKSPRESENTGV_HPP

#include "IUIBlocksPresent.hpp"
#include "logicbase/Directions.hpp"

#include <QPolygonF>

class QGraphicsItem;
class CPeace;

class CBlocksPresentGV : public IUIBlocksPresent
{
public:
    explicit CBlocksPresentGV(CAbstractGameBlock *pGameBlock);
    virtual ~CBlocksPresentGV();

    void setup();
    QGraphicsItem *itemToRender() const { return m_itemToRender; }

    void updatePos();
    CAbstractGameBlock *gameBlock() const { return m_block; }

    inline bool isVisible() const;
    void setVisible(bool visible);

    void setZValue(float z);
    float zValue() const;

    void setOffset(const PointF2D &newOffset);
    PointF2D offset() const { return m_offset; }

private:
    static QPolygonF polygonByBlocksDirection(Directions direction);

    void setupAccelerationBlock();
    void setupFreezeBlock();
    void setupGravityBlock();
    void setupSpaceBlock(); // Solid
    void setupSpawnBlock();
    void setupStartBlock();
    void setupTeleBlock();
    void setupDefaultBlock();

    CAbstractGameBlock *m_block;
    bool m_isVisible;
    PointF2D m_offset;
    QGraphicsItem *m_itemToRender;

    CPeace *m_peace;

};

#endif // CBLOCKSPRESENTGV_HPP
