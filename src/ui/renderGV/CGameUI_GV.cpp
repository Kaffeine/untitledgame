#include "CGameUI_GV.hpp"
#include "CCamera.hpp"
#include "IUIProcessor.hpp"

#include "CPeace.hpp"
#include "CAvatarPresentGV.hpp"
#include "CBlocksPresentGV.hpp"
#include "CUIRectangleGV.hpp"

#include <QGraphicsScene>
#include <QMouseEvent>

#ifdef USE_GL_IN_GV
#include <QGLWidget>
#endif

#include <QDebug>

IGameUI *IGameUI::createUI()
{
    return new CGameUI_GV();
}

CGameUI_GV::CGameUI_GV(QWidget *parent) :
    QGraphicsView(parent),
    m_processor(0),
    m_scene(new QGraphicsScene(this))
{
#ifdef USE_GL_IN_GV
    setViewport(new QGLWidget());
#endif

    m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    setFrameStyle(0);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing|QPainter::SmoothPixmapTransform|QPainter::HighQualityAntialiasing);
//    setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
    setScene(m_scene);
}

//void CGameUI_GV::restoreWidgetMode()
//{
//    setWindowFlags(Qt::FramelessWindowHint);
//    show();
//}

void CGameUI_GV::keyPressEvent(QKeyEvent *event)
{
    m_processor->gameKeyPressEvent(event);
}

void CGameUI_GV::keyReleaseEvent(QKeyEvent *event)
{
    m_processor->gameKeyReleaseEvent(event);
}

void CGameUI_GV::mousePressEvent(QMouseEvent *event)
{
    m_mouseButtons = event->buttons();
    m_keyboardModifiers = event->modifiers();
    m_processor->gameMousePressEvent(PointI2D(event->pos().x(), event->pos().y()), event->button());
}

void CGameUI_GV::mouseReleaseEvent(QMouseEvent *event)
{
    m_mouseButtons = event->buttons();
    m_keyboardModifiers = event->modifiers();
    m_processor->gameMouseReleaseEvent(PointI2D(event->pos().x(), event->pos().y()), event->button());
}

void CGameUI_GV::mouseMoveEvent(QMouseEvent *event)
{
    m_processor->gameMouseMoveEvent(PointI2D(event->pos().x(), event->pos().y()));
}

void CGameUI_GV::wheelEvent(QWheelEvent *event)
{
    m_processor->gameWheelEvent(event->delta());
}

void CGameUI_GV::showEvent(QShowEvent *event)
{
    Q_UNUSED(event)
    m_processor->camera()->setViewportSize(contentsRect().width(), contentsRect().height());
    updateUI();
}

PointF2D CGameUI_GV::uiCoordToWorldCoord(const PointI2D &uiCoordinates) const
{
    RectF2D viewGeometry = m_processor->camera()->viewGeometry();

    PointF2D totalRelevantPos;

    totalRelevantPos.setX(uiCoordinates.x() * viewGeometry.width () / width ());
    totalRelevantPos.setY(uiCoordinates.y() * viewGeometry.height() / height());

    return totalRelevantPos + viewGeometry.position();
}

void CGameUI_GV::setProcessor(IUIProcessor *pProcessor)
{
    m_processor = pProcessor;
    m_processor->setUI(this);
}

void CGameUI_GV::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    m_processor->camera()->setViewportSize(contentsRect().width(), contentsRect().height());
}

void CGameUI_GV::clean()
{
    m_blocksToUpdate.clear();
    /* Destructor of CItemsPresent perform deletion of relative QGraphicsItems. */
    for (int i = 0; i < m_blocksPresents.count(); ++i)
        delete m_blocksPresents[i];

    m_blocksPresents.clear();

    for (int i = 0; i < m_avatarsPresents.count(); ++i)
        delete m_avatarsPresents[i];

    m_avatarsPresents.clear();
}

void CGameUI_GV::addAvatar(CAvatar *pAvatar)
{
    m_avatarsPresents.append(new CAvatarPresentGV(pAvatar));

    m_scene->addItem(m_avatarsPresents.last());
    m_scene->addItem(m_avatarsPresents.last()->hookPresent());
}

void CGameUI_GV::removeAvatar(CAvatar *pAvatar)
{
    for (int i = m_avatarsPresents.count() - 1; i >= 0; --i) {
        if (m_avatarsPresents.at(i)->avatar() == pAvatar) {
            delete m_avatarsPresents.at(i);
            m_avatarsPresents.removeAt(i);
        }
    }
}

IUIRectangle *CGameUI_GV::addRectangle()
{
    m_rectangles.append(new CUIRectangleGV());
    m_scene->addItem(m_rectangles.last()->itemToRender());
    return m_rectangles.last();
}

IUIBlocksPresent *CGameUI_GV::addGameBlock(CAbstractGameBlock *pGameBlock)
{
    m_blocksPresents.append(new CBlocksPresentGV(pGameBlock));
    m_scene->addItem(m_blocksPresents.last()->itemToRender());
    return m_blocksPresents.last();
}

void CGameUI_GV::updateGameBlock(CAbstractGameBlock *pGameBlock)
{
    CBlocksPresentGV *toUpdate = blockPresentByGameBlock(pGameBlock);
    if (toUpdate) {
        /* TODO: Checks for duplicates? */
        m_blocksToUpdate.append(toUpdate);
    }
}

void CGameUI_GV::removeGameBlock(CAbstractGameBlock *pGameBlock)
{
    /* Do in this bit unefficient way to avoid possible errors in code duplication */
    removeGameBlock(blockPresentByGameBlock(pGameBlock));
}

void CGameUI_GV::setGameBlockVisible(CAbstractGameBlock *pGameBlock, bool visible)
{
    if (!pGameBlock)
        return;

    CBlocksPresentGV *pPresent = blockPresentByGameBlock(pGameBlock, m_blocksPresents);
    if (pPresent)
        pPresent->setVisible(visible);
}

void CGameUI_GV::removeGameBlock(CBlocksPresentGV *pBlockPresent)
{
    if (!pBlockPresent)
        return;

    m_blocksPresents.removeAll(pBlockPresent);
    delete pBlockPresent;
}

CAvatarPresentGV *CGameUI_GV::avatarPresentByAvatar(CAvatar *pAvatar) const
{
    if (!pAvatar)
        return 0;

    for (int i = 0; i < m_avatarsPresents.count(); ++i) {
        if (m_avatarsPresents.at(i)->avatar() == pAvatar)
            return m_avatarsPresents.at(i);
    }

    return 0;
}

CBlocksPresentGV *CGameUI_GV::blockPresentByGameBlock(CAbstractGameBlock *pGameBlock) const
{
    return blockPresentByGameBlock(pGameBlock, m_blocksPresents);
}

CBlocksPresentGV *CGameUI_GV::blockPresentByGameBlock(CAbstractGameBlock *pGameBlock, const QList<CBlocksPresentGV *> &list) const
{
    /* TODO: Redo smarter (divide itemsPresents by layer and something else...) */
    for (int i = 0; i < list.count(); ++i)
        if (list.at(i)->gameBlock() == pGameBlock)
            return list.at(i);
    return 0;
}

void CGameUI_GV::updateUI()
{
    for (int i = 0; i < m_avatarsPresents.count(); ++i)
        m_avatarsPresents[i]->updateAvatar();

    for (int i = 0; i < m_blocksToUpdate.count(); ++i) {
        m_blocksToUpdate[i]->setup();
        m_scene->addItem(m_blocksToUpdate.at(i)->itemToRender());
    }

    m_blocksToUpdate.clear();

    if (m_processor->camera()) {
        QMatrix matrix;
        matrix.scale(1.0 / m_processor->camera()->scale(), 1.0 / m_processor->camera()->scale());
        setMatrix(matrix);

        QRectF newGeom = rectToQRectF(m_processor->camera()->viewGeometry());
        m_scene->update(newGeom);
        setSceneRect(newGeom);
    }
}

QPointF CGameUI_GV::pointToQPointF(const PointF2D &point)
{
    return QPointF(point.x(), point.y());
}

QRectF CGameUI_GV::rectToQRectF(const RectF2D &rect)
{
    return QRectF(pointToQPointF(rect.position()), QSize(rect.width(), rect.height()));
}
