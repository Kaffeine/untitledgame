#include "CBlocksPresentGV.hpp"
#include "logicbase/mapBlocks/allBlocks.hpp"
#include "logicbase/CAvatar.hpp"

#include "ui/CBlocksAppearance.hpp"

#include "CPeace.hpp"
#include "CGameUI_GV.hpp"

#include <QGraphicsTextItem>
#include <QFont>

static QFont blockFont("DejaVu Sans");

CBlocksPresentGV::CBlocksPresentGV(CAbstractGameBlock *pGameBlock) :
    m_block(pGameBlock),
    m_isVisible(true),
    m_itemToRender(0)
{
    setup();
}

CBlocksPresentGV::~CBlocksPresentGV()
{
    if (m_itemToRender)
        delete m_itemToRender;
}

void CBlocksPresentGV::updatePos()
{
    switch (m_block->blockType()) {
    case BlockSolid:
    case BlockSpawn:
    case BlockTele:
    case BlockFreeze:
    default:
        m_itemToRender->setPos(CGameUI_GV::pointToQPointF(m_block->position() + m_offset));
        break;
    case BlockStart:
    case BlockAcceleration:
    case BlockGravity:
        m_itemToRender->setPos(CGameUI_GV::pointToQPointF(m_block->position() + m_offset
                                                          + (m_block->boundingRect().size() - PointF2D(m_itemToRender->boundingRect().width(), m_itemToRender->boundingRect().height())) / 2));
        break;
    }
}

bool CBlocksPresentGV::isVisible() const
{
    return m_itemToRender->isVisible();
}

void CBlocksPresentGV::setVisible(bool visible)
{
    m_isVisible = visible;
    m_itemToRender->setVisible(m_isVisible);
}

void CBlocksPresentGV::setZValue(float z)
{
    m_itemToRender->setZValue(z);
}

float CBlocksPresentGV::zValue() const
{
    return m_itemToRender->zValue();
}

void CBlocksPresentGV::setOffset(const PointF2D &newOffset)
{
    m_itemToRender->moveBy(newOffset.x() - m_offset.x(), newOffset.y() - m_offset.y());
    m_offset = newOffset;
}

void CBlocksPresentGV::setup()
{
    if (m_itemToRender) {
        delete m_itemToRender;
        m_itemToRender = 0;
    }

    switch (m_block->blockType()) {
    case BlockSolid:
        setupSpaceBlock();
        break;
    case BlockSpawn:
        setupSpawnBlock();
        break;
    case BlockStart:
        setupStartBlock();
        break;
    case BlockFreeze:
        setupFreezeBlock();
        break;
    case BlockTele:
        setupTeleBlock();
        break;
    case BlockGravity:
        setupGravityBlock();
        break;
    case BlockAcceleration:
        setupAccelerationBlock();
        break;
    default:
        setupDefaultBlock();
        break;
    }

    updatePos();
}

void CBlocksPresentGV::setupAccelerationBlock()
{
    QGraphicsTextItem *textItem = new QGraphicsTextItem();

    QFont myFont(blockFont);
    myFont.setPixelSize(CWorldObject::CellLength / 3);
    textItem->setFont(myFont);

    switch (m_block->subBlockType()) {
    default:
    case 0:
        textItem->setPlainText("ADown");
        break;
    case 1:
        textItem->setPlainText("AUp");
        break;
    case 2:
        textItem->setPlainText("ALeft");
        break;
    case 3:
        textItem->setPlainText("ARight");
        break;
    }

    m_itemToRender = textItem;
}

void CBlocksPresentGV::setupGravityBlock()
{
    QGraphicsTextItem *textItem = new QGraphicsTextItem();

    QFont myFont(blockFont);
    myFont.setPixelSize(CWorldObject::CellLength / 3);
    textItem->setFont(myFont);

    switch (m_block->subBlockType()) {
    default:
    case 0:
        textItem->setPlainText("Down");
        break;
    case 1:
        textItem->setPlainText("Up");
        break;
    case 2:
        textItem->setPlainText("Left");
        break;
    case 3:
        textItem->setPlainText("Right");
        break;
    case 4:
        textItem->setPlainText("None");
        break;
    }

    m_itemToRender = textItem;
}

void CBlocksPresentGV::setupSpaceBlock()
{
    m_peace = new CPeace();
    m_peace->m_peacePoly = polygonByBlocksDirection(m_block->direction());
    m_peace->setBoundingRect(CGameUI_GV::rectToQRectF(m_block->boundingRect()));
    m_peace->setCustomColor(CBlocksAppearance::blocksNearColor(m_block));

    m_peace->update();

    m_itemToRender = m_peace;
}

void CBlocksPresentGV::setupFreezeBlock()
{
    m_peace = new CPeace();
    m_peace->m_peacePoly = polygonByBlocksDirection(m_block->direction());
    m_peace->setBoundingRect(CGameUI_GV::rectToQRectF(m_block->boundingRect()));
    m_peace->setCustomColor(CBlocksAppearance::blocksNearColor(m_block));

    QPainterPath overdraw;

    QFont myFont(blockFont);
    myFont.setPixelSize(m_block->height() * 0.6);

    if (m_block->subBlockType() % 2)
        overdraw.addText(m_block->width() * 0.1, m_block->height() * 0.75, myFont, "U" + QString::number(m_block->subBlockType() / 2));
    else
        overdraw.addText(m_block->width() * 0.1, m_block->height() * 0.75, myFont, "F" + QString::number(m_block->subBlockType() / 2));

    if (m_block->direction() == DirectionNone)
        m_peace->m_overdraw = overdraw;

    m_itemToRender = m_peace;
}

void CBlocksPresentGV::setupSpawnBlock()
{
    m_peace = new CPeace();
    m_peace->setBoundingRect(CGameUI_GV::rectToQRectF(m_block->boundingRect()));
    m_peace->setCustomColor(CBlocksAppearance::blocksNearColor(m_block));

    QPolygonF blocksDefaultPoly = polygonByBlocksDirection(m_block->direction());

    if (blocksDefaultPoly.count() > 2) {
        QPainterPath overdraw;

        if (m_block->direction() == DirectionNone)
            overdraw.addEllipse(CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterVCenter]), CAvatar::width() / 2, CAvatar::height() / 2);
        else
            overdraw.addEllipse((blocksDefaultPoly.at(0) + blocksDefaultPoly.at(1) + blocksDefaultPoly.at(2)) / 3.0, CWorldObject::CellLength * 0.2, CWorldObject::CellLength * 0.2);

        if (m_block->subBlockType() == 1) {
            if (m_block->direction() == DirectionNone) {
                overdraw.addPolygon(QPolygonF()
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterTop ])
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterBottom]));
                overdraw.addPolygon(QPolygonF()
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftVCenter])
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::RightVCenter ]));
            }
            else {
                overdraw.addPolygon(QPolygonF()
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterTop ])
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterBottom]));

                overdraw.addPolygon(QPolygonF()
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftVCenter])
                                    << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::RightVCenter]));
            }
        }

        m_peace->m_overdraw = overdraw;
    }

    m_itemToRender = m_peace;
}

void CBlocksPresentGV::setupStartBlock()
{
    QGraphicsTextItem *textItem = new QGraphicsTextItem();

    QFont myFont(blockFont);
    myFont.setPixelSize(CWorldObject::CellLength / 3);
    textItem->setFont(myFont);

    if (m_block->subBlockType() == 0)
        textItem->setPlainText("Start");
    else if (m_block->subBlockType() == 1)
        textItem->setPlainText("Finish");
    else
        textItem->setPlainText(QString("CP%1").arg(m_block->subBlockType() - 1));

    m_itemToRender = textItem;
}

void CBlocksPresentGV::setupTeleBlock()
{
    m_peace = new CPeace();
    m_peace->m_peacePoly = polygonByBlocksDirection(m_block->direction());
    m_peace->setBoundingRect(CGameUI_GV::rectToQRectF(m_block->boundingRect()));
    m_peace->setCustomColor(CBlocksAppearance::blocksNearColor(m_block));

    QPainterPath overdraw;

    QFont myFont(blockFont);
    myFont.setPixelSize(m_block->height() * 0.75);

    overdraw.addText(m_block->width() * 0.25, m_block->height() * 0.8, myFont, QString::number(m_block->subBlockType() / 2));

    if (!(m_block->subBlockType() % 2))
        overdraw.addEllipse(CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterVCenter]), CWorldObject::CellLength * 0.5, CWorldObject::CellLength * 0.5);

    m_peace->m_overdraw = overdraw;

    m_itemToRender = m_peace;
}

void CBlocksPresentGV::setupDefaultBlock()
{
    m_itemToRender = new QGraphicsTextItem("Not implemented");
}

QPolygonF CBlocksPresentGV::polygonByBlocksDirection(Directions direction)
{
    QPolygonF polygon;

    switch (direction) {
    default:
    case DirectionNone:
        polygon << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftTop]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::RightTop]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::RightBottom]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftBottom]);
        break;
    case DirectionUp:
        polygon << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftTop]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::RightTop]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterVCenter]);
        break;
    case DirectionDown:
        polygon << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftVCenter]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::RightVCenter]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterTop]);
        break;
    case DirectionLeft:
        polygon << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftTop]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftBottom]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterVCenter]);
        break;
    case DirectionRight:
        polygon << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::LeftVCenter]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterTop]) << CGameUI_GV::pointToQPointF(CBlocksAppearance::Points[CBlocksAppearance::HCenterBottom]);
        break;
    }

    return polygon;
}
