#ifndef CAVATARPRESENTGV_HPP
#define CAVATARPRESENTGV_HPP

#include <QGraphicsItem>

class CAvatar;
class CHookPresentGV;

class CAvatarPresentGV : public QGraphicsItem
{
public:
    explicit CAvatarPresentGV(CAvatar *pAvatar);
    ~CAvatarPresentGV();

    inline CAvatar *avatar() const { return m_avatar; }
    void setAvatar(CAvatar *pAvatar);

    QGraphicsItem *hookPresent() const;
    void updateAvatar();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

private:
    CAvatar *m_avatar;
    CHookPresentGV *m_hookPresent;

    QColor m_color;
    QColor m_borderColor;

    QRectF m_boundingRect;
};

#endif // CAVATARPRESENTGV_HPP
