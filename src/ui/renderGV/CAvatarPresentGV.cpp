#include "CAvatarPresentGV.hpp"
#include "CHookPresentGV.hpp"

#include "logicbase/CAvatar.hpp"
#include "logicbase/CAvatarAppearance.hpp"
#include "logicbase/CHookAction.hpp"

#include "CGameUI_GV.hpp"

#include <QPainter>

CAvatarPresentGV::CAvatarPresentGV(CAvatar *pAvatar):
    m_avatar(pAvatar),
    m_hookPresent(new CHookPresentGV())
{
    m_color = QColor(0xff, 0, 0);
    m_borderColor = QColor(0, 0, 0);
    setZValue(10);

    m_boundingRect = CGameUI_GV::rectToQRectF(pAvatar->boundingRect());

    updateAvatar();

    m_hookPresent->setVisible(false);
    m_hookPresent->setZValue(12);
}

CAvatarPresentGV::~CAvatarPresentGV()
{
    delete m_hookPresent;
}

void CAvatarPresentGV::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    QPen pen;
    pen.setColor(m_borderColor);
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(3);

    painter->setPen(pen);
    painter->setBrush(m_color);
    painter->drawRect(boundingRect());
}

QRectF CAvatarPresentGV::boundingRect() const
{
    return m_boundingRect;
}

void CAvatarPresentGV::updateAvatar()
{
    setPos(CGameUI_GV::pointToQPointF(m_avatar->position()));
    CHookAction *hookAction = qobject_cast<CHookAction*>(m_avatar->currentAltAction());
    if (hookAction && hookAction->hookLength() != 0) {
        m_hookPresent->setVisible(true);
        m_hookPresent->redrawAt(CGameUI_GV::pointToQPointF(m_avatar->center()), CGameUI_GV::pointToQPointF(hookAction->hookEnd()));
    }
    else
        m_hookPresent->setVisible(false);

    if (m_avatar->freezedTime() > 0)
        m_borderColor = QColor(0, 0, 0x80);
    else
        m_borderColor = QColor(0, 0, 0);

    if (m_avatar->appearance())
        m_color.setNamedColor(m_avatar->appearance()->color());

    update();
}

QGraphicsItem *CAvatarPresentGV::hookPresent() const
{
    return m_hookPresent;
}
