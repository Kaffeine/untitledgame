#include "CUIRectangleGV.hpp"
#include <QGraphicsRectItem>
#include <QPen>

#include "CGameUI_GV.hpp"

CUIRectangleGV::CUIRectangleGV()
{
    m_itemToRender = new QGraphicsRectItem();
    m_itemToRender->setPen(QPen(Qt::darkBlue, 4));
    m_itemToRender->setVisible(false);
}

CUIRectangleGV::~CUIRectangleGV()
{
    delete m_itemToRender;
}

bool CUIRectangleGV::isVisible() const
{
    return m_itemToRender->isVisible();
}

void CUIRectangleGV::setVisible(bool newVisible)
{
    m_itemToRender->setVisible(newVisible);
}

void CUIRectangleGV::setRect(float x, float y, float width, float height)
{
    m_itemToRender->setRect(x, y, width, height);
}

void CUIRectangleGV::setZValue(float z)
{
    m_itemToRender->setZValue(z);
}

float CUIRectangleGV::zValue() const
{
    return m_itemToRender->zValue();
}

void CUIRectangleGV::setOffset(const PointF2D &newOffset)
{
    m_itemToRender->setPos(CGameUI_GV::pointToQPointF(newOffset));
}

PointF2D CUIRectangleGV::offset() const
{
    return PointF2D(m_itemToRender->pos().x(), m_itemToRender->pos().y());
}
