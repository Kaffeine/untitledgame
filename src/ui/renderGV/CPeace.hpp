#ifndef CPEACE_HPP
#define CPEACE_HPP

#include <QGraphicsItem>
#include "logicbase/Directions.hpp"

class CPeace : public QGraphicsItem
{
public:
    CPeace();

    void setBoundingRect(QRectF newBoundingRect);
    void setCustomColor(QColor newColor);
    inline QRectF boundingRect() const { return m_boundingRect; }
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QPolygonF m_peacePoly;
    QPainterPath m_overdraw;

protected:
    QRectF m_boundingRect;
    QColor fillColor;

};

#endif // CPEACE_HPP
