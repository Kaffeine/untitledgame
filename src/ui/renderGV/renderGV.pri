
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += $$PWD

contains(options, gl_in_gv) {
    QT += opengl
    DEFINES += USE_GL_IN_GV
    message("Accelerate GraphicsView via OpenGL.")
} else {
    message("Do not accelerate GraphicsView via OpenGL.")
}

SOURCES += \
    $$PWD/CGameUI_GV.cpp \
    $$PWD/CPeace.cpp \
    $$PWD/CUIRectangleGV.cpp \
    $$PWD/CAvatarPresentGV.cpp \
    $$PWD/CHookPresentGV.cpp \
    $$PWD/CBlocksPresentGV.cpp

HEADERS  +=  \
    $$PWD/CGameUI_GV.hpp \
    $$PWD/CPeace.hpp \
    $$PWD/CUIRectangleGV.hpp \
    $$PWD/CAvatarPresentGV.hpp \
    $$PWD/CHookPresentGV.hpp \
    $$PWD/CBlocksPresentGV.hpp
