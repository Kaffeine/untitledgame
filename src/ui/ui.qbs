import qbs 1.0

Product {
    Depends { name: "logicbase" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui", "opengl"] }
    name: "ui"
    type: "dynamiclibrary"

    cpp.includePaths: [
        ".."
    ]

    cpp.dynamicLibraries: "GL"

    Group {
        name: "commonfiles"
        files: [
            "CBlocksAppearance.cpp",
            "CBlocksAppearance.hpp",
            "CCamera.cpp",
            "CCamera.hpp",
            "CConsoleLineHelper.cpp",
            "CConsoleLineHelper.hpp",

            "IGameUI.hpp",
            "IUIBlocksPresent.hpp",
            "IUIPrimitive.hpp",
            "IUIProcessor.hpp",
            "IUIRectangle.hpp"
        ]
    }

    Group {
        name: "renderGL"
	prefix: name + '/'
        files: [
            "CAvatarPresentGL.cpp",
            "CAvatarPresentGL.hpp",
            "CBlocksPresentGL.cpp",
            "CBlocksPresentGL.hpp",
            "CGameUI_GL.cpp",
            "CGameUI_GL.hpp",

            "CGLBase.hpp",
            "CUIRectangleGL.cpp",
            "CUIRectangleGL.hpp"
        ]
    }
}
