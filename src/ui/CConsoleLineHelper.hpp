#ifndef CCONSOLELINEHELPER_HPP
#define CCONSOLELINEHELPER_HPP

#include <QObject>

class CConsole;

class QLineEdit;
class QPlainTextEdit;

class CConsoleLineHelper : public QObject
{
    Q_OBJECT
public:
    explicit CConsoleLineHelper(QObject *parent = 0);
    void setObjects(QLineEdit *pInput, QPlainTextEdit *pOutput, CConsole *pConsole);
    bool eventFilter(QObject *object, QEvent *event);

public slots:
    void whenConsoleOutput(const QString &text);

private:
    void getAutocompletion();
    int m_currentHistoryCommandNum;
    QLineEdit *m_input;
    QPlainTextEdit *m_output;
    CConsole *m_console;
    
};

#endif // CCONSOLELINEHELPER_HPP
