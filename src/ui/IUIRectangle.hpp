#ifndef IUIRECTANGLE_HPP
#define IUIRECTANGLE_HPP

#include "IUIPrimitive.hpp"

#include <QRectF>

class IUIRectangle : public IUIPrimitive
{
public:
//    virtual QRectF rect() const = 0;
    virtual void setRect(float x, float y, float width, float height) = 0;
    void setRect(const QRectF &rectangle) { setRect(rectangle.x(), rectangle.y(), rectangle.width(), rectangle.height()); }
};

#endif // IUIRECTANGLE_HPP
