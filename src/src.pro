
TEMPLATE = subdirs

include(../options.pri)
include(common.pri)

SUBDIRS += logicbase
SUBDIRS += gamebase
SUBDIRS += network
SUBDIRS += ui

gamebase.depends = logicbase
network.depends = logicbase
ui.depends = logicbase gamebase

contains(options, editor) {
    SUBDIRS += editor
    editor.depends = logicbase gamebase ui
}

contains(options, client) {
    SUBDIRS += client
    client.depends = logicbase gamebase ui network
}

contains(options, server) {
    SUBDIRS += server
    server.depends = logicbase network
}

SUBDIRS += netdescriptor
netdescriptor.depends = logicbase network
