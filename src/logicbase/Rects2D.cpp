#include "Rects2D.hpp"
#include "Functions.hpp"

RectF2D RectF2D::fromTwoPoints(const PointF2D &point1, const PointF2D &point2)
{
    RectF2D result;
    result.m_position = minPoint(point1, point2);
    result.m_size     = maxPoint(point1, point2);
    return result;
}

void RectF2D::clamp(PointF2D *point) const
{
    if (point->x() < left())
        point->setX(left());
    else if (point->x() > right())
        point->setX(right());

    /* Y direction is from top to down. */
    if (point->y() < top())
        point->setY(top());
    else if (point->y() > bottom())
        point->setY(bottom());
}
