#ifndef CHOOKACTION_HPP
#define CHOOKACTION_HPP

#include "CAbstractAvatarAction.hpp"

class CHookAction : public CAbstractAvatarAction
{
    Q_OBJECT
    Q_PROPERTY(qreal maxLength READ maxLength WRITE setMaxLength)
    Q_PROPERTY(qreal rollingSpeed READ rollingSpeed WRITE setRollingSpeed)
    Q_PROPERTY(qreal hookAvatarMaxDuration READ hookAvatarMaxDuration WRITE setHookAvatarMaxDuration)

public:
    enum HookStates {
        HookBegin,
        HookedPoint,
        HookedAvatar,
        HookEnd,
        HookNone
    };

    explicit CHookAction(CAvatar *avatar);

    void doAction(const PointF2D &dest);
    void cancelAction();

    void shiftTime(const double &dT);
    void doImpact(const double &dT);

    void freezed();

    inline qreal hookLength() const { return m_currentLength; }
    inline PointF2D hookVector() const { return m_hookVector; }
    inline PointF2D hookVectorNormalized() const { return m_normalizedVector; }
    PointF2D hookEnd() const;
    HookStates state() const;
    inline CAvatar *hookedAvatar() const { return m_hookedAvatar; }

    inline qreal rollingSpeed() const { return m_rollingSpeed; }
    float hookSpeed() const;

    PointF2D hookedAvatarSpeedup() const;
    PointF2D hookedAvatarAcceleration() const;

    inline qreal maxLength() const { return m_maxLength; }

    inline qreal hookAvatarMaxDuration() const { return m_hookedAvatarMaxDuration; }

public slots:
    void setRollingSpeed(qreal newRollingSpeed);
    void setMaxLength(qreal maxLength);
    void setHookAvatarMaxDuration(qreal newHookAvatarMaxDuration);

private:
    void setHookMissed();
    void setHooked(const PointF2D &where);
    void setHooked(CAvatar *whom);

    HookStates m_state;
    PointF2D m_normalizedVector;

    float m_currentLength;
    float m_hookSpeedLimit;
    float m_hookerFlySpeed;
    float m_maxLength;
    float m_hookedAvatarMaxDuration;

    PointF2D m_hookVector;
    PointF2D m_hookedPoint;
    CAvatar *m_hookedAvatar;
    PointF2D m_hookedAvatarSpeedup;
    float m_hookedTimeRemains;
    float m_hookProcessedLength;

    float m_rollingSpeed;
    float m_hookRollingAcceleration;
    float m_hookMinimumLength;
};

#endif // CHOOKACTION_HPP
