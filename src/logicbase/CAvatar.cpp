#include "CAvatar.hpp"

#include "CAvatarAppearance.hpp"
#include "CWorld.hpp"
#include "Functions.hpp"

#include "CAbstractAvatarAction.hpp"

#include "CHammerAction.hpp"
#include "CHookAction.hpp"

#include <qmath.h>
#include <QDebug>

/* Avatar is logical player (or bot) representation in game world. */
CAvatar::CAvatar(QObject *parent) :
    QObject(parent),
    CWorldObject(),
    m_currentAction(0),
    m_currentAltAction(0),
    m_appearance(new CAvatarAppearance(this))
{
    m_width  = CAvatar::width();
    m_height = CAvatar::height();
    m_mass = 1;

    m_jumpsAvailable = 0;
    m_freezedTime    = 0;
    m_touches        = DirectionNone;

    m_willDirections = 0;

    m_player = 0;

    /* Settings */
    m_willForce      = CellLength * 93;
    m_willSpeedLimit = CellLength * 9.3;

    m_airSlippingFactor = 0.15;

    m_groundedEffectiveFrictionMinimumValue = 0.2;
    m_groundedFrictionTransitionValue  = 0.1;
    m_willFrictionModificator = 0.3;

    m_jumpSpeed             = CellLength * 21;
    m_slowingWhileFreefall  = CellLength * 8;
    m_slowingWhileDraft     = CellLength * 100;

    m_currentAction = new CHammerAction(this);
    m_currentAltAction = new CHookAction(this);
}

CAvatar::~CAvatar()
{
    if (m_world)
        m_world->removeAvatar(this);
}

void CAvatar::setPlayer(CPlayer *newPlayer)
{
    m_player = newPlayer;
}

qreal CAvatar::width()
{
    return 28; //CellLength * 0.85;
}

qreal CAvatar::height()
{
    return 28; //CellLength * 0.85;
}

void CAvatar::move(const PointF2D &motion)
{
    setPosition(m_position + motion);
}

void CAvatar::setFreezedTime(qreal time)
{
    m_freezedTime = time;
    if (m_freezedTime > 0) {

    }
    else if (m_freezedTime < 0)
        m_freezedTime = 0;
}

Directions CAvatar::gravityDirection() const
{
    if (m_gravityDirection == DirectionExtra)
        return m_world->gravityDirection();

    return m_gravityDirection;
}

void CAvatar::setOwnGravityDirection(Directions newGravity)
{
    if (m_gravityDirection == newGravity)
        return;

    m_gravityDirection = newGravity;
}

void CAvatar::bound(const PointF2D &topLeft, const PointF2D &bottomRight)
{
    PointF2D newPos = position();

    if (minX() < topLeft.x()) {
        newPos.setX(topLeft.x());
        m_speed.setX(0);
    }
    else if (maxX() > bottomRight.x()) {
        newPos.setX(bottomRight.x() - width());
        m_speed.setX(0);
    }

    if (minY() < topLeft.y()) {
        newPos.setY(topLeft.y());
        m_speed.setY(0);
    }
    else if (maxY() > bottomRight.y()) {
        newPos.setY(bottomRight.y() - height());
        m_speed.setY(0);
    }

    setPosition(newPos);
}

void CAvatar::killedEvent(CWorldObject *killer)
{
    if (killer == this) {
        /* Suicide */
    }
}

RectF2D CAvatar::boundingRect() const
{
    return RectF2D(0, 0, width(), height());
}

bool CAvatar::containPoint(const PointF2D &point) const
{
    return (vectorsLength(point - size() / 2) <= (width() / 2));
}

void CAvatar::doImpact(const double &dT)
{
    if (currentAction())
        currentAction()->doImpact(dT);

    if (currentAltAction())
        currentAltAction()->doImpact(dT);
}

void CAvatar::shiftTime(const double &dT)
{
    qreal stopTime = dT + 1;

    PointF2D newSupposedPosition = supposedPosition(dT); //, &stopTime);

    if (stopTime < dT) {
        m_speed.setX(0);
        m_speed.setY(0);
        updateSpeeds(dT - stopTime);
    }
    else
        updateSpeeds(dT);

    if (currentAction())
        currentAction()->shiftTime(dT);

    if (currentAltAction())
        currentAltAction()->shiftTime(dT);

    if (freezedTime() > 0) {
        m_freezedTime -= dT;
        if (freezedTime() < 0)
            m_freezedTime = 0;
    }

    setPosition(newSupposedPosition);

    return;
}

PointF2D CAvatar::supposedPosition(qreal dT) const
{
    PointF2D supposedNewPosition = position();

    supposedNewPosition += dT * speed();

    return supposedNewPosition;
}

PointF2D CAvatar::respectTouches(const PointF2D &motion) const
{
    PointF2D boundedMotion = motion;

    quint8 activeDirections = directionsByVector(motion) & m_touches;

    if (activeDirections & DirectionHorizontal)
        boundedMotion.setX(0);

    if (activeDirections & DirectionVertical)
        boundedMotion.setY(0);

    return boundedMotion;
}

void CAvatar::updateSpeeds(float dT)
{
    float dT1Freezed   = 0;
    float dT2Unfreezed = 0;

    if (freezedTime() > dT)
        dT1Freezed = dT;
    else if (freezedTime() < dT) {
        dT1Freezed = freezedTime();
        dT2Unfreezed = dT - dT1Freezed;
    }
    else
        dT2Unfreezed = dT;

    if (dT1Freezed)
        updateSpeedsHelper(dT1Freezed, 0);

    if (dT2Unfreezed)
        updateSpeedsHelper(dT2Unfreezed, m_willDirections);

    if (m_willDirections & WillJump) {
        m_willDirections &= ~WillJump;

        if (m_jumpsAvailable > 0) {
            --m_jumpsAvailable;
        }
    }

    if (isOnGround())
        m_jumpsAvailable = 1;
}

void CAvatar::updateSpeedsHelper(float dT, quint8 willDirection)
{
    PointF2D unlimitedSpeedUp = (unlimitedForce() + externalForce())   / mass() * dT;
    PointF2D frictionsSpeedUp = frictionsForce(speed(), willDirection) / mass() * dT;
    PointF2D willSpeedUp      = willForce(willDirection)               / mass() * dT;

    m_speed += m_additionalPulse / mass();
    m_additionalPulse.reset();

    PointF2D jumpSpeedUpCache = jumpSpeedUp(willDirection);

    if (!jumpSpeedUpCache.isNull()) {
        if (jumpSpeedUpCache.x())
            jumpSpeedUpCache.setX(jumpSpeedUpCache.x() - speed().x());
        if (jumpSpeedUpCache.y())
            jumpSpeedUpCache.setY(jumpSpeedUpCache.y() - speed().y());

        willSpeedUp += jumpSpeedUpCache;
    }

    quint8 zeroDirectionsFlags = DirectionNone;

    if (willSpeedUp.isNull()) {
        quint8 speedDirectionBeforeFriction;
        PointF2D possibleSpeed = speed() + unlimitedSpeedUp;
        speedDirectionBeforeFriction = directionsByVector(possibleSpeed);
        if (speedDirectionBeforeFriction != directionsByVector(possibleSpeed + frictionsSpeedUp)) {
            quint8 commonDirections = speedDirectionBeforeFriction & directionsByVector(possibleSpeed + frictionsSpeedUp);
            if (speedDirectionBeforeFriction & ~commonDirections & DirectionHorizontal)
                zeroDirectionsFlags |= DirectionHorizontal;

            if (speedDirectionBeforeFriction & ~commonDirections & DirectionVertical)
                zeroDirectionsFlags |= DirectionVertical;
        }
    }
    else {
        PointF2D othersSpeedUp = unlimitedSpeedUp + frictionsSpeedUp;
        quint8 willSpeedUpDirection = directionsByVector(willSpeedUp);
        if (willSpeedUpDirection == DirectionLeft) {
            if ((speed() + othersSpeedUp + willSpeedUp).x() < -willSpeedLimit()) { // Speedup to left is more, than willSpeedLimit
                willSpeedUp.setX(-willSpeedLimit() - (speed().x() + othersSpeedUp.x()));
                if (willSpeedUp.x() > 0) // if willSpeedUpDirection is changed, then set according speedup to zero
                    willSpeedUp.setX(0);
            }
        }
        if (willSpeedUpDirection == DirectionRight) {
            if ((speed() + othersSpeedUp + willSpeedUp).x() > willSpeedLimit()) { // Speedup to right is more, than willSpeedLimit
                willSpeedUp.setX(willSpeedLimit() - (speed().x() + othersSpeedUp.x()));
                if (willSpeedUp.x() < 0) // if willSpeedUpDirection is changed, then set according speedup to zero
                    willSpeedUp.setX(0);
            }
        }
    }

    m_speed += unlimitedSpeedUp + frictionsSpeedUp + willSpeedUp;

    if (zeroDirectionsFlags & DirectionHorizontal)
        m_speed.setX(0);

    if (zeroDirectionsFlags & DirectionVertical)
        m_speed.setY(0);

    m_speed = respectTouches(m_speed);
}

PointF2D CAvatar::gravityForce() const
{
    return vectorByDirection(gravityDirection()) * gravityMass() * mass(); // * mass() / mass();
}

PointF2D CAvatar::unlimitedForce() const
{
    return gravityForce();
}

PointF2D CAvatar::willForce(quint8 willDirection) const
{
    willDirection &= DirectionMask;
    /* Depends on contact between avatar and surface, more or less of will force will be valueble.
     * For example, if avatar is stand on ice, it almost can't do will-moving due to slipping. */

    qreal slippingFactor;

    qreal frictionValue = m_frictions.value(gravityDirection() & m_touches);

    if (gravityDirection() & m_touches) {
        if (frictionValue > m_groundedEffectiveFrictionMinimumValue)
            slippingFactor = 0;
        else if (frictionValue > m_groundedFrictionTransitionValue) {
            qreal transitionLength = m_groundedEffectiveFrictionMinimumValue - m_groundedFrictionTransitionValue;
            slippingFactor = 1 - (frictionValue - m_groundedFrictionTransitionValue) / transitionLength;
        }
        else {
            slippingFactor = 1;
        }

        if (slippingFactor > 0)
            slippingFactor *= 0.93;
    }
    else {
        slippingFactor = m_airSlippingFactor;
    }

    return vectorByDirections(willDirection) * (1 - slippingFactor) * m_willForce;
}

PointF2D CAvatar::frictionsForce(const PointF2D &sourceSpeed, quint8 willDirections) const
{
    PointF2D frictionsForceResult;

    quint8 sourceSpeedDirections = directionsByVector(sourceSpeed);

    if (gravityDirection() & m_touches) {
        if ((gravityDirection() & m_touches & DirectionVertical) && (sourceSpeedDirections & DirectionHorizontal)) {
            if (sourceSpeed.x() > 0)
                frictionsForceResult.setX(- m_frictions.value(m_touches & (DirectionVertical)) * m_slowingWhileDraft);
            else if (sourceSpeed.x() < 0)
                frictionsForceResult.setX(+ m_frictions.value(m_touches & (DirectionVertical)) * m_slowingWhileDraft);
        }
        else if (gravityDirection() & m_touches & (DirectionHorizontal)) {
            if (sourceSpeed.y() > 0)
                frictionsForceResult.setY(- m_frictions.value(m_touches & (DirectionHorizontal)) * m_slowingWhileDraft);
            else if (sourceSpeed.y() < 0)
                frictionsForceResult.setY(+ m_frictions.value(m_touches & (DirectionHorizontal)) * m_slowingWhileDraft);
        }
    }
    else {
        /* Air friction */
        if (sourceSpeed.x() > 0)
            frictionsForceResult.setX(-m_slowingWhileFreefall);
        if (sourceSpeed.x() < 0)
            frictionsForceResult.setX(+m_slowingWhileFreefall);

        if (sourceSpeed.y() > 0)
            frictionsForceResult.setY(-m_slowingWhileFreefall);
        if (sourceSpeed.y() < 0)
            frictionsForceResult.setY(+m_slowingWhileFreefall);
    }

    quint8 negativeFrictionDirections = directionsByVector(-frictionsForceResult);
    if (negativeFrictionDirections & willDirections) {
        if (negativeFrictionDirections & willDirections & DirectionHorizontal)
            frictionsForceResult.setX(frictionsForceResult.x() * m_willFrictionModificator);

        if (negativeFrictionDirections & willDirections & DirectionVertical)
            frictionsForceResult.setY(frictionsForceResult.y() * m_willFrictionModificator);
    }

    return frictionsForceResult;
}


PointF2D CAvatar::jumpSpeedUp(quint8 willDirections) const
{
    if (jumpIsAvailable() && (willDirections & WillJump)) {
        switch (gravityDirection()) {
        case DirectionLeft:
            return PointF2D(m_jumpSpeed, 0);
        case DirectionRight:
            return PointF2D(-m_jumpSpeed, 0);
        case DirectionUp:
            return PointF2D(0, m_jumpSpeed);
        case DirectionDown:
            return PointF2D(0, -m_jumpSpeed);
        default:
            break;
        }
    }
    return PointF2D();
}

bool CAvatar::jumpIsAvailable() const
{
    if (isOnGround())
        return true;

    return m_jumpsAvailable > 0;
}

bool CAvatar::isOnGround() const
{
    return (m_frictions.value(gravityDirection() & m_touches, 0) > m_groundedEffectiveFrictionMinimumValue);
}

void CAvatar::printWills(quint8 wills)
{
    qDebug() << "Wills:";
    qDebug() << "WillLeft   " << bool(wills & WillLeft  );
    qDebug() << "WillRight  " << bool(wills & WillRight );
    qDebug() << "WillUp     " << bool(wills & WillUp    );
    qDebug() << "WillDown   " << bool(wills & WillDown  );
    qDebug() << "WillJump   " << bool(wills & WillJump  );
    qDebug() << "WillAction1" << bool(wills & WillAction1);
    qDebug() << "WillAction2" << bool(wills & WillAction2);
}

void CAvatar::addTouch(quint8 touchDirection, qreal friction)
{
    m_touches |= touchDirection;
    m_frictions.insert(touchDirection, m_frictions.value(touchDirection, 0) + friction);
}

void CAvatar::cleanTouches()
{
    if (m_touches == DirectionNone)
        return;

    m_touches = DirectionNone;
    m_frictions.clear();
}

CAbstractAvatarAction *CAvatar::currentAction() const
{
    return m_currentAction;
}

CAbstractAvatarAction *CAvatar::currentAltAction() const
{
    return m_currentAltAction;
}

void CAvatar::doAction(int action, const PointF2D &dest)
{
    if (action == WillAction1) {
        if (!currentAction())
            return;

        currentAction()->doAction(dest);
    }
    else if (action == WillAction2) {
        if (!currentAltAction())
            return;

        currentAltAction()->doAction(dest);
    }
}

void CAvatar::cancelAction(int action)
{
    if (action == WillAction1) {
        if (!currentAction())
            return;

        currentAction()->cancelAction();
    }
    else if (action == WillAction2) {
        if (!currentAltAction())
            return;

        currentAltAction()->cancelAction();
    }
}

void CAvatar::addExternalForce(const PointF2D &addition)
{
    m_externalForce += addition;
}

void CAvatar::addExternalPulse(const PointF2D &addPulse)
{
    m_additionalPulse += addPulse;
}

void CAvatar::cleanExternalForces()
{
    m_externalForce.reset();
}

PointF2D CAvatar::externalForce() const
{
    return m_externalForce;
}

quint8 CAvatar::willDirections() const
{
    if (m_freezedTime > 0)
        return 0;

    return m_willDirections;
}

void CAvatar::setWillDirections(quint8 newWills)
{
    m_willDirections = newWills;
}

void CAvatar::doSuicide()
{
    emit wantSuicide(this);
}
