#include "CWorldObject.hpp"
#include "CWorld.hpp"

const int CWorldObject::CellLength = 32;
const PointF2D CWorldObject::CellSize = PointF2D(CellLength, CellLength);

/* This is pure logical class.
 * Provide some geometry stuff. */
CWorldObject::CWorldObject() :
    m_world(0)
{
}

CWorldObject::~CWorldObject()
{
}

void CWorldObject::setPosition(const PointF2D &position)
{
    /* Don't use "m_position == position", because it perform qFuzzyCompare() instead of exacly value-to-value matching. */
    if ((m_position.x() == position.x()) && (m_position.y() == position.y()))
        return;

    m_position = position;
    positionChangedEvent();
}

void CWorldObject::setPosition(float newPosX, float newPosY)
{
    if ((m_position.x() == newPosX) && (m_position.y() == newPosY))
        return;

    m_position = PointF2D(newPosX, newPosY);
    positionChangedEvent();
}

void CWorldObject::setWorld(CWorld *newWorld)
{
    if (m_world == newWorld)
        return;

    m_world = newWorld;
    setWorldEvent();
}

void CWorldObject::setWorldEvent()
{

}

void CWorldObject::positionChangedEvent()
{

}

Directions CWorldObject::gravityDirection() const
{
    if (m_world)
        return m_world->gravityDirection();

    return DirectionNone;
}

float CWorldObject::gravityMass() const
{
    if (m_world)
        return m_world->gravityMass();

    return 0;
}
