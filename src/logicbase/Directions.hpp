#ifndef DIRECTIONS_HPP
#define DIRECTIONS_HPP

enum Directions {
    DirectionNone  = 0,
    DirectionLeft  = 0b0000001,
    DirectionRight = 0b0000010,
    DirectionUp    = 0b0000100,
    DirectionDown  = 0b0001000,
    DirectionHorizontal = DirectionLeft|DirectionRight,
    DirectionVertical   = DirectionUp  |DirectionDown,
    DirectionExtra = 0b0010000, // Used as marker that gravity should be taken from superior.
    DirectionMask  = DirectionHorizontal | DirectionVertical
};

#endif // DIRECTIONS_HPP
