#include "CHookAction.hpp"

#include "CAvatar.hpp"
#include "CWorld.hpp"

#include "mapBlocks/CSolidBlock.hpp"

#include "Functions.hpp"

CHookAction::CHookAction(CAvatar *avatar) :
    CAbstractAvatarAction(avatar)
{
    m_state    = HookNone;
    m_hookedAvatar = 0;
    m_currentLength = 0;

    m_hookSpeedLimit          = CWorldObject::CellLength * 12;
    m_hookerFlySpeed          = CWorldObject::CellLength * 90;
    m_hookMinimumLength       = CWorldObject::CellLength * 1.5;
    m_rollingSpeed            = CWorldObject::CellLength * 15;
    m_hookRollingAcceleration = CWorldObject::CellLength * 36 * 2; // Gravity * 2

    m_hookedTimeRemains = 0;

    setHookAvatarMaxDuration(1.25);
    setMaxLength(CWorldObject::CellLength * 50);
}

void CHookAction::doAction(const PointF2D &dest)
{
    if (!prepareAction(dest))
        return;

    m_state = HookBegin;
    m_currentLength    = 0;
    m_normalizedVector = normalizedVector(m_actionDestination - m_avatar->center());
    m_hookVector.reset();
    m_hookProcessedLength = 0;
}

void CHookAction::cancelAction()
{
    if (m_state == HookNone)
        return;

    m_state = HookEnd;
    m_hookedAvatar = 0;
}

void CHookAction::shiftTime(const double &dT)
{
    if (m_hookedTimeRemains > 0)
        m_hookedTimeRemains -= dT;

    switch (m_state) {
    case HookNone:
        return;
        break;
    case HookBegin:
        m_currentLength += dT * m_hookerFlySpeed;

        if (m_currentLength >= m_maxLength)
            m_currentLength = m_maxLength;

        m_normalizedVector = normalizedVector(m_actionDestination - m_avatar->center());
        m_hookVector = m_normalizedVector * m_currentLength;

        /* Collision processing */
        if (true) {
            SWorldsObjectCollision collisionInfo;
            PointF2D hookActiveZoneBegin;
            PointF2D hookActiveZoneEnd;
            hookActiveZoneEnd   = avatar()->center() + hookVector();
            hookActiveZoneBegin = avatar()->center() + hookVectorNormalized() * m_hookProcessedLength;

            m_hookProcessedLength = m_currentLength;

            avatar()->world()->getFirstCollision(hookActiveZoneBegin, hookActiveZoneEnd, &collisionInfo, avatar());

            if (collisionInfo.block) {
                if ((collisionInfo.block->subBlockType() == CSolidBlock::Unhookable) || (collisionInfo.block->subBlockType() == CSolidBlock::Ice)) {
                    return setHookMissed();
                }
                else {
                    return setHooked(collisionInfo.point);
                }
            }
            else if (collisionInfo.avatar) {
                return setHooked(collisionInfo.avatar);
            }
        }

        if (m_currentLength == m_maxLength) {
            setHookMissed();
        }

        break;
    case HookEnd:
        m_currentLength -= dT * m_hookerFlySpeed;
        if (m_currentLength < 0) {
            m_currentLength = 0;
            m_hookVector.reset();
            m_state = HookNone;
        }
        else {
            m_hookVector = m_normalizedVector * m_currentLength;
        }
        break;
    case HookedAvatar:
        m_hookedPoint = m_hookedAvatar->center();
        if (m_hookedTimeRemains < 0) {
            m_state = HookEnd;
        }
        /* Fall-through by design */
    case HookedPoint:
        m_hookVector       = m_hookedPoint - avatar()->center();
        m_currentLength    = vectorsLength(m_hookVector);
        m_normalizedVector = normalizedVector(m_hookVector);
        break;
    }
}

void CHookAction::doImpact(const double &dT)
{
    if ((m_state != HookedPoint) && (m_state != HookedAvatar))
        return;

    PointF2D wantedSpeed = m_normalizedVector * m_rollingSpeed / 3;

    if (m_currentLength < m_hookMinimumLength) {
        wantedSpeed *= m_currentLength / m_hookMinimumLength;
    }

    if (m_avatar->gravityDirection() & (DirectionLeft|DirectionRight))
        wantedSpeed.setY(wantedSpeed.y() * 2);
    if (m_avatar->gravityDirection() & (DirectionUp|DirectionDown))
        wantedSpeed.setX(wantedSpeed.x() * 2);

    //    bool hookWantToReduce = (vectorsLength(normalizedVector(sourceSpeed) + center() - m_hookedPoint) >= m_hookCurrentLength);

    qreal maxCompensation = CWorldObject::CellLength * 10;
    qreal compensated = 0;

    PointF2D sourceSpeed = m_avatar->speed();

    if (wantedSpeed.x() > 0) {
        if (sourceSpeed.x() >= 0) {
            wantedSpeed.setX(wantedSpeed.x() - sourceSpeed.x());
            if (wantedSpeed.x() < 0)
                wantedSpeed.setX(0);
        }
        else {
            if (wantedSpeed.x() + sourceSpeed.x() < 0) { // Compensation (Want right, but moving left)
                if (wantedSpeed.x() + maxCompensation + sourceSpeed.x() < 0) {
                    wantedSpeed.setX(wantedSpeed.x() + maxCompensation);
                    compensated += -sourceSpeed.x() - wantedSpeed.x();
                }
                else {
                    compensated += wantedSpeed.x() + sourceSpeed.x();
                    wantedSpeed.setX(-sourceSpeed.x());
                }
            }
        }
    }
    else {
        if (sourceSpeed.x() <= 0) {
            wantedSpeed.setX(wantedSpeed.x() - sourceSpeed.x());
            if (wantedSpeed.x() > 0)
                wantedSpeed.setX(0);
        }
        else {
            if (wantedSpeed.x() + sourceSpeed.x() > 0) { // Compensation (Want left, but moving right)
                if (wantedSpeed.x() - maxCompensation + sourceSpeed.x() > 0) {
                    wantedSpeed.setX(wantedSpeed.x() - maxCompensation);
                    //                    compensated += -sourceSpeed.x() - wantedSpeed.x();
                }
                else {
                    //                    compensated += wantedSpeed.x() + sourceSpeed.x();
                    wantedSpeed.setX(-sourceSpeed.x());
                }
            }
        }
    }

    if (wantedSpeed.y() > 0) {
        if (sourceSpeed.y() >= 0) {
            wantedSpeed.setY(wantedSpeed.y() - sourceSpeed.y());
            if (wantedSpeed.y() < 0)
                wantedSpeed.setY(0);
        }
        else {
            if (wantedSpeed.y() + sourceSpeed.y() < 0) { // Compensation (Want down, but moving up)
                if (wantedSpeed.y() + maxCompensation + sourceSpeed.y() < 0)
                    wantedSpeed.setY(wantedSpeed.y() + maxCompensation);
                else {
                    wantedSpeed.setY(-sourceSpeed.y());
                }
            }
        }
    }
    else {
        if (sourceSpeed.y() <= 0) {
            wantedSpeed.setY(wantedSpeed.y() - sourceSpeed.y());
            if (wantedSpeed.y() > 0)
                wantedSpeed.setY(0);
        }
        else {
            if (wantedSpeed.y() + sourceSpeed.y() > 0) { // Compensation (Want up, but moving down)
                if (wantedSpeed.y() - maxCompensation + sourceSpeed.y() > 0)
                    wantedSpeed.setY(wantedSpeed.y() - maxCompensation);
                else {
                    wantedSpeed.setY(-sourceSpeed.y());
                }
            }
        }
    }

    if (m_state == HookedAvatar) {
        m_avatar->addExternalSpeedup(wantedSpeed / 2);
        m_hookedAvatar->addExternalSpeedup(-wantedSpeed / 2);
    }
    else {
        m_avatar->addExternalSpeedup(wantedSpeed);
    }
}

void CHookAction::freezed()
{
    m_hookedAvatar = 0;
    m_state    = HookNone;
}

float CHookAction::hookSpeed() const
{
    return m_hookerFlySpeed;
}

void CHookAction::setMaxLength(qreal hookMaxLength)
{
    m_maxLength = hookMaxLength;
}

void CHookAction::setHookAvatarMaxDuration(qreal newHookAvatarMaxDuration)
{
    m_hookedAvatarMaxDuration = newHookAvatarMaxDuration;
}

void CHookAction::setRollingSpeed(qreal newRollingSpeed)
{
    m_rollingSpeed = newRollingSpeed;
}

PointF2D CHookAction::hookEnd() const
{
    if (m_state == HookedPoint)
        return m_hookedPoint;
    else if (m_state == HookedAvatar)
        return m_hookedAvatar->center();

    return m_hookVector + m_avatar->center();
}

CHookAction::HookStates CHookAction::state() const
{
    return m_state;
}

void CHookAction::setHookMissed()
{
    m_state    = HookEnd;
    m_hookedAvatar = 0;

    setActionSuccess(false);
}

void CHookAction::setHooked(const PointF2D &where)
{
    m_hookedPoint       = where;
    m_hookVector        = m_hookedPoint - m_avatar->center();
    m_currentLength = vectorsLength(m_hookVector);
    m_state         = HookedPoint;

    setActionSuccess(true);
}

void CHookAction::setHooked(CAvatar *whom)
{
    m_hookedAvatar      = whom;
    m_hookVector        = whom->center() - m_avatar->center();
    m_currentLength = vectorsLength(m_hookVector);
    m_hookedTimeRemains = m_hookedAvatarMaxDuration;
    m_state         = HookedAvatar;

    setActionSuccess(true);
}

PointF2D CHookAction::hookedAvatarSpeedup() const
{
    return PointF2D();
//    qreal oldDistance = vectorsLength(position() - m_hookedAvatar->position());
//    qreal newDistance = vectorsLength(position() + speed() - m_hookedAvatar->position() - m_hookedAvatar->speed());

//    if (newDistance > oldDistance)
//        return speed() - m_hookedAvatar->speed() -m_hookVectorNormalized * m_hookRollingSpeed;
//    else
//        return -m_hookVectorNormalized * m_hookRollingSpeed;
}

PointF2D CHookAction::hookedAvatarAcceleration() const
{
    return PointF2D();
    return -m_normalizedVector * m_hookRollingAcceleration;
}
