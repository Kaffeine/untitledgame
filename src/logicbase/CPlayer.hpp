#ifndef CPLAYER_HPP
#define CPLAYER_HPP

#include <QObject>
#include <QPointF>

#include "Points2D.hpp"

class CAvatar;
class CGameNetwork;

class CPlayer : public QObject
{
    friend class CGameNetwork;

    Q_OBJECT
public:
    enum PlayerActions {
        ActionLeft    = 0b000000001,
        ActionRight   = 0b000000010,
        ActionUp      = 0b000000100,
        ActionDown    = 0b000001000,
        ActionJump    = 0b000010000,
        ActionAction  = 0b000100000,
        ActionAltAction = 0b001000000,
        ActionSuicide = 0b010000000,
        ActionEmote   = 0b100000000
    };

    explicit CPlayer(QObject *parent = 0);

    inline CAvatar *avatar() const { return m_avatar; }
    void setAvatar(CAvatar *newAvatar);

    void applyActions();

    void doAction(PointF2D destinationPoint);
    void doAltAction(PointF2D destinationPoint);
    void cancelAction();
    void cancelAltAction();

    PointF2D lookDestination() const { return m_lookDestination; }
    void setLookDestination(const PointF2D &newLookDest);
    inline PointF2D actionDestination() const { return m_act1DestinationPoint; }
    inline PointF2D altActionDestination() const { return m_act2DestinationPoint; }

    void doSuicide();

    void setCheckpointsCount(int newCheckpointsCount);

    bool isFinished();

    inline QString name() const { return m_name; }
    inline quint16 actions() const { return m_actions; }

signals:
    void started();
    void finished();
    void checkpointPassed(int checkpointId);
    void avatarChanged(CAvatar *newAvatar);
    void actionsChanged(quint16 newActions);
    void lookDestinationChanged();

public slots:
    void setStarted();
    void setFinished();
    void setCheckpointPassed(int checkpointId);

private slots:
    void whenMoved();
    void whenJumped();
    void setNullAvatar();

protected:
    void protectedDoActionHelper(PlayerActions action, const PointF2D &dest);
    void protectedCancelActionHelper(PlayerActions action);

    virtual void beforeSetAvatarEvent(CAvatar *newAvatar);
    virtual void startedEvent();
    virtual void finishedEvent();
    virtual void checkpointPassedEvent(int checkpointId);
    virtual void movedEvent();
    virtual void jumpedEvent();

    virtual void doActionEvent(PlayerActions action, const PointF2D &dest);
    virtual void caneclActionEvent(PlayerActions action);

    quint16 m_actions;

    PointF2D m_act1DestinationPoint;
    PointF2D m_act2DestinationPoint;

    PointF2D m_lookDestination;
    int m_nextCheckpoint;
    int m_checkpointsCount;
    CAvatar *m_avatar;
    bool m_finished;
    QString m_name;

};

#endif // CPLAYER_HPP
