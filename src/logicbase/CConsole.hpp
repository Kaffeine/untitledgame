#ifndef CCONSOLE_HPP
#define CCONSOLE_HPP

#include <QObject>
#include <QStringList>

struct SConsoleCommands {
    QObject *object;
    QString slot;
    QString command;
};

class CConsole : public QObject
{
    Q_OBJECT
public:
    explicit CConsole(QObject *parent = 0);
    bool addConsoleObject(QObject *newObject, QString name);
    bool addConsoleCommand(QObject *executer, QString slot, const QString &command);
    inline QStringList history() const { return m_commandsHistory; }
    QString historyAtNumFromEnd(int num) const;
    QObject *objectByName(const QString &objName) const;

    QStringList autocomplete(const QString &fromStr) const;

signals:
    void output(QString outputText);
    
public slots:
    bool executeCommand(QString line);

private slots:
    bool cmd_ls(QString line);
    bool cmd_get(QObject *target, QString propertyName);
    bool cmd_set(QObject *target, QString propertyName, QString propertyValue);

private:
    void fillBlacklist();
    QStringList objectsSlots(QObject *targetObj) const;
//    inline QStringList objectsSlots(const QString &objName) const { return objectsSlots(objectByName(objName)); }
    QList<QObject *> m_accessibleObjects;
    static const QLatin1String m_commandGetProperty;
    static const QLatin1String m_commandSetProperty;
    QStringList m_slotsBlacklist;
    QList<SConsoleCommands> m_commandsList;
    QStringList m_commandsHistory;
    const static int m_commandsHistoryLimit = 32;

    QStringList m_1stLevelList;

};

#endif // CCONSOLE_HPP
