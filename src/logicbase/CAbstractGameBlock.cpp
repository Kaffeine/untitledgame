#include "CAbstractGameBlock.hpp"
#include "CExtendedArray.hpp"
#include "Functions.hpp"

/* Note: after subclassing add entry to CBlocksFactory. */
CAbstractGameBlock::CAbstractGameBlock() :
    CWorldObject(),
    m_isVisible(true),
    m_layer(0)
{
    /* It's hack. Constructor can't call overloaded functions,
     * so subitemsCount() do not provide real information */
    m_subBlockType = 255;
}

void CAbstractGameBlock::setSubBlockType(quint16 subblockId)
{
    if ((subblockId >= subBlockTypesCount()) || (m_subBlockType == subblockId))
        return;

    m_subBlockType = subblockId;
    subblockChangedEvent();
}

PointI2D CAbstractGameBlock::cell() const
{
    return m_cell;
}

PointF2D CAbstractGameBlock::cellPosition() const
{
    return cellToPos(cell());
}

RectF2D CAbstractGameBlock::boundingRect() const
{
    return m_boundingRect;
}

bool CAbstractGameBlock::containPoint(const PointF2D &point) const
{
    if ((point.x() < 0) || (point.x() > width()) || (point.y() < 0) || (point.y() > height()))
        return false;

    if (direction() == DirectionNone)
        return true;
    else
        return shortCoordinatesIsInDirection(point, direction());

    /* Whats about DirectionExtra? */
}

void CAbstractGameBlock::setDirection(Directions newDirection)
{
    m_direction = newDirection;

    switch (newDirection) {
    default:
    case DirectionNone:
        m_width  = CellLength;
        m_height = CellLength;
        break;
    case DirectionUp:
        m_width  = CellLength;
        m_height = CellLength / 2;
        break;
    case DirectionDown:
        m_width  = CellLength;
        m_height = CellLength / 2;
        break;
    case DirectionLeft:
        m_width  = CellLength / 2;
        m_height = CellLength;
        break;
    case DirectionRight:
        m_width  = CellLength / 2;
        m_height = CellLength;
        break;
    }

    _setBoundingRect(RectF2D(0, 0, m_width, m_height));
    _updatePosFromCell(); /* Cell depend on direction */

    blocksDirectionChangedEvent();
}

void CAbstractGameBlock::setCell(const PointI2D &newCell)
{
    m_cell = newCell;

    _updatePosFromCell();
}

void CAbstractGameBlock::setCellByPos(const PointF2D &srcPos)
{
    setCell(posToCell(srcPos));
}

/* DirectionDown there mean that avatars bottom is on item's top. */
void CAbstractGameBlock::touchedEvent(CAvatar *avatar, Directions touchDirection, qreal length)
{
    Q_UNUSED(avatar)
    Q_UNUSED(touchDirection)
    Q_UNUSED(length)
}

void CAbstractGameBlock::_updatePosFromCell()
{
    if (m_direction == DirectionRight)
        setPosition(m_cell.x() * CellLength + CellLength / 2, m_cell.y() * CellLength);
    else if (m_direction == DirectionDown)
        setPosition(m_cell.x() * CellLength, m_cell.y() * CellLength + CellLength / 2);
    else
        setPosition(m_cell * CellLength);
}

void CAbstractGameBlock::_setBoundingRect(RectF2D newBoundingRect)
{
    if (m_boundingRect != newBoundingRect) {
        m_boundingRect = newBoundingRect;
        boundingRectChangedEvent();
    }
}

bool CAbstractGameBlock::read(CExtendedArray *source)
{
    m_cell.setX(source->readQUInt32());
    m_cell.setY(source->readQUInt32());

    setDirection((Directions) source->readQUInt8()); /* _updatePosFromCell() called in setDirection(). */
    setSubBlockType(source->readQUInt16());
    return readEvent(source);
}

bool CAbstractGameBlock::write(CExtendedArray *dest) const
{
    dest->writeQUInt32(m_cell.x());
    dest->writeQUInt32(m_cell.y());
    dest->writeQUInt8(m_direction);
    dest->writeQUInt16(subBlockType());
    return writeEvent(dest);
}

void CAbstractGameBlock::setLayer(CLayer *pLayer)
{
    m_layer = pLayer;
}

void CAbstractGameBlock::positionChangedEvent()
{
    m_cell = posToCell(m_position);
}

void CAbstractGameBlock::setZ(qreal z)
{
    m_z = z;
    /* TODO: zChangedEvent()? */
}
