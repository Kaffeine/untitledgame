#ifndef CWORLD_HPP
#define CWORLD_HPP

#include <QObject>
#include "STouchEvent.hpp"
#include "CollisionsHelper.hpp"

#include "Directions.hpp"
#include "Points2D.hpp"
#include "Rects2D.hpp"

class QTime;
class QTimer;
class QFile;
class QTextStream;

class CLayer;
class CAvatar;
class CWorldObject;
class CExtendedArray;
class CAbstractGameBlock;

struct SAvatarsTransition {
    CAvatar *who;
    QString whereto;
};

struct SWorldsObjectCollision {
    CWorldObject *object;
    CAvatar *avatar;
    CAbstractGameBlock *block;
    qreal length;
    PointF2D point;
};

typedef QList < CWorldObject * > CWorldObjectsList;

class CWorld : public QObject
{
    Q_OBJECT
public:
    explicit CWorld(QObject *parent = 0);
    virtual ~CWorld();
    void reset();
    inline int minX() const { return m_minX; }
    inline int minY() const { return m_minY; }
    inline int maxX() const { return m_maxX; }
    inline int maxY() const { return m_maxY; }
    CLayer *layer(int n) const;
    inline int layersCount() { return m_layers.count(); }
    inline int checkpointsCount() const { return m_checkpointsCount; }
    inline QString signature() const { return m_signature; }
    inline QString nextWorldSignature() const { return m_nextWorldSignature; }
    inline quint32 time() const { return m_worldMSec; }
    void setTime(quint32 newTime);

    CAvatar *avatarAt(int id) const;
    inline int avatarId(CAvatar *pAvatar) const { return m_avatars.indexOf(pAvatar); }
    inline int avatarsCount() const { return m_avatars.count(); }

    CAbstractGameBlock *blockAt(const PointF2D &coord, int layerN) const;
    inline CAbstractGameBlock *blockAt(qreal x, qreal y, int layerN) const { return blockAt(PointF2D(x, y), layerN); }

    bool isActive() const;

    inline QString title() const { return m_title; }

    inline RectF2D rect() const { return RectF2D(m_minX, m_minY, m_maxX - m_minX, m_maxY - m_minY); }

    CAbstractGameBlock *spawnForAvatar(CAvatar *pAvatar = 0) const;

    void setGravityDirection(Directions newDirection);
    inline Directions gravityDirection() const { return m_gravityDirection; }
    float gravityMass() const { return m_gravityMass; }

    void getFirstCollision(const PointF2D &begin, const PointF2D &end, SWorldsObjectCollision *description, CWorldObject *objToIgnore = 0) const;

signals:
    void aboutReloadBegin();
    void aboutReload();
    void avatarSpawned(CAvatar *pAvatar);
    void avatarLeaveWorld(CAvatar *pAvatar, QString destWorld);
    void avatarRemovedBegin(CAvatar *pAvatar);
    void avatarRemoved(CAvatar *pAvatar);
    void signatureChanged(QString newTitle);

public slots:
    bool save(CExtendedArray *dest);
    bool load(const QString &signature, CExtendedArray *source);

    CAvatar *spawnAvatar();
    bool addAvatar(CAvatar *pAvatar);
    void removeAvatar(CAvatar *pAvatar);
    void deleteAvatar(CAvatar *pAvatar);
    void start(int initialTick = 0);
    void stop();
    void setPaused(bool paused);
    inline void togglePause() { setPaused(!isActive()); }
    void setSignature(const QString &signature);
    void setNextWorldSignature(const QString &signature);
    void setTitle(QString newTitle);

    void startRecording() { }
    void stopRecording() { }
    void playRecord() { }

    void playRecord(const QString &fileName) { Q_UNUSED(fileName); }
    bool dumpRecord(const QString &fileName) { Q_UNUSED(fileName); return false; }

private slots:
    void gameTick();

    void whenAvatarSuicide(CAvatar *who);

private:
    void initLog();
    void resetInternally();
    void addAvatarToLeavingQueue(CAvatar *who, QString destWorld);
    void processAvatarsLeaving();
    void processCurrentEvents();
    void processAvatarsAction(CAvatar *currentAvatar);
    void processAvatarsHookInProgress(CAvatar *who) const;
    void playTick() { }
    void doRecording() { }
    QList<SCollisionDescription> getAvatarsToBlocksCollision(qreal dTime) const;
    bool getFirstCollisionHelper(const PointF2D &begin, const PointF2D &end, SWorldsObjectCollision *description, const CWorldObjectsList &objectsList) const;
    void doTouches(CAvatar *avatar, const QList<STouchEvent> &touchesList) const;
    void shiftTime(qreal dTime, int fixAvatarId = -1, const SCollisionDescription &description = SCollisionDescription());
    static const int ticksPerSecond;

    QString m_signature;
    QString m_nextWorldSignature;

    int m_minX;
    int m_minY;
    int m_maxX;
    int m_maxY;
    QList<CLayer *> m_layers;
    QList<CAvatar *> m_avatars;
    QTimer *m_ticksTrigger;
    int m_checkpointsCount;
    QTime *m_worldTime;
    int m_worldMSec;

    CExtendedArray *m_recordBuffer;
    bool m_doRecording;
    bool m_doPlaying;
    int m_recordingFPS;

    QList<SAvatarsTransition> m_avatarsTransitions;

    // Meta-info, such as title and author.
    QString m_title;

    QFile *m_logFile;
    bool m_logEnabled;
    QTextStream *m_logStream;

    Directions m_gravityDirection;
    float m_gravityMass;

};

#endif // CWORLD_HPP
