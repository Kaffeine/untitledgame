#ifndef CABSTRACTAVATARACTION_HPP
#define CABSTRACTAVATARACTION_HPP

#include <QObject>

#include "Points2D.hpp"

class CAvatar;

class CAbstractAvatarAction : public QObject
{
    Q_OBJECT
public:
    explicit CAbstractAvatarAction(CAvatar *avatar);

    virtual void activate();
    virtual void deactivate();
    virtual void freezed();

    PointF2D actionVector() const;
    virtual void doAction(const PointF2D &dest); // Destination is in world coordinates
    virtual void cancelAction() { }

    virtual void shiftTime(const double &dT);
    virtual void doImpact(const double &dT) { Q_UNUSED(dT); }

    virtual PointF2D applySpeed(const PointF2D &baseSpeed) { return baseSpeed; }

    void setActionSelected(bool newSelected);

    double successTimeout() const { return m_successTimeout; }
    double failTimeout() const { return m_failTimeout; }

    float radius() const { return m_radius; }

    inline CAvatar *avatar() const { return m_avatar; }

signals:

public slots:

protected:
    bool prepareAction(const PointF2D &dest);
    void setActionSuccess(bool success);

    CAvatar *m_avatar;
    PointF2D m_actionDestination;
    double m_actionTimeout;

    double m_successTimeout;
    double m_failTimeout;

    float m_radius;
    bool m_pending;

};

#endif // CABSTRACTAVATARACTION_HPP
