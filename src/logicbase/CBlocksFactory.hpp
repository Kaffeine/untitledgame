#ifndef CBLOCKSFACTORY_HPP
#define CBLOCKSFACTORY_HPP

class CAbstractGameBlock;

class CBlocksFactory
{
public:
    CBlocksFactory();
    static CAbstractGameBlock *newGameBlock(int type, int index = 0);
    static CAbstractGameBlock *cloneBlock(CAbstractGameBlock *pBlock);
    static int subblocksCount(int type);
};

#endif // CBLOCKSFACTORY_HPP
