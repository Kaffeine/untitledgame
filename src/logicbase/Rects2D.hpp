#ifndef RECTS2D_HPP
#define RECTS2D_HPP

#include "Points2D.hpp"

class RectI2D
{
public:
    inline RectI2D();
    RectI2D(const PointI2D &point1, const PointI2D &point2);
};

class RectF2D
{
public:
    inline RectF2D();
    inline RectF2D(const PointF2D &pos, const PointF2D &size);
    inline RectF2D(const float &x, const float &y, const float &width = 0, const float &height = 0);
    friend inline bool operator==(const RectF2D &, const RectF2D &);
    friend inline bool operator!=(const RectF2D &, const RectF2D &);

    inline float x() const;
    inline float y() const;
    inline float width() const;
    inline float height() const;

    inline float left() const;
    inline float right() const;
    inline float top() const;
    inline float bottom() const;

    inline PointF2D position() const;
    inline PointF2D size() const;

    void clamp(PointF2D *point) const;

    static RectF2D fromTwoPoints(const PointF2D &point1, const PointF2D &point2);

private:
    PointF2D m_position;
    PointF2D m_size;

};

inline RectF2D::RectF2D() { }
inline RectF2D::RectF2D(const PointF2D &pos, const PointF2D &size) { m_position = pos; m_size = size; }
inline RectF2D::RectF2D(const float &x, const float &y, const float &width, const float &height) { m_position = PointF2D(x, y); m_size = PointF2D(width, height); }
inline float RectF2D::x() const { return m_position.x(); }
inline float RectF2D::y() const { return m_position.y(); }
inline float RectF2D::width()  const { return m_size.x(); }
inline float RectF2D::height() const { return m_size.y(); }

inline float RectF2D::left() const { return x(); }
inline float RectF2D::right() const { return x() + width(); }
inline float RectF2D::top() const { return y(); }
inline float RectF2D::bottom() const { return y() + height(); }

inline PointF2D RectF2D::position() const { return m_position; }
inline PointF2D RectF2D::size() const { return m_size; }

inline bool operator==(const RectF2D &r1, const RectF2D &r2) { return (r1.m_position == r2.m_position) && (r1.m_size == r2.m_size); }
inline bool operator!=(const RectF2D &r1, const RectF2D &r2) { return (r1.m_position != r2.m_position) || (r1.m_size != r2.m_size); }

#endif // RECTS2D_HPP
