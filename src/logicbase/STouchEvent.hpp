#ifndef STOUCHEVENT_HPP
#define STOUCHEVENT_HPP

class CAbstractGameBlock;
class CWorldObject;

struct STouchEvent {
    CAbstractGameBlock *item;
    qreal touchLengthX;
    qreal touchLengthY;
};

#endif // STOUCHEVENT_HPP
