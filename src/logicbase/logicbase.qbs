import qbs 1.0

Product {
    Depends { name: "extendedarray" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core"] }
    name: "logicbase"
    type: "dynamiclibrary"

    cpp.includePaths: [
        "../../libs/CExtendedArray"
    ]

    Group {
        name: "files"
        files: [
            "CPlayer.cpp",
            "CPlayer.hpp",
            "CAvatar.cpp",
            "CAvatar.hpp",
            "CWorld.cpp",
            "CWorld.hpp",
            "CWorldUtils.cpp",
            "CWorldUtils.hpp",
            "CWorldObject.cpp",
            "CWorldObject.hpp",
            "CLayer.cpp",
            "CLayer.hpp",
            "CBlocksFactory.cpp",
            "CBlocksFactory.hpp",
            "CAbstractGameBlock.cpp",
            "CAbstractGameBlock.hpp",
            "Functions.cpp",
            "Functions.hpp",
            "CollisionsHelper.cpp",
            "CollisionsHelper.hpp",
            "CConsole.cpp",
            "CConsole.hpp",
            "CAvatarAppearance.cpp",
            "CAvatarAppearance.hpp"
        ]
    }
    Group {
        name: "mapBlocks"
        prefix: name + '/'

        files: [
            "CSolidBlock.cpp",
            "CSolidBlock.hpp",
            "CSpawnExitBlock.cpp",
            "CSpawnExitBlock.hpp",
            "CStartFinishBlock.cpp",
            "CStartFinishBlock.hpp",
            "CTeleBlock.cpp",
            "CTeleBlock.hpp",
            "CGravityModBlock.cpp",
            "CGravityModBlock.hpp",
            "CAccelerationBlock.cpp",
            "CAccelerationBlock.hpp",
            "CFreezeBlock.cpp",
            "CFreezeBlock.hpp"
        ]
    }
}
