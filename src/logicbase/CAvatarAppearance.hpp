#ifndef CAVATARAPPEARANCE_HPP
#define CAVATARAPPEARANCE_HPP

#include <QObject>

#include "Points2D.hpp"

/* CAvatarAppearance is not really base part of game logic, but used in every
 * game-related binary to know how each avatar looks like (skin, colors,
 * eye-direction, etc).
 *
 * CAvatarAppearance can exists without CAvatar as means to store appearance. */

class CAvatarAppearance : public QObject
{
    Q_OBJECT
public:
    explicit CAvatarAppearance(QObject *parent = 0);
    inline QString color() const { return m_color; }
    inline PointF2D eyeDestination() const { return m_eyeDestination; }
    inline float eyesSize() const { return m_eyesSize; }
    inline int eyesCount() const { return m_eyesCount; }
    PointF2D positionOfEye(int eye = 0) const; // Position of center of eye, relative to avatar's center.

    inline float footsSize() const { return m_footsSize; }
    inline int footsCount() const { return m_footsCount; }

    void copyAppearance(CAvatarAppearance *source);
    void connectToAppearance(CAvatarAppearance *source);
    void disconnectFromAppearance(CAvatarAppearance *source);
    void reset();

    static const int maxEyesCount = 4;

    void updateEyesPosition();
signals:
    void colorChanged(const QString &newColor);

    void eyesSizeChanged(float newSize);
    void eyesCountChanged(int newCount);

    void footsSizeChanged(float newSize);
    void footsCountChanged(int newCount);

public slots:
    void setColor(const QString &newColor);
    void setEyeDestination(const PointF2D &newDestination); // Relative to avatar's center.
    void setEyesCount(int newEyesCount);
    void setEyesSize(float newEyesSize);

    void setFootsCount(int newFootsCount);
    void setFootsSize(float newFootsSize);

private:
    QString m_color;
    QString m_borderColor;
    PointF2D m_eyeDestination;
    float m_eyesSize;
    PointF2D m_positionOfEyes[maxEyesCount];
    int m_eyesCount;

    float m_footsSize;
    int m_footsCount;

};

#endif // CAVATARAPPEARANCE_HPP
