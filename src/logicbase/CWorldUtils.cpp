#include "CWorldUtils.hpp"

#include "CWorld.hpp"
#include "CExtendedArray.hpp"

#include <QFile>
#include <QDir>
#include <QDebug>

const QString CWorldUtils::fileExtension = QLatin1String(".map");
const QString CWorldUtils::fileSignature = QLatin1String("UntitledWorld");

/* Wrapper around CWorld.
 * Purpose: logic independent things, such as files IO, checksums and meta-information. */

bool CWorldUtils::loadWorldByFilename(CWorld *pWorld, QString fileName)
{
    qDebug() << tr("Load world from file: %1").arg(fileName);

    if (fileName.isEmpty())
        return false;

    fileName = fixedFilename(fileName);

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << tr("Count not open file: %1").arg(fileName);
        return false;
    }

    CExtendedArray inputBuffer = file.readAll();
    file.close();

    if (inputBuffer.size() <= fileSignature.size()) {
        qDebug() << tr("File is too small to be a world of this game.");
        return false;
    }

    QByteArray currentFileSignature = inputBuffer.readBytes(fileSignature.size());
    if (currentFileSignature != fileSignature) {
        qDebug() << tr("File is not compatible world (invalid signature).");
        return false;
    }

    QString signature = inputBuffer.readQString();

    return pWorld->load(signature, &inputBuffer);
}

bool CWorldUtils::saveAs(CWorld *pWorld, QString fileName)
{
    qDebug() << tr("Save world to file: %1").arg(fileName);

    if (fileName.isEmpty())
        return false;

    fileName = fixedFilename(fileName);

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
        return false;

    CExtendedArray outBuffer;
    if (!saveAsBuffer(pWorld, &outBuffer)) {
        file.close();
        return false;
    }

    file.write(outBuffer);
    file.close();

    return true;
}

bool CWorldUtils::saveAsBuffer(CWorld *pWorld, CExtendedArray *buffer)
{
    buffer->writeBytes(fileSignature.toLocal8Bit().constData(), fileSignature.size());
    buffer->writeQString(pWorld->signature());

    return pWorld->save(buffer);
}

bool CWorldUtils::isWorldLoadable(QString fileName)
{
    if (fileName.isEmpty())
        return false;

    fileName = fixedFilename(fileName);

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return false;

    CExtendedArray inputBuffer = file.readAll();
    file.close();

    /* TODO: Checksum */
    if (inputBuffer.count() > 0)
        return true;

    return false;
}

bool CWorldUtils::isWorldsSignatureAvailable(QString signature)
{
    return isWorldLoadable(signature);
}

bool CWorldUtils::saveBufferAsFile(const QByteArray &buffer, QString fileName)
{
    QDir dirForFile(fileName.left(fileName.lastIndexOf("/")));
    if (!dirForFile.exists()) {
        if (QDir::isRelativePath(fileName))
            dirForFile.mkpath(".");
        else
            return false;
    }

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
        return false;

    file.write(buffer);
    file.close();
    return true;
}

QString CWorldUtils::fixedFilename(QString fileName)
{
    if (fileName.isEmpty())
        return fileName;

    static QString dir("maps/");
    if (!fileName.contains(dir))
        fileName = dir + fileName;

    if (!fileName.endsWith(fileExtension))
        fileName.append(fileExtension);

    return fileName;
}

bool CWorldUtils::loadWorldBySignature(CWorld *pWorld, QString signature)
{
    return loadWorldByFilename(pWorld, signature);
}
