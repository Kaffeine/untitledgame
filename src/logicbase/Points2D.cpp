#include "Points2D.hpp"
#include <qmath.h>

float PointF2D::length() const
{
    return qSqrt(m_x * m_x + m_y * m_y);
}

PointF2D PointF2D::normalized()
{
    if (isNull())
        return PointF2D();

    float len = length();
    return PointF2D(m_x / len, m_y / len);
}
