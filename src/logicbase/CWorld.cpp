#include "CWorld.hpp"
#include "CExtendedArray.hpp"
#include "CLayer.hpp"
#include "BlocksTypes.hpp"
#include "CAvatar.hpp"
#include "CAbstractGameBlock.hpp"

#include "mapBlocks/CSpawnExitBlock.hpp"

/* Needed for hook */
#include "mapBlocks/CSolidBlock.hpp"

//#include "CWorldObject.hpp"
#include "Functions.hpp"

#include <QTimer>
#include <QDebug>
#include <qmath.h>
#include <QTime>

#include <QFile>
#include <QDir>

const int CWorld::ticksPerSecond = 100;

const qreal c_minimalStep = 0.00001;

/* This is pure logical class.
 * Process CLayers with CAbstractGameBlocks and CAvatar.
 * Provide save/load capability */
CWorld::CWorld(QObject *parent) :
    QObject(parent),
    m_recordBuffer(0),
    m_doRecording(false),
    m_doPlaying(false)
{
    QDir dir;
    dir.mkdir("logs");
    m_logEnabled = false;
    m_logFile   = new QFile(this);
    m_logStream = new QTextStream(m_logFile);

    for (int i = 0; i < BlocksTypesCount; ++i) {
        m_layers.append(new CLayer(this));
        m_layers.last()->m_layerType = i;
        m_layers.last()->setWorld(this);
    }

    m_worldTime    = new QTime();
    m_ticksTrigger = new QTimer(this);
    m_ticksTrigger->setInterval(1000 / ticksPerSecond);
    m_ticksTrigger->setSingleShot(false);
    connect(m_ticksTrigger, SIGNAL(timeout()), SLOT(gameTick()));

    m_gravityDirection = DirectionDown;
    m_gravityMass = CWorldObject::CellLength * 36;
}

CWorld::~CWorld()
{
    delete m_logStream;
    delete m_logFile;
    delete m_worldTime;
}

void CWorld::reset()
{
    resetInternally();
    emit aboutReload();
}

void CWorld::resetInternally()
{
    m_title = "nameless world";
    m_maxX = 0;
    m_maxY = 0;
    m_nextWorldSignature.clear();
    m_checkpointsCount = 0;

    for (int i = 0; i < m_layers.count(); ++i) {
        m_layers[i]->clear();
    }

    for (int i = 0; i < m_avatars.count(); ++i)
        delete m_avatars[i];
    m_avatars.clear();

    m_minX = 0xffffff; // Not super magic, just some big values
    m_minY = 0xffffff;
    m_maxX = 0;
    m_maxY = 0;
}

bool CWorld::save(CExtendedArray *dest)
{
    dest->writeQString(m_title);
    dest->writeQUInt32(m_maxX);
    dest->writeQUInt32(m_maxY);
    dest->writeQString(m_nextWorldSignature);
    dest->writeQUInt32(m_layers.count());

    for (int i = 0; i < m_layers.count(); ++i) {
        m_layers.at(i)->write(dest);
    }

    return true;
}

void CWorld::initLog()
{
    m_logFile->close();
    m_logFile->setFileName("logs/" + m_signature + ".log");
    m_logFile->open(QIODevice::WriteOnly);

    *m_logStream << "World's log." << endl;
    *m_logStream << "Signature: " << m_signature << endl;
}

bool CWorld::load(const QString &signature, CExtendedArray *source)
{
    emit aboutReloadBegin();

    if (m_signature != signature) {
        m_signature = signature;
        emit signatureChanged(m_signature);
    }

    if (m_logEnabled)
        initLog();

    m_title = source->readQString();
    source->readQUInt32(); // Width. Ignored at this time
    source->readQUInt32(); // Height. Ignored at this time
    m_nextWorldSignature = source->readQString();
    source->readQUInt32(); // Layers count. Ignored at this time

    for (int i = 0; i < m_layers.count(); ++i)
        m_layers.at(i)->read(source);

    m_minX = layer(BlockSolid)->minX();
    m_minY = layer(BlockSolid)->minY();
    m_maxX = layer(BlockSolid)->maxX();
    m_maxY = layer(BlockSolid)->maxY();

    for (int i = 0; i < layer(BlockStart)->blocksCount(); ++i) {
        CAbstractGameBlock *currentBlock = layer(BlockStart)->blockAt(i);
        if (currentBlock->subBlockType() > 1) {
            if (m_checkpointsCount < currentBlock->subBlockType() - 1) {
                m_checkpointsCount = currentBlock->subBlockType() - 1;
            }
        }
    }

    emit aboutReload();

    return true;
}

CLayer *CWorld::layer(int n) const
{
    if ((n >= 0) && (n < m_layers.count()))
        return m_layers.at(n);

    return 0;
}

void CWorld::setTime(quint32 newTime)
{
    m_worldMSec = newTime;
}

CAvatar *CWorld::avatarAt(int id) const
{
    if ((id >= 0) && (id < m_avatars.count()))
        return m_avatars.at(id);

    return 0;
}

CAbstractGameBlock *CWorld::blockAt(const PointF2D &coord, int layerN) const
{
    CLayer *choosedLayer = layer(layerN);
    if (!choosedLayer)
        return 0;

    return choosedLayer->blockAtCoord(coord);
}

void CWorld::doTouches(CAvatar *avatar, const QList<STouchEvent> &touchesList) const
{
    /* Blocks will correct gravity on touch */
    for (int i = 0; i < touchesList.count(); ++i) {
        STouchEvent curTouchEvent = touchesList.at(i);

        if ((curTouchEvent.touchLengthX > 0) && (curTouchEvent.touchLengthY > 0)) {
            /* It is impossible to touch several edges of same item. Corner-collision case, so fix position. */

            if (m_logEnabled) {
                *m_logStream << QString("Imposible touch (%1,%2) at %3x%4")
                                .arg(curTouchEvent.touchLengthX).arg(curTouchEvent.touchLengthY)
                                .arg(avatar->position().x()).arg(avatar->position().y()) << endl;
            }

            if (curTouchEvent.touchLengthX > curTouchEvent.touchLengthY) {
                /* Revert Y motion. */
                /* Is avatar closer to item minY, that to item maxY? */
                if (qAbs(curTouchEvent.item->minY() - avatar->maxY()) <
                        qAbs(curTouchEvent.item->maxY() - avatar->minY()))
                    avatar->setPosition(avatar->minX(), curTouchEvent.item->minY() - avatar->height());
                else
                    avatar->setPosition(avatar->minX(), curTouchEvent.item->maxY());

                if (curTouchEvent.item->minY() == avatar->maxY())
                    curTouchEvent.item->touchedEvent(avatar, DirectionDown, curTouchEvent.touchLengthX);
                else if (curTouchEvent.item->maxY() == avatar->minY())
                    curTouchEvent.item->touchedEvent(avatar, DirectionUp, curTouchEvent.touchLengthX);
            }
            else {
                /* Revert X motion */
                /* Is avatar closer to item minX, that to item maxX? */
                if (qAbs(curTouchEvent.item->minX() - avatar->maxX()) <
                        qAbs(curTouchEvent.item->maxX() - avatar->minX())) {
                    /* Avatar closer to item minY, that to item maxY */
                    avatar->setPosition(curTouchEvent.item->minX() - avatar->width(), avatar->minY());
                }
                else
                    avatar->setPosition(curTouchEvent.item->maxX(), avatar->minY());

                if (curTouchEvent.item->minX() == avatar->maxX())
                    curTouchEvent.item->touchedEvent(avatar, DirectionRight, curTouchEvent.touchLengthY);
                else if (curTouchEvent.item->maxX() == avatar->minX())
                    curTouchEvent.item->touchedEvent(avatar, DirectionLeft, curTouchEvent.touchLengthY);
            }

            if (m_logEnabled) {
                *m_logStream << QString("Solved to %1%2")
                                .arg(avatar->position().x()).arg(avatar->position().y()) << endl;
            }

            continue;
        }

        if (m_logEnabled) {
            *m_logStream << QString("Touch event with block at %1, %2, touchLength: %3/%4")
                            .arg(curTouchEvent.item->minX()).arg(curTouchEvent.item->minY())
                            .arg(curTouchEvent.touchLengthX).arg(curTouchEvent.touchLengthY) << endl;

            CWorldObject *pObjToDebug = avatar;

            *m_logStream << QString("Avatar geometry: %1-%2 x %3-%4")
                            .arg(pObjToDebug->minX()).arg(pObjToDebug->maxX())
                            .arg(pObjToDebug->minY()).arg(pObjToDebug->maxY()) << endl;

            pObjToDebug = curTouchEvent.item;

            *m_logStream << QString("Block geometry: %1-%2 x %3-%4")
                            .arg(pObjToDebug->minX()).arg(pObjToDebug->maxX())
                            .arg(pObjToDebug->minY()).arg(pObjToDebug->maxY()) << endl;
        }

        if (curTouchEvent.touchLengthX > 0) {
            if (curTouchEvent.item->minY() == avatar->maxY())
                curTouchEvent.item->touchedEvent(avatar, DirectionDown, curTouchEvent.touchLengthX);
            else if (curTouchEvent.item->maxY() == avatar->minY())
                curTouchEvent.item->touchedEvent(avatar, DirectionUp, curTouchEvent.touchLengthX);
        }
        else if (curTouchEvent.touchLengthY > 0) {
            if (curTouchEvent.item->minX() == avatar->maxX())
                curTouchEvent.item->touchedEvent(avatar, DirectionRight, curTouchEvent.touchLengthY);
            else if (curTouchEvent.item->maxX() == avatar->minX())
                curTouchEvent.item->touchedEvent(avatar, DirectionLeft, curTouchEvent.touchLengthY);
        }
    }
}

QList<SCollisionDescription> CWorld::getAvatarsToBlocksCollision(qreal dTime) const
{
    QList<SCollisionDescription> collisionsToReturn;

    for (int currentAvatarId = 0; currentAvatarId < m_avatars.count(); ++currentAvatarId) {
        SCollisionDescription currentAvatarFirstCollision;
        CAvatar *currentAvatar = m_avatars.at(currentAvatarId);

        PointF2D newPos = currentAvatar->supposedPosition(dTime);

        QList<CAbstractGameBlock *> possibleCollisionsList = layer(BlockSolid)->blocksInRect(newPos, newPos + currentAvatar->size());

        if (possibleCollisionsList.isEmpty()) {
            currentAvatarFirstCollision.time.setX(dTime + 1);
            currentAvatarFirstCollision.time.setY(dTime + 1);
            currentAvatarFirstCollision.point = newPos;
            collisionsToReturn.append(currentAvatarFirstCollision);
            continue; // Review next avatar
        }

        QList<SCollisionDescription> collisionsOfCurrentAvatar = getCollisionsDescriptions(possibleCollisionsList, currentAvatar->position(), newPos, currentAvatar->size(), dTime);

        collisionsToReturn.append(getFirstCollisionFromList(collisionsOfCurrentAvatar));
    }

    return collisionsToReturn;
}

void CWorld::getFirstCollision(const PointF2D &begin, const PointF2D &end, SWorldsObjectCollision *description, CWorldObject *objToIgnore) const
{
    description->avatar = 0;
    description->block  = 0;
    description->object = 0;
    description->length = 0;

    CWorldObjectsList objsList;

    /* Check for collision with blocks */
    QList<CAbstractGameBlock *> blocksList = layer(BlockSolid)->blocksInRect(begin, end);
    for (int i = 0; i < blocksList.count(); ++i) {
        if (blocksList.at(i) == objToIgnore)
            continue;

        if (blocksList.at(i)->subBlockType() != CSolidBlock::Hookthrow)
            objsList.append(blocksList.at(i));
    }

    if (getFirstCollisionHelper(begin, end, description, objsList)) {
        description->block  = (CAbstractGameBlock *) description->object;
        description->avatar = 0;
    }

    objsList.clear();

    for (int i = 0; i < m_avatars.count(); ++i) {
        if (m_avatars.at(i) == objToIgnore)
            continue;

        objsList.append(m_avatars.at(i));
    }

    if (getFirstCollisionHelper(begin, end, description, objsList)) {
        description->block  = 0;
        description->avatar = (CAvatar *) description->object;
    }
}

bool CWorld::getFirstCollisionHelper(const PointF2D &begin, const PointF2D &end, SWorldsObjectCollision *description, const CWorldObjectsList &objectsList) const
{
    bool betterCollisionFounded = false;

    PointF2D point;
    qreal collisionLength;
    qreal maxDistance = vectorsLength(end - begin);

    if (!objectsList.isEmpty()) {
        for (int i = 0; i < objectsList.count(); ++i) {
            collisionLength = collisionTest(begin, end - begin, objectsList.at(i), &point);
            if ((collisionLength > 0) && (collisionLength <= maxDistance)) {
                if (!description->object || (description->length > collisionLength)) {
                    betterCollisionFounded = true;

                    description->object = objectsList.at(i);
                    description->point  = point;
                    description->length = collisionLength;
                }
            }
        }
    }

    return betterCollisionFounded;
}

void CWorld::processAvatarsAction(CAvatar *currentAvatar)
{

}

void CWorld::processCurrentEvents()
{
    if (m_logEnabled) {
        *m_logStream << QString("processCurrentEvents") << endl;
        *m_logStream << QString("cleanup") << endl;
    }

    /* Tick cleans */
    for (int currentAvatarId = 0; currentAvatarId < m_avatars.count(); ++currentAvatarId) {
        m_avatars.at(currentAvatarId)->cleanTouches();
        m_avatars.at(currentAvatarId)->cleanExternalForces();

        if (m_logEnabled)
            *m_logStream << QString("Avatar %1 is in position (%2,%3)").arg(currentAvatarId)
                            .arg(m_avatars.at(currentAvatarId)->position().x())
                            .arg(m_avatars.at(currentAvatarId)->position().y()) << endl;
    }

    /* Tick dirty */
    for (int currentAvatarId = 0; currentAvatarId < m_avatars.count(); ++currentAvatarId) {
        CAvatar *currentAvatar = m_avatars.at(currentAvatarId);
        QList<CAbstractGameBlock *> collisionsList;

        for (int currentLayer = 0; currentLayer < BlocksTypesCount; ++currentLayer) {
            switch (currentLayer) {
            case BlockSolid:
                if (m_logEnabled) {
                    *m_logStream << QString("Do touches for %1").arg(currentAvatarId) << endl;
                }
                doTouches(currentAvatar, layer(BlockSolid)->softTouches(currentAvatar->position(), currentAvatar->position() + currentAvatar->size()));
                break;
            case BlockSpawn:
                collisionsList = layer(BlockSpawn)->blocksInRect(currentAvatar->position(),
                                                                 currentAvatar->position() + currentAvatar->size());
                if (!collisionsList.isEmpty()) {
                    for (int i = 0; i < collisionsList.count(); ++i) {
                        if (collisionsList.at(i)->subBlockType() == CSpawnExitBlock::Exit)
                            addAvatarToLeavingQueue(currentAvatar, m_nextWorldSignature);
                    }
                }
                break;
            default:
                collisionsList = layer(currentLayer)->blocksInRect(currentAvatar->position(),
                                                                   currentAvatar->position() + currentAvatar->size());
                if (!collisionsList.isEmpty()) {
                    for (int i = 0; i < collisionsList.count(); ++i) {
                        collisionsList[i]->crossedEvent(currentAvatar);
                    }
                }
            }
        }

        if (m_logEnabled) {
            *m_logStream << QString("Avatar %1 touches: %2").arg(currentAvatarId)
                            .arg(m_avatars.at(currentAvatarId)->touchesDirections()) << endl;
        }

//        if (currentAvatar->wantAction() >= 0)
//            processAvatarsAction(currentAvatar);

//        /* Hook */
//        if (currentAvatar->hookState() == CAvatar::HookBegin)
//            processAvatarsHookInProgress(currentAvatar);

//        if (currentAvatar->hookState() == CAvatar::HookedAvatar)
//            currentAvatar->hookedAvatar()->addExternalForce(currentAvatar->hookedAvatarAcceleration());
    }
}

void CWorld::processAvatarsHookInProgress(CAvatar *who) const
{
//    PointF2D hookActiveZoneBegin;
//    PointF2D hookActiveZoneEnd;

//    hookActiveZoneEnd = who->center() + who->hookVector();
//    hookActiveZoneBegin = who->center() + who->hookVectorNormalized() * who->hookProcessedLength();

//    who->setHookProcessedLength(who->hookLength());

//    SWorldsObjectCollision collisionInfo;

//    getFirstCollision(hookActiveZoneBegin, hookActiveZoneEnd, &collisionInfo, who);

//    if (collisionInfo.block) {
//        if ((collisionInfo.block->subBlockType() == CSolidBlock::Unhookable) || (collisionInfo.block->subBlockType() == CSolidBlock::Ice))
//            return who->setHookMissed();
//        else
//            return who->setHooked(collisionInfo.point);
//    }
//    if (collisionInfo.avatar)
//        who->setHooked(collisionInfo.avatar);
}

void CWorld::gameTick()
{
    processAvatarsLeaving();

    int delta = m_worldTime->restart();
    delta = 1000 / ticksPerSecond;

    int newWorldMSec = m_worldMSec + delta;

    if (m_logEnabled)
        *m_logStream << QString("tick: %1/%2").arg(m_worldMSec).arg(newWorldMSec) << endl;

    qreal remainingTimeToProcess = delta / 1000.0; // Convert from "ticks" to time in seconds

    int internalCyclesRemains = 50;

    if (!m_avatars.isEmpty()) {
        while (true) {
            if (m_logEnabled)
                *m_logStream << QString("internal cycle remains: %1").arg(internalCyclesRemains) << endl;

            --internalCyclesRemains;

            if (!internalCyclesRemains) {
                qDebug() << "Failed endless loop!";
                break;
            }
            int firstCollidedAvatarIndex = -1;

            processCurrentEvents();

            QList<SCollisionDescription> collisions = getAvatarsToBlocksCollision(remainingTimeToProcess);

            qreal time = remainingTimeToProcess + 1;

            for (int currentAvatarId = 0; currentAvatarId < m_avatars.count(); ++currentAvatarId) {
                /* find first time of any avatar collision */
                for (int i = 0; i < collisions.count(); ++i) {
                    if (collisions.at(i).time.x() < time) {
                        time = collisions.at(i).time.x();
                        firstCollidedAvatarIndex = i;
                    }
                    if (collisions.at(i).time.y() < time) {
                        time = collisions.at(i).time.y();
                        firstCollidedAvatarIndex = i;
                    }
                }
            }

            if (m_logEnabled)
                *m_logStream << QString("first avatar collision: time %1, index: %2").arg(time).arg(firstCollidedAvatarIndex) << endl;

            if (time > remainingTimeToProcess)
                break;

            if (time < c_minimalStep)
                time = 0;

            shiftTime(time, firstCollidedAvatarIndex, collisions.at(firstCollidedAvatarIndex));
            remainingTimeToProcess -= time;
        }
    }

    shiftTime(remainingTimeToProcess);

    for (int currentAvatarId = 0; currentAvatarId < m_avatars.count(); ++currentAvatarId)
        m_avatars.at(currentAvatarId)->bound(PointF2D(m_minX, m_minY), PointF2D(m_maxX, m_maxY));

    if (m_logEnabled)
        *m_logStream << QString("tick %1 takes %2 ms").arg(m_worldMSec).arg(m_worldTime->elapsed()) << endl;

    m_worldMSec = newWorldMSec;
}

void CWorld::whenAvatarSuicide(CAvatar *who)
{
    who->killedEvent(who);
    deleteAvatar(who);
}

void CWorld::shiftTime(qreal dTime, int fixAvatarId, const SCollisionDescription &description)
{
    if (m_logEnabled)
        *m_logStream << QString("Shift time: %1 (%2)").arg(dTime).arg(fixAvatarId) << endl;

    if (dTime < c_minimalStep) {
        if (m_logEnabled)
            *m_logStream << QString("Refuse to shift time: time is less than minimal.") << endl;

        return;
    }

    if (!m_avatars.isEmpty()) {
        for (int i = 0; i < m_avatars.count(); ++i)
            m_avatars.at(i)->doImpact(dTime);

        for (int i = 0; i < m_avatars.count(); ++i) {
            CAvatar *currentAvatar = m_avatars[i];

            currentAvatar->shiftTime(dTime);

            if (i == fixAvatarId) {
                PointF2D destPoint = m_avatars.at(fixAvatarId)->position();

                if (m_logEnabled)
                    *m_logStream << QString("Fix avatar position from (%1, %2)").arg(destPoint.x()).arg(destPoint.y()) << endl;

                /* At this point happen collision with item collisionsList[collisionBlockIndex].
                 * Due to float-point calculations new position may be bit different from truly predicted, so correct it. */
                if (description.components & ComponentX)
                    destPoint.setX(description.point.x());

                if (description.components & ComponentY)
                    destPoint.setY(description.point.y());

                if (m_logEnabled)
                    *m_logStream << QString("Fix avatar position to (%1, %2)").arg(destPoint.x()).arg(destPoint.y()) << endl;

                m_avatars.at(fixAvatarId)->setPosition(destPoint);
            }
        }
    }
    m_worldMSec += dTime;
}

void CWorld::addAvatarToLeavingQueue(CAvatar *who, QString where)
{
    SAvatarsTransition transition;
    transition.who   = who;
    transition.whereto = where;

    m_avatarsTransitions.append(transition);
}

void CWorld::processAvatarsLeaving()
{
    if (m_avatarsTransitions.isEmpty())
        return;

    for (int i = 0; i < m_avatarsTransitions.count(); ++i) {
        SAvatarsTransition currentTransition = m_avatarsTransitions.takeLast();

        if ((currentTransition.whereto != m_signature) && (!currentTransition.whereto.isEmpty())) {
            m_avatars.removeAll(currentTransition.who);
            emit avatarLeaveWorld(currentTransition.who, currentTransition.whereto);
        }
    }
}

CAvatar *CWorld::spawnAvatar()
{
    if (m_logEnabled)
        *m_logStream << QString("SpawnAvatar") << endl;

    CAvatar *newAvatar;
    if (spawnForAvatar()) {
        newAvatar = new CAvatar(); // Warn: do not set "this" as parent, bacause avatars can be transfered across worlds.
        addAvatar(newAvatar);
        return newAvatar;
    }

    return 0;
}

bool CWorld::addAvatar(CAvatar *pAvatar)
{
    if (m_logEnabled)
        *m_logStream << QString("AddAvatar") << endl;

    if (!pAvatar)
        return false;

    CAbstractGameBlock *pSpawnBlock = spawnForAvatar(pAvatar);

    if (!pSpawnBlock)
        return false;

    pAvatar->setWorld(this);
    pAvatar->setPosition(pSpawnBlock->position());
    pAvatar->setOwnGravityDirection(DirectionExtra);

    connect(pAvatar, SIGNAL(wantSuicide(CAvatar*)), SLOT(whenAvatarSuicide(CAvatar*)));

    m_avatars.append(pAvatar);

    if (m_logEnabled)
        *m_logStream << QString("Avatar %1 spawned at %2x%3").arg(avatarId(pAvatar)).arg(pSpawnBlock->position().x()).arg(pSpawnBlock->position().y());

    emit avatarSpawned(pAvatar);
    return true;
}

void CWorld::removeAvatar(CAvatar *pAvatar)
{
    if (!pAvatar)
        return;

    if (m_avatars.indexOf(pAvatar) < 0)
        return;

    emit avatarRemovedBegin(pAvatar);

//    for (int i = 0; i < m_avatars.count(); ++i) {
//        if (m_avatars.at(i)->hookedAvatar() == pAvatar)
//            m_avatars.at(i)->setHookMissed();
//    }

    disconnect(pAvatar, SIGNAL(wantSuicide(CAvatar*)), this, SLOT(whenAvatarSuicide(CAvatar*)));

    m_avatars.removeAll(pAvatar);
    emit avatarRemoved(pAvatar);
}

void CWorld::deleteAvatar(CAvatar *pAvatar)
{
    if (!pAvatar)
        return;

    removeAvatar(pAvatar);
    pAvatar->deleteLater();
}

void CWorld::start(int initialTick)
{
    if (m_logEnabled)
        *m_logStream << QString("Start simulation from tick: %1").arg(initialTick) << endl;

    if (!m_ticksTrigger->isActive()) {
        m_ticksTrigger->start();
        m_worldTime->start();
    }
    else {
        if (m_logEnabled)
            *m_logStream << QString("Simulation is already started") << endl;
    }

    m_worldMSec = initialTick;
}

void CWorld::stop()
{
    m_ticksTrigger->stop();
    m_worldMSec = 0;
}

void CWorld::setPaused(bool paused)
{
    if (paused) {
        m_ticksTrigger->stop();
        /* There is no reason to keep few ms. Isn't it? */
    //        m_worldMSec += m_worldTime->elapsed();

        /* There is no m_worldTime->stop(), because it is not timer, but time. */
    }
    else {
        m_ticksTrigger->start();
        m_worldTime->start();
    }
}

void CWorld::setSignature(const QString &signature)
{
    m_signature = signature;
}

void CWorld::setNextWorldSignature(const QString &signature)
{
    m_nextWorldSignature = signature;
}

void CWorld::setTitle(QString newTitle)
{
    m_title = newTitle;
}

bool CWorld::isActive() const
{
    return m_ticksTrigger->isActive();
}

CAbstractGameBlock *CWorld::spawnForAvatar(CAvatar *pAvatar) const
{
    Q_UNUSED(pAvatar)

    for (int i = 0; i < m_layers.at(BlockSpawn)->blocksCount(); ++i) {
        if (m_layers.at(BlockSpawn)->blockAt(i)->subBlockType() == CSpawnExitBlock::Spawn)
            return m_layers.at(BlockSpawn)->blockAt(i);
    }

    return 0;
}

void CWorld::setGravityDirection(Directions newDirection)
{
    m_gravityDirection = newDirection;
}
