# Common files for client, server and editor.

QT = core

TEMPLATE = lib
TARGET = logicbase
CONFIG += dll

include(../common.pri)
LIBS += -lextendedarray

SOURCES += \
    CPlayer.cpp \
    CAvatar.cpp \
    CWorld.cpp \
    CWorldUtils.cpp \
    CWorldObject.cpp \
    CLayer.cpp \
    CBlocksFactory.cpp \
    CAbstractGameBlock.cpp \
    mapBlocks/CSolidBlock.cpp \
    mapBlocks/CSpawnExitBlock.cpp \
    mapBlocks/CStartFinishBlock.cpp \
    mapBlocks/CTeleBlock.cpp \
    mapBlocks/CGravityModBlock.cpp \
    mapBlocks/CAccelerationBlock.cpp \
    mapBlocks/CFreezeBlock.cpp \
    Functions.cpp \
    CollisionsHelper.cpp \
    CConsole.cpp \
    CAvatarAppearance.cpp \
    Points2D.cpp \
    Rects2D.cpp \
    CAbstractAvatarAction.cpp \
    CHammerAction.cpp \
    CHookAction.cpp

HEADERS  +=  \
    Directions.hpp \
    STouchEvent.hpp \
    CPlayer.hpp \
    CAvatar.hpp \
    CWorld.hpp \
    CWorldUtils.hpp \
    CWorldObject.hpp \
    CLayer.hpp \
    CBlocksFactory.hpp \
    CAbstractGameBlock.hpp \
    mapBlocks/allBlocks.hpp \
    mapBlocks/CSolidBlock.hpp \
    mapBlocks/CSpawnExitBlock.hpp \
    mapBlocks/CStartFinishBlock.hpp \
    mapBlocks/CTeleBlock.hpp \
    mapBlocks/CGravityModBlock.hpp \
    mapBlocks/CAccelerationBlock.hpp \
    mapBlocks/CFreezeBlock.hpp \
    Functions.hpp \
    CollisionsHelper.hpp \
    CConsole.hpp \
    CAvatarAppearance.hpp \
    BlocksTypes.hpp \
    Points2D.hpp \
    Rects2D.hpp \
    CAbstractAvatarAction.hpp \
    CHammerAction.hpp \
    CHookAction.hpp
