#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include "Directions.hpp"
#include "Points2D.hpp"

#include <QPoint>

PointF2D minPoint(const PointF2D &point1, const PointF2D &point2);
PointF2D maxPoint(const PointF2D &point1, const PointF2D &point2);
float pointYOnLineByTwoPoints(const PointF2D &knownPoint1, const PointF2D &knownPoint2, float destX);
float pointXOnLineByTwoPoints(const PointF2D &knownPoint1, const PointF2D &knownPoint2, float destY);

PointF2D vectorByDirection(Directions direction);
PointF2D vectorByDirections(quint8 direction);
float vectorsLength(const PointF2D &srcVector);
PointF2D normalizedVector(const PointF2D &vector);
quint8 directionsByVector(const PointF2D &vector);

Directions partByCoordinates(const PointF2D &srcCoord);
bool shortCoordinatesIsInDirection(const PointF2D &srcCoord, Directions dir);

PointF2D cellToPos(const PointI2D &cell);
PointF2D cellToPos(quint32 cellX, quint32 cellY);
PointI2D posToCell(const PointF2D &srcPos);
PointI2D posUpToCell(const PointF2D &srcPos);
PointF2D roundPosToCell(const PointF2D &srcPos);
PointF2D roundUpPosToCell(const PointF2D &srcPos);
float angleByVector(const PointF2D &vector);

#endif // FUNCTIONS_HPP
