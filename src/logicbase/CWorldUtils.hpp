#ifndef CWORLDUTILS_HPP
#define CWORLDUTILS_HPP

#include <QObject>

class CWorld;
class CExtendedArray;

class CWorldUtils : public QObject
{
public:
    const static QString fileExtension;
    const static QString fileSignature;

    static QString fixedFilename(QString fileName);
    
public:
    static bool loadWorldBySignature(CWorld *pWorld, QString signature);
    static bool loadWorldByFilename(CWorld *pWorld, QString fileName);

    static bool saveAs(CWorld *pWorld, QString fileName);
    static bool saveAsBuffer(CWorld *pWorld, CExtendedArray *buffer);
    static bool isWorldLoadable(QString fileName);
    static bool isWorldsSignatureAvailable(QString signature);

    static bool saveBufferAsFile(const QByteArray &buffer, QString fileName);
};

#endif // CWORLDUTILS_HPP
