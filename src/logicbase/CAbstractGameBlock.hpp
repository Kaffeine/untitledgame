#ifndef CABSTRACTGAMEBLOCK_HPP
#define CABSTRACTGAMEBLOCK_HPP

#include "Directions.hpp"
#include "CWorldObject.hpp"

class CExtendedArray;

class CLayer;
class CAvatar;
class CBlocksFactory;

#include <QtGlobal>

class CAbstractGameBlock : public CWorldObject
{
    friend class CBlocksFactory;
public:
    CAbstractGameBlock();
    virtual ~CAbstractGameBlock() { }
    void setSubBlockType(quint16 subBlockType);
    inline quint16 subBlockType() const { return m_subBlockType; }
    virtual quint16 subBlockTypesCount() const = 0;

    /* TODO: Move to CWorldObject or left it here? Check it after CItems implementation. */
    PointI2D cell() const;
    PointF2D cellPosition() const;
    RectF2D boundingRect() const;
    bool containPoint(const PointF2D &point) const;

    inline Directions direction() const { return m_direction; }
    void setDirection(Directions newDirection);
    inline void setCell(quint32 cellX, quint32 cellY) { return setCell(PointI2D(cellX, cellY)); }
    void setCell(const PointI2D &newCell);
    void setCellByPos(const PointF2D &srcPos);
    qreal z() const { return m_z; }

    void setZ(qreal z);
    virtual void crossedEvent(CAvatar *avatar) { Q_UNUSED(avatar); }
    virtual void touchedEvent(CAvatar *avatar, Directions touchDirection, qreal length);

    inline int blockType() const { return m_blockType; }

    /* Save/load */
    bool read(CExtendedArray *source);
    bool write(CExtendedArray *dest) const;

    void setLayer(CLayer *pLayer);
    CLayer *layer() const { return m_layer; }

protected:
    void positionChangedEvent();
    virtual void subblockChangedEvent() { }
    virtual void blocksDirectionChangedEvent() { }
    virtual void boundingRectChangedEvent() { }
    virtual bool readEvent(CExtendedArray *source) { Q_UNUSED(source); return true; }
    virtual bool writeEvent(CExtendedArray *dest) const { Q_UNUSED(dest); return true; }

    quint16 m_subBlockType;
    Directions m_direction;

    PointI2D m_cell;

    qreal m_z;
    bool m_isVisible;
    CLayer *m_layer;

private:
    void _updatePosFromCell();
    void _setBoundingRect(RectF2D newBoundingRect);

    RectF2D m_boundingRect;

    /* Block type used by GUI classes to know, what to render (Instead of using MetaObject and/or RTTI). */
    int m_blockType;
};

#endif // CABSTRACTGAMEBLOCK_HPP
