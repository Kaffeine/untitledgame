#include "Functions.hpp"
#include "CWorldObject.hpp"
#include <qmath.h>

#define NEGATIVE_SUPPORT
#define SQUARE_ROOT_OF_TWO 1.41421355

PointF2D minPoint(const PointF2D &point1, const PointF2D &point2)
{
    PointF2D topLeft;
    topLeft.setX(qMin(point1.x(), point2.x()));
    topLeft.setY(qMin(point1.y(), point2.y()));
    return topLeft;
}

PointF2D maxPoint(const PointF2D &point1, const PointF2D &point2)
{
    PointF2D bottomRight;
    bottomRight.setX(qMax(point1.x(), point2.x()));
    bottomRight.setY(qMax(point1.y(), point2.y()));
    return bottomRight;
}

float pointYOnLineByTwoPoints(const PointF2D &knownPoint1, const PointF2D &knownPoint2, float destX)
{
    return (destX - knownPoint1.x()) * (knownPoint2.y() - knownPoint1.y()) / (knownPoint2.x() - knownPoint1.x()) + knownPoint1.y();
}

float pointXOnLineByTwoPoints(const PointF2D &knownPoint1, const PointF2D &knownPoint2, float destY)
{
    return (destY - knownPoint1.y()) * (knownPoint2.x() - knownPoint1.x()) / (knownPoint2.y() - knownPoint1.y()) + knownPoint1.x();
}

PointF2D vectorByDirection(Directions direction)
{
    switch (direction) {
    case DirectionLeft:
        return PointF2D(-1, 0);
        break;
    case DirectionRight:
        return PointF2D(1, 0);
        break;
    case DirectionUp:
        return PointF2D(0, -1);
        break;
    case DirectionDown:
        return PointF2D(0, 1);
        break;
    case DirectionNone:
    default:
        return PointF2D(0, 0);
        break;
    }
}

PointF2D vectorByDirections(quint8 direction)
{
    if ((direction & DirectionHorizontal) == DirectionHorizontal)
        direction &= ~DirectionHorizontal;

    if ((direction & DirectionVertical) == DirectionVertical)
        direction &= ~DirectionVertical;

    if ((direction & DirectionVertical) && (direction & DirectionHorizontal)) {
        PointF2D result;
        result += vectorByDirection(Directions(direction & DirectionVertical));
        result += vectorByDirection(Directions(direction & DirectionHorizontal));
        result /= SQUARE_ROOT_OF_TWO;
        return result;
    }
    else
        return vectorByDirection(Directions(direction));
}

quint8 directionsByVector(const PointF2D &vector)
{
    quint8 direction = DirectionNone;
    if (vector.x() > 0)
        direction |= DirectionRight;
    else if (vector.x() < 0)
        direction |= DirectionLeft;

    if (vector.y() > 0)
        direction |= DirectionDown;
    else if (vector.y() < 0)
        direction |= DirectionUp;

    return direction;
}

float vectorsLength(const PointF2D &srcVector)
{
    return qSqrt(srcVector.x() * srcVector.x() + srcVector.y() * srcVector.y());
}

PointF2D normalizedVector(const PointF2D &vector)
{
    if (vector.isNull())
        return vector;

    return vector / vectorsLength(vector);
}

Directions partByCoordinates(const PointF2D &srcCoord)
{
    if (srcCoord.x() < CWorldObject::CellLength * 0.50) {
        if (srcCoord.y() < CWorldObject::CellLength * 0.50) {
            qreal y = pointYOnLineByTwoPoints(PointF2D(0, 0), PointF2D(CWorldObject::CellLength, CWorldObject::CellLength), srcCoord.x());
            if (srcCoord.y() < y)
                return DirectionUp;
            else
                return DirectionLeft;
        }
        else {
            qreal y = pointYOnLineByTwoPoints(PointF2D(0, CWorldObject::CellLength), PointF2D(CWorldObject::CellLength, 0), srcCoord.x());
            if (srcCoord.y() > y)
                return DirectionDown;
            else
                return DirectionLeft;
        }
    }
    else {
        if (srcCoord.y() < CWorldObject::CellLength * 0.50) {
            qreal y = pointYOnLineByTwoPoints(PointF2D(0, CWorldObject::CellLength), PointF2D(CWorldObject::CellLength, 0), srcCoord.x());
            if (srcCoord.y() < y)
                return DirectionUp;
            else
                return DirectionRight;
        }
        else {
            qreal y = pointYOnLineByTwoPoints(PointF2D(0, 0), PointF2D(CWorldObject::CellLength, CWorldObject::CellLength), srcCoord.x());
            if (srcCoord.y() > y)
                return DirectionDown;
            else
                return DirectionRight;
        }
    }
}

bool shortCoordinatesIsInDirection(const PointF2D &srcCoord, Directions dir)
{
    if (dir == DirectionRight)
        return (partByCoordinates(srcCoord + PointF2D(CWorldObject::CellLength * 0.5, 0)) == dir);

    if (dir == DirectionDown)
        return (partByCoordinates(srcCoord + PointF2D(0, CWorldObject::CellLength * 0.5)) == dir);

    return (partByCoordinates(srcCoord) == dir);
}

PointF2D cellToPos(const PointI2D &cell)
{
    return PointF2D(cell * CWorldObject::CellLength);
}

PointF2D cellToPos(quint32 cellX, quint32 cellY)
{
    return cellToPos(PointI2D(cellX, cellY));
}

PointI2D posToCell(const PointF2D &srcPos)
{
#ifdef NEGATIVE_SUPPORT
    if (srcPos.x() >= 0)
        return PointI2D(srcPos.x() / CWorldObject::CellLength, srcPos.y() / CWorldObject::CellLength);
    else {
        SPointI2D result = srcPos / CWorldObject::CellLength;
        if (srcPos.x() < result.x * CWorldObject::CellLength)
            --result.x;

        if (srcPos.y() < result.y * CWorldObject::CellLength)
            --result.y;

        return result;
    }
#else
    return PointI2D(srcPos.x() / CWorldObject::CellLength, srcPos.y() / CWorldObject::CellLength);
#endif
}

PointI2D posUpToCell(const PointF2D &srcPos)
{
    int cellX = srcPos.x() / CWorldObject::CellLength;
    if ((srcPos.x() - cellX * CWorldObject::CellLength) > 0)
        ++cellX;

    int cellY = srcPos.y() / CWorldObject::CellLength;
    if ((srcPos.y() - cellY * CWorldObject::CellLength) > 0)
        ++cellY;


    return PointI2D(cellX, cellY);
}

PointF2D roundPosToCell(const PointF2D &srcPos)
{
    return cellToPos(posToCell(srcPos));
}

PointF2D roundUpPosToCell(const PointF2D &srcPos)
{
    return cellToPos(posUpToCell(srcPos));
}

float angleByVector(const PointF2D &vector)
{
    return atan2(vector.x(), vector.y()) * 180 / M_PI;
}
