#include "CConsole.hpp"
#include <QVariant>
#include <QStringList>
#include <QMetaProperty>

#include <QDebug>

const QLatin1String CConsole::m_commandGetProperty("get");
const QLatin1String CConsole::m_commandSetProperty("set");

CConsole::CConsole(QObject *parent) :
    QObject(parent)
{
    fillBlacklist();
    addConsoleCommand(this, "cmd_ls", "ls");
}

bool CConsole::addConsoleObject(QObject *newObject, QString name)
{
    if (name.isEmpty())
        return false;

    /* TODO: Introduce own objects names list and don't touch Qt's objects names. */
    newObject->setObjectName(name);

    if (m_accessibleObjects.indexOf(newObject) < 0) {
        m_accessibleObjects.append(newObject);

        m_1stLevelList.append(name + " "); // Add extra space, because noone can execute object as is.
    }

    return true;
}

bool CConsole::addConsoleCommand(QObject *object, QString slot, const QString &command)
{
    if (!object || slot.isEmpty() || command.isEmpty())
        return false;

    for (int i = 0; i < m_commandsList.count(); ++i)
        if (command == m_commandsList.at(i).command)
            return false;

    SConsoleCommands newCommand;
    newCommand.command = command;
    newCommand.object  = object;
    newCommand.slot    = slot;

    m_commandsList.append(newCommand);
    m_1stLevelList.append(command);

    return true;
}

QString CConsole::historyAtNumFromEnd(int num) const
{
    if ((num >= 0) && (num < m_commandsHistory.count()))
        return m_commandsHistory.at(m_commandsHistory.count() - 1 - num);
    return QString();
}

bool CConsole::executeCommand(QString line)
{
    m_commandsHistory.append(line);
    if (m_commandsHistory.count() > m_commandsHistoryLimit)
        m_commandsHistory.takeFirst();

    output(">" + line);

    int index = line.indexOf(" ");

    QString key;
    if (index > 0)
        key = line.left(index);
    else
        key = line;

    for (int i = 0; i < m_commandsList.count(); ++i) {
        if (key == m_commandsList.at(i).command) {
            QMetaObject::invokeMethod(m_commandsList.at(i).object, m_commandsList.at(i).slot.toLocal8Bit().constData(), Qt::QueuedConnection, Q_ARG(QString, line));
            return true;
        }
    }

    QObject *targetObj = objectByName(key);

    if (!targetObj) {
        emit output(tr("No such command or object (\"%1\").").arg(key));
        return false;
    }

    QStringList args = line.split(" ", QString::SkipEmptyParts);

    bool ok = false;

    if (args.count() == 2)
        ok = QMetaObject::invokeMethod(targetObj, args.at(1).toLocal8Bit().constData(), Qt::QueuedConnection);
    else if (args.count() == 3) {
        if (args.at(1) == m_commandGetProperty)
            ok = cmd_get(targetObj, args.at(2));
        else {
            ok = QMetaObject::invokeMethod(targetObj, args.at(1).toLocal8Bit().constData(), Qt::QueuedConnection,
                                           Q_ARG(QString, args.at(2)));
        }
    }
    else if (args.count() == 4) {
        if (args.at(1) == m_commandSetProperty)
            ok = cmd_set(targetObj, args.at(2), args.at(3));
        else
            ok = false;
    }

    if (!ok)
        emit output(tr("Fail"));

    return ok;
}

bool CConsole::cmd_ls(QString line)
{
    QStringList args = line.split(" ", QString::SkipEmptyParts);

    QObject *targetObj = 0;
    QString targetObjName;

    if (args.count() > 1)
        targetObjName = args.at(1);

    if (targetObjName.isEmpty()) {
        emit output(tr("Objects:"));
        for (int i = 0; i < m_accessibleObjects.count(); ++i) {
            emit output(QString("\t%1").arg(m_accessibleObjects.at(i)->objectName()));
        }
        return true;
    }

    targetObj = objectByName(args.at(1));
    qDebug() << "ls: ret obj:" << targetObj << targetObj->metaObject()->methodCount();

    /* Skip property "objectName" */
    emit output(tr("Object %1 have %2 properties:").arg(targetObjName).arg(targetObj->metaObject()->propertyCount() - 1));
    for (int i = 0; i < targetObj->metaObject()->propertyCount(); ++i) {
        if (targetObj->metaObject()->property(i).name() == QLatin1String("objectName"))
            continue;

        emit output(QString("\t%1").arg(targetObj->metaObject()->property(i).name()));
    }

    QStringList slotsList = objectsSlots(targetObj);

    emit output(tr("Object %1 have %2 slots:").arg(targetObjName).arg(slotsList.count()));
    for (int i = 0; i < slotsList.count(); ++i)
        emit output(QString("\t%1").arg(slotsList.at(i)));

    return true;
}

bool CConsole::cmd_get(QObject *target, QString propertyName)
{
    QVariant retValue = target->property(propertyName.toLocal8Bit().constData());
    if (retValue.isValid())
        emit output(propertyName + ": " + retValue.toString());

    return retValue.isValid();
}

bool CConsole::cmd_set(QObject *target, QString propertyName, QString propertyValue)
{
    QVariant retValue = target->setProperty(propertyName.toLocal8Bit().constData(), propertyValue);
    if (retValue.isValid())
        emit output(propertyName + ": " + retValue.toString());

    return retValue.isValid();
}

QObject *CConsole::objectByName(const QString &objName) const
{
    for (int i = 0; i < m_accessibleObjects.count(); ++i) {

        qDebug() << "Find object:" << objName << m_accessibleObjects.at(i);

        if (m_accessibleObjects.at(i)->objectName() == objName) {
            qDebug() << "Get it!" << m_accessibleObjects.at(i);
            return m_accessibleObjects.at(i);
        }
    }

    return 0;
}

QStringList CConsole::autocomplete(const QString &line) const
{
    QStringList args = line.split(" ", QString::SkipEmptyParts);
    int queredParameter = args.count();

    if ((queredParameter > 0) && line.endsWith(" "))
        ++queredParameter;

    if (!queredParameter)
        queredParameter = 1;

    QStringList variants;

    QString begins;

    if (!line.endsWith(" "))
        begins = args.last();

    if (queredParameter == 1) {
        if (begins.isEmpty())
            variants = m_1stLevelList;
        else {
            for (int i = 0; i < m_1stLevelList.count(); ++i) {
                if (m_1stLevelList.at(i).startsWith(begins))
                    variants.append(m_1stLevelList.at(i));
            }
        }
    }
    else {
        QString objectName = args.at(0);

        QObject *targetObj = objectByName(objectName);
        if (targetObj) {
            if (queredParameter == 2) {
                variants = objectsSlots(targetObj);

                if (!begins.isEmpty()) {
                    for (int i = variants.count() - 1; i >= 0 ; --i) {
                        if (!variants.at(i).startsWith(begins))
                            variants.removeAt(i);
                    }
                }
            }
        }
    }

    variants.sort();

    return variants;
}

void CConsole::fillBlacklist()
{
    QObject obj;

    int methodsCount = obj.metaObject()->methodCount();

    for (int i = 0; i < methodsCount; ++i) {
        if (obj.metaObject()->method(i).methodType() == QMetaMethod::Slot) {
#if QT_VERSION >= 0x050000
            m_slotsBlacklist.append(obj.metaObject()->method(i).methodSignature());
#else
            m_slotsBlacklist.append(obj.metaObject()->method(i).signature());
#endif
        }
    }
}

QStringList CConsole::objectsSlots(QObject *targetObj) const
{
    QStringList slotsList;

    int methodsCount = targetObj->metaObject()->methodCount();

    for (int i = 0; i < methodsCount; ++i) {
        if (targetObj->metaObject()->method(i).access() != QMetaMethod::Public)
            continue;

        if (targetObj->metaObject()->method(i).methodType() == QMetaMethod::Slot) {
#if QT_VERSION >= 0x050000
            if (m_slotsBlacklist.indexOf(QString(targetObj->metaObject()->method(i).methodSignature())) < 0)
                slotsList.append(targetObj->metaObject()->method(i).methodSignature());
#else
            if (m_slotsBlacklist.indexOf(QString(targetObj->metaObject()->method(i).signature())) < 0)
                slotsList.append(targetObj->metaObject()->method(i).signature());
#endif
        }
    }

    return slotsList;
}
