#ifndef CAVATAR_HPP
#define CAVATAR_HPP

#include <QObject>
#include <QMap>

#include "Directions.hpp"
#include "CWorldObject.hpp"

class CPlayer;
class CGameNetwork;
class CAvatarAppearance;
class CAbstractAvatarAction;

class CAvatar : public QObject, public CWorldObject
{
    friend class CPlayer;
    friend class CGameNetwork;
    Q_OBJECT

public:
    enum Wills {
        WillLeft    = DirectionLeft,
        WillRight   = DirectionRight,
        WillUp      = DirectionUp,
        WillDown    = DirectionDown,
        WillJump    = 0b0010000,
        WillAction1 = 0b0100000,
        WillAction2 = 0b1000000
    };

    /* Avatars constructor called only by CWorld::spawnAvatar() */
    CAvatar(QObject *parent = 0);
    virtual ~CAvatar();

    inline CPlayer *player() const { return m_player; }
    void setPlayer(CPlayer *newPlayer);

    /* <-- */
    static qreal width();
    static qreal height();
    /* --> */

    void move(const PointF2D & motion);

    inline double freezedTime() const { return m_freezedTime; }
    void setFreezedTime(qreal time);

    Directions gravityDirection() const;
    void setOwnGravityDirection(Directions newGravity);

    void bound(const PointF2D &topLeft, const PointF2D &bottomRight);

    inline float willSpeedLimit() { return m_willSpeedLimit; }

    /* Interface for world: */
    void killedEvent(CWorldObject *killer); // TODO: QList <CWorldObject *> ?

    /* Interface for items: */
    void addTouch(quint8 touchDirection, qreal friction = 0.0);
    void doWantComplete();
    void doWantTele(quint8 teleId);

    void addExternalForce(const PointF2D &addition);
    void addExternalPulse(const PointF2D &addPulse);
    void addExternalSpeedup(const PointF2D &addSpeedup) { addExternalPulse(addSpeedup * mass()); }

    /* Interface for players: */
    void setWillDirections(quint8 newWills);
    void doSuicide();

    /* Collision detection */
    RectF2D boundingRect() const;
    bool containPoint(const PointF2D &point) const;

    /* Physics */
    void doImpact(const double &dT);

    /* ShiftTime should only update own position */
    void shiftTime(const double &dT);

    PointF2D supposedPosition(qreal dT) const;

    quint8 touchesDirections() const { return m_touches; }
    void cleanExternalForces();
    void cleanTouches();

    PointF2D speed() const { return m_speed; }

    CAbstractAvatarAction *currentAction() const;
    CAbstractAvatarAction *currentAltAction() const;

    void doAction(int action, const PointF2D &dest);
    void cancelAction(int action);

    quint8 willDirections() const;
    bool jumpIsAvailable() const;
    bool isOnGround() const;

    static void printWills(quint8 wills);

signals:
    void moved();
    void jumped();
    void wantSuicide(CAvatar *who);

private:
    PointF2D respectTouches(const PointF2D & motion) const;
    void updateSpeeds(float dT);
    void updateSpeedsHelper(float dT, quint8 willDirection);

    PointF2D gravityForce() const;

    PointF2D unlimitedForce() const;
    PointF2D limitedForce(quint8 willDirection) const;
    PointF2D willForce(quint8 willDirection) const;
    PointF2D frictionsForce(const PointF2D &sourceSpeed, quint8 willDirections) const;
    PointF2D externalForce() const;
    PointF2D jumpSpeedUp(quint8 willDirections) const;

    PointF2D m_additionalPulse;
    PointF2D m_speed;
    double m_freezedTime;

    Directions m_gravityDirection;
    PointF2D m_externalForce;
    quint8 m_willDirections;
    quint8 m_jumpsAvailable;
    PointF2D m_accumulatedMagicSpeed;

    quint8 m_touches;
    QMap<quint8, qreal> m_frictions;

    float m_willForce;
    float m_airSlippingFactor;

    float m_willSpeedLimit;
    float m_jumpSpeed;
    float m_slowingWhileFreefall;
    float m_slowingWhileDraft;
    float m_groundedEffectiveFrictionMinimumValue;
    float m_groundedFrictionTransitionValue;
    float m_willFrictionModificator;

    CAbstractAvatarAction *m_currentAction;
    CAbstractAvatarAction *m_currentAltAction;

    /* Setted by CPlayer class */
    CPlayer *m_player;

    /* Appearance */
public:
    CAvatarAppearance *appearance() const { return m_appearance; }

signals:
    void appearanceChanged(CAvatar *pAvatar);

private:
    CAvatarAppearance *m_appearance;

};

#endif // CAVATAR_HPP
