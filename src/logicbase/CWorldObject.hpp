#ifndef CWORLDOBJECT_HPP
#define CWORLDOBJECT_HPP

#include "Points2D.hpp"
#include "Rects2D.hpp"

#include "Directions.hpp"

class CWorld;

class CWorldObject
{
public:
    CWorldObject();
    virtual ~CWorldObject();

    inline PointF2D position() const { return m_position; }
    void setPosition(const PointF2D &position);
    void setPosition(float newPosX, float newPosY);

    inline float minX() const { return m_position.x(); }
    inline float maxX() const { return m_position.x() + m_width; }
    inline float minY() const { return m_position.y(); }
    inline float maxY() const { return m_position.y() + m_height; }
    inline float width () const { return m_width; }
    inline float height() const { return m_height; }
    inline PointF2D size() const { return PointF2D(m_width, m_height); }
    inline PointF2D center() const { return position() + size() / 2; }
    inline float mass() const { return m_mass; }

    virtual RectF2D boundingRect() const = 0;
    virtual bool containPoint(const PointF2D &point) { (void)point; return false; }

    CWorld *world() const { return m_world; }
    void setWorld(CWorld *newWorld);

    const static int CellLength;
    const static PointF2D CellSize;

protected:
    virtual void setWorldEvent();
    virtual void positionChangedEvent();
    virtual Directions gravityDirection() const;
    virtual float gravityMass() const;

    PointF2D m_position;
    float m_width;
    float m_height;
    CWorld *m_world;
    float m_mass;

};

#endif // CWORLDOBJECT_HPP
