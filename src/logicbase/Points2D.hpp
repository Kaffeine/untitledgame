#ifndef POINTS2D_HPP
#define POINTS2D_HPP

class PointI2D;
class PointF2D;

struct SPointI2D {
    inline SPointI2D() { x = 0; y = 0; }
    inline SPointI2D(int _x, int _y) { x = _x; y = _y; }
    inline SPointI2D(const SPointI2D &point) { x = point.x; y = point.y; }
    inline SPointI2D(const PointI2D &point);
    inline SPointI2D(const PointF2D &point);

    inline void operator=(const SPointI2D &point) { x = point.x; y = point.y; }

    int x;
    int y;
};

class PointI2D
{
public:
    inline PointI2D();
    inline PointI2D(int _x, int _y);
    inline PointI2D(const PointI2D &pointI);
    inline PointI2D(const PointF2D &pointF);
    inline PointI2D(const SPointI2D &point);

    inline PointI2D &operator=(const PointI2D &point);

    inline int x() const;
    inline int y() const;
    inline void setX(int newX);
    inline void setY(int newY);

    friend inline const PointI2D operator*(const PointI2D &, int);

private:
    int m_x;
    int m_y;
};

class PointF2D
{
public:
    inline PointF2D(float newX = 0, float newY = 0);
    inline PointF2D(const PointF2D &p);
    inline PointF2D(const PointI2D &p);
    inline PointF2D(const SPointI2D &p);

    inline PointF2D &operator=(const PointF2D &p);

    inline float x() const;
    inline float y() const;
    inline void setX(float newX);
    inline void setY(float newY);

    inline bool isNull() const;
    inline bool isAlmostNull(const float &dividor = 0.001) const;

    inline void reset();

    float length() const;
    PointF2D normalized();

    PointF2D &operator+=(const PointF2D &p);
    PointF2D &operator-=(const PointF2D &p);
    PointF2D &operator*=(float c);
    PointF2D &operator/=(float c);

    friend inline bool operator==(const PointF2D &, const PointF2D &);
    friend inline bool operator!=(const PointF2D &, const PointF2D &);
    friend inline const PointF2D operator+(const PointF2D &, const PointF2D &);
    friend inline const PointF2D operator-(const PointF2D &, const PointF2D &);

    friend inline const PointF2D operator*(float, const PointF2D &);
    friend inline const PointF2D operator*(const PointF2D &, double);
    friend inline const PointF2D operator*(const PointF2D &, float);
    friend inline const PointF2D operator*(const PointF2D &, int);

    friend inline const PointF2D operator/(float, const PointF2D &);
    friend inline const PointF2D operator/(const PointF2D &, float);

    friend inline const PointF2D operator-(const PointF2D &);

private:
    float m_x;
    float m_y;
};

inline SPointI2D::SPointI2D(const PointI2D &point) { x = point.x(); y = point.y(); }
inline SPointI2D::SPointI2D(const PointF2D &point) { x = point.x(); y = point.y(); }

/* PointI2D Inlines */

inline PointI2D::PointI2D() { m_x = 0; m_y = 0; }
inline PointI2D::PointI2D(int _x, int _y) { m_x = _x; m_y = _y; }
inline PointI2D::PointI2D(const PointI2D &pointI) { m_x = pointI.m_x; m_y = pointI.m_y; }
inline PointI2D::PointI2D(const PointF2D &pointF) { m_x = pointF.x(); m_y = pointF.y(); }
inline PointI2D::PointI2D(const SPointI2D &point) { m_x = point.x; m_y = point.y; }

inline PointI2D &PointI2D::operator=(const PointI2D &point) { m_x = point.m_x; m_y = point.m_y; return *this; }

inline int PointI2D::x() const { return m_x; }
inline int PointI2D::y() const { return m_y; }
inline void PointI2D::setX(int newX) { m_x = newX; }
inline void PointI2D::setY(int newY) { m_y = newY; }

inline const PointI2D operator*(const PointI2D &point, int multiplier) { return PointI2D(point.m_x * multiplier, point.m_y * multiplier); }

/* PointF2D Inlines */

inline PointF2D::PointF2D(float newX, float newY) : m_x(newX), m_y(newY) { }
inline PointF2D::PointF2D(const PointF2D &p) : m_x(p.m_x), m_y(p.m_y) { }
inline PointF2D::PointF2D(const PointI2D &p) : m_x(p.x()), m_y(p.y()) { }
inline PointF2D::PointF2D(const SPointI2D &p) : m_x(p.x), m_y(p.y) { }

inline PointF2D &PointF2D::operator=(const PointF2D &p) { m_x = p.m_x; m_y = p.m_y; return *this; }

inline float PointF2D::x() const { return m_x; }
inline float PointF2D::y() const { return m_y; }
inline void PointF2D::setX(float newX) { m_x = newX; }
inline void PointF2D::setY(float newY) { m_y = newY; }

inline bool PointF2D::isNull() const { return ((m_x == 0) && (m_y == 0)); }

bool PointF2D::isAlmostNull(const float &dividor) const
{
    if ((m_x > dividor) || (m_x < -dividor) || (m_y > dividor) || (m_y < -dividor))
        return false;

    return true;
}

inline void PointF2D::reset() { m_x = 0; m_y = 0; }

inline PointF2D &PointF2D::operator+=(const PointF2D &p) { m_x += p.m_x; m_y += p.m_y; return *this; }
inline PointF2D &PointF2D::operator-=(const PointF2D &p) { m_x -= p.m_x; m_y -= p.m_y; return *this; }
inline PointF2D &PointF2D::operator/=(float c) { m_x /= c; m_y /= c; return *this; }
inline PointF2D &PointF2D::operator*=(float c) { m_x *= c; m_y *= c; return *this; }

inline bool operator==(const PointF2D &p1, const PointF2D &p2) { return (p1.m_x == p2.m_x) && (p1.m_y == p2.m_y); }
inline bool operator!=(const PointF2D &p1, const PointF2D &p2) { return (p1.m_x != p2.m_x) || (p1.m_y != p2.m_y); }
inline const PointF2D operator+(const PointF2D &p1, const PointF2D &p2) { return PointF2D(p1.m_x + p2.m_x, p1.m_y + p2.m_y); }
inline const PointF2D operator-(const PointF2D &p1, const PointF2D &p2) { return PointF2D(p1.m_x - p2.m_x, p1.m_y - p2.m_y); }

inline const PointF2D operator*(float multiplier, const PointF2D &p) { return PointF2D(p.m_x * multiplier, p.m_y * multiplier); }
inline const PointF2D operator*(const PointF2D &p, double multiplier) { return PointF2D(p.m_x * multiplier, p.m_y * multiplier); }
inline const PointF2D operator*(const PointF2D &p, float multiplier) { return PointF2D(p.m_x * multiplier, p.m_y * multiplier); }
inline const PointF2D operator*(const PointF2D &p, int multiplier) { return PointF2D(p.m_x * multiplier, p.m_y * multiplier); }

inline const PointF2D operator/(float dividor, const PointF2D &p) { return PointF2D(p.m_x / dividor, p.m_y / dividor); }
inline const PointF2D operator/(const PointF2D &p, float dividor) { return PointF2D(p.m_x / dividor, p.m_y / dividor); }

inline const PointF2D operator-(const PointF2D &p) { return PointF2D(-p.m_x, -p.m_y); }

#endif // POINTS2D_HPP
