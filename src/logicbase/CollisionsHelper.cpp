#include "CollisionsHelper.hpp"
#include "CAbstractGameBlock.hpp"

#include "Functions.hpp"

SCollisionDescription getFirstCollisionFromList(const QList<SCollisionDescription> &collisionsDescriptions)
{
    /* find first time of current avatar collision */
    qreal time = collisionsDescriptions.at(0).time.x() + 1;

    int firstCollisionIndex = 0;

    for (int i = 0; i < collisionsDescriptions.count(); ++i) {
        if (collisionsDescriptions.at(i).time.x() < time) {
            time = collisionsDescriptions.at(i).time.x();
            firstCollisionIndex = i;
        }

        if (collisionsDescriptions.at(i).time.y() < time) {
            time = collisionsDescriptions.at(i).time.y();
            firstCollisionIndex = i;
        }
    }

    return collisionsDescriptions.at(firstCollisionIndex);
}

QList<SCollisionDescription> getCollisionsDescriptions(const QList<CAbstractGameBlock *> &itemsList, const PointF2D &oldPos, const PointF2D &newPos, const PointF2D &size, qreal dTime)
{
    QList<SCollisionDescription> collisionsDescriptions;
    PointF2D supposedSpeed = (newPos - oldPos) / dTime;

    int i;
    for (i = 0; i < itemsList.count(); ++i) {
        /* At this point we know, that collision was happen. */
        qreal xTime;
        qreal yTime;
        qreal edgeXPosValue = 0.0;
        qreal edgeYPosValue = 0.0;

        xTime = dTime + 1;
        yTime = dTime + 1;

        quint8 components = 0;

        if (    (oldPos.x() + size.x() < itemsList.at(i)->minX()) &&
                (newPos.x() + size.x() > itemsList.at(i)->minX())) {
            components |= ComponentX;
            edgeXPosValue = itemsList.at(i)->minX() - size.x();
            xTime = (edgeXPosValue - oldPos.x()) / supposedSpeed.x();
        }
        else if (( oldPos.x() > itemsList.at(i)->maxX() ) &&
                 ( newPos.x() < itemsList.at(i)->maxX() )) {
            components |= ComponentX;
            edgeXPosValue = itemsList.at(i)->maxX();
            /* Formula is same as above because both diff and speed have changed sign */
            xTime = (edgeXPosValue - oldPos.x()) / supposedSpeed.x();
        }

        if (    (oldPos.y() + size.y() < itemsList.at(i)->minY()) &&
                (newPos.y() + size.y() > itemsList.at(i)->minY())) {
            components |= ComponentY;
            edgeYPosValue = itemsList.at(i)->minY() - size.y();
            yTime = (edgeYPosValue - oldPos.y()) / supposedSpeed.y();
        }
        else if (( oldPos.y() > itemsList.at(i)->maxY() ) &&
                 ( newPos.y() < itemsList.at(i)->maxY() )) {
            components |= ComponentY;
            edgeYPosValue = itemsList.at(i)->maxY();
            yTime = (edgeYPosValue - oldPos.y()) / supposedSpeed.y();
        }

        SCollisionDescription internalCollisions;
        internalCollisions.time.setX(xTime);
        internalCollisions.time.setY(yTime);
        internalCollisions.components = components;
        internalCollisions.point.setX(edgeXPosValue);
        internalCollisions.point.setY(edgeYPosValue);
        collisionsDescriptions.append(internalCollisions);
    }

    return collisionsDescriptions;
}

QList<CAbstractGameBlock *> getCollisionsList(const QList<CAbstractGameBlock *> &itemsList, const PointF2D &oldPos, const PointF2D &newPos, const PointF2D &size)
{
    Q_UNUSED(size)
    QList<CAbstractGameBlock *> collisionsToReturn;
    qreal hookX1;
    qreal hookX2;

    for (int i = 0; i < itemsList.count(); ++i) {
        CWorldObject *obj = itemsList.at(i);

        hookX1 = pointXOnLineByTwoPoints(oldPos, newPos, obj->minY());
        hookX2 = pointXOnLineByTwoPoints(oldPos, newPos, obj->maxY());
        if ((hookX1 > obj->maxX()) && (hookX2 > obj->maxX())) {
            continue;
        }

        /* When introduce size usage, add it here like this:
         * if ((hookX1 + size.x() < obj->mixX()) && (hookX2 + size.x() < obj->mixX())) { ... */
        hookX1 = pointXOnLineByTwoPoints(oldPos, newPos, obj->minY());
        hookX2 = pointXOnLineByTwoPoints(oldPos, newPos, obj->maxY());
        if ((hookX1 < obj->minX()) && (hookX2 < obj->minX())) {
            continue;
        }

        collisionsToReturn.append(itemsList.at(i));
    }
    return collisionsToReturn;
}


qreal collisionTest(const PointF2D &from, const PointF2D &vector, CWorldObject *object, PointF2D *pReturnCollisionPoint)
{
    /* Explanation on picture. */
    qreal testX1;
    qreal testX2;

    if (vector.x() > 0) { // If direction is right
        if (object->maxX() < from.x()) // But object is lefter than origin
            return -1;
    }
    else {
        if (object->minX() > from.x())
            return -1;
    }

    if (vector.y() > 0) {
        if (object->maxY() < from.y())
            return -1;
    }
    else {
        if (object->minY() > from.y())
            return -1;
    }

    testX1 = pointXOnLineByTwoPoints(from, from + vector, object->minY());
    testX2 = pointXOnLineByTwoPoints(from, from + vector, object->maxY());

    if ((testX1 > object->maxX()) && (testX2 > object->maxX())) // Vector is righter
        return -1;

    if ((testX1 < object->minX()) && (testX2 < object->minX())) // Vector is lefter
        return -1;

    if (pReturnCollisionPoint) {
        PointF2D collisionPoint;
//        qDebug() << "X1: " << testX1 << "X2: " <<testX2;
        if (from.x() < object->minX()) {
            /* Green cases */
//            qDebug() << "Green cases";
            if (from.y() > object->maxY()) {
                /* Case 3 */
                if (testX2 < object->minX())
                    collisionPoint = PointF2D(object->minX(), pointYOnLineByTwoPoints(from, from + vector, object->minX()));
                else
                    collisionPoint = PointF2D(testX2, object->maxY());
            }
            else if (from.y() > object->minY()) {
                /* Case 6 */
                collisionPoint = PointF2D(object->minX(), pointYOnLineByTwoPoints(from, from + vector, object->minX()));
            }
            else {
                /* Case 9 */
                if (testX1 < object->minX())
                    collisionPoint = PointF2D(object->minX(), pointYOnLineByTwoPoints(from, from + vector, object->minX()));
                else
                    collisionPoint = PointF2D(testX1, object->minY());
            }
        }
        else if (from.x() < object->maxX()) {
            /* Violet cases */
//            qDebug() << "Violet cases";
            /* There is unimplemented case then "from" is inside object. */
            if (from.y() > object->maxY()) {
                /* Case 2 */
                collisionPoint = PointF2D(testX2, object->maxY());
            }
            else {
                /* Case 8 */
                collisionPoint = PointF2D(testX1, object->minY());
            }
        }
        else {
            /* White cases */
//            qDebug() << "White cases";
            if (from.y() > object->maxY()) {
                /* Case 1 */
                if (testX2 > object->maxX())
                    collisionPoint = PointF2D(object->maxX(), pointYOnLineByTwoPoints(from, from + vector, object->maxX()));
                else
                    collisionPoint = PointF2D(testX2, object->maxY());
            }
            else if (from.y() > object->minY()) {
                /* Case 4 */
                collisionPoint = PointF2D(object->maxX(), pointYOnLineByTwoPoints(from, from + vector, object->maxX()));
            }
            else {
                /* Case 7 */
                if (testX1 < object->maxX())
                    collisionPoint = PointF2D(testX1, object->minY());
                else
                    collisionPoint = PointF2D(object->maxX(), pointYOnLineByTwoPoints(from, from + vector, object->maxX()));
            }
        }

        pReturnCollisionPoint->setX(collisionPoint.x());
        pReturnCollisionPoint->setY(collisionPoint.y());
        return vectorsLength(from - collisionPoint);
    }

    return vectorsLength(from - object->center());
}
