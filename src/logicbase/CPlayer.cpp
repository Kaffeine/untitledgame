#include "CPlayer.hpp"
#include "CAvatar.hpp"
#include "CAbstractAvatarAction.hpp"
#include "CAvatarAppearance.hpp"

/* This is logic class */
CPlayer::CPlayer(QObject *parent) :
    QObject(parent),
    m_avatar(0)
{
    m_finished = false;
    m_actions = 0;
    m_nextCheckpoint   = -1;
    m_checkpointsCount =  0;
}

void CPlayer::setAvatar(CAvatar *newAvatar)
{
    beforeSetAvatarEvent(newAvatar);

    if (m_avatar)
        m_avatar->setWillDirections(0);

    m_avatar = newAvatar;
    m_avatar->setWillDirections(0);

    connect(m_avatar, SIGNAL(moved())    , SLOT(whenMoved()));
    connect(m_avatar, SIGNAL(jumped())   , SLOT(whenJumped()), Qt::DirectConnection);
    connect(m_avatar, SIGNAL(destroyed()), SLOT(setNullAvatar()), Qt::DirectConnection);

    m_avatar->setPlayer(this);

    emit avatarChanged(m_avatar);
}

void CPlayer::applyActions()
{
    if (!m_avatar)
        return;

    quint8 newWillDirections = 0;

    if (actions() & ActionLeft)
        newWillDirections |= CAvatar::WillLeft;

    if (actions() & ActionRight)
        newWillDirections |= CAvatar::WillRight;

    if (actions() & ActionUp)
        newWillDirections |= CAvatar::WillUp;

    if (actions() & ActionDown)
        newWillDirections |= CAvatar::WillDown;

    if (actions() & ActionJump)
        newWillDirections |= CAvatar::WillJump;

    if (actions() & ActionAction)
        newWillDirections |= CAvatar::WillAction1;

    if (actions() & ActionAltAction)
        newWillDirections |= CAvatar::WillAction2;

    m_avatar->setWillDirections(newWillDirections);

    if (actions() & ActionSuicide)
        m_avatar->doSuicide();

    emit actionsChanged(m_actions);
}

void CPlayer::doAction(PointF2D destinationPoint)
{
    protectedDoActionHelper(ActionAction, destinationPoint);
}

void CPlayer::doAltAction(PointF2D destinationPoint)
{
    protectedDoActionHelper(ActionAltAction, destinationPoint);
}

void CPlayer::cancelAction()
{
    protectedCancelActionHelper(ActionAction);
}

void CPlayer::cancelAltAction()
{
    protectedCancelActionHelper(ActionAltAction);
}

void CPlayer::setLookDestination(const PointF2D &newLookDest)
{
    if (m_lookDestination == newLookDest)
        return;

    m_lookDestination = newLookDest;
    emit lookDestinationChanged();

    if (m_avatar) {
        m_avatar->appearance()->setEyeDestination(newLookDest - m_avatar->center());
    }
}

void CPlayer::doSuicide()
{
    m_actions |= ActionSuicide;
}

void CPlayer::setCheckpointsCount(int newCheckpointsCount)
{
    if (newCheckpointsCount >= 0)
        m_checkpointsCount = newCheckpointsCount;
}

bool CPlayer::isFinished()
{
    return m_finished;
}

void CPlayer::whenMoved()
{
    movedEvent();
}

void CPlayer::whenJumped()
{
    m_actions &= ~ActionJump;
    jumpedEvent();
}

void CPlayer::setNullAvatar()
{
    m_actions = 0;
    m_avatar = 0;
    emit avatarChanged(m_avatar);
}

void CPlayer::protectedDoActionHelper(CPlayer::PlayerActions action, const PointF2D &dest)
{
    if (!m_avatar)
        return;

    if (action == ActionAction) {
        m_avatar->doAction(CAvatar::WillAction1, dest);
    }
    else if (action == ActionAltAction) {
        m_avatar->doAction(CAvatar::WillAction2, dest);
    }

    doActionEvent(action, dest);

    m_actions |= action;
    applyActions();
}

void CPlayer::protectedCancelActionHelper(CPlayer::PlayerActions action)
{
    if (!m_avatar)
        return;

    if (action == ActionAction) {
        m_avatar->cancelAction(CAvatar::WillAction1);
    }
    else if (action == ActionAltAction) {
        m_avatar->cancelAction(CAvatar::WillAction2);
    }

    caneclActionEvent(action);

    m_actions &= ~action;
    applyActions();
}

void CPlayer::beforeSetAvatarEvent(CAvatar *newAvatar)
{
    Q_UNUSED(newAvatar)
}

void CPlayer::startedEvent()
{

}

void CPlayer::finishedEvent()
{

}

void CPlayer::checkpointPassedEvent(int checkpointId)
{
    Q_UNUSED(checkpointId);
}

void CPlayer::movedEvent()
{

}

void CPlayer::jumpedEvent()
{

}

void CPlayer::doActionEvent(CPlayer::PlayerActions action, const PointF2D &dest)
{
    Q_UNUSED(action)
    Q_UNUSED(dest)
}

void CPlayer::caneclActionEvent(CPlayer::PlayerActions action)
{
    Q_UNUSED(action)
}

void CPlayer::setStarted()
{
    m_nextCheckpoint = 0;
    startedEvent();
    emit started();
}

void CPlayer::setFinished()
{
    if (m_nextCheckpoint != m_checkpointsCount)
        return;

    m_finished = true;
    finishedEvent();
    emit finished();
}

void CPlayer::setCheckpointPassed(int checkpointId)
{
    if (checkpointId != m_nextCheckpoint)
        return;

    ++m_nextCheckpoint;

    checkpointPassedEvent(m_nextCheckpoint);
    emit checkpointPassed(m_nextCheckpoint);
}
