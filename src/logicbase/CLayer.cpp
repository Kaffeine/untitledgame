#include "CLayer.hpp"
#include "CAbstractGameBlock.hpp"
#include "CBlocksFactory.hpp"
#include "Functions.hpp"
#include "CExtendedArray.hpp"

#include <QDebug>

/* Pure logic class */
CLayer::CLayer(QObject *parent) :
    QObject(parent),
    m_layerType(0),
    m_world(0)
{
    clear();
}

CLayer::~CLayer()
{
    removeBlocks();
}

void CLayer::setWorld(CWorld *newWorld)
{
    m_world = newWorld;
}

PointF2D CLayer::size() const
{
    return PointF2D(m_maxX - m_minX, m_maxY - m_minY);
}

QPoint CLayer::sizeInCells() const
{
    return QPoint(size().x() / CWorldObject::CellLength, size().y() / CWorldObject::CellLength);
}

void CLayer::setSize(QPoint newSizeInCells)
{
    m_minX = 0;
    m_minY = 0;
    m_maxX = newSizeInCells.x() * CWorldObject::CellLength;
    m_maxY = newSizeInCells.y() * CWorldObject::CellLength;
}

void CLayer::adjustMinMax()
{
    for (int i = 0; i < blocksCount(); ++i) {
        CAbstractGameBlock *currentBlock = m_blocks.at(i);

        if (currentBlock->minX() < m_minX)
            m_minX = currentBlock->minX();

        if (currentBlock->minY() < m_minY)
            m_minY = currentBlock->minY();

        if (currentBlock->maxX() > m_maxX)
            m_maxX = currentBlock->maxX();

        if (currentBlock->maxY() > m_maxY)
            m_maxY = currentBlock->maxY();
    }
}

int CLayer::minX() const
{
    return m_minX;
}

int CLayer::minY() const
{
    return m_minY;
}

int CLayer::maxX() const
{
    return m_maxX;
}

int CLayer::maxY() const
{
    return m_maxY;
}

int CLayer::width() const
{
    return m_maxX / CWorldObject::CellLength;
}

int CLayer::height() const
{
    return m_maxY / CWorldObject::CellLength;
}

bool CLayer::isNull() const
{
    if (!width() || !height())
        return true;

    return false;
}

void CLayer::clear()
{
    removeBlocks();

    m_minX = 0xffffff; // Not super magic, just some big values, but FIXME.
    m_minY = 0xffffff;
    m_maxX = 0;
    m_maxY = 0;

    emit itemsCountChanged();
}

void CLayer::removeBlock(CAbstractGameBlock *pBlock)
{
    if (!pBlock)
        return;

    int index = m_blocks.indexOf(pBlock);
    if (index >= 0) {
        m_blocks.removeAt(index);
        emit itemsCountChanged();
    }

    /* Is it proper action? */
    delete pBlock;
}

/* The base function */
void CLayer::addBlock(CAbstractGameBlock *pBlock)
{
    pBlock->setLayer(this);
    pBlock->setWorld(m_world);
    pBlock->setZ(m_layerType);
    m_blocks.append(pBlock);
    emit itemsCountChanged();
}

CAbstractGameBlock *CLayer::addBlock()
{
    CAbstractGameBlock *block = CBlocksFactory::newGameBlock(m_layerType);
    addBlock(block);
    return block;
}

CAbstractGameBlock *CLayer::addBlock(const PointF2D &coord)
{
    CAbstractGameBlock *block = addBlock();
    block->setCellByPos(coord);
    return block;
}

CAbstractGameBlock *CLayer::addBlock(const PointF2D &coord, Directions direction, int subblock)
{
    CAbstractGameBlock *block = addBlock(coord);
    block->setDirection(direction);
    block->setSubBlockType(subblock);
    return block;
}

/* TODO: Sort items and look it up via bisection method */
CAbstractGameBlock *CLayer::blockAtCoord(const PointF2D &coord) const
{
    int i;
    for (i = 0; i < m_blocks.count(); ++i) {
        CAbstractGameBlock *currentBlock = m_blocks.at(i);
        if ((currentBlock->minX() <= coord.x()) && (currentBlock->minY() <= coord.y()) &&
                (coord.x() < currentBlock->maxX()) && (coord.y() < currentBlock->maxY())) {
            if (currentBlock->containPoint(coord - currentBlock->position()))
                return currentBlock;
        }
    }
    return 0;
}

CAbstractGameBlock *CLayer::blockAtCell(const PointI2D &cell) const
{
    for (int i = 0; i < m_blocks.count(); ++i) {
        CAbstractGameBlock *currentBlock = m_blocks.at(i);
        if (currentBlock->cell() == cell)
            return currentBlock;
    }
    return 0;
}

CAbstractGameBlock *CLayer::blockAtCell(const PointI2D &cell, Directions direction) const
{
    for (int i = 0; i < m_blocks.count(); ++i) {
        CAbstractGameBlock *currentBlock = m_blocks.at(i);
        if ((currentBlock->cell() == cell) && (currentBlock->direction() == direction))
            return currentBlock;
    }
    return 0;
}

QList<CAbstractGameBlock *> CLayer::blocksInRect(const PointF2D &point1, const PointF2D &point2) const
{
    PointF2D topLeft     = minPoint(point1, point2);
    PointF2D bottomRight = maxPoint(point1, point2);

    QList<CAbstractGameBlock *> blocksInRectList;

    int i;
    for (i = 0; i < m_blocks.count(); ++i) {
        CAbstractGameBlock *currentBlock = m_blocks.at(i);

        if (bottomRight.x() <= currentBlock->minX())
            continue;

        if (bottomRight.y() <= currentBlock->minY())
            continue;

        if (currentBlock->maxX() <= topLeft.x())
            continue;

        if (currentBlock->maxY() <= topLeft.y())
            continue;

        blocksInRectList.append(currentBlock);
    }

    return blocksInRectList;
}

QList<STouchEvent> CLayer::softTouches(const PointF2D &topLeft, const PointF2D &bottomRight) const
{
    QList<STouchEvent> touchesList;

    int i;
    for (i = 0; i < m_blocks.count(); ++i) {
        CAbstractGameBlock *currentBlock = m_blocks.at(i);

        qreal touchLengthX;
        qreal touchLengthY;
        qreal beginCoord;
        qreal endCoord;

        beginCoord   = qMax(topLeft.x()    , currentBlock->minX());
        endCoord     = qMin(bottomRight.x(), currentBlock->maxX());
        touchLengthX = endCoord - beginCoord;

        if (touchLengthX < 0)
            continue;

        beginCoord   = qMax(topLeft.y()    , currentBlock->minY());
        endCoord     = qMin(bottomRight.y(), currentBlock->maxY());
        touchLengthY = endCoord - beginCoord;

        if (touchLengthY < 0)
            continue;

        if ((touchLengthX == 0) && (touchLengthY == 0))
            continue;

        STouchEvent currentTouch;
        currentTouch.item = currentBlock;
        currentTouch.touchLengthX = touchLengthX;
        currentTouch.touchLengthY = touchLengthY;

        touchesList.append(currentTouch);
    }

    return touchesList;
}

QList<CAbstractGameBlock *> CLayer::getNeighbors(CAbstractGameBlock *rootBlock) const
{
    QList<CAbstractGameBlock *> neighborsList;
    neighborsList = blocksInRect(rootBlock->position() - CWorldObject::CellSize * 1.1, rootBlock->position() + CWorldObject::CellSize * 1.1);
    neighborsList.removeAll(rootBlock);
    return neighborsList;
}

QList<CAbstractGameBlock *> CLayer::getBlocksInCell(CAbstractGameBlock *rootBlock) const
{
    return getBlocksInCell(rootBlock->cell());
}

QList<CAbstractGameBlock *> CLayer::getBlocksInCell(const PointI2D &cell) const
{
    QList<CAbstractGameBlock *> blocksInCellList;

    CAbstractGameBlock *pBlock;
    PointF2D basePoint = cellToPos(cell);

    pBlock = blockAtCoord(basePoint + PointF2D(CWorldObject::CellLength * 0.25, CWorldObject::CellLength * 0.50)); // Left
    if (pBlock)
        blocksInCellList.append(pBlock);

    pBlock = blockAtCoord(basePoint + PointF2D(CWorldObject::CellLength * 0.50, CWorldObject::CellLength * 0.25)); // Top
    if (pBlock)
        blocksInCellList.append(pBlock);

    pBlock = blockAtCoord(basePoint + PointF2D(CWorldObject::CellLength * 0.75, CWorldObject::CellLength * 0.50)); // Right
    if (pBlock)
        blocksInCellList.append(pBlock);

    pBlock = blockAtCoord(basePoint + PointF2D(CWorldObject::CellLength * 0.50, CWorldObject::CellLength * 0.75)); // Bottom
    if (pBlock)
        blocksInCellList.append(pBlock);

    return blocksInCellList;
}

bool CLayer::read(CExtendedArray *source)
{
    removeBlocks();
    m_layerType = source->readQUInt16();
    quint32 itemsCount = source->readQUInt32();

    for (quint32 i = 0; i < itemsCount; ++i)
        addBlock()->read(source);

    adjustMinMax();

    emit itemsCountChanged();
    return true;
}

bool CLayer::write(CExtendedArray *dest) const
{
    dest->writeQUInt16(m_layerType);
    dest->writeQUInt32(m_blocks.count());

    for (int i = 0; i < m_blocks.count(); ++i)
        m_blocks.at(i)->write(dest);

    return true;
}

void CLayer::printContent() const
{
    QByteArray contentToBePrinted;
    contentToBePrinted.resize(width() * height());
    contentToBePrinted.fill(' ');

    for (int i = 0; i < m_blocks.count(); ++i)
        contentToBePrinted[m_blocks.at(i)->cell().x() + m_blocks.at(i)->cell().y() * width()] = '1' + m_blocks.at(i)->subBlockType();

    qDebug() << "Layer's Content:";
    for (int y = 0; y < height(); ++y)
        qDebug() << contentToBePrinted.mid(y * width(), width());
}

CAbstractGameBlock *CLayer::blockAt(int index) const
{
    if ((index < 0) || (index > m_blocks.count()))
        return 0;

    return m_blocks.at(index);
}

void CLayer::insertRow(int row)
{
    QList<CAbstractGameBlock *> affectedBlocks = blocksInRect(PointF2D(0, row * CWorldObject::CellLength), PointF2D(10000, 10000));
    foreach (CAbstractGameBlock *pBlock, affectedBlocks)
        pBlock->setPosition(pBlock->position() + PointF2D(0, CWorldObject::CellLength));
}

void CLayer::insertColumn(int column)
{
    QList<CAbstractGameBlock *> affectedBlocks = blocksInRect(PointF2D(column * CWorldObject::CellLength, 0), PointF2D(10000, 10000));
    foreach (CAbstractGameBlock *pBlock, affectedBlocks)
        pBlock->setPosition(pBlock->position() + PointF2D(CWorldObject::CellLength, 0));
}

void CLayer::removeRow(int row)
{
    /* Due to implementation of blocksInRect(), it is better to find affected block and parse it with condition,
     * than to call blocksInRect() twice and get blocks to be removed and blocks to be moved lists. */
    QList<CAbstractGameBlock *> affectedBlocks = blocksInRect(PointF2D(0, row * CWorldObject::CellLength), PointF2D(10000, 10000));
    foreach (CAbstractGameBlock *pBlock, affectedBlocks) {
        if (pBlock->cell().y() == row)
            removeBlock(pBlock);
        else
            pBlock->setPosition(pBlock->position() - PointF2D(0, CWorldObject::CellLength));
    }
}

void CLayer::removeColumn(int column)
{
    QList<CAbstractGameBlock *> affectedBlocks = blocksInRect(PointF2D(column * CWorldObject::CellLength, 0), PointF2D(10000, 10000));
    foreach (CAbstractGameBlock *pBlock, affectedBlocks)
        if (pBlock->cell().x() == column)
            removeBlock(pBlock);
        else
            pBlock->setPosition(pBlock->position() - PointF2D(CWorldObject::CellLength, 0));
}

void CLayer::removeBlocks()
{
    for (int i = 0; i < m_blocks.count(); ++i)
        delete m_blocks[i];

    m_blocks.clear();
}
