#ifndef CLAYER_HPP
#define CLAYER_HPP

#include <QObject>
#include <QList>
#include <QPointF>
#include "STouchEvent.hpp"
#include "Directions.hpp"

#include "Points2D.hpp"

class CAbstractGameBlock;
class CExtendedArray;
class CWorld;

class CLayer : public QObject
{
    Q_OBJECT

public:
    explicit CLayer(QObject *parent = 0);
    ~CLayer();

    inline CWorld *world() const { return m_world; }
    void setWorld(CWorld *newWorld);

    PointF2D size() const;
    QPoint sizeInCells() const;
    void setSize(QPoint newSizeInCells);
    void adjustMinMax();
    int minX() const;
    int minY() const;
    int maxX() const;
    int maxY() const;
    int width() const;
    int height() const;
    bool isNull() const;

    void clear();
    inline bool isEmpty() const { return m_blocks.isEmpty(); }
    inline int blocksCount() const { return m_blocks.count(); }
    void addBlock(CAbstractGameBlock *pBlock);
    void removeBlock(CAbstractGameBlock *pBlock);
    CAbstractGameBlock *addBlock();
    CAbstractGameBlock *addBlock(const PointF2D &coord);
    CAbstractGameBlock *addBlock(const PointF2D &coord, Directions direction, int subblock);
    CAbstractGameBlock *blockAtCoord(const PointF2D &coord) const;
    CAbstractGameBlock *blockAtCell(const PointI2D &cell) const;
    CAbstractGameBlock *blockAtCell(const PointI2D &cell, Directions direction) const;
    QList<CAbstractGameBlock *> blocksInRect(const PointF2D &point1, const PointF2D &point2) const;
    QList<STouchEvent> softTouches(const PointF2D &topLeft, const PointF2D &bottomRight) const;

    QList<CAbstractGameBlock *> getNeighbors(CAbstractGameBlock *rootBlock) const;
    QList<CAbstractGameBlock *> getBlocksInCell(CAbstractGameBlock *rootBlock) const;
    QList<CAbstractGameBlock *> getBlocksInCell(const PointI2D &cell) const;

    bool read(CExtendedArray *source);
    bool write(CExtendedArray *dest) const;

    void printContent() const;
    CAbstractGameBlock *blockAt(int index) const;

    quint16 m_layerType;

    void insertRow(int row);
    void insertColumn(int column);

    void removeRow(int row);
    void removeColumn(int column);

signals:
    void itemsCountChanged();

private:
    QList<CAbstractGameBlock *> m_blocks;
    void removeBlocks();
    CWorld *m_world;

    int m_minX;
    int m_minY;
    int m_maxX;
    int m_maxY;

};

#endif // CLAYER_HPP
