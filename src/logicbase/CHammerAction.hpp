#ifndef CHAMMERACTION_HPP
#define CHAMMERACTION_HPP

#include "CAbstractAvatarAction.hpp"

class CHammerAction : public CAbstractAvatarAction
{
    Q_OBJECT
public:
    explicit CHammerAction(CAvatar *avatar);

    void doAction(const PointF2D &dest);
    void doImpact(const double &dT);

    float hammerForce() const;

signals:

public slots:

private:
    float m_minimalForce;
    float m_efficientRadius;
    float m_hammerForce;

};

#endif // CHAMMERACTION_HPP
