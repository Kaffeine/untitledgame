#include "CAbstractAvatarAction.hpp"

#include "CAvatar.hpp"
#include "Functions.hpp"

CAbstractAvatarAction::CAbstractAvatarAction(CAvatar *avatar) :
    QObject(avatar),
    m_avatar(avatar),
    m_actionTimeout(0),
    m_successTimeout(0),
    m_failTimeout(0),
    m_radius(0),
    m_pending(false)
{
}

void CAbstractAvatarAction::activate()
{

}

void CAbstractAvatarAction::deactivate()
{

}

void CAbstractAvatarAction::freezed()
{

}
void CAbstractAvatarAction::doAction(const PointF2D &dest)
{
    prepareAction(dest);
}

PointF2D CAbstractAvatarAction::actionVector() const
{
    return normalizedVector(m_actionDestination - m_avatar->position()) * radius();
}

void CAbstractAvatarAction::shiftTime(const double &dT)
{
    if (m_actionTimeout > 0)
        m_actionTimeout -= dT;
}

void CAbstractAvatarAction::setActionSelected(bool newSelected)
{

}

void CAbstractAvatarAction::setActionSuccess(bool success)
{
    if (success)
        m_actionTimeout = m_successTimeout;
    else
        m_actionTimeout = m_failTimeout;
}

bool CAbstractAvatarAction::prepareAction(const PointF2D &dest)
{
    if (m_actionTimeout > 0)
        return false;

    if (m_avatar->freezedTime())
        return false;

    m_actionDestination = dest;

    return true;
}
