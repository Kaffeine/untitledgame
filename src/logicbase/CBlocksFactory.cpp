#include "CBlocksFactory.hpp"
#include "mapBlocks/allBlocks.hpp"

CBlocksFactory::CBlocksFactory()
{

}

CAbstractGameBlock *CBlocksFactory::newGameBlock(int type, int index)
{
    CAbstractGameBlock *newBlock;
    switch (type) {
    case BlockSolid:
        newBlock = new CSolidBlock();
        break;
    case BlockSpawn:
        newBlock = new CSpawnExitBlock();
        break;
    case BlockStart:
        newBlock = new CStartFinishBlock();
        break;
    case BlockFreeze:
        newBlock = new CFreezeBlock();
        break;
    case BlockTele:
        newBlock = new CTeleBlock();
        break;
    case BlockGravity:
        newBlock = new CGravityModBlock();
        break;
    case BlockAcceleration:
        newBlock = new CAccelerationBlock();
        break;
    default:
        return 0;
        break;
    }

    newBlock->m_blockType = type;
    newBlock->setDirection(DirectionNone);
    newBlock->setSubBlockType(index);
    return newBlock;
}

CAbstractGameBlock *CBlocksFactory::cloneBlock(CAbstractGameBlock *pBlock)
{
    CAbstractGameBlock *pNewBlock;

    pNewBlock = CBlocksFactory::newGameBlock(pBlock->blockType());
    pNewBlock->setPosition(pBlock->position());
    pNewBlock->setDirection(pBlock->direction());
    pNewBlock->setSubBlockType(pBlock->subBlockType());
    pNewBlock->setZ(pBlock->z());

    return pNewBlock;
}

int CBlocksFactory::subblocksCount(int type)
{
    /* Warning! Name c_subitemsCount is just convention.
     * Can't use Class::c_subitemsCount() because it's can't be virtual and static at same time. */
    switch (type) {
    case BlockSolid:
        return CSolidBlock       ::c_subBlocksCount;
    case BlockSpawn:
        return CSpawnExitBlock   ::c_subBlocksCount;
    case BlockStart:
        return CStartFinishBlock ::c_subBlocksCount;
    case BlockFreeze:
        return CFreezeBlock      ::c_subBlocksCount;
    case BlockTele:
        return CTeleBlock        ::c_subBlocksCount;
    case BlockGravity:
        return CGravityModBlock  ::c_subBlocksCount;
    case BlockAcceleration:
        return CAccelerationBlock::c_subBlocksCount;
    default:
        return 0;
    }
}
