#ifndef CTELEBLOCK_HPP
#define CTELEBLOCK_HPP

#include "../CAbstractGameBlock.hpp"

class CTeleBlock : public CAbstractGameBlock
{
public:
    CTeleBlock();
    quint16 subBlockTypesCount() const { return c_subBlocksCount; }
    void crossedEvent(CAvatar *pAvatar);

    /* Note: c_subitemsCount is convention, but not virtual. */
    static const quint16 c_subBlocksCount = 8;
};

#endif // CTELEBLOCK_HPP
