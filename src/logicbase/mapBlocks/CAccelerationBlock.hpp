#ifndef CACCELERATIONBLOCK_HPP
#define CACCELERATIONBLOCK_HPP

#include "../CAbstractGameBlock.hpp"

class CAccelerationBlock : public CAbstractGameBlock
{
public:
    enum SubBlocks {
        Down,
        Up,
        Left,
        Right,
        SubBlocksCount
    };

    CAccelerationBlock();
    quint16 subBlockTypesCount() const { return c_subBlocksCount; }
    void subblockChangedEvent();
    void crossedEvent(CAvatar *avatar);

    /* Note: c_subitemsCount is convention, but not virtual. */
    static const quint16 c_subBlocksCount = SubBlocksCount;

protected:
    Directions m_accelerationDirection;
    static const float force;
};

#endif // CACCELERATIONBLOCK_HPP
