#ifndef ALLBLOCKS_HPP
#define ALLBLOCKS_HPP

#include "CSolidBlock.hpp"
#include "CSpawnExitBlock.hpp"
#include "CStartFinishBlock.hpp"
#include "CGravityModBlock.hpp"
#include "CTeleBlock.hpp"
#include "CFreezeBlock.hpp"
#include "CAccelerationBlock.hpp"

#include "../BlocksTypes.hpp"

#endif // ALLBLOCKS_HPP
