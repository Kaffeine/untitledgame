#include "CAccelerationBlock.hpp"
#include "../CAvatar.hpp"
#include "../Functions.hpp"

const float CAccelerationBlock::force = CAccelerationBlock::CellLength * 200;

CAccelerationBlock::CAccelerationBlock()
{
}

void CAccelerationBlock::subblockChangedEvent()
{
    switch (m_subBlockType) {
    default:
    case Down:
        m_accelerationDirection = DirectionDown;
        break;
    case Up:
        m_accelerationDirection = DirectionUp;
        break;
    case Left:
        m_accelerationDirection = DirectionLeft;
        break;
    case Right:
        m_accelerationDirection = DirectionRight;
        break;
    }

}

void CAccelerationBlock::crossedEvent(CAvatar *avatar)
{
    avatar->addExternalForce(vectorByDirection(m_accelerationDirection) * force);
}
