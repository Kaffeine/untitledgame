#ifndef CSOLIDBLOCK_HPP
#define CSOLIDBLOCK_HPP

#include "../CAbstractGameBlock.hpp"

class CSolidBlock : public CAbstractGameBlock
{
public:
    enum SubBlocks {
        Hookable,
        Unhookable,
        Hookthrow,
        Ice,
        SubBlocksCount
    };

    CSolidBlock();
    quint16 subBlockTypesCount() const { return c_subBlocksCount; }
    void touchedEvent(CAvatar *avatar, Directions touchDirection, qreal length);

    /* Note: c_subitemsCount is convention, but not virtual. */
    static const quint16 c_subBlocksCount = SubBlocksCount;
};

#endif // CSOLIDBLOCK_HPP
