#ifndef CFREEZEBLOCK_HPP
#define CFREEZEBLOCK_HPP

#include "../CAbstractGameBlock.hpp"

class CFreezeBlock : public CAbstractGameBlock
{
public:
    CFreezeBlock();
    quint16 subBlockTypesCount() const;
    void crossedEvent(CAvatar *pAvatar);

    /* Note: c_subBlocksCount is convention, but not virtual. */
    static const quint16 c_subBlocksCount;
    static const qreal c_radialFreezeFactor;
};

#endif // CFREEZEBLOCK_HPP
