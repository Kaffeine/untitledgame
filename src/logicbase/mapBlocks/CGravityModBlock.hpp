#ifndef CGRAVITYMODBLOCK_HPP
#define CGRAVITYMODBLOCK_HPP

#include "../CAbstractGameBlock.hpp"

class CGravityModBlock : public CAbstractGameBlock
{
public:
    enum SubBlocks {
        Down,
        Up,
        Left,
        Right,
        None,
        Extra,
        SubBlocksCount
    };

    CGravityModBlock();
    quint16 subBlockTypesCount() const { return c_subBlocksCount; }
    void subblockChangedEvent();
    void crossedEvent(CAvatar *avatar);

    /* Note: c_subitemsCount is convention, but not virtual. */
    static const quint16 c_subBlocksCount = SubBlocksCount;

protected:
    Directions m_gravityDirection;
};

#endif // CGRAVITYMODBLOCK_HPP
