#include "CSolidBlock.hpp"
#include "../CAvatar.hpp"

CSolidBlock::CSolidBlock() :
    CAbstractGameBlock()
{
}

void CSolidBlock::touchedEvent(CAvatar *avatar, Directions touchDirection, qreal length)
{
    /* TODO: Extract "0.9" into tunable variable instead of hardcode. */
    static const qreal Friction = 0.9 / CellLength;
    if (m_subBlockType == Ice)
        avatar->addTouch(touchDirection, Friction * length * 0.02);
    else
        avatar->addTouch(touchDirection, Friction * length);
}
