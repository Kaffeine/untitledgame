#ifndef CSTARTFINISHBLOCK_HPP
#define CSTARTFINISHBLOCK_HPP

#include "../CAbstractGameBlock.hpp"

class CStartFinishBlock : public CAbstractGameBlock
{
public:
    enum SubBlocks {
        Start,
        Finish,
        Pause,
        SubBlocksMinimumCount
    };

    CStartFinishBlock();
    quint16 subBlockTypesCount() const { return c_subBlocksCount; }
    void crossedEvent(CAvatar *pAvatar);

    /* Note: c_subitemsCount is convention, but not virtual. */
    static const quint16 c_subBlocksCount = SubBlocksMinimumCount + 2;
};

#endif // CSTARTFINISHBLOCK_HPP
