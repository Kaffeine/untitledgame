#include "CGravityModBlock.hpp"
#include "../CAvatar.hpp"
#include "../CWorld.hpp"

CGravityModBlock::CGravityModBlock() :
    CAbstractGameBlock()
{
    subblockChangedEvent();
}

void CGravityModBlock::subblockChangedEvent()
{
    switch (m_subBlockType) {
    default:
    case Down:
        m_gravityDirection = DirectionDown;
        break;
    case Up:
        m_gravityDirection = DirectionUp;
        break;
    case Left:
        m_gravityDirection = DirectionLeft;
        break;
    case Right:
        m_gravityDirection = DirectionRight;
        break;
    case None:
        m_gravityDirection = DirectionNone;
        break;
    case Extra:
        m_gravityDirection = DirectionExtra;
    }
}

void CGravityModBlock::crossedEvent(CAvatar *avatar)
{
//        avatar->world()->setGravityDirection(m_gravityDirection);

    avatar->setOwnGravityDirection(m_gravityDirection);
}
