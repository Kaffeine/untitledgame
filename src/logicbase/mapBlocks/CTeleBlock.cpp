#include "CTeleBlock.hpp"
#include "../CAvatar.hpp"

#include "../CLayer.hpp"

CTeleBlock::CTeleBlock() :
    CAbstractGameBlock()
{
}

void CTeleBlock::crossedEvent(CAvatar *pAvatar)
{
    if (m_subBlockType % 2)
        return;

    int teleFromId = m_subBlockType / 2;
    for (int i = 0; i < layer()->blocksCount(); ++i) {
        if (layer()->blockAt(i)->subBlockType() == teleFromId * 2 + 1) {
            pAvatar->setPosition(layer()->blockAt(i)->cellPosition());
            break;
        }
    }
}
