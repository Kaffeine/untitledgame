#ifndef CSPAWNEXITBLOCK_HPP
#define CSPAWNEXITBLOCK_HPP

#include "../CAbstractGameBlock.hpp"

class CSpawnExitBlock : public CAbstractGameBlock
{
public:
    enum SubBlocks {
        Spawn,
        Exit,
        SubBlocksCount
    };

    CSpawnExitBlock();
    quint16 subBlockTypesCount() const { return c_subBlocksCount; }

    /* Note: c_subitemsCount is convention, but not virtual. */
    static const quint16 c_subBlocksCount = SubBlocksCount;
};

#endif // CSPAWNEXITBLOCK_HPP
