#include "CFreezeBlock.hpp"
#include "../CAvatar.hpp"
#include "../Functions.hpp"

const quint16 CFreezeBlock::c_subBlocksCount = 4;
const qreal CFreezeBlock::c_radialFreezeFactor = 0.125;

CFreezeBlock::CFreezeBlock() :
    CAbstractGameBlock()
{
}

quint16 CFreezeBlock::subBlockTypesCount() const
{
    /* Freeze/unfreeze | Freeze center/unfreeze center */
    return c_subBlocksCount;
}

void CFreezeBlock::crossedEvent(CAvatar *pAvatar)
{
    if (m_subBlockType > 1) {
        if (vectorsLength(center() - pAvatar->center()) > (pAvatar->width() / 2 + width() * c_radialFreezeFactor))
            return;
    }

    if (m_subBlockType % 2)
        pAvatar->setFreezedTime(0.0);
    else
        pAvatar->setFreezedTime(3.0);
}
