#include "CHammerAction.hpp"

#include "CAvatar.hpp"
#include "CWorld.hpp"
#include "Functions.hpp"

CHammerAction::CHammerAction(CAvatar *avatar) :
    CAbstractAvatarAction(avatar)
{
    m_radius          = CWorldObject::CellLength * 1.05 + CAvatar::width();
    m_efficientRadius = CWorldObject::CellLength * 1.02 + CAvatar::width();
    m_minimalForce    = CWorldObject::CellLength * 10;
    m_hammerForce     = CWorldObject::CellLength * 17;

    m_successTimeout  = 0.5;
    m_failTimeout     = 0.1;
}

float CHammerAction::hammerForce() const
{
    return m_hammerForce;
}

void CHammerAction::doAction(const PointF2D &dest)
{
    if (m_pending || !prepareAction(dest))
        return;

    m_pending = true;
}

void CHammerAction::doImpact(const double &dT)
{
    Q_UNUSED(dT);

    if (!m_pending)
        return;

    bool success = false;

    for (int i = 0; i < m_avatar->world()->avatarsCount(); ++i) {
        CAvatar *affectedAvatar = m_avatar->world()->avatarAt(i);
        if (affectedAvatar == m_avatar)
            continue;

        float distance = vectorsLength(affectedAvatar->center() - m_avatar->center());
        if (distance > radius())
            continue;

        /* TODO: Accuracy hammer collision. */

        success = true;
        float xMotion, yMotion;

        if (m_avatar->position().x() < affectedAvatar->position().x() - m_avatar->width() * 0.05)
            xMotion = 0.36;
        else if (m_avatar->position().x() > affectedAvatar->position().x() + m_avatar->width() * 0.05)
            xMotion = -0.36;
        else
            xMotion = 0;

        if (m_avatar->position().y() + m_avatar->height() * 0.5 > affectedAvatar->position().y())
            yMotion = -0.8;
        else
            yMotion = 0.8;

        float force;

        if (distance > m_efficientRadius) {
            float factor = (distance - m_efficientRadius) / (m_radius - m_efficientRadius);
            force = hammerForce() * (1 - factor) + m_minimalForce * factor;
        }
        else
            force = hammerForce();

        affectedAvatar->addExternalPulse(PointF2D(xMotion, yMotion) * force);
    }

    setActionSuccess(success);
    m_pending = false;
}
