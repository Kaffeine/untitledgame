#include "CAvatarAppearance.hpp"

#include "CAvatar.hpp"
#include "Functions.hpp"

CAvatarAppearance::CAvatarAppearance(QObject *parent) :
    QObject(parent)
{
    reset();
}

PointF2D CAvatarAppearance::positionOfEye(int eye) const
{
    if ((eye < 0) || (eye >= eyesCount()))
        return PointF2D();

    return m_positionOfEyes[eye];
}

void CAvatarAppearance::copyAppearance(CAvatarAppearance *source)
{
    setColor(source->color());

    setEyesSize(source->eyesSize());
    setEyesCount(source->eyesCount());

    setFootsSize(source->footsSize());
    setFootsCount(source->footsCount());
}

void CAvatarAppearance::connectToAppearance(CAvatarAppearance *source)
{
    copyAppearance(source);

    connect(source, SIGNAL(colorChanged(QString)), this, SLOT(setColor(QString)));
    connect(source, SIGNAL(eyesSizeChanged(float)), this, SLOT(setEyesSize(float)));
    connect(source, SIGNAL(eyesCountChanged(int)), this, SLOT(setEyesCount(int)));
    connect(source, SIGNAL(footsSizeChanged(float)), this, SLOT(setFootsSize(float)));
    connect(source, SIGNAL(footsCountChanged(int)), this, SLOT(setFootsCount(int)));
}

void CAvatarAppearance::disconnectFromAppearance(CAvatarAppearance *source)
{
    disconnect(source, SIGNAL(colorChanged(QString)), this, SLOT(setColor(QString)));
    disconnect(source, SIGNAL(eyesSizeChanged(float)), this, SLOT(setEyesSize(float)));
    disconnect(source, SIGNAL(eyesCountChanged(int)), this, SLOT(setEyesCount(int)));
    disconnect(source, SIGNAL(footsSizeChanged(float)), this, SLOT(setFootsSize(float)));
    disconnect(source, SIGNAL(footsCountChanged(int)), this, SLOT(setFootsCount(int)));
}

void CAvatarAppearance::reset()
{
    setColor("white");

    setEyesSize(CAvatar::width() * 0.25);
    setEyesCount(1);

    setFootsSize(CAvatar::width() * 0.3);
    setFootsCount(2);

    setEyeDestination(PointF2D(0, 0));
}

void CAvatarAppearance::setColor(const QString &newColor)
{
    if (m_color == newColor)
        return;

    m_color = newColor;

    emit colorChanged(m_color);
}

void CAvatarAppearance::updateEyesPosition()
{
    qreal eyeDistance = CAvatar::width() / 14;
    qreal eyesLength = eyesSize() * eyesCount() + eyeDistance * (eyesCount() - 1);
    qreal eyeMaxRadius = CAvatar::width() / 5;

    if (eyesLength / 2 < eyeMaxRadius)
        eyeMaxRadius -= eyesLength * 0.5; // Reduce radius to fit half of eyes, include distance

    PointF2D centerOfEyes;

    if (vectorsLength(m_eyeDestination) > eyeMaxRadius)
        centerOfEyes = normalizedVector(m_eyeDestination) * eyeMaxRadius;
    else
        centerOfEyes = m_eyeDestination;

    for (int i = 0; i < eyesCount(); ++i) {
        m_positionOfEyes[i] = centerOfEyes + PointF2D(eyesSize() * 0.5 - eyesLength * 0.5 + i * (eyesSize() + eyeDistance), 0);
    }
}

void CAvatarAppearance::setEyeDestination(const PointF2D &newDestination)
{
    if (m_eyeDestination == newDestination)
        return;

    m_eyeDestination = newDestination;

    if (!eyesCount())
        return;

    updateEyesPosition();
}

void CAvatarAppearance::setEyesCount(int newEyesCount)
{
    if (newEyesCount < 0)
        newEyesCount = 0;

    if (newEyesCount > maxEyesCount)
        newEyesCount = maxEyesCount;

    if (eyesCount() == newEyesCount)
        return;

    m_eyesCount = newEyesCount;
    updateEyesPosition();

    emit eyesCountChanged(newEyesCount);
}

void CAvatarAppearance::setEyesSize(float newEyesSize)
{
    if (newEyesSize > CAvatar::width())
        newEyesSize = CAvatar::width();

    if (newEyesSize < 0)
        newEyesSize = 0;

    if (eyesSize() == newEyesSize)
        return;

    m_eyesSize = newEyesSize;
    updateEyesPosition();

    emit eyesSizeChanged(newEyesSize);
}

void CAvatarAppearance::setFootsCount(int newFootsCount)
{
    if (newFootsCount < 0)
        newFootsCount = 0;

    if (newFootsCount > 3)
        newFootsCount = 3;

    if (footsCount() == newFootsCount)
        return;

    m_footsCount = newFootsCount;

    emit footsCountChanged(newFootsCount);
}

void CAvatarAppearance::setFootsSize(float newFootsSize)
{
    if (newFootsSize > CAvatar::width())
        newFootsSize = CAvatar::width();

    if (newFootsSize < 0)
        newFootsSize = 0;

    if (footsSize() == newFootsSize)
        return;

    m_footsSize = newFootsSize;

    emit footsSizeChanged(newFootsSize);
}
