#ifndef COLLISIONSHELPER_HPP
#define COLLISIONSHELPER_HPP

#include <QObject>
#include <QList>

#include "Points2D.hpp"

class CWorldObject;
class CAbstractGameBlock;

static const quint8 ComponentX = 0b01;
static const quint8 ComponentY = 0b10;

struct SCollisionDescription {
    PointF2D time;
    quint8 components;
    PointF2D point;
};

qreal collisionTest(const PointF2D &from, const PointF2D &vector, CWorldObject *object, PointF2D *pReturnCollisionPoint = 0);

SCollisionDescription getFirstCollisionFromList(const QList<SCollisionDescription> &collisionsDescriptions);
QList<CAbstractGameBlock*> getCollisionsList(const QList<CAbstractGameBlock*> &itemsList, const PointF2D &oldPos, const PointF2D &newPos, const PointF2D &size);
QList<SCollisionDescription> getCollisionsDescriptions(const QList<CAbstractGameBlock*> &itemsList, const PointF2D &oldPos, const PointF2D &newPos, const PointF2D &size, qreal dTime);

#endif // COLLISIONSHELPER_HPP
