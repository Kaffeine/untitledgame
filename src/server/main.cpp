#include <QCoreApplication>
#include "CServer.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    CServer server;
    server.startServer(25577);

    return a.exec();
}
