#ifndef CPEERPLAYER_HPP
#define CPEERPLAYER_HPP

#include "logicbase/CPlayer.hpp"

class CNetBase;

class CAvatarAppearance;
class CWorld;

class CPeerPlayer : public CPlayer
{
    Q_OBJECT
public:
    enum PeerStates {
        State,
        State2,
        StateReady,
        StateDisconnected
    };

    explicit CPeerPlayer(CNetBase *net, QObject *parent = 0);
    ~CPeerPlayer();
    QString address();
    PeerStates state() const;
    CWorld *world() const { return m_world; }

    void doPing();

signals:
    void messageSignal(CPeerPlayer *who, const QString &message, int type); /* TODO: Replace CPeerPlayer to CPlayer, then can player will provide proper id. */
    void sendLog(QString message);
    void connected(CPeerPlayer *who);
    void disconnected(CPeerPlayer *who);
    void stateChanged(CPeerPlayer *who, int newState);
    void avatarActionsChanged(CPeerPlayer *who);
    void peerLookDestChanged(CPeerPlayer *who);
    void fileRequest(CPeerPlayer *who, quint8 type, QString fileName);
    
public slots:
    void sendMessage(const QString &message, int type);
    void setWorld(CWorld *pWorld);
    void sendAvatarSpawned(CAvatar *pAvatar);
    void sendAvatarRemoved(CAvatar *pAvatar);
    void sendAvatarsData();
    void sendWorldTime();
    void sendAvatarActions(CAvatar *pAvatar);
    void sendAvatarLookDest(CAvatar *pAvatar);
    void sendAppearance(CAvatarAppearance *pAppearance, int appearanceId);
    void sendFile(quint8 type, QString fileName, const QByteArray &fileContent);

protected:
    void startedEvent();
    void finishedEvent();
    void checkpointPassedEvent(int checkpointId);
    void movedEvent();
    void jumpedEvent();

private slots:
    void processPackageContent();
    void processNewState();

    void whenConnected();
    void whenDisconnected();

    void debugSink(QString text);

private:
    CNetBase *m_net;
    QString m_worldSignature;
    PeerStates m_state;
    CWorld *m_world;
    bool m_enableAnticheat;
    quint32 m_pingTime;
    
};

#endif // CPEERPLAYER_HPP
