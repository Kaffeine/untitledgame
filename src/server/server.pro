QT = core network

TARGET = server
TEMPLATE = app

include(../common.pri)

LIBS += -lextendedarray -lNetBase
LIBS += -llogicbase -lnetwork

SOURCES += main.cpp \
    CPeerPlayer.cpp \
    CServer.cpp

HEADERS += \
    CPeerPlayer.hpp \
    CServer.hpp
