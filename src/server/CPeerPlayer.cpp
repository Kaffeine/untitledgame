#include "CPeerPlayer.hpp"
#include "CNetBase.hpp"
#include "network/CGameNetwork.hpp"

#include "logicbase/CWorld.hpp"
#include "logicbase/CAvatar.hpp"
#include "logicbase/CAvatarAppearance.hpp"

#include <QDebug>

CPeerPlayer::CPeerPlayer(CNetBase *net, QObject *parent) :
    CPlayer(parent),
    m_net(net),
    m_world(0),
    m_enableAnticheat(false)
{
    qDebug() << "New connection:" << address();
    connect(m_net, SIGNAL(processPackage()), SLOT(processPackageContent()), Qt::DirectConnection);
    connect(m_net, SIGNAL(connected())     , SLOT(whenConnected()));
    connect(m_net, SIGNAL(disconnected())  , SLOT(whenDisconnected()));
    //    connect(m_net, SIGNAL(sendLog(QString)), this, SLOT(debugSink(QString)), Qt::DirectConnection);
}

CPeerPlayer::~CPeerPlayer()
{
    if (m_avatar) // If delete should be avoided, set m_avatar to 0 before delete.
        delete m_avatar;
}

QString CPeerPlayer::address()
{
    return m_net->socketAddress();
}

CPeerPlayer::PeerStates CPeerPlayer::state() const
{
    return m_state;
}

void CPeerPlayer::doPing()
{
    m_pingTime = m_world->time();
    m_net->writeNetCommand(CGameNetwork::NetCommandPing);
    m_net->writeNetCommand(CGameNetwork::NetCommandRequest);
    m_net->requestSendPackage();
}

void CPeerPlayer::startedEvent()
{

}

void CPeerPlayer::finishedEvent()
{

}

void CPeerPlayer::checkpointPassedEvent(int checkpointId)
{
    Q_UNUSED(checkpointId)
}

void CPeerPlayer::movedEvent()
{

}

void CPeerPlayer::jumpedEvent()
{

}

void CPeerPlayer::processPackageContent()
{
    qDebug() << "Peer:" << address() << "at time:" << m_world->time();
    quint8 com = m_net->readNetCommand();
    qDebug() << "    " << "processPackageContent:" << CGameNetwork::commandToText(com);

    if (com == CGameNetwork::NetCommandChat) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandReceive) {
            int type = m_net->readQUInt8();
            QString message = m_net->readQString();
            emit messageSignal(this, message, type);
        }
        else {
            /* TODO: Chat message confirms? */
            qDebug() << "    " << "!!!Chat, but not Receive!";
        }
    }
    else if (com == CGameNetwork::NetCommandWorld) {
        com = m_net->readNetCommand();
        if (com == CGameNetwork::NetCommandAck) {
            m_state = StateReady;
            qDebug() << "    " << "State become ready" << m_state;
            emit stateChanged(this, m_state);
        }
        else if (com == CGameNetwork::NetCommandAccept) {
            qDebug() << "    " << "!!!Client accept world, but can't load it.";
        }
        else {
            qDebug() << "    " << "!!!World, but not Ack!";
        }
    }
    else if (com == CGameNetwork::NetCommandAvatarSpawn) {
        com = m_net->readNetCommand();
        if (com == CGameNetwork::NetCommandReceive) {
            qDebug() << "    " << "Peer want to spawn.";
            if (m_avatar) {
                qDebug() << address() << "Peer already have avatar. Reject.";
                m_net->writeNetCommand(CGameNetwork::NetCommandAvatarSpawn);
                m_net->writeNetCommand(CGameNetwork::NetCommandReject);
            }
            else {
                qDebug() << "    " << "Spawn avatar in world.";
                m_avatar = m_world->spawnAvatar();

                if (!m_avatar) {
                    qDebug() << "    " << "World can't spawn avatar. Reject.";
                    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarSpawn);
                    m_net->writeNetCommand(CGameNetwork::NetCommandReject);
                }
            }
        }
        else if (com == CGameNetwork::NetCommandAck) {
            quint8 avId = m_net->readQUInt8();
            qDebug() << "    " << "Peer ack that avatar" << avId << "spawned.";
            if (!m_world->avatarAt(avId)) {
                /* Something bad */
                qDebug() << "Something bad: accepted that peer spawned, but at serverside it is false.";
//                m_net->doDisconnect("Accepted that peer spawned, but at serverside it is false.");
            }

            if ((m_avatar) && (m_world->avatarAt(avId) == m_avatar)) {
                qDebug() << "    " << "Peer ack that peer's avatar spawned, so let him know about owning.";
                m_net->writeNetCommand(CGameNetwork::NetCommandPlayerAvatar);
                m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
                m_net->writeQUInt8(avId);
            }
        }
        else {
            qDebug() << "    " << "!!!AvatarSpawn with unknown code";
        }
    }
    else if (com == CGameNetwork::NetCommandWorldTime) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandAck) {
            qDebug() << "    " << "WorldTime: Ack.";
            quint32 worldTime = m_net->readQUInt32();
            qDebug() << "    " << "Accepted worldTime:" << worldTime << ", current:" << m_world->time();
            qDebug() << "    " << "WorldTime show delay" << m_world->time() - worldTime;
        }
        else
            qDebug() << "    " << "!!!WorldTime, but not ack!";
    }
    else if (com == CGameNetwork::NetCommandPlayerAvatar) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandAck)
            qDebug() << "    " << "PlayerAvatar: Ack.";
        else
            qDebug() << "    " << "!!!PlayerAvatar, but not ack!";
    }
    else if (com == CGameNetwork::NetCommandPlayerActions) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandReceive) {
            quint32 worldTime = m_net->readQUInt32();
            qDebug() << "    " << "Accepted worldTime:" << worldTime << ", current:" << m_world->time();

            CExtendedArray playerActionsData = m_net->readBlock();
            CExtendedArray avatarState = m_net->readBlock();

            if (m_avatar) {
                CGameNetwork::unpackActionsOfPlayer(this, &playerActionsData);

                applyActions();

                if (!m_enableAnticheat) {
                    CGameNetwork::setStateToAvatar(avatar(), &avatarState);
                }

                emit avatarActionsChanged(this);
            }
            else {
                qDebug() << "    " << "!!!PlayerActions, but player have no avatar!";
            }
        }
        else
            qDebug() << "    " << "!!!PlayerActions, but not receive!";
    }
    else if (com == CGameNetwork::NetCommandPlayerLookDest) {
        com = m_net->readNetCommand();
        if (com == CGameNetwork::NetCommandReceive) {
            PointF2D newLookDest;
            newLookDest.setX(m_net->readQReal());
            newLookDest.setY(m_net->readQReal());
            setLookDestination(newLookDest);
            emit peerLookDestChanged(this);
        }
    }
    else if (com == CGameNetwork::NetCommandPing) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandAck) {
            qDebug() << "    " << "Ping ack at " << m_world->time() << "vs" << m_pingTime;
        }
    }
    else if (com == CGameNetwork::NetCommandFullSnapshot) {
        if (m_net->readNetCommand() == CGameNetwork::NetCommandAck)
            qDebug() << "    " << "FullSnapshot: send ack.";
        else
            qDebug() << "    " << "!!!FullSnapshot:, but not ack!";
    }
    else if (com == CGameNetwork::NetCommandFile) {
        com = m_net->readNetCommand();
        if (com == CGameNetwork::NetCommandRequest) {
            com = m_net->readNetCommand();
            if (com == CGameNetwork::NetCommandWorld) {
                QString signature = m_net->readQString();
                emit fileRequest(this, com, signature); // Done in signal-slots to future multithreading.
            }
        }
        else if (com == CGameNetwork::NetCommandAck) {
            qDebug() << "    " << "FileRequest ack.";
        }
    }

    m_net->requestSendPackage();

//    qDebug() << "At end:" << m_net->atEnd() << "|" << m_net->m_incomingBuffer.position() << "/" << m_net->m_incomingBuffer.count();
    /* Singlethread code! */
    if (!m_net->atEnd())
        return processPackageContent();
}

void CPeerPlayer::processNewState()
{
    //    if (m_state == StateReady)
}

void CPeerPlayer::whenConnected()
{
    emit connected(this);
}

void CPeerPlayer::whenDisconnected()
{
    emit disconnected(this);
}

void CPeerPlayer::debugSink(QString text)
{
    if (m_world)
        qDebug() << QString("%1|NetDebug: %2").arg(m_world->time()).arg(text);
    else
        qDebug() << QString("NetDebug: %1").arg(text);
}

void CPeerPlayer::sendMessage(const QString &message, int type)
{
    if (m_net) {
        m_net->writeQUInt8(CGameNetwork::NetCommandChat);
        m_net->writeQUInt8(CGameNetwork::NetCommandReceive);
        m_net->writeQUInt8(type);
        m_net->writeQString(message);
        m_net->requestSendPackage();
    }
}

void CPeerPlayer::setWorld(CWorld *pWorld)
{
    qDebug() << "Peer:" << address();
    m_world = pWorld;

    m_worldSignature = m_world->signature();

    qDebug() << "    " << "Gameconnected.";
    qDebug() << "    " << "Set world:" << m_worldSignature;
    m_net->writeNetCommand(CGameNetwork::NetCommandWorld);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQString(m_worldSignature);
    m_net->requestSendPackage();
}

void CPeerPlayer::sendAvatarSpawned(CAvatar *pAvatar)
{
    qDebug() << "Peer:" << address();
    quint8 avId = m_world->avatarId(pAvatar);

    if (m_world->avatarId(pAvatar) < 0) {
        qDebug() << "    " << "Spawned avatar is not in world of current peer.";
        return;
    }

    qDebug() << "    " << "Send avatar with id" << avId << "spawned.";
    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarSpawn);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt8(avId);
    m_net->writeQUInt8(avId); // Appearance
    m_net->requestSendPackage();
}

void CPeerPlayer::sendAvatarRemoved(CAvatar *pAvatar)
{
    qDebug() << "Peer:" << address();
    quint8 avId = m_world->avatarId(pAvatar);

    if (m_world->avatarId(pAvatar) < 0) {
        qDebug() << "    " << "Removed avatar is not in world of current peer.";
        return;
    }

    qDebug() << "    " << "Send avatar with id" << avId << "removed.";
    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarRemove);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt8(avId);
    m_net->requestSendPackage();
}

void CPeerPlayer::sendAvatarsData()
{
    if (m_state != StateReady)
        return;

    qDebug() << "Peer:" << address();
    qDebug() << "    " << "sendAvatarsData()";

    /* TODO: Control sum for blocks? */
    CExtendedArray snapshot;
    qDebug() << "    " << "timestamp:" << m_world->time();
    snapshot.writeQUInt32(m_world->time());
    snapshot.writeQUInt8(m_world->avatarsCount());
    qDebug() << "    " << "write avatars count:" << m_world->avatarsCount();
    for (int i = 0; i < m_world->avatarsCount(); ++i) {
        snapshot.writeQUInt8(i);
        CGameNetwork::getStateOfAvatar(m_world->avatarAt(i), &snapshot);
    }
    m_net->writeNetCommand(CGameNetwork::NetCommandFullSnapshot);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeBlock(&snapshot);
    m_net->requestSendPackage();
}

void CPeerPlayer::sendWorldTime()
{
    m_net->writeNetCommand(CGameNetwork::NetCommandWorldTime);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt32(m_world->time());
    m_net->requestSendPackage();
}

void CPeerPlayer::sendAvatarActions(CAvatar *pAvatar)
{
//    qDebug() << "Peer:" << address();
    quint8 avId = m_world->avatarId(pAvatar);

    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarsWills);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt8(avId);
    CExtendedArray willData;
    CGameNetwork::packWillsOfAvatar(pAvatar, &willData);
    m_net->writeBlock(willData);
    m_net->requestSendPackage();
}

void CPeerPlayer::sendAvatarLookDest(CAvatar *pAvatar)
{
    quint8 avId = m_world->avatarId(pAvatar);
    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarLookDest);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt8(avId);

    m_net->writeQReal(pAvatar->appearance()->eyeDestination().x());
    m_net->writeQReal(pAvatar->appearance()->eyeDestination().y());
    m_net->requestSendPackage();
}

void CPeerPlayer::sendAppearance(CAvatarAppearance *pAppearance, int appearanceId)
{
    m_net->writeNetCommand(CGameNetwork::NetCommandAvatarAppearance);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt8(appearanceId);

    CExtendedArray appearanceData;
    CGameNetwork::packAvatarAppearance(pAppearance, &appearanceData);

    m_net->writeBlock(appearanceData);
    m_net->requestSendPackage();
}

void CPeerPlayer::sendFile(quint8 type, QString fileName, const QByteArray &fileContent)
{
    m_net->writeNetCommand(CGameNetwork::NetCommandFile);
    m_net->writeNetCommand(CGameNetwork::NetCommandReceive);
    m_net->writeQUInt8(type);

    m_net->writeNamedBlock(fileName, fileContent);
    m_net->requestSendPackage();
}
