import qbs 1.0

Product {
    Depends { name: "extendedarray" }
    Depends { name: "netbase" }
    Depends { name: "logicbase" }
    Depends { name: "network" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "network"] }
    name: "server"
    type: "application"

    cpp.includePaths: [
        "..",
        "../../libs/CExtendedArray",
        "../../libs/NetBase"
    ]

    Group {
        name: "files"
        files: [
            "CPeerPlayer.cpp",
            "CPeerPlayer.hpp",
            "CServer.cpp",
            "CServer.hpp",
	    "main.cpp"
        ]
    }
}
