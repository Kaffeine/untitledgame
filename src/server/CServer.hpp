#ifndef CSERVER_HPP
#define CSERVER_HPP

#include <QObject>
#include <QList>
#include <QMap>

QT_BEGIN_NAMESPACE
class QTcpServer;
class QTimer;
QT_END_NAMESPACE

class CWorld;
class CAvatarAppearance;

class CPeerPlayer;

class CAvatar;
class CExtendedArray;

const int MaximumClients = 10;

class CServer : public QObject
{
    Q_OBJECT
public:
    explicit CServer(QObject *parent = 0);
    bool startServer(int port);
    
signals:
    void sendLog(QString text);
    
public slots:
    void acceptConnection();
    void addLog(QString text);
    bool addWorld(QString worldName);

private slots:
    void processMessages(CPeerPlayer *CPeerPlayer, const QString &message, int type);
    void whenPeerConnected(CPeerPlayer *who);
    void whenPeerDisconnected(CPeerPlayer *who);
    void whenPeerStateChanged(CPeerPlayer *who, int newState);

    void broadcastState();

    void whenWorldSpawnAvatar(CAvatar *pAvatar);
    void whenWorldRemoveAvatar(CAvatar *pAvatar);
    void whenPeersAvatarActionsChanged(CPeerPlayer *who);
    void whenPeerFileRequest(CPeerPlayer *who, quint8 type, QString fileName);
    void whenPeerLookDestChanged(CPeerPlayer *who);

    void serverTick();

private:
    QByteArray *updateRawForPlayer(qint16 playerId);
    CPeerPlayer *getPeerByAvatar(CAvatar *pAvatar) const;

    CExtendedArray *m_raw;
    QTimer         *m_networkTimer;
    QTcpServer     *m_serverSocket;
    int             m_netConnectedCount;
    int             m_netPort;
    qint32          m_netServerVersion;
    qint32          m_netClientVersion; // minimal version

    QList<CWorld *>      m_worlds;
    QList<CPeerPlayer *> m_peers;

    QList<CAvatarAppearance *> m_appearances;

    quint16 m_rawSkipUpdateFactor;
    quint16 m_rawSkipUpdateCounter;
    QTimer *m_ticksTrigger;
    static const int tickPerSec;
    static const int pingSkips;
    
};

#endif // CSERVER_HPP
