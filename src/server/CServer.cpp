#include "CServer.hpp"
#include "CPeerPlayer.hpp"

#include "CNetBase.hpp"
#include "network/CGameNetwork.hpp"

#include "logicbase/CAvatar.hpp"
#include "logicbase/CAvatarAppearance.hpp"
#include "logicbase/CWorld.hpp"
#include "logicbase/CWorldUtils.hpp"

#include <QTcpServer>
#include <QTimer>
#include <QTime>

const int CServer::tickPerSec = 1;
const int CServer::pingSkips = 5;

CServer::CServer(QObject *parent) :
    QObject(parent)
{
    qDebug() << "Untitled server startup...";

    m_ticksTrigger = new QTimer(this);
    m_ticksTrigger->setInterval(1000 / tickPerSec);
    m_ticksTrigger->setSingleShot(false);
    connect(m_ticksTrigger, SIGNAL(timeout())       , SLOT(serverTick()));
    connect(this,           SIGNAL(sendLog(QString)), SLOT(addLog(QString)));

    addWorld("maps/public/01");

    for (int i = 0; i < m_worlds.count(); ++i)
        m_worlds.at(i)->start();

    qDebug() << "Worlds started at" << QTime::currentTime().msec();

    m_appearances.append(new CAvatarAppearance(this));
    m_appearances.last()->setFootsCount(2);
    m_appearances.last()->setColor("red");
    m_appearances.append(new CAvatarAppearance(this));
    m_appearances.last()->setFootsCount(2);
    m_appearances.last()->setColor("orange");
}

void CServer::acceptConnection()
{
    CPeerPlayer *newPlayer = new CPeerPlayer(new CNetBase(m_serverSocket->nextPendingConnection(), CGameNetwork::protocolVersion));
    connect(newPlayer, SIGNAL(sendLog(QString)), SLOT(addLog(QString)));
    connect(newPlayer, SIGNAL(messageSignal(CPeerPlayer*,QString,int)), SLOT(processMessages(CPeerPlayer*,QString,int)));
    connect(newPlayer, SIGNAL(disconnected(CPeerPlayer*)), SLOT(whenPeerDisconnected(CPeerPlayer*)));
    connect(newPlayer, SIGNAL(connected(CPeerPlayer*)), SLOT(whenPeerConnected(CPeerPlayer*)));
    connect(newPlayer, SIGNAL(stateChanged(CPeerPlayer*,int)), SLOT(whenPeerStateChanged(CPeerPlayer*,int)));
    connect(newPlayer, SIGNAL(avatarActionsChanged(CPeerPlayer*)), SLOT(whenPeersAvatarActionsChanged(CPeerPlayer*)));
    connect(newPlayer, SIGNAL(fileRequest(CPeerPlayer*,quint8,QString)), SLOT(whenPeerFileRequest(CPeerPlayer*,quint8,QString)));
    connect(newPlayer, SIGNAL(peerLookDestChanged(CPeerPlayer*)), SLOT(whenPeerLookDestChanged(CPeerPlayer*)));

    processMessages(0, "New player joined from " + newPlayer->address(), CGameNetwork::MessageAll);
    m_peers.append(newPlayer);
}

bool CServer::startServer(int port)
{
    m_netPort = port;
    m_serverSocket = new QTcpServer(this);
    if (!m_serverSocket->listen(QHostAddress::Any, m_netPort)) {
        emit sendLog(QString("Couldn't start listen port %1.").arg(m_netPort));
        return false;
    }
    connect(m_serverSocket, SIGNAL(newConnection()), SLOT(acceptConnection()));

    m_ticksTrigger->start();

    emit sendLog(QString("Server listen on port %1").arg(m_netPort));

    return true;
}

void CServer::addLog(QString text)
{
    qDebug() << text;
}

void CServer::processMessages(CPeerPlayer *who, const QString &message, int type)
{
    QString senderName = "Server";
    if (who)
        senderName = who->address();

    if (type == CGameNetwork::MessageAll)
        qDebug() << "Chat:" << who << "all:" << message;
    else if (type == CGameNetwork::MessageTeam)
        qDebug() << "Chat:" << who << "team:" << message;
    else
        qDebug() << "Chat:" << who << message << "type:" << type; // Message!

    for (int i = 0; i < m_peers.count(); ++i) {
        m_peers.at(i)->sendMessage(senderName + ": " + message, type);
    }
}

void CServer::whenPeerConnected(CPeerPlayer *who)
{
    who->setWorld(m_worlds.at(0));

    for (int i = 0; i < m_appearances.count(); ++i)
        who->sendAppearance(m_appearances.at(i), i);
}

void CServer::whenPeerDisconnected(CPeerPlayer *who)
{
    qDebug() << "Server: peer disconnected";
    m_peers.removeAll(who);
    processMessages(0, "Player disconnected", CGameNetwork::MessageAll);

    who->world()->removeAvatar(who->avatar());
    who->deleteLater();
}

void CServer::whenPeerStateChanged(CPeerPlayer *who, int newState)
{
    Q_UNUSED(newState)

    if (!who)
        return;

    qDebug() << "Peer " << who->address() << "change state to" << who->state();

    if (who->state() == CPeerPlayer::StateReady) {
        qDebug() << "    " << "State ready.";

        if (!who->world()) {
            qDebug() << "    " << "!!!!World isn't setuped.";
            return;
        }

        who->sendWorldTime();

        qDebug() << "    " << "Send info about all spawned avatars.";
        for (int i = 0; i < who->world()->avatarsCount(); ++i) {
            who->sendAvatarSpawned(who->world()->avatarAt(i));
        }
    }

    /* TODO: Check that client accept avatars before send datas. */
    qDebug() << "    " << "Send avatars data.";
    who->sendAvatarsData();
}

void CServer::broadcastState()
{

}

void CServer::whenWorldSpawnAvatar(CAvatar *pAvatar)
{
    CWorld *pWorld = (CWorld *) sender();

    if (!pWorld)
        return;

    qDebug() << "World: " << pWorld->signature() << ":Spawn avatar.";

    for (int i = 0; i < m_peers.count(); ++i) {
        qDebug() << "    " << "Peer" << i << "is in state " << m_peers.at(i)->state();
        if (m_peers.at(i)->state() == CPeerPlayer::StateReady) {
            qDebug() << "    " << "Peer" << i << "is ready, so send.";
            pAvatar->appearance()->copyAppearance(m_appearances.at(pWorld->avatarId(pAvatar)));
            m_peers.at(i)->sendAvatarSpawned(pAvatar);
        }
    }
}

void CServer::whenWorldRemoveAvatar(CAvatar *pAvatar)
{
    CWorld *pWorld = (CWorld *) sender();

    if (!pWorld)
        return;

    qDebug() << "World: " << pWorld->signature() << ":Remove avatar.";

    for (int i = 0; i < m_peers.count(); ++i) {
        qDebug() << "    " << "Peer" << i << "is in state " << m_peers.at(i)->state();
        if (m_peers.at(i)->state() == CPeerPlayer::StateReady) {
            qDebug() << "    " << "Peer" << i << "is ready, so send.";
            m_peers.at(i)->sendAvatarRemoved(pAvatar);
        }
    }
}

void CServer::whenPeersAvatarActionsChanged(CPeerPlayer *who)
{
    qDebug() << "peersActionsChanged";
    if (who->state() != CPeerPlayer::StateReady) {
        qDebug() << "Peer" << who->address() << "is not in ready-state, but signalize about change actions!";
        return;
    }

    CAvatar *pAvatar = who->avatar();

    if (!pAvatar) {
        qDebug() << "Peer havn't avatar. Should be mistake. Return.";
        return;
    }

    for (int i = 0; i < m_peers.count(); ++i) {
        if (m_peers.at(i)->state() == CPeerPlayer::StateReady) {
            if (m_peers.at(i)->world() != who->world())
                continue;

            if (m_peers.at(i) == who)
                continue;

            qDebug() << "Send to" << m_peers.at(i)->address();
            m_peers.at(i)->sendAvatarActions(pAvatar);
        }
    }
}

void CServer::whenPeerFileRequest(CPeerPlayer *who, quint8 type, QString fileName)
{
    if (type == CGameNetwork::NetCommandWorld) {
        if (who->world()->signature() == fileName) {
            CExtendedArray buffer;
            CWorldUtils::saveAsBuffer(who->world(), &buffer);
            who->sendFile(type, fileName, buffer);
        }
    }
}

void CServer::whenPeerLookDestChanged(CPeerPlayer *who)
{
    for (int i = 0; i < m_peers.count(); ++i) {
        if (m_peers.at(i)->state() == CPeerPlayer::StateReady) {
            if (m_peers.at(i)->world() != who->world())
                continue;

            if (m_peers.at(i) == who)
                continue;

            m_peers.at(i)->sendAvatarLookDest(who->avatar());
        }
    }
}

void CServer::serverTick()
{
    if (m_peers.isEmpty())
        return;

    static int parity = 0;

    QTime time;
    time.restart();
    for (int i = 0; i < m_peers.count(); ++i) {
        if (!parity)
            m_peers.at(i)->doPing();

        m_peers.at(i)->sendAvatarsData();
    }
    ++parity;

    if (parity > pingSkips)
        parity = 0;

    //    qDebug() << "serverTick take: " << time.elapsed();
}

bool CServer::addWorld(QString worldName)
{
    CWorld *pWorld;
    pWorld = new CWorld(this);
    if (!CWorldUtils::loadWorldByFilename(pWorld, worldName)) {
        sendLog(QString("Can't load world %1").arg(worldName));
        delete pWorld;
        return false;
    }
    connect(pWorld, SIGNAL(avatarSpawned(CAvatar*))     , SLOT(whenWorldSpawnAvatar(CAvatar*)));
    connect(pWorld, SIGNAL(avatarRemovedBegin(CAvatar*)), SLOT(whenWorldRemoveAvatar(CAvatar*)), Qt::DirectConnection);
    m_worlds.append(pWorld);
    sendLog(QString("Loaded world %1").arg(worldName));
    return true;
}

CPeerPlayer *CServer::getPeerByAvatar(CAvatar *pAvatar) const
{
    if (pAvatar) {
        for (int i = 0; i < m_peers.count(); ++i) {
            if (m_peers.at(i)->avatar() == pAvatar)
                return m_peers.at(i);
        }
    }

    return 0;
}
